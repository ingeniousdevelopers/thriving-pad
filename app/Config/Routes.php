<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);



/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// ['filter'=>'isLoggedIn'], 


$routes->group('myprofile', ['filter' => 'isUser'], function ($routes) {
	$routes->get('', 'Myaccount::myprofile');
	$routes->post('', 'Myaccount::myprofile');
	$routes->get('change-password', 'Myaccount::changepassword');
	$routes->post('change-password', 'Myaccount::changepassword');
});

$routes->group('/', ['filter' => 'isUser'], function ($routes) {
	$routes->get('', 'User::dashboard');
	$routes->get('instructions', 'User::instructions');
	$routes->get('livesession', 'Livesessions::livesession');

	$routes->get('live-sessions/week1', 'Livesessions::week1');
	$routes->post('live-sessions/wolquestionnaire', 'Livesessions::wolquestionnaire');
	$routes->post('live-sessions/celebratingyou', 'Livesessions::celebratingyou');
	$routes->post('live-sessions/discusskey', 'Livesessions::discusskey');
	$routes->post('live-sessions/questionnaire', 'Livesessions::questionnaire');

	$routes->get('live-sessions/week2', 'Livesessions::week2');
	$routes->post('live-sessions/acceptance', 'Livesessions::acceptance');
	$routes->post('live-sessions/basevalues-satisfaction', 'Livesessions::basevalues_satisfaction');
	$routes->post('live-sessions/basevalues-dissatisfaction', 'Livesessions::basevalues_dissatisfaction');

	$routes->get('live-sessions/week3', 'Livesessions::week3');
	$routes->post('live-sessions/letsalign', 'Livesessions::letsalign');
	$routes->post('live-sessions/habits', 'Livesessions::habits');
	$routes->post('live-sessions/delabelling', 'Livesessions::delabelling');

	$routes->get('live-sessions/week4', 'Livesessions::week4');
	$routes->post('live-sessions/positivemoments', 'Livesessions::positivemoments');
	$routes->post('live-sessions/negativemoments', 'Livesessions::negativemoments');
	$routes->post('live-sessions/storysharing', 'Livesessions::storysharing');
	$routes->post('live-sessions/generatepcode', 'Livesessions::generatepcode');
	$routes->post('live-sessions/discussncode', 'Livesessions::discussncode');
	$routes->post('live-sessions/insertncodechart', 'Livesessions::insertncodechart');


	$routes->get('live-sessions/week5', 'Livesessions::week5');
	$routes->post('live-sessions/desirequestion', 'Livesessions::desirequestion');
	$routes->post('live-sessions/desireyear', 'Livesessions::desireyear');
	$routes->post('live-sessions/discover', 'Livesessions::discover');
	$routes->post('live-sessions/nandpactivity', 'Livesessions::nandpactivity');
	$routes->post('live-sessions/discusspcode', 'Livesessions::discusspcode');


	$routes->get('live-sessions/week6', 'Livesessions::week6');
	$routes->post('live-sessions/challengeovercome', 'Livesessions::challengeovercome');
	$routes->post('live-sessions/successalign', 'Livesessions::successalign');


	$routes->get('live-sessions/week7', 'Livesessions::week7');
	$routes->post('live-sessions/decision', 'Livesessions::decision');
	$routes->post('live-sessions/smartgoals', 'Livesessions::smartgoals');

	$routes->get('live-sessions/week8', 'Livesessions::week8');
	$routes->post('live-sessions/scenarioone', 'Livesessions::scenarioone');
	$routes->post('live-sessions/scenariotwo', 'Livesessions::scenariotwo');
	$routes->post('live-sessions/scenariothree', 'Livesessions::scenariothree');
	$routes->post('live-sessions/mastermind', 'Livesessions::mastermind');
	$routes->post('live-sessions/toxic', 'Livesessions::toxic');

	$routes->get('live-sessions/week11', 'Livesessions::week11');

	$routes->get('live-sessions/week12', 'Livesessions::week12');


	$routes->get('live-sessions/week9', 'Livesessions::week9');
	$routes->get('live-sessions/accomplishment', 'Livesessions::accomplishment');
	$routes->get('live-sessions/myhabit', 'Livesessions::myhabit');
	$routes->get('live-sessions/breakthrough', 'Livesessions::breakthrough');
	$routes->post('live-sessions/insertaccomplishment', 'Livesessions::insertaccomplishment');
	$routes->post('live-sessions/insertmyhappy', 'Livesessions::insertmyhappy');
	$routes->post('live-sessions/inserthabit', 'Livesessions::inserthabit');
	$routes->post('live-sessions/insertbreakthrough', 'Livesessions::insertbreakthrough');

	$routes->get('live-sessions/week10', 'Livesessions::week10');
	$routes->post('live-sessions/blueprint', 'Livesessions::blueprint');
	$routes->post('live-sessions/visionboard', 'Livesessions::visionboard');
	$routes->get('live-sessions/generatereport', 'Report::index');


	$routes->get('wheel-of-life', 'Tools::wheeloflife');
	$routes->post('wheel-of-life/create', 'Tools::wheeloflife_create');

	$routes->get('ncodes', 'Tools::ncodes');
	$routes->post('tools/insertncode', 'Tools::insertncode');
	$routes->post('tools/updatencode', 'Tools::updatencode');


	$routes->get('outcome-reinforcement', 'Tools::outcomereinfrocement');
	$routes->post('outcome-reinforcement/create', 'Tools::outcomereinfrocement_create');

	$routes->get('decision-wheel', 'Tools::decisionwheel');
	$routes->post('decision-wheel/create', 'Tools::decision_create');

	$routes->post('', 'Tools::redo');

	$routes->get('base-values', 'Tools::baselinevalues');
	$routes->post('base-values/insbasevalue', 'Tools::insbasevalue');
	$routes->post('base-values/updbasvalue', 'Tools::updbasvalue');
});


$routes->group('resultncode', function ($routes) {
	$routes->post('tools/resultncode', 'Tools::resultncode');
});

$routes->group('report', function ($routes) {
	$routes->get('live-sessions/week10', 'Report::index');
});


$routes->group('resultbasevalue', function ($routes) {
	$routes->post('base-values/resultbasevalue', 'Tools::resultbasevalue');
});

$routes->group('dashboard', ['filter' => 'isUser'], function ($routes) {
	$routes->get('', 'User::dashboard');
});

$routes->group('login', ['filter' => 'isNotLoggedIn'], function ($routes) {
	$routes->get('', 'Login::index');
});

$routes->group('logout',  function ($routes) {
	$routes->get('', 'Login::logout');
});


$routes->group('register', ['filter' => 'isNotLoggedIn'], function ($routes) {
	$routes->get('', 'Register::index');
});

$routes->group('selectslot', ['filter' => 'isLoggedIn'], function ($routes) {
	$routes->get('', 'Register::selectslot');
	$routes->get('pay', 'Register::pay');
	$routes->post('verifypay', 'Register::verifypay');
	$routes->get('time', 'Register::fetchtimebyday');
});


// admin routes 
$routes->group('admin', ['filter' => 'isAdmin'], function ($routes) {
	$routes->get('', 'Admin::index');
	$routes->get('users', 'Admin::users');
	$routes->post('users', 'Admin::users');
	$routes->post('meet', 'Admin::meet');
	$routes->get('batchidget', 'Admin::batchidget');
	$routes->post('fetchsession_data', 'Admin::fetchsession_data');
	$routes->post('sessionlinks', 'Admin::sessionlinks');
	$routes->get('viewlink', 'Admin::viewlink');
	$routes->post('changelink', 'Admin::changelink');
	$routes->get('blockstatus', 'Admin::blockstatus');
	$routes->get('user_profile', 'Admin::users_profile');
	$routes->get('fetch_data', 'Admin::fetch_data');
	$routes->post('fetch_data', 'Admin::fetch_data');
	$routes->get('user_edit', 'Admin::user_edit');
	$routes->post('user_edit', 'Admin::user_edit');
	$routes->get('delete_slot', 'Admin::delete_slot');
	$routes->get('startbatch', 'Admin::startbatch');
	$routes->get('endbatch', 'Admin::endbatch');
	$routes->get('batches', 'Admin::batches');
	$routes->get('adduser', 'Admin::adduser');
	$routes->post('adduser', 'Admin::adduser');
	$routes->post('remove', 'Admin::remove');
	$routes->get('insertlink', 'Admin::insertlink');
	$routes->post('insertlink', 'Admin::insertlink');
	$routes->get('slots', 'Admin::slots');

	$routes->get('live-sessions/week2/(:any)', 'Admin::week2/$1');

	$routes->get('live-sessions/week3/(:any)', 'Admin::week3/$1');

	$routes->get('live-sessions/week4/(:any)', 'Admin::week4/$1');

	$routes->get('live-sessions/week5/(:any)', 'Admin::week5/$1');

	$routes->get('live-sessions/week6/(:any)', 'Admin::week6/$1');

	$routes->get('live-sessions/week7/(:any)', 'Admin::week7/$1');

	$routes->get('live-sessions/week8/(:any)', 'Admin::week8/$1');

	$routes->get('live-sessions/week9/(:any)', 'Admin::week9/$1');
	$routes->get('live-sessions/breakthrough/(:any)', 'Admin::breakthrough/$1');
	$routes->get('live-sessions/myhabit/(:any)', 'Admin::myhabit/$1');
	$routes->get('live-sessions/accomplishment/(:any)', 'Admin::accomplishment/$1');

	$routes->get('live-sessions/week10/(:any)', 'Admin::week10/$1');
	$routes->get('/report/(:any)', 'Report::repost/$1');


	$routes->get('wheel-of-life/(:any)', 'Admin::wheeloflife/$1');
	$routes->get('ncodes/(:any)', 'Admin::ncodes/$1');

	$routes->get('outcome-reinforcement/(:any)', 'Admin::outcomereinfrocement/$1');
	$routes->get('decision-wheel/(:any)', 'Admin::decisionwheel/$1');
	$routes->get('base-values/(:any)', 'Admin::baselinevalues/$1');

	$routes->post('resultncode/(:any)', 'Admin::resultncode/$1');

	$routes->post('', 'Admin::redo');
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}