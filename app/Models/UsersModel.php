<?php

namespace App\Models;

use \CodeIgniter\Model;

class UsersModel extends Model
{
    public function userslist()
    {
        $builder = $this->db->table('users');
        $builder->select('users.id, users.username, users.batch_id, slots.slot_name, batches.next_session');
        $builder->join('slots', 'users.slot_id=slots.slot_id');
        $builder->join('batches', 'users.batch_id=batches.batch_id');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function user($user_id)
    {
        $builder = $this->db->table('users');
        $builder->select('*');
        $builder->where('id', $user_id);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function registeruser()
    {
        $query = $this->db->query("select count(*) as regcount from users");
        $result = $query->getResultArray();
        return $result;
    }
    public function paiduser()
    {
        $query = $this->db->query("select count(*) as paidcount from users where reg_status=1");
        $result = $query->getResultArray();
        return $result;
    }
    public function unpaiduser()
    {
        $query = $this->db->query("select count(*) as unpaidcount from users where reg_status=0");
        $result = $query->getResultArray();
        return $result;
    }
}