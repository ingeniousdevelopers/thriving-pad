<?php

namespace App\Models;

use \CodeIgniter\Model;

class MyProfileModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function fetch($email)
    {
        $builder = $this->db->table('users');
        $builder->where('email', $email);
        $result = $builder->get();
        if (count($result->getResultArray()) == 1) {
            return $result->getRowArray();
        } else {
            return false;
        }
    }

    function make_query($batchid)
    {
        $query = "SELECT * FROM users WHERE batch_id= '" . $batchid . "'";
        return $query;
    }

    public function fetched_data($batchid)
    {
        $query = $this->db->query($this->make_query($batchid));
        $results = $query->getResultArray();
        return $results;
    }

    function makeslot_query($slotid)
    {
        $query = "SELECT * FROM users WHERE slot_id= '" . $slotid . "'";
        return $query;
    }
    public function fetchedsession_data($slotid)
    {
        $query = $this->db->query($this->makesesslot_query($slotid));
        $results = $query->getResultArray();
        return $results;
    }
    function makebatch_query($slotid)
    {
        $query = "SELECT * FROM batches WHERE slot_id= '" . $slotid . "'";
        return $query;
    }
    public function fetchedsession_week($slotid)
    {
        $query = $this->db->query($this->makebatch_query($slotid));
        $results = $query->getResultArray();
        return $results;
    }

    function makesesslot_query($slotid)
    {
        $query = "SELECT * FROM slots WHERE slot_id= '" . $slotid . "'";
        return $query;
    }
    public function fetchedslot_data($slotid)
    {
        $query = $this->db->query($this->makeslot_query($slotid));
        $results = $query->getResultArray();
        return $results;
    }


    function makes_query($slotid, $batchid)
    {
        $query = "SELECT * FROM users WHERE slot_id= '" . $slotid . "' or batch_id= '" . $batchid . "'";
        return $query;
    }



    public function fetchedited_data($slotid, $batchid)
    {
        $query = $this->db->query($this->makes_query($slotid, $batchid));
        $results = $query->getResultArray();
        return $results;
    }

    function makesuser_query($slotid, $batchid)
    {
        $query = "SELECT * FROM users WHERE batch_id= 'null'";
        return $query;
    }
    public function fetcheduser_data($slotid, $batchid)
    {
        $query = $this->db->query($this->makesuser_query($slotid, $batchid));
        $results = $query->getResultArray();
        return $results;
    }

    public function fetchedbatch_data($slotid)
    {
        $query = $this->db->query($this->makesesslot_query($slotid));
        $results = $query->getResultArray();
        return $results;
    }

    public function fetch_with_id($id)
    {
        $builder = $this->db->table('users');
        $builder->where('id', $id);
        $result = $builder->get();
        if (count($result->getResultArray()) == 1) {
            return $result->getRowArray();
        } else {
            return false;
        }
    }

    public function fetch_batch_id($id)
    {
        $builder = $this->db->table('users');
        $builder->where('batch_id', $id);
        $result = $builder->get();
        if (count($result->getResultArray()) == 1) {
            return $result->getRowArray();
        } else {
            return false;
        }
    }

    public function updatemyprofile($id, $data)
    {
        $builder = $this->db->table('users');
        $builder->where('id', $id);
        $builder->update($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function changepassword($email, $data)
    {
        $builder = $this->db->table('users');
        $builder->where('email', $email);
        $builder->update($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function block_status($id)
    {
        $builder = $this->db->table('users');
        if ($data = ['status' => 'active',]) {
            $data = [
                'status' => 'inactive',
            ];
            $builder->where('id', $id);
            $res = $builder->update($data);
            if ($this->db->affectedRows() == 1) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function updatemeet($id, $data)
    {
        $builder = $this->db->table('users');
        $builder->where('id', $id);
        $builder->update($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }




    public function getbatchrow($batchid, $slotid)
    {
        $builder = $this->db->table('users');
        $builder->select('users.id, users.username, users.batch_id, slots.slot_name, batches.next_session');
        $builder->join('slots', 'users.slot_id=slots.slot_id');
        $builder->join('batches', 'users.batch_id=batches.batch_id');
        $builder->where('users.batch_id', $batchid);
        $builder->orWhere('users.slot_id', $slotid);

        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
}