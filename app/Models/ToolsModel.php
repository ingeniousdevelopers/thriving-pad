<?php

namespace App\Models;

use CodeIgniter\CLI\Console;
use \CodeIgniter\Model;

class ToolsModel extends Model
{

    public function createWheeloflife($data)
    {
        $builder = $this->db->table('wheeloflife');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchWheeloflife($userid)
    {
        $builder = $this->db->table('wheeloflife');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function createoutcome($data)
    {
        $builder = $this->db->table('outcome');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchoutcome($userid)
    {
        $builder = $this->db->table('outcome');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function fetchout($userid)
    {
        $builder = $this->db->table('outcome');
        $builder->select('potential_priority1,potential_priority2,potential_priority3');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function wolquestionnaire($data)
    {
        $builder = $this->db->table('wolquestionnaire');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchwolquestionnaire($userid)
    {
        $builder = $this->db->table('wolquestionnaire');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function celebratingyou($data)
    {
        $builder = $this->db->table('celebratingyou');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchcelebratingyou($userid)
    {
        $builder = $this->db->table('celebratingyou');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function discusskey($data)
    {
        $builder = $this->db->table('week1_discusskey');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchdiscusskey($userid)
    {
        $builder = $this->db->table('week1_discusskey');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function acceptance($data)
    {
        $builder = $this->db->table('acceptance');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchacceptance($userid)
    {
        $builder = $this->db->table('acceptance');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function basevaluessatis($data)
    {
        $builder = $this->db->table('basevaluessatis');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchbasevaluessatis($userid)
    {
        $builder = $this->db->table('basevaluessatis');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function basevaluesdissatis($data)
    {
        $builder = $this->db->table('basevaluesdissatis');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchbasevaluedissatis($userid)
    {
        $builder = $this->db->table('basevaluesdissatis');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function letsalign($data)
    {
        $builder = $this->db->table('letsalign');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchletsalign($userid)
    {
        $builder = $this->db->table('letsalign');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function habits($data)
    {
        $builder = $this->db->table('habits');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchhabits($userid)
    {
        $builder = $this->db->table('habits');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function delabelling($data)
    {
        $builder = $this->db->table('delabelling');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchdelabelling($userid)
    {
        $builder = $this->db->table('delabelling');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function positivemoments($data)
    {
        $builder = $this->db->table('positivemoments');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchpositivemoments($userid)
    {
        $builder = $this->db->table('positivemoments');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function negativemoments($data)
    {
        $builder = $this->db->table('negativemoments');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchnegativemoments($userid)
    {
        $builder = $this->db->table('negativemoments');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function storysharing($data)
    {
        $builder = $this->db->table('storysharing');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchstorysharing($userid)
    {
        $builder = $this->db->table('storysharing');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function fetchdesire($userid)
    {
        $builder = $this->db->table('desires');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insdesires($data)
    {
        $builder = $this->db->table('desires');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function upddesires($data, $userid)
    {
        $builder = $this->db->table('desires');
        $builder->where('user_id', $userid);
        $res = $builder->update($data);

        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchncodevalue()
    {
        $builder = $this->db->table('ncodevalue');
        $builder->select('*');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function fetchncode($userid)
    {
        $builder = $this->db->table('ncode');
        $builder->select('user_id,array_ncode');
        $builder->where('user_id', $userid);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insncodes($data)
    {
        $builder = $this->db->table('ncode');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }


    public function updncodes($userid, $data)
    {

        $builder = $this->db->table('ncode');
        $builder->select('play_count');
        $builder->where('user_id', $userid);
        $result = $builder->get();



        $builder4 = $this->db->table('ncode');
        $builder4->orwhere('play_count', ['play_count' => 1]);
        $builder4->orwhere('play_count', ['play_count' => 2]);
        $builder4->where('user_id', $userid);


        $res = $builder4->update($data);

        if (count($result->getResultArray()) > 0) {
            $count_array = $result->getResultArray();

            $count_old = $count_array[0]['play_count'];
            $count_new = ['play_count' => $count_old + 1];
            $builder->where('user_id', $userid);
            $res = $builder->update($count_new);

            if ($this->db->affectedRows() == 1) {
                $data_code = $data['array_ncode'];

                $data_code = explode('~', $data_code);


                $builder3 = $this->db->table('ncode');
                $builder3->select('array_ncode');
                $builder3->where('user_id', $userid);
                $result = $builder3->get();

                $uncheck = array();
                if (count($result->getResultArray()) > 0) {
                    $code_array = $result->getResultArray();
                    $code_old = $code_array[0]['array_ncode'];
                    $oldcode = explode('~', $code_old);

                    $res = array_diff($oldcode, $data_code);

                    foreach ($res as $r) {
                        $uncheck[] = $r;
                    }
                    $new_code = array_merge($data_code, $res);

                    $newcode = implode('~', $new_code);

                    $builder3 = $this->db->table('ncode');
                    $new_code = [
                        'array_ncode' => $newcode,
                    ];

                    $builder3->where('user_id', $userid);
                    $res = $builder3->update($new_code);
                    $builder3->where('play_count', ['play_count' => 3]);
                    $builder3->orwhere('filter', ['filter' => 5]);
                    $builder3->orwhere('filter', ['filter' => 4]);
                    $builder3->orwhere('filter', ['filter' => 3]);
                    $builder3->orwhere('filter', ['filter' => 2]);
                    $builder3->orwhere('filter', ['filter' => 1]);
                }


                $builder1 = $this->db->table('ncode');
                $builder1->select('filter');
                $builder1->where('play_count', ['play_count' => 4]);
                $builder1->orwhere('play_count', ['play_count' => 5]);
                $builder1->orwhere('play_count', ['play_count' => 6]);
                $builder1->orwhere('play_count', ['play_count' => 7]);
                $builder1->orwhere('play_count', ['play_count' => 8]);
                $builder1->where('user_id', $userid);
                $result = $builder1->get();



                if (count($result->getResultArray()) > 0) {
                    $count_array = $result->getResultArray();
                    $filter_old = $count_array[0]['filter'];
                    $filter_new = ['filter' => $filter_old - 1];
                    $builder1->where('user_id', $userid);
                    $res = $builder1->update($filter_new);
                }

                $builder2 = $this->db->table('ncode');
                $builder2->select('status');
                $builder2->where('play_count', ['play_count' => 8]);
                $builder2->where('user_id', $userid);
                $result = $builder2->get();


                if (count($result->getResultArray()) > 0) {
                    $count_array = $result->getResultArray();

                    $status_old = $count_array[0]['status'];
                    $status_new = ['status' => $status_old + 1];
                    $res = $builder2->update($status_new);
                    $builder2->where('user_id', $userid);
                }
            }
            return true;
        } else {
            return false;
        }
    }



    public function fetchbaselinevalue()
    {
        $builder = $this->db->table('basevalues');
        $builder->select('*');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }


    public function insertbaseval($data)
    {
        $builder = $this->db->table('baselinevalues');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchuserbasevalue($userid)
    {
        $builder = $this->db->table('baselinevalues');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function updbaseval($userid, $data)
    {

        $builder = $this->db->table('baselinevalues');
        $builder->select('play_count');
        $builder->where('user_id', $userid);
        $result = $builder->get();



        $builder4 = $this->db->table('baselinevalues');
        $builder4->orwhere('play_count', ['play_count' => 1]);
        $builder4->orwhere('play_count', ['play_count' => 2]);
        $builder4->where('user_id', $userid);


        $res = $builder4->update($data);

        if (count($result->getResultArray()) > 0) {

            $count_array = $result->getResultArray();

            $count_old = $count_array[0]['play_count'];
            $count_new = ['play_count' => $count_old + 1];
            $builder->where('user_id', $userid);
            $res = $builder->update($count_new);

            if ($this->db->affectedRows() == 1) {
                $data_code = $data['array_baselinevalues'];

                $data_code = explode('~', $data_code);


                $builder3 = $this->db->table('baselinevalues');
                $builder3->select('array_baselinevalues');
                $builder3->where('user_id', $userid);
                $result = $builder3->get();

                $uncheck = array();
                if (count($result->getResultArray()) > 0) {
                    $code_array = $result->getResultArray();
                    $code_old = $code_array[0]['array_baselinevalues'];
                    $oldcode = explode('~', $code_old);

                    $res = array_diff($oldcode, $data_code);

                    foreach ($res as $r) {
                        $uncheck[] = $r;
                    }
                    $new_code = array_merge($data_code, $res);

                    $newcode = implode('~', $new_code);

                    $builder3 = $this->db->table('baselinevalues');
                    $new_code = [
                        'array_baselinevalues' => $newcode,
                    ];

                    $builder3->where('user_id', $userid);
                    $res = $builder3->update($new_code);
                    $builder3->where('play_count', ['play_count' => 3]);
                    $builder3->orwhere('filter', ['filter' => 5]);
                    $builder3->orwhere('filter', ['filter' => 4]);
                    $builder3->orwhere('filter', ['filter' => 3]);
                    $builder3->orwhere('filter', ['filter' => 2]);
                    $builder3->orwhere('filter', ['filter' => 1]);
                }


                $builder1 = $this->db->table('baselinevalues');
                $builder1->select('filter');
                $builder1->where('play_count', ['play_count' => 4]);
                $builder1->orwhere('play_count', ['play_count' => 5]);
                $builder1->orwhere('play_count', ['play_count' => 6]);
                $builder1->orwhere('play_count', ['play_count' => 7]);
                $builder1->orwhere('play_count', ['play_count' => 8]);
                $builder1->where('user_id', $userid);
                $result = $builder1->get();



                if (count($result->getResultArray()) > 0) {
                    $count_array = $result->getResultArray();
                    $filter_old = $count_array[0]['filter'];
                    $filter_new = ['filter' => $filter_old - 1];
                    $builder1->where('user_id', $userid);
                    $res = $builder1->update($filter_new);
                }

                $builder2 = $this->db->table('baselinevalues');
                $builder2->select('status');
                $builder2->where('play_count', ['play_count' => 8]);
                $builder2->where('user_id', $userid);
                $result = $builder2->get();


                if (count($result->getResultArray()) > 0) {
                    $count_array = $result->getResultArray();

                    $status_old = $count_array[0]['status'];
                    $status_new = ['status' => $status_old + 1];
                    $res = $builder2->update($status_new);
                    $builder2->where('user_id', $userid);
                }
            }
            return true;
        } else {
            return false;
        }
    }

    public function insdiscover($data)
    {
        $builder = $this->db->table('discover');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchdiscover($userid)
    {
        $builder = $this->db->table('discover');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insactivity($data)
    {
        $builder = $this->db->table('npactivity');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchnpactivity($userid)
    {
        $builder = $this->db->table('npactivity');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function inspcode($data)
    {
        $builder = $this->db->table('discusspcode');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchdispcode($userid)
    {
        $builder = $this->db->table('discusspcode');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insgenpcode($data)
    {
        $builder = $this->db->table('generatepcode');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchgenpcode($userid)
    {
        $builder = $this->db->table('generatepcode');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insncode($data)
    {
        $builder = $this->db->table('discussncode');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchdisncode($userid)
    {
        $builder = $this->db->table('discussncode');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function fetchncodechart()
    {
        $builder = $this->db->table('ncodechart');
        $builder->select('*');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insncodechart($data)
    {
        $builder = $this->db->table('chartresult');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchnchartres($userid)
    {
        $builder = $this->db->table('chartresult');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function inschallengeover($data)
    {
        $builder = $this->db->table('challengeovercome');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchchallengeover($userid)
    {
        $builder = $this->db->table('challengeovercome');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function inssucessalign($data)
    {
        $builder = $this->db->table('successalign');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchsuccessalign($userid)
    {
        $builder = $this->db->table('successalign');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insdecision($data)
    {
        $builder = $this->db->table('decision');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchdecision($userid)
    {
        $builder = $this->db->table('decision');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function inssmartgoals($data)
    {
        $builder = $this->db->table('smartgoals');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchsmartgoals($userid)
    {
        $builder = $this->db->table('smartgoals');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insscenarioone($data)
    {
        $builder = $this->db->table('scenarioone');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchscenarioone($userid)
    {
        $builder = $this->db->table('scenarioone');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insscenariotwo($data)
    {
        $builder = $this->db->table('scenariotwo');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchscenariotwo($userid)
    {
        $builder = $this->db->table('scenariotwo');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insscenariothree($data)
    {
        $builder = $this->db->table('scenariothree');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchscenariothree($userid)
    {
        $builder = $this->db->table('scenariothree');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insmastermind($data)
    {
        $builder = $this->db->table('mastermind');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchmastermind($userid)
    {
        $builder = $this->db->table('mastermind');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function instoxic($data)
    {
        $builder = $this->db->table('toxic');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchtoxic($userid)
    {
        $builder = $this->db->table('toxic');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insdecisionwheel($qid, $data)
    {
        $id = $data['user_id'];
        if ($qid == 1) {
            $builder = $this->db->table('decisionwheel');
            $data = [
                'q1' => $data['q1'],
                'user_id' => $data['user_id'],
            ];
            $res = $builder->insert($data);
        }

        $builder = $this->db->table('decisionwheel');
        $builder->where('user_id', $id);
        $builder->select('q2,q3,q4,q5,q6,q7,q8,q9');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            if ($qid == 2) {
                $count_new = ['q2' => $data['q2']];
                $data = [
                    'q2' => $count_new
                ];
            } elseif ($qid == 3) {
                $count_new = ['q3' => $data['q3']];
                $data = [
                    'q3' => $count_new
                ];
            } elseif ($qid == 4) {
                $count_new = ['q4' => $data['q4']];
                $data = [
                    'q4' => $count_new
                ];
            } elseif ($qid == 5) {
                $count_new = ['q5' => $data['q5']];
                $data = [
                    'q5' => $count_new
                ];
            } elseif ($qid == 6) {
                $count_new = ['q6' => $data['q6']];
                $data = [
                    'q6' => $count_new
                ];
            } elseif ($qid == 7) {
                $count_new = ['q7' => $data['q7']];
                $data = [
                    'q7' => $count_new
                ];
            } elseif ($qid == 8) {
                $count_new = ['q8' => $data['q8']];
                $data = [
                    'q8' => $count_new
                ];
            } elseif ($qid == 9) {
                $count_new = ['q9' => $data['q9']];
                $data = [
                    'q9' => $count_new
                ];
            }

            $builder1 = $this->db->table('decisionwheel');
            $builder1->where('user_id', $id);
            $res = $builder1->update($data);

            return true;
        } else {
            return false;
        }
    }

    public function fetchdecisionwheel($userid)
    {
        $builder = $this->db->table('decisionwheel');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insmyhappy($data)
    {
        $builder = $this->db->table('myhappy');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchmyhappy($userid)
    {
        $builder = $this->db->table('myhappy');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function inshabit($data)
    {
        $builder = $this->db->table('myhabit');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchhabit($userid)
    {
        $builder = $this->db->table('myhabit');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insaccomplishment($data)
    {
        $builder = $this->db->table('accomplishment');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchaccomplishment($userid)
    {
        $builder = $this->db->table('accomplishment');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function insbreakthrough($data)
    {
        $builder = $this->db->table('breakthrough');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchbreakthrough($userid)
    {
        $builder = $this->db->table('breakthrough');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function insblueprint($data, $level)
    {
        $builder = $this->db->table('blueprint');
        $builder->select('user_id');
        $result = $builder->get();
        if (!empty($data['q1'])) {
            $data = [
                'q1' => $data['q1'],
                'user_id' => $data['user_id'],
            ];
            $res = $builder->insert($data);
        }

        $builder = $this->db->table('blueprint');
        $builder->select('q2,q3,q4,q5,q6,q7,q8,q9,q10,q11,q12,q13,q14');
        $builder->where('user_id', $data['user_id']);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {

            $builder1 = $this->db->table('blueprint');
            $builder1->where('user_id', $data['user_id']);

            if (!empty($data['q2'])) {
                $res = $builder1->update(['q2' => $data['q2']]);
            } elseif (!empty($data['q3'])) {
                $res = $builder1->update(['q3' => $data['q3']]);
            } elseif (!empty($data['q4'])) {
                $res = $builder1->update(['q4' => $data['q4']]);
            } elseif (!empty($data['q5'])) {
                $res = $builder1->update(['q5' => $data['q5']]);
            } elseif (!empty($data['q6'])) {
                $res = $builder1->update(['q6' => $data['q6']]);
            } elseif (!empty($data['q7'])) {
                $res = $builder1->update(['q7' => $data['q7']]);
            } elseif (!empty($data['q8'])) {
                $res = $builder1->update(['q8' => $data['q8']]);
            } elseif (!empty($data['q9'])) {
                $res = $builder1->update(['q9' => $data['q9']]);
            } elseif (!empty($data['q10'])) {
                $res = $builder1->update(['q10' => $data['q10']]);
            } elseif (!empty($data['q11'])) {
                $res = $builder1->update(['q11' => $data['q11']]);
            } elseif (!empty($data['q12'])) {
                $res = $builder1->update(['q12' => $data['q12']]);
            } elseif (!empty($data['q13'])) {
                $res = $builder1->update(['q13' => $data['q13']]);
            } elseif (!empty($data['q14'])) {
                $res = $builder1->update(['q14' => $data['q14']]);
            }
            return true;
        } else {
            return false;
        }
    }

    public function fetchblueprint($userid)
    {
        $builder = $this->db->table('blueprint');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }


    public function updatevision($data)
    {
        $builder = $this->db->table('visionboard');
        $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function fetchvision($userid)
    {
        $builder = $this->db->table('visionboard');
        $builder->select('*');
        $builder->where('user_id', $userid);

        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function users_livesession($id)
    {

        $builder = $this->db->table('users');
        $builder->where('users.id', $id);
        $builder->select('users.id,users.batch_id,batches.session_count,batches.next_session_link');
        $builder->join('batches', 'users.batch_id=batches.batch_id');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function users_slotsession($id)
    {

        $builder = $this->db->table('batches');
        $builder->select('*');
        $builder->where('slot_id', $id);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
    public function redotable($userid, $tablename)
    {

        $this->db->query("delete from $tablename where user_id=$userid");
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function insert_questionnaire($data)
    {
        $builder = $this->db->table('questionnaire');
        $res = $builder->insert($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function fetchquestionnaire($userid)
    {
        $builder = $this->db->table('questionnaire');
        $builder->select('*');
        $builder->where('user_id', $userid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }
}