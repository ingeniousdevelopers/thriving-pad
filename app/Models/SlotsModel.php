<?php

namespace App\Models;

use \CodeIgniter\Model;

class SlotsModel extends Model
{
    public function availabledays()
    {
        $builder = $this->db->table('slots');
        $builder->select('day');
        $builder->groupBy('day');
        $builder->where('status', '1');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function availabletimebyday($day, $batch_type)
    {
        $builder = $this->db->table('slots');
        $builder->select('slot_id,time');
        $builder->where('day', $day);
        $builder->where('batch_type', $batch_type);
        if ($batch_type == 1) {
            $builder->where('count<', 1);
        }
        if ($batch_type == 2) {
            $builder->where('count<', 5);
        }
        $builder->where('status', '1');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function createbatch($slot_id)
    {
        $builder = $this->db->table('slots');
        $builder->select('count');
        $builder->where('slot_id', $slot_id);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            $count_array = $result->getResultArray();
            $count_old = $count_array[0]['count'];
            $count_new = ['count' => $count_old + 1];

            $builder1 = $this->db->table('slots');
            $builder1->where('slot_id', $slot_id);
            $res = $builder1->update($count_new);

            if ($this->db->affectedRows() == 1) {

                $builder2 = $this->db->table('batches');
                $builder2->select('batch_id');
                $builder2->where('slot_id', $slot_id);
                $builder2->where('batch_status', 1);
                $result2 = $builder2->get();

                if (count($result2->getResultArray()) > 0) {
                    $batch_array = $result2->getResultArray();
                    $batch_id = $batch_array[0]['batch_id'];
                    return $batch_id;
                } else {
                    $batch_data = [
                        'slot_id' => $slot_id,
                        'batch_status' => 0,
                    ];
                    $builder3 = $this->db->table('batches');
                    $res1 = $builder3->insert($batch_data);

                    if ($this->db->affectedRows() == 1) {

                        $builder4 = $this->db->table('batches');
                        $builder4->select('batch_id');
                        $builder4->where('slot_id', $slot_id);
                        $builder4->orWhere('batch_status', 1);
                        $result4 = $builder4->get();

                        if (count($result4->getResultArray()) > 0) {
                            $batch_array = $result4->getResultArray();
                            $batch_id = $batch_array[0]['batch_id'];
                            return $batch_id;
                        }
                    }
                }
            }
            return true;
        } else {
            return false;
        }
    }
    public function create_batch($slot_id)
    {

        $builder1 = $this->db->table('slots');
        $builder1->where('slot_id', $slot_id);
        $data = [
            'status' => 1,
        ];
        $res = $builder1->update($data);
        if ($this->db->affectedRows() == 1) {
            $builder2 = $this->db->table('batches');
            $builder2->select('batch_id');
            $builder2->where('slot_id', $slot_id);
            $builder2->orWhere('batch_status', 1);
            $result2 = $builder2->get();

            if (count($result2->getResultArray()) > 0) {
                $batch_array = $result2->getResultArray();
                $batch_id = $batch_array[0]['batch_id'];
                return $batch_id;
            } else {
                $batch_data = [
                    'slot_id' => $slot_id,
                    'batch_status' => 0
                ];
                $builder3 = $this->db->table('batches');
                $res1 = $builder3->insert($batch_data);

                if ($this->db->affectedRows() == 1) {

                    $builder4 = $this->db->table('batches');
                    $builder4->select('batch_id');
                    $builder4->where('slot_id', $slot_id);
                    $builder4->orWhere('batch_status', 1);
                    $result4 = $builder4->get();

                    if (count($result4->getResultArray()) > 0) {
                        $batch_array = $result4->getResultArray();
                        $batch_id = $batch_array[0]['batch_id'];
                        return $batch_id;
                    }
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    public function listbatches()
    {
        $builder = $this->db->table('batches');
        $builder->select('*');
        $builder->join('slots', 'slots.slot_id=batches.slot_id');
        $builder->where('batch_status', 0);
        $builder->orWhere('batch_status', 1);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            // print_r($result->getResultArray());
            //die();
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function listslots()
    {
        $builder = $this->db->table('slots');
        $builder->select('*');
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            //print_r($result->getResultArray());
            // die();
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function list_slots($slotid)
    {
        $builder = $this->db->table('slots');
        $builder->select('*');
        $builder->where('slot_id', $slotid);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            //print_r($result->getResultArray());
            // die();
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function days_fetch($days)
    {
        $builder = $this->db->table('slots');
        $builder->select('*');
        $builder->where('day', $days);
        $result = $builder->get();
        if (count($result->getResultArray()) > 0) {
            // print_r($result->getResultArray());
            // die();
            return $result->getResultArray();
        } else {
            return false;
        }
    }

    public function fetch_batch_id($id)
    {
        $builder = $this->db->table('batches');
        $builder->where('batch_id', $id);
        $result = $builder->get();
        if (count($result->getResultArray()) == 1) {
            return $result->getRowArray();
        } else {
            return false;
        }
    }

    public function delete_slot($id)
    {
        $builder = $this->db->table('slots');
        if ($data = ['status' => 1,]) {
            $builder->select('count');
            $builder->where('slot_id', $id);
            $result = $builder->get();
            if (count($result->getResultArray()) > 0) {
                $count_array = $result->getResultArray();
                $count_old = $count_array[0]['count'];
                $count_new = ['count' => $count_old - 1];

                $data = [
                    'count' => $count_new,
                ];
                $builder->where('slot_id', $id);
                $res = $builder->update($data);

                $builder2 = $this->db->table('batches');
                $builder2->where('slot_id', $id);
                $data = [
                    'batch_status' => 3,
                ];

                $res = $builder2->update($data);
                if ($this->db->affectedRows() == 1) {
                    return true;
                }
            }
        } else {
            return false;
        }
    }

    public function end_batch($id)
    {

        $builder2 = $this->db->table('batches');
        $builder2->where('batch_id', $id);
        $data = [
            'batch_status' => 2,
        ];
        $res = $builder2->update($data);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }

    public function updatemeetlink($id, $data)
    {
        $builder = $this->db->table('batches');
        $builder->where('batch_id', $id);
        $builder->update($data);

        $builder = $this->db->table('batches');
        $builder->select('session_count');
        $builder->where('batch_id', $id);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            $count_array = $result->getResultArray();
            $count_old = $count_array[0]['session_count'];
            $count_new = ['session_count' => $count_old + 1];

            $data = [
                'session_count' => $count_new,
            ];
            $builder->where('batch_id', $id);
            $res = $builder->update($data);

            if ($this->db->affectedRows() == 1) {
                return true;
            } else {
                return false;
            }
        }
    }


    public function remove_edit($slotid)
    {

        $builder = $this->db->table('slots');
        $builder->select('count');
        $builder->where('slot_id', $slotid);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            $count_array = $result->getResultArray();
            $count_old = $count_array[0]['count'];
            $count_new = ['count' => $count_old - 1];

            $data = [
                'count' => $count_new,
            ];
            $builder->where('slot_id', $slotid);
            $res = $builder->update($data);

            $builder2 = $this->db->table('users');
            $data = [
                'batch_id' => 'null',
                'slot_id' => 'null',
            ];

            $builder2->where('slot_id', $slotid);
            $builder2->update($data);
            if ($this->db->affectedRows() == 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function add_user($id, $slotid, $data)
    {

        $builder2 = $this->db->table('users');
        $builder2->where(['batch_id' => 'null', 'id' => $id]);
        $res = $builder2->update($data);

        $builder = $this->db->table('slots');
        $builder->select('count');
        $builder->where('slot_id', $slotid);
        $result = $builder->get();

        if (count($result->getResultArray()) > 0) {
            $count_array = $result->getResultArray();
            $count_old = $count_array[0]['count'];
            $count_new = ['count' => $count_old + 1];

            $data = [
                'count' => $count_new,
            ];
            $builder->where('slot_id', $slotid);
            $res = $builder->update($data);


            if ($this->db->affectedRows() == 1) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function updateseslink($slotid, $nextsession)
    {
        $builder = $this->db->table('batches');
        $builder->where('slot_id', $slotid);
        $builder->update([
            'next_session' => $nextsession, 'next_session_link' => "", 'batch_status' => 1,
        ]);
        if ($this->db->affectedRows() == 1) {
            return true;
        } else {
            return false;
        }
    }
    public function privatebatch()
    {
        $query = $this->db->query("select count(*) as privatecount from slots where batch_type=1");
        $result = $query->getResultArray();
        return $result;
    }
    public function groupbatch()
    {
        $query = $this->db->query("select count(*) as groupcount from slots where batch_type=2");
        $result = $query->getResultArray();
        return $result;
    }
    public function slotscount()
    {
        $query = $this->db->query("select count(*) as slotscount from slots");
        $result = $query->getResultArray();
        return $result;
    }
    public function slotsavailable()
    {
        $query = $this->db->query("select count(*) as slotsavailable from slots where count=0");
        $result = $query->getResultArray();
        return $result;
    }
    public function batchcomplete()
    {
        $query = $this->db->query("select count(*) as batchcompletecount from batches where batch_status=2");
        $result = $query->getResultArray();
        return $result;
    }
    public function batchcount()
    {
        $query = $this->db->query("select count(*) as batchcount from batches");
        $result = $query->getResultArray();
        return $result;
    }
    public function privateslotavailable()
    {
        $query = $this->db->query("select count(*) as privateavailable from slots where count=0 and batch_type=1");
        $result = $query->getResultArray();
        return $result;
    }
    public function groupslotavailable()
    {
        $query = $this->db->query("select count(*) as groupavailable from slots where count>=0 and batch_type=2");
        $result = $query->getResultArray();
        return $result;
    }
}