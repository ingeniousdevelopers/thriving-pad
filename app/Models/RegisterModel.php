<?php

namespace App\Models;
use \CodeIgniter\Model;

class RegisterModel extends Model{
    public function createUser($userData){
        $builder = $this->db->table('users');
        $res = $builder->insert($userData);
        if($this->db->affectedRows() == 1){
            return true;
        }else{
            return false;
        }
     }

     public function paymentsuccess($userid,$data){
        $builder = $this->db->table('users');
        $builder->where('id', $userid);
        $res = $builder->update($data);
        if($this->db->affectedRows() == 1){
            return true;
        }else{
            return false;
        }
     }
}