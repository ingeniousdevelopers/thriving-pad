<?php

namespace App\Filters;

use App\Models\UsersModel;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class UserFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        if (!session()->has('logged_user')) {
            return redirect()->to(base_url() . '/login');
        } else {
            $this->session = session();
            $userData = $this->session->get('logged_user');

            if ($userData['role'] == 1) {
                $user_id = $userData['id'];
                $this->UsersModel = new UsersModel();
                $data['user_data']  = $this->UsersModel->user($user_id);
                if ($data['user_data'][0]['reg_status']) {
                    if (isset($data['user_data'][0]['transaction_id']) && !empty($data['user_data'][0]['transaction_id']))
                    {
                        // return redirect()->to(base_url().'/dashboard');
                    }
                    else{
                        $this->session->setTempData('error', 'Redirected to Registraion, Since You Do Not Have Permission', 3);
                        return redirect()->to(base_url() . '/selectslot');
                    }
                } else {
                    $this->session->setTempData('error', 'Redirected to Registraion, Since You Do Not Have Permission', 3);
                    return redirect()->to(base_url() . '/selectslot');
                }
            } else if ($userData['role'] == 2) {
                return redirect()->to(base_url() . '/admin');
            } else {
                $this->session->setTempData('error', 'Redirected to home page, Since You Do Not Have Permission', 3);
                return redirect()->to(base_url());
            }
        }
    }
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
    }
}
