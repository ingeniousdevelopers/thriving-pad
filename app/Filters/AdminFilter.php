<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class AdminFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        if (!session()->has('logged_user')) {
            return redirect()->to(base_url() . '/login');
        } else {
            $this->session = session();
            $userData = $this->session->get('logged_user');

            if ($userData['role'] == 2) {
            }
            else{
                $this->session->setTempData('error', 'You are Not An Admin', 3);
                return redirect()->to(base_url());
            }
        }
    }
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
    }
}
