<?php

namespace App\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;

class NologinFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        if (session()->has('logged_user')) {
            $this->session = session();
            $userData = $this->session->get('logged_user');
            if ($userData['role'] == 1) {
                return redirect()->to(base_url() . '/dashboard');
            }
            if ($userData['role'] == 2) {
                return redirect()->to(base_url() . '/admin');
            }
        }
    }
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
    }
}
