<?php

namespace App\Controllers;

use App\Models\LoginModel;

class Login extends BaseController
{

    public $loginModel;
    public $session;
    public function __construct()
    {
        helper('form');
        $this->session = session();
        $this->loginModel = new LoginModel();
    }

    public function index()
    {

        $data = [];
        $data['validation'] = null;
        $data['page_title']  = "Login";


        if ($this->request->getMethod() == 'post') {

            $rules = [
                'Email' => 'required|valid_email',
                'Password'  => 'required|min_length[6]|max_length[16]',
            ];
            if ($this->validate($rules)) {
                $email = $this->request->getVar('Email');
                $pass = $this->request->getVar('Password');
                $userData = $this->loginModel->verifyEmail($email);
                if ($userData) {
                    if (password_verify($pass, $userData['password'])) {
                        if ($userData['status'] == 'active') {
                            $newdata = [
                                'username'  => $userData['username'],
                                'role'  => $userData['user_type'],
                                'id'  => $userData['id'],
                                'email' => $userData['email']
                            ];
                            $this->session->set('logged_user', $newdata);

                            if ($newdata['role'] == 1) {
                                return redirect()->to(base_url() . '/dashboard');
                            } else if ($newdata['role'] == 2) {
                                return redirect()->to(base_url() . '/admin');
                            } 
                        } else {
                            $this->session->setTempData('error', 'Please active your account, Contact Support', 3);
                            return redirect()->to(current_url());
                        }
                    } else {
                        $this->session->setTempData('error', 'Sorry! Wrong Password entered', 3);
                        return redirect()->to(current_url());
                    }
                } else {
                    $this->session->setTempData('error', 'Sorry! Email does not exists', 3);
                    return redirect()->to(current_url());
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }


        echo view('theme/header', $data);
        echo view('auth/login');
        echo view('theme/footer', $data);
    }

    function logout()
    {
        $this->session->destroy();
        return redirect()->to(base_url('login'));

    }
}
