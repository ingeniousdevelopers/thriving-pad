<?php

namespace App\Controllers;

use App\Models\RegisterModel;
use App\Models\SlotsModel;

require('razorpay/razorpay-php/Razorpay.php');

use Razorpay\Api\Api;
use Razorpay\Api\Errors\SignatureVerificationError;

class Register extends BaseController
{
    public $registerModel;
    public $session;
    public $email;
    public function __construct()
    {
        helper('form');
        helper('date');
        $this->registerModel = new RegisterModel();
        $this->session = \Config\Services::session();
        $this->email = \Config\Services::email();
    }
    public function index()
    {
        $data = [];
        $data['validation'] = null;
        $data['page_title']  = "Register";

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'Fullname' => 'required|min_length[4]|max_length[20]',
                'Email' => 'required|valid_email|is_unique[users.email]',
                'Password'  => 'required|min_length[6]',
                'ConfirmPassword' => 'required|matches[Password]',
            ];
            if ($this->validate($rules)) {
                $uniId = md5(str_shuffle('prontoweb' . time()));

                $userData  = [
                    'username' => $this->request->getVar('Fullname', FILTER_SANITIZE_STRING),
                    'email' => $this->request->getVar('Email'),
                    'user_type' => '1',
                    'password' => password_hash($this->request->getVar('Password'), PASSWORD_DEFAULT),
                    'uniid' => $uniId,
                    'status' => 'active'
                ];

                if ($this->registerModel->createUser($userData)) {

                    $this->session->setTempData('success', 'Account Created Successfully', 3);
                    return redirect()->to(base_url('login'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to create an account, Try again', 3);
                    return redirect()->to(current_url());
                }
            } else {

                $data['validation'] = $this->validator;
            }
        }

        echo view('theme/header', $data);
        echo view('auth/register');
        echo view('theme/footer', $data);
    }

    public function selectslot()
    {
        $data = [];
        $data['validation'] = null;
        $data['page_title']  = "Select Slot";
        $userData = $this->session->get('logged_user');

        $this->SlotsModel = new SlotsModel();
        $data['slots_list']  = $this->SlotsModel->availabledays();

        echo view('theme/header', $data);
        echo view('auth/selectslot', $data);
        echo view('theme/footer', $data);
    }

    public function pay()
    {
        $keyId = 'rzp_test_7Zl3Xb8FpcQuZr';
        $keySecret = 'NY9n4hDrVygu7gsllmAr7PTN';
        $displayCurrency = 'INR';
        $group_price = 1;
        $private_price = 1;
        $api = new Api($keyId, $keySecret);


        $data = [];
        $data['validation'] = null;
        $userData = $this->session->get('logged_user');
        $data['is$userData_pay'] = false;

        if ($this->request->getMethod() == 'get') {
            $rules = [
                'batchtype' => 'required',
                'day' => 'required',
                'time'  => 'required',
            ];
            if ($this->validate($rules)) {

                $data  = [
                    'batch_type' => $this->request->getVar('batchtype'),
                    'day' => $this->request->getVar('day'),
                    'time' => $this->request->getVar('time'),
                    'username' => $userData['username'],
                ];
                $data['page_title']  = "Payment Page";


                if ($data['batch_type'] == 1) {
                    $price = $private_price;
                } else {
                    $price = $group_price;
                }
                $orderData = [
                    'receipt'         => 'trans' . $userData['id'] . rand(),
                    'amount'          => $price * 100, // 2000 rupees in paise
                    'currency'        => 'INR',
                    'payment_capture' => 1 // auto capture
                ];

                $razorpayOrder = $api->order->create($orderData);

                $razorpayOrderId = $razorpayOrder['id'];

                $this->session->set('razorpay_order_id', $razorpayOrderId);

                $displayAmount = $amount = $orderData['amount'];

                if ($displayCurrency !== 'INR') {
                    $url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
                    $exchange = json_decode(file_get_contents($url), true);

                    $displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
                }

                $data['json'] = [
                    "key"               => $keyId,
                    "amount"            => $amount,
                    "name"              => "Thrivepad",
                    "description"       => "Payment for Course",
                    "image"             => base_url() . "/public/assets/img/logos/logo.png",
                    "prefill"           => [
                        "name"              => $userData['username'],
                        "email"             => $userData['email'],
                        "contact"           => "",
                    ],
                    "notes"             => [
                        "address"           => "",
                        "merchant_order_id" => $orderData['receipt'],
                    ],
                    "theme"             => [
                        "color"             => "#44a5b0"
                    ],
                    "order_id"          => $razorpayOrderId,
                ];

                $data['is_pay'] = true;
                $data['price'] = $price;


                // print_r($data);
                // die();
                echo view('theme/header', $data);
                echo view('auth/payment', $data);
                echo view('theme/footer', $data);
            } else {

                $data['validation'] = $this->validator;
                echo view('theme/header', $data);
                echo view('auth/selectslot', $data);
                echo view('theme/footer', $data);
            }
        }
    }

    public function verifypay()
    {
        $sessionData = $this->session->get('razorpay_order_id');
        $userData = $this->session->get('logged_user');
        $data = [];

        $keyId = 'rzp_test_7Zl3Xb8FpcQuZr';
        $keySecret = 'NY9n4hDrVygu7gsllmAr7PTN';
        $displayCurrency = 'INR';
        $razorpay_payment_id = $this->request->getVar('razorpay_payment_id');
        $razorpay_signature = $this->request->getVar('razorpay_signature');
        $slot_id = $this->request->getVar('slot_id');

        $success = true;
        $error = "Payment Failed";
        if (empty($razorpay_payment_id) === false) {
            $api = new Api($keyId, $keySecret);

            try {
                // Please note that the razorpay order ID must
                // come from a trusted source (session here, but
                // could be database or something else)
                $attributes = array(
                    'razorpay_order_id' =>  $sessionData,
                    'razorpay_payment_id' => $razorpay_payment_id,
                    'razorpay_signature' => $razorpay_signature
                );

                $api->utility->verifyPaymentSignature($attributes);
            } catch (SignatureVerificationError $e) {
                $success = false;
                $error = 'Razorpay Error : ' . $e->getMessage();
            }
        }

        if ($success === true) {
            $data = [];

            $this->SlotsModel = new SlotsModel();
            $createbatch  = $this->SlotsModel->createbatch($slot_id);

            if ($createbatch) {
            } else {
                $createbatch = NULL;
            }
            $data = [
                'reg_status' => 1,
                'transaction_id' => $razorpay_payment_id,
                'slot_id' => $slot_id,
                'batch_id' => $createbatch
            ];
            if ($this->registerModel->paymentsuccess($userData['id'], $data)) {
                $this->session->setTempData('success', 'Payment Successfull!! Thrivepad Slogan', 3);
                return redirect()->to(base_url('dashboard'));
            }
        } else {
            $html = "<p>Your payment failed</p>
             <p>{$error}</p>";
        }
    }


    public function fetchtimebyday()
    {

        $day = $this->request->getVar('day');
        $batch_type = $this->request->getVar('batch_type');

        $output = array();
        $this->SlotsModel = new SlotsModel();
        $results  = $this->SlotsModel->availabletimebyday($day, $batch_type);

        if (count($results) > 0) {
            foreach ($results as $row) {
                $output[] = array("value" => $row['slot_id'], "name" => $row['time']);
            }
        } else {
            $output = 'No Slots Found ';
        }




        echo json_encode($output);
    }
    //--------------------------------------------------------------------

}