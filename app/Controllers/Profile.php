<?php

namespace App\Controllers;

use App\Models\MyProfileModel;
use App\Models\LoginModel;




class Profile extends BaseController
{
	public $session;
	public function __construct()
	{
		helper('form');
		helper('url');
		$this->session = session();
	}
	


	public function userdata()
	{
        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata']  = $this->MyProfileModel->fetch_with_id($this->request->getVar('user_id'));

        echo json_encode($data);

    }
	
}