<?php

namespace App\Controllers;

use App\Models\UsersModel;
use App\Models\MyProfileModel;
use App\Models\SlotsModel;
use App\Models\ToolsModel;

class Admin extends BaseController
{
    public $session;



    public $SlotsModel;

    public function __construct()
    {
        helper('form');
        helper('url');
        $this->session = session();
        $this->SlotsModel = new SlotsModel();
        $this->ToolsModel = new ToolsModel();
    }


    public function index()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 1;
        $data['page_title'] = ' Admin Dashboard - Thrivepad';

        $this->UsersModel = new UsersModel();
        $data['reg_count']  = $this->UsersModel->registeruser();
        $data['regcount'] = $data['reg_count'][0]['regcount'];

        $data['paid_count']  = $this->UsersModel->paiduser();
        $data['paidcount'] = $data['paid_count'][0]['paidcount'];

        $data['unpaid_count']  = $this->UsersModel->unpaiduser();
        $data['unpaidcount'] = $data['unpaid_count'][0]['unpaidcount'];

        $data['private_count']  = $this->SlotsModel->privatebatch();
        $data['privatecount'] = $data['private_count'][0]['privatecount'];

        $data['group_count']  = $this->SlotsModel->groupbatch();
        $data['groupcount'] = $data['group_count'][0]['groupcount'];

        $data['slot_count']  = $this->SlotsModel->slotscount();
        $data['slotscount'] = $data['slot_count'][0]['slotscount'];

        $data['slot_available']  = $this->SlotsModel->slotsavailable();
        $data['slotsavailable'] = $data['slot_available'][0]['slotsavailable'];

        $data['batchcomplete_count']  = $this->SlotsModel->batchcomplete();
        $data['batchcompletecount'] = $data['batchcomplete_count'][0]['batchcompletecount'];

        $data['batch_count']  = $this->SlotsModel->batchcount();
        $data['batchcount'] = $data['batch_count'][0]['batchcount'];

        $data['private_available']  = $this->SlotsModel->privateslotavailable();
        $data['privateavailable'] = $data['private_available'][0]['privateavailable'];

        $data['group_available']  = $this->SlotsModel->groupslotavailable();
        $data['groupavailable'] = $data['group_available'][0]['groupavailable'];

        echo view('admin/theme/admin_header', $data);
        echo view('admin/admin_dashboard', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function users()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 2;
        $data['page_title'] = 'Users - Admin Dashboard - Thrivepad';
        $this->UsersModel = new UsersModel();

        $batchid = $this->request->getVar('searchbatchid');
        $slot_id =  $this->request->getVar('slotid');

        $this->SlotsModel = new SlotsModel();
        $data['batch_list']  = $this->SlotsModel->listbatches();
        $data['slot_list']  = $this->SlotsModel->listslots();


        $data['batchlist'] = $batchid;
        $data['slotlist'] = $slot_id;

        if ($this->request->getMethod() == 'post') {
            $this->MyProfileModel = new MyProfileModel();
            $data['user_list'] = $this->MyProfileModel->getbatchrow($batchid, $slot_id);
        } else {
            $data['user_list']  = $this->UsersModel->userslist();
        }
        echo view('admin/theme/admin_header', $data);
        echo view('admin/users', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function batches()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 3;
        $data['page_title'] = 'Batches - Admin Dashboard - Thrivepad';
        $data['batch_list']  = $this->SlotsModel->listbatches();
        $data['slot_list']  = $this->SlotsModel->listslots();

        if ($this->request->getMethod() == 'post') {

            $slot_id =  $this->request->getVar('slotid');

            $this->SlotsModel = new SlotsModel();

            $res = $this->SlotsModel->create_batch($slot_id);

            if ($res) {
                $this->session->setTempData('success', 'Successfully Batch Added', 3);
                return redirect()->to(current_url());
            } // Failed
            else {
                $this->session->setTempData('error', 'Batch Adding Failed', 3);
                return redirect()->to(current_url());
            }
        }
        echo view('admin/theme/admin_header', $data);
        echo view('admin/batches', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function slots()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 4;
        $data['page_title'] = 'Slots - Admin Dashboard - Thrivepad';
        $data['slot_list']  = $this->SlotsModel->listslots();

        $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

        $data['sun_list']  = $this->SlotsModel->days_fetch($days[0]);
        $data['mon_list']  = $this->SlotsModel->days_fetch($days[1]);
        $data['tues_list']  = $this->SlotsModel->days_fetch($days[2]);
        $data['wednes_list']  = $this->SlotsModel->days_fetch($days[3]);
        $data['thurs_list']  = $this->SlotsModel->days_fetch($days[4]);
        $data['fri_list']  = $this->SlotsModel->days_fetch($days[5]);
        $data['satur_list']  = $this->SlotsModel->days_fetch($days[6]);


        $data['batch_list']  = $this->SlotsModel->listbatches();

        echo view('admin/theme/admin_header', $data);
        echo view('admin/slots', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function user_profile($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata']  = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 1;

        $data['q'] = $this->ToolsModel->fetchwolquestionnaire($id);
        $data['c'] = $this->ToolsModel->fetchcelebratingyou($id);
        $data['d'] = $this->ToolsModel->fetchdiscusskey($id);
        $data['pq'] = $this->ToolsModel->fetchquestionnaire($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week1', $data);
        echo view('admin/theme/admin_footer', $data);
    }


    public function user_edit($id)
    {
        $data['validation'] = null;
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';

        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata']  = $this->MyProfileModel->fetch_with_id($id);

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'username' => 'required',
                'age' => 'required',
                'lastname' => 'required',
                'city' => 'required',
                'mobile' => 'required',
                'state' => 'required',

            ];
            if ($this->validate($rules)) {
                $data = array(
                    'username' => $this->request->getVar('username'),
                    'age' => $this->request->getVar('age'),
                    'lastname' => $this->request->getVar('lastname'),
                    'city' => $this->request->getVar('city'),
                    'mobile' => $this->request->getVar('mobile'),
                    'state' => $this->request->getVar('state'),

                );
                // File Upload
                $file = $this->request->getFile('file');
                if ($file &&  $file->isValid() && !$file->hasMoved()) {
                    $newName = $file->getRandomName();
                    if ($file->move(FCPATH . 'public/uploads/profilepic', $newName)) {
                        $data['profile_pic'] = $newName;
                    }
                }

                $res = $this->MyProfileModel->updatemyprofile($id, $data);

                //Success
                if ($res) {
                    $this->session->setTempData('success', 'Successfully Added', 3);
                    return redirect()->to(current_url());
                } // Failed
                else {
                    $this->session->setTempData('error', 'Something went wrong or Nothing to Update', 3);
                    return redirect()->to(current_url());
                }
            } else {

                $this->session->setTempData('error', 'Validation went wrong!', 3);
                $data['validation'] = $this->validator;
            }
        }

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_edit', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function blockstatus($id)
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data = $this->MyProfileModel->block_status($id);

        if ($data) {
            $this->session->setTempData('success', 'Successfully Blocked', 3);
            return redirect()->to(base_url('admin/users'));
        } else {
            $this->session->setTempData('error', 'This users are already blocked', 3);
            return redirect()->to(base_url('admin/users'));
        }

        echo view('admin/theme/admin_header', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function meet()
    {
        $data = [];
        $data['validation'] = null;
        $this->MyProfileModel = new MyProfileModel();
        $userData = $this->session->get('logged_user');

        $id = $this->request->getVar('userid');

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'userid' => 'required',
                'meetlink' => 'required',
                'meetdate' => 'required',
                'meettime' => 'required'
            ];

            if ($this->validate($rules)) {
                $data = array(
                    'meetlink' => $this->request->getVar('meetlink'),
                    'meetdate' => $this->request->getVar('meetdate'),
                    'meettime' =>  $this->request->getVar('meettime'),
                );
                $res = $this->MyProfileModel->updatemeet($id, $data);
                //Success
                if ($res) {
                    $this->session->setTempData('success', 'Meetlink Sent Successfully', 3);
                    return redirect()->to(base_url('admin/users'));
                } // Failed
                else {
                    $this->session->setTempData('error', ' Meetlink sent Lost!', 3);
                    return redirect()->to(base_url('admin/users'));
                }
            } else {

                $this->session->setTempData('error', 'Validation went wrong!', 3);
                $data['validation'] = $this->validator;
            }
        }
    }
    public function deleteslot($id)
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 3;
        $data['page_title'] = 'My Batches - Thrivepad';


        $data = $this->SlotsModel->delete_slot($id);

        if ($data) {
            $this->session->setTempData('success', 'Successfully Blocked', 3);
            return redirect()->to(base_url('admin/batches'));
        } else {
            $this->session->setTempData('error', 'This users are already blocked', 3);
            return redirect()->to(base_url('admin/batches'));
        }

        echo view('admin/theme/admin_header', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function endbatch($slotid)
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 3;
        $data['page_title'] = 'My Batches - Thrivepad';


        $data = $this->SlotsModel->end_batch($slotid);

        if ($data) {
            $this->session->setTempData('success', 'Successfully Batch Ended', 3);
            return redirect()->to(base_url('admin/batches'));
        } else {
            $this->session->setTempData('error', 'This Batch are already ended', 3);
            return redirect()->to(base_url('admin/batches'));
        }

        echo view('admin/theme/admin_header', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function startbatch($id)
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 3;
        $data['page_title'] = 'My Batches - Thrivepad';


        $data = $this->SlotsModel->start_batch($id);

        if ($data) {
            $this->session->setTempData('success', 'Successfully Batch Started', 3);
            return redirect()->to(base_url('admin/batches'));
        } else {
            $this->session->setTempData('error', 'This Batches are already Started', 3);
            return redirect()->to(base_url('admin/batches'));
        }

        echo view('admin/theme/admin_header', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function remove($id)
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 3;
        $data['page_title'] = 'My Batches - Thrivepad';

        //  $batchid = $this->request->getVar('batcheditid');
        $slotid = $this->request->getVar('slotid');


        $this->SlotsModel = new SlotsModel();

        $data = $this->SlotsModel->remove_edit($id, $slotid);

        if ($data) {
            $this->session->setTempData('success', 'Successfully removed', 3);
            return redirect()->to(base_url('admin/batches'));
        } else {
            $this->session->setTempData('error', 'This users are already removed', 3);
            return redirect()->to(base_url('admin/batches'));
        }

        echo view('admin/theme/admin_header', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function batchidget()
    {
        $this->SlotsModel = new SlotsModel();
        $data['batchdata']  = $this->SlotsModel->fetch_batch_id($this->request->getVar('batch_id'));

        echo json_encode($data);
    }

    public function insertlink()
    {

        $data['validation'] = null;
        $this->MyProfileModel = new MyProfileModel();
        $userData = $this->session->get('logged_user');

        $id = $this->request->getVar('batchid');

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'batchid' => 'required',
                'next_session_link' => 'required',
            ];


            if ($this->validate($rules)) {
                $data = array(
                    'next_session_link' => $this->request->getVar('next_session_link'),

                );
                $res = $this->SlotsModel->updatemeetlink($id, $data);
                //Success
                if ($res) {
                    $this->session->setTempData('success', 'Meetlink Sent Successfully', 3);
                    return redirect()->to(base_url('admin/batches'));
                }
                // Failed
                else {
                    $this->session->setTempData('error', ' Meetlink sent Lost!', 3);
                    return redirect()->to(base_url('admin/batches'));
                }
            } else {

                $this->session->setTempData('error', 'Validation went wrong!', 3);
                $data['validation'] = $this->validator;
            }
        }
    }

    public function changelink()
    {

        $data = [];
        $data['validation'] = null;
        $this->SlotsModel = new SlotsModel();
        $userData = $this->session->get('logged_user');


        $id = $this->request->getVar('batchchangeid');

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'batchchangeid' => 'required',
                'next_session_link' => 'required',
            ];

            if ($this->validate($rules)) {
                $data = array(
                    'next_session_link' => $this->request->getVar('next_session_link'),
                );
                $res = $this->SlotsModel->updatemeetlink($id, $data);
                //Success
                if ($res) {
                    $this->session->setTempData('success', 'MEETLINK UPDATED SUCCESSFULL', 3);
                    return redirect()->to(base_url('admin/batches'));
                }
                // Failed
                else {
                    $this->session->setTempData('error', 'MEETLINK UPDATION LOST!', 3);
                    return redirect()->to(base_url('admin/batches'));
                }
            } else {

                $this->session->setTempData('error', 'Validation went wrong!', 3);
                $data['validation'] = $this->validator;
            }
        }
    }

    public function fetch_data()
    {

        $data = [];
        $data['validation'] = null;
        $userData = $this->session->get('logged_user');

        $batchid = $this->request->getVar('batchid');

        $this->MyProfileModel = new MyProfileModel();

        $results = $this->MyProfileModel->fetched_data($batchid);

        $propertyHtml = '';

        if (count($results) > 0) {
            foreach ($results as $row) {
                $propertyHtml .= '
<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                         <tr>
                                    <td>' . $row['id'] . '</td>
                                    <td>' . $row['username'] . '</td>
                                        <td>
                                        <a  class="btn btn-md button-theme" href="' . base_url() . '/admin/user_profile/' . $row['id'] . '" target="_blank" class="property-img">View</a>
                                        </td>
                        
                                    </tr>
                              
                        </tbody>
                
                    </table>';
            }
        } else {
            $propertyHtml = '<h4>No Data Found </h4>';
        }

        $output = array(
            'user_list' => ($propertyHtml),
        );


        echo json_encode($output);
    }

    public function fetchslot_data()
    {

        $data = [];
        $data['validation'] = null;
        $userData = $this->session->get('logged_user');

        $batchid = $this->request->getVar('slotid');

        $this->MyProfileModel = new MyProfileModel();

        $results = $this->MyProfileModel->fetchedslot_data($batchid);

        $propertyHtml = '';

        if (count($results) > 0) {
            foreach ($results as $row) {
                $propertyHtml .= '
<table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                         <tr>
                                    <td>' . $row['id'] . '</td>
                                    <td>' . $row['username'] . '</td>
                                        <td>
                                        <a  class="btn btn-md button-theme" href="' . base_url() . '/admin/user_profile/' . $row['id'] . '" target="_blank" class="property-img">View</a>
                                        </td>
                        
                                    </tr>
                              
                        </tbody>
                
                    </table>';
            }
        } else {
            $propertyHtml = '<h4>No Data Found </h4>';
        }

        $output = array(
            'user_list' => ($propertyHtml),
        );


        echo json_encode($output);
    }

    public function adduser()
    {

        $id = $this->request->getVar('userid');
        $slotid = $this->request->getVar('sloteditid');
        $batchid = $this->request->getVar('batcheditid');

        if ($this->request->getMethod() == 'post') {
            $rules = [
                'userid' => 'required',
                'batcheditid' => 'required',
                'sloteditid' => 'required',

            ];
            if ($this->validate($rules)) {
                $data = array(
                    'batch_id' => $this->request->getVar('batcheditid'),
                    'slot_id' =>  $this->request->getVar('sloteditid'),
                );

                $this->SlotsModel = new SlotsModel();
                $res = $this->SlotsModel->add_user($id, $slotid, $data);
                //Success
                if ($res) {
                    $this->session->setTempData('success', 'User update Successfully', 3);
                    return redirect()->to(base_url('admin/batches'));
                }
                // Failed
                else {
                    $this->session->setTempData('error', ' User updation sent Lost!', 3);
                    return redirect()->to(base_url('admin/batches'));
                }
            } else {
                $this->session->setTempData('error', 'Validation went wrong!', 3);
                $data['validation'] = $this->validator;
            }
        }
    }

    public function fetchedit_data()
    {

        $data = [];
        $data['validation'] = null;
        $userData = $this->session->get('logged_user');

        $slotid = $this->request->getVar('sloteditid');
        $batchid = $this->request->getVar('batcheditid');

        $this->MyProfileModel = new MyProfileModel();

        $results = $this->MyProfileModel->fetchedited_data($slotid, $batchid);

        $propertyHtml = '';
        $userHtml = '';

        if (count($results) > 0) {
            foreach ($results as $row) {
                $propertyHtml .= '
    <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                             <tr>
                                        <td>' . $row['id'] . '</td>
                                        <td>' . $row['username'] . '</td>
                                            <td>
                                            <a  class="btn btn-md button-theme" href="' . base_url() . '/admin/user_profile/' . $row['id'] . '" target="_blank" class="property-img">View</a>
                                            <a onclick="return confirm(\'Are you sure want to delete ?\')"  class="btn btn-md button-theme" href="' . base_url() . '/admin/remove/' . $row['slot_id'] . '" class="property-img">Remove</a>
                                            </td>
                            
                                        </tr>
                                  
                            </tbody>
                    
                        </table>';
            }
        } else {
            $propertyHtml = '<h4 class="text-center">No Data Found </h4>';
        }


        $this->MyProfileModel = new MyProfileModel();

        $res = $this->MyProfileModel->fetcheduser_data($slotid, $batchid);
        $res1 = $this->MyProfileModel->fetchedbatch_data($slotid);

        if (count($res) > 0 || count($res1) > 0) {
            foreach ($res as $row) {
                foreach ($res1 as $row1) {
                    if (($row1['count'] == 0 && $row1['batch_type'] == 1) || ($row1['count'] >= 0 && $row1['batch_type'] == 2)) {
                        $userHtml = '<form  action="' . base_url() . '/admin/adduser" method="POST">    
                            <input type="hidden" name="sloteditid" id="sloteditid" value="' . $slotid . ' " />
                            <input type="hidden" name="batcheditid" id="batcheditid" value="' . $batchid . '" />
        
                            <div class="row justify-content-md-center">
                
                            <div class="col-sm-6">
                            <div class="form-group">
                            <select class="form-control" name="userid" id="userid">
                            <option value="">Select by name</option>
                                  <option value=" ' . $row['id'] . ' "> ' . $row['id'] . ' - ' . $row['username'] . ' </option>
                               </select>
                         
                                   
                        </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                     <input type="submit" class="btn btn-md button-theme" value="Add User" />
                                        </span>
                                    </div>
                                </div>
                            </div>
    
                        </div>        </form>';
                    }
                }
            }
        }
        $output = array(
            'user_list' => ($propertyHtml),
            'users_list' => ($userHtml)
        );


        echo json_encode($output);
    }
    public function fetchsession_data()
    {
        $userData = $this->session->get('logged_user');

        $slotid = $this->request->getVar('slotsesid');
        $batchid = $this->request->getVar('batchsesid');

        $this->MyProfileModel = new MyProfileModel();

        $results = $this->MyProfileModel->fetchedsession_data($slotid);
        $results1 = $this->MyProfileModel->fetchedsession_week($slotid);


        $propertyHtml = '';

        if (count($results) > 0) {
            foreach ($results as $row) {
                $day = $row['day'];
                $today = date("l");
                if ($today == "Monday"); {
                    $val = 1;
                }
                if ($today == "Tueday"); {
                    $val = 2;
                }
                if ($today == "Wednesday"); {
                    $val = 3;
                }
                if ($today == "Thrusday"); {
                    $val = 4;
                }
                if ($today == "Friday"); {
                    $val = 5;
                }
                if ($today == "Saturday"); {
                    $val = 6;
                }
                $dayvalue = $row['dayvalue'];
                $day_diff = $val - $dayvalue;

                if ($day_diff > 2) {
                    $next_day1 = date('Y-m-d', strtotime('' . $day . ' next week'));
                    $next_day2 = date('Y-m-d', strtotime('+7 day', strtotime($next_day1)));
                    $next_day3 = date('Y-m-d', strtotime('+7 day', strtotime($next_day2)));
                } else if ($day_diff <= 0) {
                    $next_day1 = date('Y-m-d', strtotime('' . $day . ' next week'));
                    $next_day2 = date('Y-m-d', strtotime('+7 day', strtotime($next_day1)));
                    $next_day3 = date('Y-m-d', strtotime('+7 day', strtotime($next_day2)));
                } else {
                    $next_day1 = date('Y-m-d', strtotime('' . $day . ' next week'));
                    $next_day2 = date('Y-m-d', strtotime('+7 day', strtotime($next_day1)));
                    $next_day3 = date('Y-m-d', strtotime('+7 day', strtotime($next_day2)));
                }



                $propertyHtml .= '
                <form  action="' . base_url() . '/admin/sessionlinks" method="POST">    
                <input type="hidden" name="slotsesid" id="slotsesid" value="' . $slotid . ' " />
                <input type="hidden" name="batchsesid" id="batchsesid" value="' . $batchid . '" />

                <div class="row justify-content-md-center">
    
                <div class="col-sm-12">
                <div class="form-group">
                <select class="form-control" name="link" id="link" required>';
                if (count($results1) > 0) {
                    foreach ($results1 as $row1) {
                        if ($row1['session_count'] == 0) {
                            $status = '1';
                        } else {
                            $status = $row1['session_count'];
                        }
                    }
                }
                $propertyHtml .= '<option value="" disabled selected="true">Select the next session date of  Week ' . $status . '</option>';
                $propertyHtml .= '         <option value="' . $next_day1 . ' ' . $row['time'] . '">' . $next_day1 . ' ' . $row['day'] . ' ' . $row['time'] . '</option>
<option value="' . $next_day2 . ' ' . $row['time'] . '">' . $next_day2 . ' ' . $row['day'] . ' ' . $row['time'] . '</option>
<option value="' . $next_day3 . ' ' . $row['time'] . '">' . $next_day3 . ' ' . $row['day'] . ' ' . $row['time'] . '</option>
</select>


</div>
</div>
<div class="col-sm-12">
    <div class="form-group">
        <div class="input-group">
            <span class="input-group-btn">
                <input type="submit" class="btn btn-md button-theme" value="Submit Session Date" />
            </span>
        </div>
    </div>
</div>

</div>
</form>';
            }
        } else {
            $propertyHtml = '<h4>No Data Found </h4>';
        }
        $output = array(
            'user_list' => ($propertyHtml),
        );
        echo json_encode($output);
    }

    public function week2($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 2;

        $data['a'] = $this->ToolsModel->fetchacceptance($id);
        $data['bs'] = $this->ToolsModel->fetchbasevaluessatis($id);
        $data['bds'] = $this->ToolsModel->fetchbasevaluedissatis($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week2', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week3($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 3;
        $data['bs'] = $this->ToolsModel->fetchletsalign($id);
        $data['h'] = $this->ToolsModel->fetchhabits($id);
        $data['dl'] = $this->ToolsModel->fetchdelabelling($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week3', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week4($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 4;
        $data['p'] = $this->ToolsModel->fetchpositivemoments($id);
        $data['n'] = $this->ToolsModel->fetchnegativemoments($id);
        $data['ss'] = $this->ToolsModel->fetchstorysharing($id);
        $data['gp'] = $this->ToolsModel->fetchgenpcode($id);
        $data['ncode'] = $this->ToolsModel->fetchncode($id);
        $data['ncodevalue'] = $this->ToolsModel->fetchncodevalue();
        $data['discussncode'] = $this->ToolsModel->fetchdisncode($id);
        $data['ncodechart'] = $this->ToolsModel->fetchncodechart();
        $data['chartres'] = $this->ToolsModel->fetchnchartres($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week4', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week5($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 5;
        $data['dq'] = $this->ToolsModel->fetchdesire($id);
        $data['discover'] = $this->ToolsModel->fetchdiscover($id);
        $data['npactivity'] = $this->ToolsModel->fetchnpactivity($id);
        $data['ncode'] = $this->ToolsModel->fetchncode($id);
        $data['ncodevalue'] = $this->ToolsModel->fetchncodevalue();
        $data['discusspcode'] = $this->ToolsModel->fetchdispcode($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week5', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week6($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 6;
        $data['challenge'] = $this->ToolsModel->fetchchallengeover($id);
        $data['success'] = $this->ToolsModel->fetchsuccessalign($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week6', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week7($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 7;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 7;
        $data['outcome'] = $this->ToolsModel->fetchout($id);
        $data['decision'] = $this->ToolsModel->fetchdecision($id);
        $data['smart'] = $this->ToolsModel->fetchsmartgoals($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week7', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week8($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 8;

        $data['scenario1'] = $this->ToolsModel->fetchscenarioone($id);
        $data['scenario2'] = $this->ToolsModel->fetchscenariotwo($id);
        $data['scenario3'] = $this->ToolsModel->fetchscenariothree($id);
        $data['master'] = $this->ToolsModel->fetchmastermind($id);
        $data['toxic'] = $this->ToolsModel->fetchtoxic($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week8', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function decisionwheel($id)
    {
        $userData = $this->session->get('logged_user');

        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['page_title'] = 'Decision Wheel - Thrivepad';
        $data['wheel'] = $this->ToolsModel->fetchdecisionwheel($id);


        echo view('admin/tools/theme/tools_header', $data);
        echo view('admin/tools/decisionwheel', $data);
    }
    public function wheeloflife($id)
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Wheel Of Life - Thrivepad';
        $data['userData'] = $userData;
        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $result = $this->ToolsModel->fetchWheeloflife($id);

        $data['wheellife'] = $this->ToolsModel->fetchWheeloflife($id);

        $data['result'] = $result[0];

        if ($data['result']) {
            echo view('admin/tools/theme/tools_header', $data);
            echo view('admin/tools/wheelofliferesult', $data);
        } else {
            echo view('admin/tools/theme/tools_header', $data);
            echo view('admin/tools/wheeloflife', $data);
        }
    }
    public function outcomereinfrocement($id)
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Outcome Reinforcement - Thrivepad';
        $data['userData'] = $userData;
        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $result = $this->ToolsModel->fetchoutcome($id);
        $data['out'] = $this->ToolsModel->fetchoutcome($id);
        $data['dq'] = $this->ToolsModel->fetchdesire($id);

        $data['result'] = $result[0];

        if ($data['result']) {
            echo view('admin/tools/theme/tools_header', $data);
            echo view('admin/tools/outcome', $data);
        } else {
            echo view('admin/tools/theme/tools_header', $data);
            echo view('admin/tools/outcome', $data);
        }
    }
    public function resultncode($id)
    {
        $userData = $this->session->get('logged_user');
        $data['userData'] = $userData;

        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['ncodevalue'] = $this->ToolsModel->fetchncodevalue();

        $data['ncode'] = $this->ToolsModel->fetchncode($id);


        echo view('admin/tools/theme/tools_header', $data);
        echo view('admin/tools/resultncode', $data);
    }
    public function week9($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 9;

        $data['happy'] = $this->ToolsModel->fetchmyhappy($id);
        $data['vision'] = $this->ToolsModel->fetchvision($id);
        $data['imag'] = $data['vision'][0]['vision_img'];

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week9', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function myhabit($id)
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'My Habit';

        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['habit'] = $this->ToolsModel->fetchhabit($id);

        echo view('admin/theme/admin_header', $data);
        echo view('livesessions/myhabit', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function breakthrough($id)
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Breakthrough';

        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['breakthrough'] = $this->ToolsModel->fetchbreakthrough($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/livesessions/breakthrough', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function accomplishment($id)
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Accomplishment';

        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['accomplishment'] = $this->ToolsModel->fetchaccomplishment($id);

        echo view('admin/theme/admin_header', $data);
        echo view('livesessions/accomplishment', $data);
        echo view('admin/theme/admin_footer', $data);
    }
    public function week10($id)
    {
        $userData = $this->session->get('logged_user');
        $data['validation'] = null;
        $data['page_no'] = 2;
        $data['page_title'] = 'My Profile - Thrivepad';


        $this->MyProfileModel = new MyProfileModel();
        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $data['live_session'] = 10;

        $data['blueprint'] = $this->ToolsModel->fetchblueprint($id);

        echo view('admin/theme/admin_header', $data);
        echo view('admin/user_profile', $data);
        echo view('admin/livesessions/live-session-header', $data);
        echo view('admin/livesessions/week10', $data);
        echo view('admin/theme/admin_footer', $data);
    }

    public function report($id)
    {
        $userData = $this->session->get('logged_user');


        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetHeader('WEEK-10|SUCCESS BLUEPRINT|{PAGENO}');
        $mpdf->SetFooter();

        $this->ToolsModel = new ToolsModel();
        $this->MyProfileModel = new MyProfileModel();

        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);

        $results = $this->ToolsModel->fetchblueprint($id);


        if (count($results) > 0) {
            foreach ($results as $row) {
                $html = '
<h4>1) Write down all your 6 Base-Values.</h4>
<p style="margin-left:20px">' . $row['q1'] . '</p>
<h4>2) List all your P-codes</h4>
<p style="margin-left:20px">' . $row['q2'] . '</p>
<h4>3) List your ultimate SMART GOAL.</h4>
<p style="margin-left:20px">' . $row['q3'] . '</p>
<h4>4) List priority steps to reach the SMART GOAL.</h4>
<p style="margin-left:20px">' . $row['q4'] . '</p>
<h4>5) Write down top 3 desires to be fulfilled in next 3 years.</h4>
<p style="margin-left:20px">' . $row['q5'] . '</p>
<h4>6) Write down top 3 desires to be fulfilled in next 5 years.</h4>
<p style="margin-left:20px">' . $row['q6'] . '</p>
<h4>7) List your mastermind circle and support team who empower you to take action.</h4>
<p style="margin-left:20px">' . $row['q7'] . '</p>
<h4>8) Write your ultimate definition of success, what it means to you? and what will be different? How life will be
    different when you will be successful?</h4>
<p style="margin-left:20px">' . $row['q8'] . '</p>
<h4>9) Anything else you know that needs to change that’s left out?</h4>
<p style="margin-left:20px">' . $row['q9'] . '</p>
<h4>10) What new skills do you need to learn?</h4>
<p style="margin-left:20px">' . $row['q10'] . '</p>
<h4>11) Which part of your character needs to improve? </h4>
<p style="margin-left:20px">' . $row['q11'] . '</p>
<h4>12) What additional P-codes do you need to embrace to achieve your Desires? These P-codes should be based on your
    vision board.</h4>
<p style="margin-left:20px">' . $row['q12'] . '</p>
<h4>13) How will you continue to use the personal insights gained through this program in the future?</h4>
<p style="margin-left:20px">' . $row['q13'] . '</p>
<h4>14) Write down your power statement?</h4>
<p style="margin-left:20px">' . $row['q14'] . '</p>
';
            }
        }
        $mpdf->WriteHTML($html);
        return redirect()->to($mpdf->Output('Report.pdf', 'I'));
    }

    public function redo($id)
    {
        $userData = $this->session->get('logged_user');

        $this->ToolsModel = new ToolsModel();
        $this->MyProfileModel = new MyProfileModel();

        $data['profiledata'] = $this->MyProfileModel->fetch_with_id($id);
        if ($this->request->getMethod() == 'post') {

            $tab = $this->request->getVar('tab');
            $url = $this->request->getVar('url');

            if ($this->ToolsModel->redotable($id, $tab)) {
                $this->session->setTempData('success', 'REDO SUBMITTED SUCCESSFULL', 3);
                return redirect()->to($url);
            } else {
                $this->session->setTempData('error', 'This activity was not attended,we do not taken redo', 3);
                return redirect()->to($url);
            }
        }
    }
    public function sessionlinks()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 3;
        $data['page_title'] = 'Batches - Admin Dashboard - Thrivepad';

        if ($this->request->getMethod() == 'post') {

            $slotid = $this->request->getVar('slotsesid');
            $batchid = $this->request->getVar('batchsesid');
            $nextsession = $this->request->getVar('link');

            $this->SlotsModel = new SlotsModel();

            $res = $this->SlotsModel->updateseslink($slotid, $nextsession);

            if ($res) {
                $this->session->setTempData('success', 'NEXT SESSION LINK ADDED SUCCESSFULL', 3);
                return redirect()->to(base_url('admin/batches'));
            } // Failed
            else {
                $this->session->setTempData('error', 'NEXT SESSION LINK UPDATION LOST', 3);
                return redirect()->to(base_url('admin/batches'));
            }
        }
        echo view('admin/theme/admin_header', $data);
        echo view('admin/batches', $data);
        echo view('admin/theme/admin_footer', $data);
    }
}