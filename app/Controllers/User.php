<?php

namespace App\Controllers;

use App\Models\ToolsModel;


class User extends BaseController
{
    public $session;
    public function __construct()
    {

        helper('form');
        helper('url');
        $this->session = session();
    }


    public function dashboard()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 1;
        $data['page_title'] = 'Dashboard - Thrivepad';

        $this->ToolsModel = new ToolsModel();
        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];

        echo view('theme/profile_header', $data);
        echo view('my_dashboard', $data);
        echo view('theme/profile_footer', $data);
    }
}