<?php

namespace App\Controllers;

use \Mpdf\Mpdf;
use App\Models\ToolsModel;


class Report extends BaseController
{
    public $session;
    public $toolsModel;
    public function __construct()
    {
        helper('form');
        helper('url');
        $this->session = session();
        $this->toolsModel = new ToolsModel();
    }

    public function index()
    {
        $userData = $this->session->get('logged_user');


        $mpdf = new \Mpdf\Mpdf();
        $mpdf->SetHeader('WEEK-10|SUCCESS BLUEPRINT|{PAGENO}');
        $mpdf->SetFooter();
        $this->ToolsModel = new ToolsModel();

        $results = $this->ToolsModel->fetchblueprint($userData['id']);


        if (count($results) > 0) {
            foreach ($results as $row) {
                $html = '
<h4>1) Write down all your 6 Base-Values.</h4><p style="margin-left:20px">' . $row['q1'] . '</p>
<h4>2) List all your P-codes</h4><p style="margin-left:20px">' . $row['q2'] . '</p>
<h4>3) List your ultimate SMART GOAL.</h4><p style="margin-left:20px">' . $row['q3'] . '</p>
<h4>4) List priority steps to reach the SMART GOAL.</h4><p style="margin-left:20px">' . $row['q4'] . '</p>
<h4>5) Write down top 3 desires to be fulfilled in next 3 years.</h4><p style="margin-left:20px">' . $row['q5'] . '</p>
<h4>6) Write down top 3 desires to be fulfilled in next 5 years.</h4><p style="margin-left:20px">' . $row['q6'] . '</p>
<h4>7) List your mastermind circle and support team who empower you to take action.</h4><p style="margin-left:20px">' . $row['q7'] . '</p>
<h4>8) Write your ultimate definition of success, what it means to you? and what will be different? How life will be different when you will be successful?</h4><p style="margin-left:20px">' . $row['q8'] . '</p>
<h4>9) Anything else you know that needs to change that’s left out?</h4><p style="margin-left:20px">' . $row['q9'] . '</p>
<h4>10) What new skills do you need to learn?</h4><p style="margin-left:20px">' . $row['q10'] . '</p>
<h4>11) Which part of your character needs to improve? </h4><p style="margin-left:20px">' . $row['q11'] . '</p>
<h4>12)	What additional P-codes do you need to embrace to achieve your Desires? These P-codes should be based on your vision board.</h4><p style="margin-left:20px">' . $row['q12'] . '</p>
<h4>13)	How will you continue to use the personal insights gained through this program in the future?</h4><p style="margin-left:20px">' . $row['q13'] . '</p>
<h4>14)	Write down your power statement?</h4><p style="margin-left:20px">' . $row['q14'] . '</p>
                                 ';
            }
        }
        $mpdf->WriteHTML($html);
        return redirect()->to($mpdf->Output('Report.pdf', 'I'));
    }
}