<?php

namespace App\Controllers;

use App\Models\MyProfileModel;
use App\Models\LoginModel;


class Myaccount extends BaseController
{
	public $session;
	public function __construct()
	{
		helper('form');
		helper('url');
		$this->session = session();
	}


	public function myprofile()
	{
		$data['validation'] = null;
		$userData = $this->session->get('logged_user');
		$data['page_no'] = 10;
		$data['page_title'] = 'My Profile - Thrivepad';
		if ($this->request->getMethod() == 'post') {
			$this->MyProfileModel = new MyProfileModel();
			$rules = [
				'username' => 'required',
				'age' => 'required',
				'lastname' => 'required',
				'city' => 'required',
				'mobile' => 'required',
				'state' => 'required',
			
			];
			if ($this->validate($rules)) {
				$data = array(
					'username' => $this->request->getVar('username'),
					'age' => $this->request->getVar('age'),
					'lastname' => $this->request->getVar('lastname'),
				'city' => $this->request->getVar('city'),
				'mobile' => $this->request->getVar('mobile'),
				'state' => $this->request->getVar('state'),
			
				);
				// File Upload
				$file = $this->request->getFile('file');
				if ($file &&  $file->isValid() && !$file->hasMoved()) {
					$newName = $file->getRandomName();
					if ($file->move(FCPATH . 'public/uploads/profilepic', $newName)) {
						$data['profile_pic'] = $newName;
					}
				}

				$res = $this->MyProfileModel->updatemyprofile($userData['id'], $data);

				//Success
				if ($res) {
					$this->session->setTempData('success', 'Successfully Added', 3);
					return redirect()->to(current_url());
				} // Failed
				else {
					$this->session->setTempData('error', 'Something went wrong or Nothing to Update', 3);
					return redirect()->to(current_url());
				}
			} else {

				$this->session->setTempData('error', 'Validation went wrong!', 3);
				$data['validation'] = $this->validator;
			}
		}

		$this->MyProfileModel = new MyProfileModel();
		$data['profiledata']  = $this->MyProfileModel->fetch($userData['email']);
		echo view('theme/profile_header', $data);
		echo view('my_profile', $data);
		echo view('theme/profile_footer', $data);
	}
	
	
	public function changepassword()
	{

		$userData = $this->session->get('logged_user');
		$this->MyProfileModel = new MyProfileModel();
		$this->LoginModel = new LoginModel();

		if ($this->request->getMethod() == 'post') {

			$rules = [
				'currentpassword' => 'required|min_length[6]|max_length[16]',
				'newpassword'  => 'required|min_length[6]|max_length[16]',
				'confirmnewpassword'  => 'required|min_length[6]|max_length[16]|matches[newpassword]'
			];
			if ($this->validate($rules)) {
				$pass = $this->request->getVar('currentpassword');
				$dbData = $this->LoginModel->verifyEmail($userData['email']);

				if (password_verify($pass, $dbData['password'])) {
					$data['password'] = password_hash($this->request->getVar('newpassword'), PASSWORD_DEFAULT);
					$res = $this->MyProfileModel->changepassword($userData['email'], $data);


					if ($res) {
						$this->session->setTempData('password-success', 'Success! New Password Changed', 3);
						return redirect()->to(base_url('myprofile'));
					} else {
						$this->session->setTempData('password-error', 'Error!  Something Went Wrong', 3);
						return redirect()->to(base_url('myprofile'));
					}
				} else {
					$this->session->setTempData('password-error', 'Error! Incorrect Current Password', 3);
					return redirect()->to(base_url('myprofile'));
				}
			} else {
				$this->session->setTempData('password-error', $this->validator->listErrors(), 3);
				$data['validation'] = $this->validator;
				return redirect()->to(base_url('myprofile'));
			}
		} else {
			return redirect()->to(base_url('myprofile'));
		}
	}

}
