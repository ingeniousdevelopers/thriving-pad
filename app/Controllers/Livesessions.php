<?php

namespace App\Controllers;

use App\Models\ToolsModel;


class Livesessions extends BaseController
{
    public $session;
    public $registerModel;

    public function __construct()
    {
        helper('form');
        helper('url');
        $this->session = session();
        $userData = $this->session->get('logged_user');

        $this->ToolsModel = new ToolsModel();
        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];
    }

    public function livesession()
    {

        $data['page_no'] = 4;
        $userData = $this->session->get('logged_user');

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];

        echo view('theme/profile_header', $data);
        echo view('theme/livesession', $data);
        echo view('theme/profile_footer', $data);
    }


    public function week1()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 1- Live Session - Thrivepad';
        $data['live_session'] = 1;

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];

        $data['q'] = $this->ToolsModel->fetchwolquestionnaire($userData['id']);
        $data['c'] = $this->ToolsModel->fetchcelebratingyou($userData['id']);
        $data['d'] = $this->ToolsModel->fetchdiscusskey($userData['id']);
        $data['pq'] = $this->ToolsModel->fetchquestionnaire($userData['id']);

        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week1', $data);
        echo view('theme/profile_footer', $data);
    }

    public function wolquestionnaire()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'questionnaire1' => 'required|max_length[500]',
                'questionnaire2' => 'required|max_length[500]',
                'questionnaire3' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {
                $questionnaire1 = $this->request->getVar('questionnaire1');
                $questionnaire2 = $this->request->getVar('questionnaire2');
                $questionnaire3 = $this->request->getVar('questionnaire3');

                $data = [
                    'q1'  => $questionnaire1,
                    'q2'  => $questionnaire2,
                    'q3'  => $questionnaire3,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->wolquestionnaire($data)) {
                    $this->session->setTempData('success', 'WOL QUESTIONNAIRE SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function celebratingyou()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'answer' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {
                $answer = $this->request->getVar('answer');

                $data = [
                    'answer'  => $answer,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->celebratingyou($data)) {
                    $this->session->setTempData('success', 'CELEBRATING YOU SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function discusskey()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'discusskey' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {
                $discusskey = $this->request->getVar('discusskey');

                $data = [
                    'discusskey'  => $discusskey,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->discusskey($data)) {
                    $this->session->setTempData('success', 'DISCUSS KEY-Takeaways SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }


    public function week2()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 2- Live Session - Thrivepad';
        $data['live_session'] = 2;

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        $data['a'] = $this->ToolsModel->fetchacceptance($userData['id']);
        $data['bs'] = $this->ToolsModel->fetchbasevaluessatis($userData['id']);
        $data['bds'] = $this->ToolsModel->fetchbasevaluedissatis($userData['id']);


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week2', $data);
        echo view('theme/profile_footer', $data);
    }



    public function acceptance()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'acceptance' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'acceptance' => $this->request->getVar('acceptance'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->acceptance($data)) {
                    $this->session->setTempData('success', 'ACCEPTANCE ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week2'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week2'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }


    public function basevalues_satisfaction()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {


            $rules = [
                's1' => 'required|max_length[500]',
                's2' => 'required|max_length[500]',
                's3' => 'required|max_length[500]',
                's4' => 'required|max_length[500]',
                's5' => 'required|max_length[500]',
                's6' => 'required|max_length[500]',
                's7' => 'required|max_length[500]',
                's8' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {

                $data = [
                    's1' =>  $this->request->getVar('s1'),
                    's2' =>  $this->request->getVar('s2'),
                    's3' =>  $this->request->getVar('s3'),
                    's4' =>  $this->request->getVar('s4'),
                    's5' =>  $this->request->getVar('s5'),
                    's6' =>  $this->request->getVar('s6'),
                    's7' =>  $this->request->getVar('s7'),
                    's8' =>  $this->request->getVar('s8'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->basevaluessatis($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week2'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week2'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }



    public function basevalues_dissatisfaction()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'ds1' => 'required|max_length[500]',
                'ds2' => 'required|max_length[500]',
                'ds3' => 'required|max_length[500]',
                'ds4' => 'required|max_length[500]',
                'ds5' => 'required|max_length[500]',
                'ds6' => 'required|max_length[500]',
                'ds7' => 'required|max_length[500]',
                'ds8' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'ds1' =>  $this->request->getVar('ds1'),
                    'ds2' =>  $this->request->getVar('ds2'),
                    'ds3' =>  $this->request->getVar('ds3'),
                    'ds4' =>  $this->request->getVar('ds4'),
                    'ds5' =>  $this->request->getVar('ds5'),
                    'ds6' =>  $this->request->getVar('ds6'),
                    'ds7' =>  $this->request->getVar('ds7'),
                    'ds8' =>  $this->request->getVar('ds8'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->basevaluesdissatis($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week2'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week2'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function week3()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 3- Live Session - Thrivepad';
        $data['live_session'] = 3;

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        $data['bs'] = $this->ToolsModel->fetchletsalign($userData['id']);
        $data['h'] = $this->ToolsModel->fetchhabits($userData['id']);
        $data['dl'] = $this->ToolsModel->fetchdelabelling($userData['id']);

        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week3', $data);
        echo view('theme/profile_footer', $data);
    }

    public function letsalign()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {


            $rules = [
                's1' => 'required|max_length[500]',
                's2' => 'required|max_length[500]',
                's3' => 'required|max_length[500]',
                's4' => 'required|max_length[500]',
                's5' => 'required|max_length[500]',
                's6' => 'required|max_length[500]',
                's7' => 'required|max_length[500]',
                's8' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {

                $data = [
                    's1' =>  $this->request->getVar('s1'),
                    's2' =>  $this->request->getVar('s2'),
                    's3' =>  $this->request->getVar('s3'),
                    's4' =>  $this->request->getVar('s4'),
                    's5' =>  $this->request->getVar('s5'),
                    's6' =>  $this->request->getVar('s6'),
                    's7' =>  $this->request->getVar('s7'),
                    's8' =>  $this->request->getVar('s8'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->letsalign($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week3'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week3'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function habits()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'habits' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'habits' => $this->request->getVar('habits'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->habits($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week3'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week3'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function delabelling()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'lables' => 'required|max_length[500]',
                'onelable' => 'required|max_length[500]',
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'lables' => $this->request->getVar('lables'),
                    'onelable' => $this->request->getVar('onelable'),
                    'q1' => $this->request->getVar('q1'),
                    'q2' => $this->request->getVar('q2'),
                    'q3' => $this->request->getVar('q3'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->delabelling($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week3'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week3'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function week4()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 4- Live Session - Thrivepad';
        $data['live_session'] = 4;

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        $data['p'] = $this->ToolsModel->fetchpositivemoments($userData['id']);
        $data['n'] = $this->ToolsModel->fetchnegativemoments($userData['id']);
        $data['ss'] = $this->ToolsModel->fetchstorysharing($userData['id']);
        $data['gp'] = $this->ToolsModel->fetchgenpcode($userData['id']);
        $data['ncode'] = $this->ToolsModel->fetchncode($userData['id']);
        $data['ncodevalue'] = $this->ToolsModel->fetchncodevalue();
        $data['discussncode'] = $this->ToolsModel->fetchdisncode($userData['id']);
        $data['ncodechart'] = $this->ToolsModel->fetchncodechart();
        $data['chartres'] = $this->ToolsModel->fetchnchartres($userData['id']);




        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week4', $data);
        echo view('theme/profile_footer', $data);
    }

    public function positivemoments()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {


            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
                'q7' => 'required|max_length[500]',
                'q8' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'q1' =>  $this->request->getVar('q1'),
                    'q2' =>  $this->request->getVar('q2'),
                    'q3' =>  $this->request->getVar('q3'),
                    'q4' =>  $this->request->getVar('q4'),
                    'q5' =>  $this->request->getVar('q5'),
                    'q6' =>  $this->request->getVar('q6'),
                    'q7' =>  $this->request->getVar('q7'),
                    'q8' =>  $this->request->getVar('q8'),
                    'user_id' => $userData['id']
                ];

                $images = $this->request->getFiles('images');
                //    print_r($images);
                // die();
                if (!empty($images['images'][0])) {
                    $imgArray = $this->uploadFiles();
                    $comma_separated = implode(",", $imgArray);
                    $data['images'] = $comma_separated;
                }

                if ($this->ToolsModel->positivemoments($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function negativemoments()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {


            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
                'q7' => 'required|max_length[500]',
                'q8' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'q1' =>  $this->request->getVar('q1'),
                    'q2' =>  $this->request->getVar('q2'),
                    'q3' =>  $this->request->getVar('q3'),
                    'q4' =>  $this->request->getVar('q4'),
                    'q5' =>  $this->request->getVar('q5'),
                    'q6' =>  $this->request->getVar('q6'),
                    'q7' =>  $this->request->getVar('q7'),
                    'q8' =>  $this->request->getVar('q8'),
                    'user_id' => $userData['id']
                ];

                $images = $this->request->getFiles('images');
                if (!empty($images['images'][0])) {
                    $imgArray = $this->uploadFiles();
                    $comma_separated = implode(",", $imgArray);
                    $data['images'] = $comma_separated;
                }


                if ($this->ToolsModel->negativemoments($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    private function uploadFiles()
    {
        $files = $this->request->getFiles('images');
        $images = array();
        foreach ($files['images'] as $img) {
            if ($img->isValid() && !$img->hasMoved()) {
                if ($img->move(FCPATH . 'public/uploads', $img->getRandomName())) {
                    $images[] = $img->getName();
                }
            }
        }

        return $images;
    }

    public function storysharing()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {


            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'q1' =>  $this->request->getVar('q1'),
                    'q2' =>  $this->request->getVar('q2'),
                    'q3' =>  $this->request->getVar('q3'),
                    'q4' =>  $this->request->getVar('q4'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->storysharing($data)) {
                    $this->session->setTempData('success', 'ACITIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function week5()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 4;
        $data['page_title'] = 'Week 5- Live Session - Thrivepad';
        $data['live_session'] = 5;

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        $data['dq'] = $this->ToolsModel->fetchdesire($userData['id']);
        $data['discover'] = $this->ToolsModel->fetchdiscover($userData['id']);
        $data['npactivity'] = $this->ToolsModel->fetchnpactivity($userData['id']);
        $data['ncode'] = $this->ToolsModel->fetchncode($userData['id']);
        $data['ncodevalue'] = $this->ToolsModel->fetchncodevalue();
        $data['discusspcode'] = $this->ToolsModel->fetchdispcode($userData['id']);


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week5', $data);
        echo view('theme/profile_footer', $data);
    }

    public function desirequestion()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $dq = $this->request->getVar('dq');

            $rules = [
                'question' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $dq = implode("~", $dq);

                $question = [
                    'question' => $dq,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insdesires($question)) {
                    $this->session->setTempData('success', 'DESIRES QUESTIONS SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function desireyear()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        $userid = $userData['id'];

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'year' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $y = $this->request->getVar('y');
                $y = implode("~", $y);

                $question = [
                    'year' => $y,
                ];


                if ($this->ToolsModel->upddesires($question, $userid)) {
                    $this->session->setTempData('success', 'DESIRES YEAR SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function discover()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $discovered = $this->request->getVar('discovered');
            $life = $this->request->getVar('life');

            $rules = [
                'discovered' => 'required|max_length[500]',
                'life' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $discoveranswer = [
                    'discovered' => $discovered,
                    'life' => $life,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insdiscover($discoveranswer)) {
                    $this->session->setTempData('success', 'DISCOVER SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function nandpactivity()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $nactivity = $this->request->getVar('nactivity');
            $pactivity = $this->request->getVar('pactivity');

            $rules = [
                'nactivity' => 'required|max_length[500]',
                'pactivity' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $npactivity = [
                    'nactivity' => $nactivity,
                    'pactivity' => $pactivity,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insactivity($npactivity)) {
                    $this->session->setTempData('success', 'N and P ACTIVITY SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function discusspcode()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $rules = [
                's1' => 'required|max_length[500]',
                's2' => 'required|max_length[500]',
                's3' => 'required|max_length[500]',
                's4' => 'required|max_length[500]',
                's5' => 'required|max_length[500]',
                's6' => 'required|max_length[500]',
                's7' => 'required|max_length[500]',
                's8' => 'required|max_length[500]',
            ];


            $pcode_id = $this->request->getVar('pcode_id');
            $s1 = $this->request->getVar('s1');
            $s2 = $this->request->getVar('s2');
            $s3 = $this->request->getVar('s3');
            $s4 = $this->request->getVar('s4');
            $s5 = $this->request->getVar('s5');
            $s6 = $this->request->getVar('s6');
            $s7 = $this->request->getVar('s7');
            $s8 = $this->request->getVar('s8');

            $pcode_id = implode("~", $pcode_id);
            $s1 = implode("~", $s1);
            $s2 = implode("~", $s2);
            $s3 = implode("~", $s3);
            $s4 = implode("~", $s4);
            $s5 = implode("~", $s5);
            $s6 = implode("~", $s6);
            $s7 = implode("~", $s7);
            $s8 = implode("~", $s8);

            if ($this->validate($rules)) {
                $pcode = [
                    'pcode_id' => $pcode_id,
                    's1' => $s1,
                    's2' => $s2,
                    's3' => $s3,
                    's4' => $s4,
                    's5' => $s5,
                    's6' => $s6,
                    's7' => $s7,
                    's8' => $s8,
                    'user_id' => $userData['id']
                ];


                if ($this->ToolsModel->inspcode($pcode)) {
                    $this->session->setTempData('success', 'P-CODE SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week5'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function generatepcode()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $currentopt = $this->request->getVar('currentopt');
            $statement = $this->request->getVar('statement');


            $rules = [
                'currentopt' => 'required|max_length[500]',
                'statement' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {

                $genpcode = [
                    'currentopt' => $currentopt,
                    'statement' => $statement,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insgenpcode($genpcode)) {
                    $this->session->setTempData('success', 'GENERATE P-CODES SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function discussncode()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $ncode_id = $this->request->getVar('ncode_id');
            $s1 = $this->request->getVar('s1');
            $s2 = $this->request->getVar('s2');
            $s3 = $this->request->getVar('s3');
            $s4 = $this->request->getVar('s4');
            $s5 = $this->request->getVar('s5');
            $s6 = $this->request->getVar('s6');
            $s7 = $this->request->getVar('s7');
            $s8 = $this->request->getVar('s8');

            $ncode_id = implode("~", $ncode_id);
            $s1 = implode("~", $s1);
            $s2 = implode("~", $s2);
            $s3 = implode("~", $s3);
            $s4 = implode("~", $s4);
            $s5 = implode("~", $s5);
            $s6 = implode("~", $s6);
            $s7 = implode("~", $s7);
            $s8 = implode("~", $s8);


            $rules = [
                's1' => 'required|max_length[500]',
                's2' => 'required|max_length[500]',
                's3' => 'required|max_length[500]',
                's4' => 'required|max_length[500]',
                's5' => 'required|max_length[500]',
                's6' => 'required|max_length[500]',
                's7' => 'required|max_length[500]',
                's8' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $ncode = [
                    'ncode_id' => $ncode_id,
                    's1' => $s1,
                    's2' => $s2,
                    's3' => $s3,
                    's4' => $s4,
                    's5' => $s5,
                    's6' => $s6,
                    's7' => $s7,
                    's8' => $s8,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insncode($ncode)) {
                    $this->session->setTempData('success', 'N_CODES SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function insertncodechart()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $emotion = $this->request->getVar('emotion');

            $emotion = implode("~", $emotion);

            $rules = [
                'emotion' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $emotioncode = [
                    'emotion' => $emotion,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insncodechart($emotioncode)) {
                    $this->session->setTempData('success', 'N_CODES SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week4'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function week6()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 4;
        $data['page_title'] = 'Week 6- Live Session - Thrivepad';
        $data['live_session'] = 6;

        $data['challenge'] = $this->ToolsModel->fetchchallengeover($userData['id']);
        $data['success'] = $this->ToolsModel->fetchsuccessalign($userData['id']);

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week6', $data);
        echo view('theme/profile_footer', $data);
    }

    public function challengeovercome()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $desire = $this->request->getVar('desire');
            $baseval = $this->request->getVar('baseval');
            $achieve = $this->request->getVar('achieve');
            $challenges = $this->request->getVar('challenges');
            $pcodes = $this->request->getVar('pcodes');
            $brainstorm = $this->request->getVar('brainstorm');

            $achieve = implode("~", $achieve);
            $challenges = implode("~", $challenges);
            $pcodes = implode("~", $pcodes);
            $brainstorm = implode("~", $brainstorm);


            $rules = [
                'desire' => 'required|max_length[500]',
                'baseval' => 'required|max_length[500]',
                'achieve' => 'required|max_length[500]',
                'challenges' => 'required|max_length[500]',
                'pcodes' => 'required|max_length[500]',
                'brainstorm' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $challenge = [
                    'desire' => $desire,
                    'baseval' => $baseval,
                    'achieve' => $achieve,
                    'challenges' => $challenges,
                    'pcodes' => $pcodes,
                    'brainstorm' => $brainstorm,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->inschallengeover($challenge)) {
                    $this->session->setTempData('success', 'CHALLENGE OVERCOME SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week6'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week6'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function successalign()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $achieved = $this->request->getVar('achieved');
            $skills = $this->request->getVar('skills');
            $successachieve = $this->request->getVar('successachieve');
            $successdate = $this->request->getVar('successdate');


            $rules = [
                'achieved' => 'required|max_length[500]',
                'skills' => 'required|max_length[500]',
                'successachieve' => 'required|max_length[500]',
                'successdate' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $success = [
                    'achieved' => $achieved,
                    'skills' => $skills,
                    'successachieve' => $successachieve,
                    'successdate' => $successdate,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->inssucessalign($success)) {
                    $this->session->setTempData('success', 'SUCCESS ALIGN SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week6'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week6'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function week7()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 4;
        $data['page_title'] = 'Week 7- Live Session - Thrivepad';
        $data['live_session'] = 7;

        $data['outcome'] = $this->ToolsModel->fetchout($userData['id']);
        $data['decision'] = $this->ToolsModel->fetchdecision($userData['id']);
        $data['smart'] = $this->ToolsModel->fetchsmartgoals($userData['id']);

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];

        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week7', $data);
        echo view('theme/profile_footer', $data);
    }


    public function decision()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $decision = $this->request->getVar('decision');

            $rules = [
                'decision' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $decision = [
                    'decision' => $decision,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insdecision($decision)) {
                    $this->session->setTempData('success', 'DECISION SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week7'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week7'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function smartgoals()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $potential_priority = $this->request->getVar('potential_priority');
            $taken = $this->request->getVar('taken');
            $response = $this->request->getVar('response');
            $start_date = $this->request->getVar('start_date');
            $end_date = $this->request->getVar('end_date');
            $resource = $this->request->getVar('resource');
            $challenges = $this->request->getVar('challenges');
            $outcome = $this->request->getVar('outcome');

            $taken1 = $this->request->getVar('taken1');
            $response1 = $this->request->getVar('response1');
            $start_date1 = $this->request->getVar('start_date1');
            $end_date1 = $this->request->getVar('end_date1');
            $resource1 = $this->request->getVar('resource1');
            $challenges1 = $this->request->getVar('challenges1');
            $outcome1 = $this->request->getVar('outcome1');

            $taken2 = $this->request->getVar('taken2');
            $response2 = $this->request->getVar('response2');
            $start_date2 = $this->request->getVar('start_date2');
            $end_date2 = $this->request->getVar('end_date2');
            $resource2 = $this->request->getVar('resource2');
            $challenges2 = $this->request->getVar('challenges2');
            $outcome2 = $this->request->getVar('outcome2');

            $rules = [
                'potential_priority' => 'required|max_length[500]',
                'taken' => 'required|max_length[500]',
                'response' => 'required|max_length[500]',
                'start_date' => 'required|max_length[500]',
                'end_date' => 'required|max_length[500]',
                'resource' => 'required|max_length[500]',
                'challenges' => 'required|max_length[500]',
                'outcome' => 'required|max_length[500]',
                'taken1' => 'required|max_length[500]',
                'response1' => 'required|max_length[500]',
                'start_date1' => 'required|max_length[500]',
                'end_date1' => 'required|max_length[500]',
                'resource1' => 'required|max_length[500]',
                'challenges1' => 'required|max_length[500]',
                'outcome1' => 'required|max_length[500]',
                'taken2' => 'required|max_length[500]',
                'response2' => 'required|max_length[500]',
                'start_date2' => 'required|max_length[500]',
                'end_Date2' => 'required|max_length[500]',
                'resource2' => 'required|max_length[500]',
                'challenges2' => 'required|max_length[500]',
                'outcome2' => 'required|max_length[500]',
            ];

            $potential_priority = implode("~", $potential_priority);
            $taken = implode("~", $taken);
            $response = implode("~", $response);
            $start_date = implode("~", $start_date);
            $end_date = implode("~", $end_date);
            $resource = implode("~", $resource);
            $challenges = implode("~", $challenges);
            $outcome = implode("~", $outcome);

            $taken1 = implode("~", $taken1);
            $response1 = implode("~", $response1);
            $start_date1 = implode("~", $start_date1);
            $end_date1 = implode("~", $end_date1);
            $resource1 = implode("~", $resource1);
            $challenges1 = implode("~", $challenges1);
            $outcome1 = implode("~", $outcome1);

            $taken2 = implode("~", $taken2);
            $response2 = implode("~", $response2);
            $start_date2 = implode("~", $start_date2);
            $end_date2 = implode("~", $end_date2);
            $resource2 = implode("~", $resource2);
            $challenges2 = implode("~", $challenges2);
            $outcome2 = implode("~", $outcome2);

            if ($this->validate($rules)) {
                $smart = [
                    'desires' => $potential_priority,
                    'taken' => $taken,
                    'response' => $response,
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'resource' => $resource,
                    'challenges' => $challenges,
                    'outcome' => $outcome,
                    'taken1' => $taken1,
                    'response1' => $response1,
                    'start_date1' => $start_date1,
                    'end_date1' => $end_date1,
                    'resource1' => $resource1,
                    'challenges1' => $challenges1,
                    'outcome1' => $outcome1,
                    'taken2' => $taken2,
                    'response2' => $response2,
                    'start_date2' => $start_date2,
                    'end_date2' => $end_date2,
                    'resource2' => $resource2,
                    'challenges2' => $challenges2,
                    'outcome2' => $outcome2,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->inssmartgoals($smart)) {
                    $this->session->setTempData('success', 'SMART GOALS SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week7'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week7'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function week8()
    {
        $userData = $this->session->get('logged_user');
        $data['page_no'] = 4;
        $data['page_title'] = 'Week 8- Live Session - Thrivepad';
        $data['live_session'] = 8;

        $data['scenario1'] = $this->ToolsModel->fetchscenarioone($userData['id']);
        $data['scenario2'] = $this->ToolsModel->fetchscenariotwo($userData['id']);
        $data['scenario3'] = $this->ToolsModel->fetchscenariothree($userData['id']);
        $data['master'] = $this->ToolsModel->fetchmastermind($userData['id']);
        $data['toxic'] = $this->ToolsModel->fetchtoxic($userData['id']);

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week8', $data);
        echo view('theme/profile_footer', $data);
    }


    public function scenarioone()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $scenarios1 = $this->request->getVar('scenarios1');


            $rules = [
                'scenarios1' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $scenarios = [
                    'scenarios' => $scenarios1,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insscenarioone($scenarios)) {
                    $this->session->setTempData('success', 'SCENARIOS SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function scenariotwo()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $scenarios2 = $this->request->getVar('scenarios2');


            $rules = [
                'scenarios2' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $scenarios = [
                    'scenarios' => $scenarios2,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insscenariotwo($scenarios)) {
                    $this->session->setTempData('success', 'SCENARIOS SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function scenariothree()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $q1 = $this->request->getVar('q1');
            $q2 = $this->request->getVar('q2');
            $q3 = $this->request->getVar('q3');
            $q4 = $this->request->getVar('q4');
            $q5 = $this->request->getVar('q5');
            $q6 = $this->request->getVar('q6');
            $q7 = $this->request->getVar('q7');
            $q8 = $this->request->getVar('q8');
            $q9 = $this->request->getVar('q9');

            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
                'q7' => 'required|max_length[500]',
                'q8' => 'required|max_length[500]',
                'q9' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $scenarios = [
                    'q1' =>  $q1,
                    'q2' =>  $q2,
                    'q3' =>  $q3,
                    'q4' =>  $q4,
                    'q5' =>  $q5,
                    'q6' =>  $q6,
                    'q7' =>  $q7,
                    'q8' =>  $q8,
                    'q9' =>  $q9,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insscenariothree($scenarios)) {
                    $this->session->setTempData('success', 'SCENARIOS SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function mastermind()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $q1 = $this->request->getVar('q1');
            $q2 = $this->request->getVar('q2');
            $q3 = $this->request->getVar('q3');
            $q4 = $this->request->getVar('q4');
            $q5 = $this->request->getVar('q5');
            $q6 = $this->request->getVar('q6');
            $q7 = $this->request->getVar('q7');
            $q8 = $this->request->getVar('q8');
            $q9 = $this->request->getVar('q9');
            $q10 = $this->request->getVar('q10');
            $q11 = $this->request->getVar('q11');

            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
                'q7' => 'required|max_length[500]',
                'q8' => 'required|max_length[500]',
                'q9' => 'required|max_length[500]',
                'q10' => 'required|max_length[500]',
                'q11' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $master = [
                    'q1' =>  $q1,
                    'q2' =>  $q2,
                    'q3' =>  $q3,
                    'q4' =>  $q4,
                    'q5' =>  $q5,
                    'q6' =>  $q6,
                    'q7' =>  $q7,
                    'q8' =>  $q8,
                    'q9' =>  $q9,
                    'q10' =>  $q10,
                    'q11' =>  $q11,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insmastermind($master)) {
                    $this->session->setTempData('success', 'MASTERMIND SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function toxic()
    {
        $userData = $this->session->get('logged_user');
        $data = [];
        $data['validation'] = null;
        if ($this->request->getMethod() == 'post') {

            $q1 = $this->request->getVar('q1');
            $q2 = $this->request->getVar('q2');
            $q3 = $this->request->getVar('q3');
            $q4 = $this->request->getVar('q4');

            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $toxic = [
                    'q1' =>  $q1,
                    'q2' =>  $q2,
                    'q3' =>  $q3,
                    'q4' =>  $q4,

                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->instoxic($toxic)) {
                    $this->session->setTempData('success', 'TOXIC SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week8'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function week9()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 9- Live Session - Thrivepad';
        $data['live_session'] = 9;

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];

        $data['happy'] = $this->ToolsModel->fetchmyhappy($userData['id']);
        $data['vision'] = $this->ToolsModel->fetchvision($userData['id']);

        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week9', $data);
        echo view('theme/profile_footer', $data);
    }

    public function week10()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 10 Live Session - Thrivepad';
        $data['live_session'] = 10;

        $data['blueprint'] = $this->ToolsModel->fetchblueprint($userData['id']);

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week10', $data);
        echo view('theme/profile_footer', $data);
    }
    public function week11()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 11 Live Session - Thrivepad';
        $data['live_session'] = 11;

        $data['blueprint'] = $this->ToolsModel->fetchblueprint($userData['id']);

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week11', $data);
        echo view('theme/profile_footer', $data);
    }
    public function week12()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 4;
        $data['page_title'] = 'Week 12 Live Session - Thrivepad';
        $data['live_session'] = 12;

        $data['blueprint'] = $this->ToolsModel->fetchblueprint($userData['id']);

        $data['ses_co'] = $this->ToolsModel->users_livesession($userData['id']);
        $data['count'] = $data['ses_co'][0]['session_count'];
        $data['link'] = $data['ses_co'][0]['next_session_link'];


        echo view('theme/profile_header', $data);
        echo view('livesessions/live-session-header', $data);
        echo view('livesessions/week12', $data);
        echo view('theme/profile_footer', $data);
    }
    public function myhabit()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 11;
        $data['page_title'] = 'My Habit';

        $data['habit'] = $this->ToolsModel->fetchhabit($userData['id']);

        echo view('theme/profile_header', $data);
        echo view('livesessions/myhabit', $data);
        echo view('theme/profile_footer', $data);
    }
    public function insertmyhappy()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $q1 = $this->request->getVar('q1');
            $q2 = $this->request->getVar('q2');
            $q3 = $this->request->getVar('q3');
            $q4 = $this->request->getVar('q4');
            $q5 = $this->request->getVar('q5');
            $q6 = $this->request->getVar('q6');
            $q7 = $this->request->getVar('q7');
            $q8 = $this->request->getVar('q8');
            $q9 = $this->request->getVar('q9');

            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
                'q7' => 'required|max_length[500]',
                'q8' => 'required|max_length[500]',
                'q9' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $scenarios = [
                    'q1' =>  $q1,
                    'q2' =>  $q2,
                    'q3' =>  $q3,
                    'q4' =>  $q4,
                    'q5' =>  $q5,
                    'q6' =>  $q6,
                    'q7' =>  $q7,
                    'q8' =>  $q8,
                    'q9' =>  $q9,
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insmyhappy($scenarios)) {
                    $this->session->setTempData('success', 'HAPPINESS BOOSTER SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week9'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week9'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function inserthabit()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'habit' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'habit' => $this->request->getVar('habit'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->inshabit($data)) {
                    $this->session->setTempData('success', 'HABIT SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/myhabit'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/myhabit'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function insertaccomplishment()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'accomplishment' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {

                $data = [
                    'accomplishment' => $this->request->getVar('accomplishment'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insaccomplishment($data)) {
                    $this->session->setTempData('success', 'ACCOMPLISHMENT SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/accomplishment'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/accomplishment'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function insertbreakthrough()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'breakthrough' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $data = [
                    'breakthrough' => $this->request->getVar('breakthrough'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insbreakthrough($data)) {
                    $this->session->setTempData('success', 'BREAKTHROUGH SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/breakthrough'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/breakthrough'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function accomplishment()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 12;
        $data['page_title'] = 'Accomplishment';

        $data['accomplishment'] = $this->ToolsModel->fetchaccomplishment($userData['id']);

        echo view('theme/profile_header', $data);
        echo view('livesessions/accomplishment', $data);
        echo view('theme/profile_footer', $data);
    }
    public function breakthrough()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 13;
        $data['page_title'] = 'Breakthrough';

        $data['breakthrough'] = $this->ToolsModel->fetchbreakthrough($userData['id']);

        echo view('theme/profile_header', $data);
        echo view('livesessions/breakthrough', $data);
        echo view('theme/profile_footer', $data);
    }

    public function blueprint()
    {
        $userData = $this->session->get('logged_user');
        $userid = $userData['id'];

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $level = $this->request->getVar('level');


            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
                'q7' => 'required|max_length[500]',
                'q8' => 'required|max_length[500]',
                'q9' => 'required|max_length[500]',
                'q10' => 'required|max_length[500]',
                'q11' => 'required|max_length[500]',
                'q12' => 'required|max_length[500]',
                'q13' => 'required|max_length[500]',
                'q14' => 'required|max_length[500]',
            ];

            $q1 = $this->request->getVar('q1');
            $q2 = $this->request->getVar('q2');
            $q3 = $this->request->getVar('q3');
            $q4 = $this->request->getVar('q4');
            $q5 = $this->request->getVar('q5');
            $q6 = $this->request->getVar('q6');
            $q7 = $this->request->getVar('q7');
            $q8 = $this->request->getVar('q8');
            $q9 = $this->request->getVar('q9');
            $q10 = $this->request->getVar('q10');
            $q11 = $this->request->getVar('q11');
            $q12 = $this->request->getVar('q12');
            $q13 = $this->request->getVar('q13');
            $q14 = $this->request->getVar('q14');
            if ($this->validate($rules)) {

                $scenarios = [
                    'q1' =>  $q1,
                    'q2' =>  $q2,
                    'q3' =>  $q3,
                    'q4' =>  $q4,
                    'q5' =>  $q5,
                    'q6' =>  $q6,
                    'q7' =>  $q7,
                    'q8' =>  $q8,
                    'q9' =>  $q9,
                    'q10' =>  $q10,
                    'q11' =>  $q11,
                    'q12' =>  $q12,
                    'q13' =>  $q13,
                    'q14' =>  $q14,
                    'user_id' => $userid
                ];

                if ($this->ToolsModel->insblueprint($scenarios, $level)) {
                    $this->session->setTempData('success', 'BLUEPRINT SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week10'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week10'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
    public function visionboard()
    {

        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'vision_img' => 'required|max_length[500]',
            ];

            if ($this->validate($rules)) {
                $file = $this->request->getFile('vision_img');

                $data['user_id'] = $userData['id'];

                if ($file &&  $file->isValid() && !$file->hasMoved()) {
                    $newName = $file->getRandomName();
                    if ($file->move(FCPATH . 'public/uploads/week9', $newName)) {
                        $data['vision_img'] = $newName;
                    }
                }
                if ($this->ToolsModel->updatevision($data)) {
                    $this->session->setTempData('success', 'VISION BOARD SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week9'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week9'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function questionnaire()
    {
        $userData = $this->session->get('logged_user');

        $data = [];
        $data['validation'] = null;

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'q1' => 'required|max_length[500]',
                'q2' => 'required|max_length[500]',
                'q3' => 'required|max_length[500]',
                'q4' => 'required|max_length[500]',
                'q5' => 'required|max_length[500]',
                'q6' => 'required|max_length[500]',
            ];
            if ($this->validate($rules)) {

                $data = [
                    'q1' =>  $this->request->getVar('q1'),
                    'q2' =>  $this->request->getVar('q2'),
                    'q3' =>  $this->request->getVar('q3'),
                    'q4' =>  $this->request->getVar('q4'),
                    'q5' =>  $this->request->getVar('q5'),
                    'q6' =>  $this->request->getVar('q6'),
                    'user_id' => $userData['id']
                ];

                if ($this->ToolsModel->insert_questionnaire($data)) {
                    $this->session->setTempData('success', 'QUESTIONNAIRE SUBMITTED SUCCESSFULL', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                    return redirect()->to(base_url('live-sessions/week1'));
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }
}