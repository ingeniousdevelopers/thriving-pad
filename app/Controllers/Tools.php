<?php

namespace App\Controllers;

use App\Models\ToolsModel;


class Tools extends BaseController
{
    public $session;
    public $toolsModel;
    public function __construct()
    {
        helper('form');
        helper('url');
        $this->session = session();
        $this->toolsModel = new ToolsModel();
    }

    public function baselinevalues()
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Baseline Values - Thrivepad';
        $data['userData'] = $userData;

        $data['basevalues'] = $this->toolsModel->fetchbaselinevalue();
        $data['baselinevalue'] = $this->toolsModel->fetchuserbasevalue($userData['id']);

        if ($data['baselinevalue'][0]['filter'] == 1) {
            return redirect()->to(base_url() . '/tools/resultbasevalue');
            $this->resultbasevalue();
        }
        echo view('tools/theme/tools_header', $data);
        echo view('tools/baselinevalues', $data);
    }

    public function baselinevalues_getresult()
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Baseline Values - Thrivepad';
        $data['userData'] = $userData;

        if ($this->request->getMethod() == 'POST') {
            $rules = [
                'batchtype' => 'required',
                'day' => 'required',
                'time'  => 'required',
            ];
            if ($this->validate($rules)) {

                $data  = [
                    'batch_type' => $this->request->getVar('batchtype'),
                    'day' => $this->request->getVar('day'),
                    'time' => $this->request->getVar('time'),
                    'username' => $userData['username'],
                ];

                if ($this->registerModel->createbaselinevalues($data)) {

                    $this->session->setTempData('success', 'Account Created Successfully', 3);
                    return redirect()->to(base_url('login'));
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to create an account, Try again', 3);
                    return redirect()->to(current_url());
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
        echo view('tools/theme/tools_header', $data);
        echo view('tools/baselinevalues', $data);
    }

    public function wheeloflife()
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Wheel Of Life - Thrivepad';
        $data['userData'] = $userData;

        $result = $this->toolsModel->fetchWheeloflife($userData['id']);

        $data['result'] = $result[0];

        if ($data['result']) {
            echo view('tools/theme/tools_header', $data);
            echo view('tools/wheelofliferesult', $data);
        } else {
            echo view('tools/theme/tools_header', $data);
            echo view('tools/wheeloflife', $data);
        }
    }



    public function wheeloflife_create()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 6;
        $data['page_title'] = 'Wheel Of Life - Thrivepad';
        $data['userData'] = $userData;
        if ($this->request->getMethod() == 'post') {
            $rules = [
                'col1' => 'required',
                'col2' => 'required',
                'col3'  => 'required',
                'col4'  => 'required',
                'col5'  => 'required',
                'col6'  => 'required',
                'col7'  => 'required',
                'col8'  => 'required',
            ];
            if ($this->validate($rules)) {

                $data  = [
                    'user_id' => $userData['id'],
                    'col1' => $this->request->getVar('col1'),
                    'col2' => $this->request->getVar('col2'),
                    'col3' => $this->request->getVar('col3'),
                    'col4' => $this->request->getVar('col4'),
                    'col5' => $this->request->getVar('col5'),
                    'col6' => $this->request->getVar('col6'),
                    'col7' => $this->request->getVar('col7'),
                    'col8' => $this->request->getVar('col8'),
                ];

                if ($this->toolsModel->createWheeloflife($data)) {

                    $data['result'] = $this->toolsModel->fetchWheeloflife($userData['id']);

                    if ($data['result']) {
                        echo view('tools/theme/tools_header', $data);
                        echo view('tools/wheelofliferesult', $data);
                    } else {
                        $this->session->setTempData('error', 'Sorry! Unable to Fetch The Data, Try again', 3);
                        return redirect()->to(current_url());
                    }
                } else {
                    $this->session->setTempData('error', 'Sorry! Unable to Insert the data, Try again', 3);
                    return redirect()->to(base_url());
                }
            } else {
                $data['validation'] = $this->validator;
            }
        }
    }

    public function outcomereinfrocement()
    {
        $userData = $this->session->get('logged_user');

        $data['page_title'] = 'Outcome Reinforcement - Thrivepad';
        $data['userData'] = $userData;

        $result = $this->toolsModel->fetchoutcome($userData['id']);
        $data['out'] = $this->toolsModel->fetchoutcome($userData['id']);
        $data['dq'] = $this->toolsModel->fetchdesire($userData['id']);

        $data['result'] = $result[0];

        if ($data['result']) {
            echo view('tools/theme/tools_header', $data);
            echo view('tools/outcome', $data);
        } else {
            echo view('tools/theme/tools_header', $data);
            echo view('tools/outcome', $data);
        }
    }

    public function outcomereinfrocement_create()
    {
        $userData = $this->session->get('logged_user');

        $data['page_no'] = 6;
        $data['page_title'] = 'Outcome Reinforcement - Thrivepad';
        $data['userData'] = $userData;
        if ($this->request->getMethod() == 'post') {
            $rules = [
                'potential_priority1' => 'required',
                'predict_ranking1' => 'required',
                'time_upfront1'  => 'required',
                'time_ongoing1'  => 'required',
                'difficulty1'  => 'required',
                'enjoyment1'  => 'required',
                'impact_90_day1'  => 'required',
                'impact_25_yr1'  => 'required',
                'total_score1'  => 'required',
                'potential_priority2' => 'required',
                'predict_ranking2' => 'required',
                'time_upfront2'  => 'required',
                'time_ongoing2'  => 'required',
                'difficulty2'  => 'required',
                'enjoyment2'  => 'required',
                'impact_90_day2'  => 'required',
                'impact_25_yr2'  => 'required',
                'total_score2'  => 'required',
                'potential_priority3' => 'required',
                'predict_ranking3' => 'required',
                'time_upfront3'  => 'required',
                'time_ongoing3'  => 'required',
                'difficulty3'  => 'required',
                'enjoyment3'  => 'required',
                'impact_90_day3'  => 'required',
                'impact_25_yr3'  => 'required',
                'total_score3'  => 'required',
            ];
            if ($this->validate($rules)) {

                $data  = [
                    'user_id' => $userData['id'],
                    'potential_priority1' => $this->request->getVar('potential_priority1'),
                    'predict_ranking1' => $this->request->getVar('predict_ranking1'),
                    'time_upfront1'  => $this->request->getVar('time_upfront1'),
                    'time_ongoing1'  => $this->request->getVar('time_ongoing1'),
                    'difficulty1'  => $this->request->getVar('difficulty1'),
                    'enjoyment1'  => $this->request->getVar('enjoyment1'),
                    'impact_90_day1'  => $this->request->getVar('impact_90_day1'),
                    'impact_25_yr1'  => $this->request->getVar('impact_25_yr1'),
                    'total_score1'  => $this->request->getVar('total_score1'),
                    'potential_priority2' => $this->request->getVar('potential_priority2'),
                    'predict_ranking2' => $this->request->getVar('predict_ranking2'),
                    'time_upfront2'  => $this->request->getVar('time_upfront2'),
                    'time_ongoing2'  => $this->request->getVar('time_ongoing2'),
                    'difficulty2'  => $this->request->getVar('difficulty2'),
                    'enjoyment2'  => $this->request->getVar('enjoyment2'),
                    'impact_90_day2'  => $this->request->getVar('impact_90_day2'),
                    'impact_25_yr2'  => $this->request->getVar('impact_25_yr2'),
                    'total_score2'  => $this->request->getVar('total_score2'),
                    'potential_priority3' => $this->request->getVar('potential_priority3'),
                    'predict_ranking3' => $this->request->getVar('predict_ranking3'),
                    'time_upfront3'  => $this->request->getVar('time_upfront3'),
                    'time_ongoing3'  => $this->request->getVar('time_ongoing3'),
                    'difficulty3'  => $this->request->getVar('difficulty3'),
                    'enjoyment3'  => $this->request->getVar('enjoyment3'),
                    'impact_90_day3'  => $this->request->getVar('impact_90_day3'),
                    'impact_25_yr3'  => $this->request->getVar('impact_25_yr3'),
                    'total_score3'  => $this->request->getVar('total_score3'),
                ];


                if ($this->toolsModel->createoutcome($data)) {

                    $data['result'] = $this->toolsModel->fetchoutcome($userData['id']);

                    if ($data['result']) {
                        $data['result']['status'] = 1;
                        $num1 = $data['total_score1'];
                        $num2 = $data['total_score2'];
                        $num3 = $data['total_score3'];

                        if ($num1 > $num2 && $num1 > $num3) {
                            $data['result']['high_value'] = $data['potential_priority1'];
                        } elseif ($num2 > $num1 && $num2 > $num3) {
                            $data['result']['high_value'] = $data['potential_priority2'];
                        } elseif ($num3 > $num1 && $num3 > $num2) {
                            $data['result']['high_value'] = $data['potential_priority3'];
                        } else {
                            if ($num2 == $num1) {
                                $data['result']['high_value'] = $data['potential_priority1'];
                            } else {
                                if ($num3 == $num2) {
                                    $data['result']['high_value'] = $data['potential_priority2'];
                                } else {
                                    $data['result']['high_value'] = $data['potential_priority3'];
                                }
                            }
                        }
                        echo json_encode($data['result']);
                    }
                }
            } else {
                $data['result']['status'] = 3;
            }
        }
    }

    public function ncodes()
    {
        $userData = $this->session->get('logged_user');


        $data['page_title'] = 'N Codes & P Codes - Thrivepad';
        $data['userData'] = $userData;

        $data['ncodevalue'] = $this->toolsModel->fetchncodevalue();
        $data['ncode'] = $this->toolsModel->fetchncode($userData['id']);

        if ($data['ncode'][0]['filter'] == 1) {
            return redirect()->to(base_url() . '/tools/resultncode');
            $this->resultncode();
        }
        echo view('tools/theme/tools_header', $data);
        echo view('tools/ncodes', $data);
    }

    public function resultncode()
    {
        $userData = $this->session->get('logged_user');
        $data['userData'] = $userData;

        $data['ncodevalue'] = $this->toolsModel->fetchncodevalue();
        $data['ncode'] = $this->toolsModel->fetchncode($userData['id']);

        echo view('tools/theme/tools_header', $data);
        echo view('tools/resultncode', $data);
    }
    public function insertncode()
    {
        $userData = $this->session->get('logged_user');
        if ($this->request->getMethod() == 'post') {

            $sentence = $this->request->getVar('sentence');

            $sentence = implode("~", $sentence);

            $n_code = [
                'array_ncode' => $sentence,
                'user_id' => $userData['id'],
                'status' => $this->request->getVar('status'),
                'play_count' => $this->request->getVar('level'),
                'filter' => $this->request->getVar('filter'),
            ];

            if ($this->toolsModel->insncodes($n_code)) {
                $this->session->setTempData('success', 'NCode SUBMITTED SUCCESSFULL', 3);
                return redirect()->to(base_url('tools/ncodes'));
            } else {
                $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                return redirect()->to(base_url('tools/ncodes'));
            }
        }
    }
    public function updatencode()
    {
        $userData = $this->session->get('logged_user');
        $userid = $userData['id'];

        if ($this->request->getMethod() == 'post') {

            $sentence = $this->request->getVar('sentence');

            $sentence = implode("~", $sentence);

            $n_code = [
                'array_ncode' => $sentence,
            ];


            if ($this->toolsModel->updncodes($userid, $n_code)) {
                $this->session->setTempData('success', 'NCode SUBMITTED SUCCESSFULL', 3);
                return redirect()->to(base_url('tools/ncodes'));
            } else {
                $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                return redirect()->to(base_url('tools/ncodes'));
            }
        }
    }

    public function resultbasevalue()
    {
        $userData = $this->session->get('logged_user');
        $data['userData'] = $userData;

        $data['basevalues'] = $this->toolsModel->fetchbaselinevalue();
        $data['baselinevalue'] = $this->toolsModel->fetchuserbasevalue($userData['id']);


        echo view('tools/theme/tools_header', $data);
        echo view('tools/resultbasevalue', $data);
    }
    public function insbasevalue()
    {
        $userData = $this->session->get('logged_user');
        if ($this->request->getMethod() == 'post') {

            $va_words = $this->request->getVar('va_words');

            $va_words = implode("~", $va_words);
            print_r($va_words);
            $arr_baseval = [
                'array_baselinevalues' => $va_words,
                'user_id' => $userData['id'],
                'status' => $this->request->getVar('status'),
                'play_count' => $this->request->getVar('level'),
                'filter' => $this->request->getVar('filter'),
            ];

            if ($this->toolsModel->insertbaseval($arr_baseval)) {
                $this->session->setTempData('success', 'BASEVALUE SUBMITTED SUCCESSFULL', 3);
                return redirect()->to(base_url('tools/baselinevalues'));
            } else {
                $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                return redirect()->to(base_url('tools/baselinevalues'));
            }
        }
    }

    public function updbasvalue()
    {
        $userData = $this->session->get('logged_user');
        $userid = $userData['id'];

        if ($this->request->getMethod() == 'post') {

            $va_words = $this->request->getVar('va_words');

            $va_words = implode("~", $va_words);

            $arr_baseval = [
                'array_baselinevalues' => $va_words,
            ];


            if ($this->toolsModel->updbaseval($userid, $arr_baseval)) {
                $this->session->setTempData('success', 'BASEVALUE SUBMITTED SUCCESSFULL', 3);
                return redirect()->to(base_url('tools/baselinevalues'));
            } else {
                $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                return redirect()->to(base_url('tools/baselinevalues'));
            }
        }
    }
    public function decisionwheel()
    {
        $userData = $this->session->get('logged_user');
        $data['userData'] = $userData;

        $data['page_title'] = 'Decision Wheel - Thrivepad';
        $data['wheel'] = $this->toolsModel->fetchdecisionwheel($userData['id']);


        echo view('tools/theme/tools_header', $data);
        echo view('tools/decisionwheel', $data);
    }
    public function decision_create()
    {
        $userData = $this->session->get('logged_user');
        $userid = $userData['id'];

        if ($this->request->getMethod() == 'post') {

            $q1 = $this->request->getVar('qans');
            $q2 = $this->request->getVar('qans');
            $q3 = $this->request->getVar('qans');
            $q4 = $this->request->getVar('qans');
            $q5 = $this->request->getVar('qans');
            $q6 = $this->request->getVar('qans');
            $q7 = $this->request->getVar('qans');
            $q8 = $this->request->getVar('qans');
            $q9 = $this->request->getVar('qans');
            $qid = $this->request->getVar('qid');

            $decision = [
                'q1' => $q1,
                'q2' => $q2,
                'q3' => $q3,
                'q4' => $q4,
                'q5' => $q5,
                'q6' => $q6,
                'q7' => $q7,
                'q8' => $q8,
                'q9' => $q9,
                'user_id' => $userid,
            ];

            if ($this->toolsModel->insdecisionwheel($qid, $decision)) {
                $this->session->setTempData('success', 'DECISION WHEEL SUBMITTED SUCCESSFULL', 3);
                return redirect()->to(base_url('tools/decisionwheel'));
            } else {
                $this->session->setTempData('error', 'Sorry! Unable to Submit, Try again', 3);
                return redirect()->to(base_url('tools/decisionwheel'));
            }
        }
    }
    public function redo()
    {
        $userData = $this->session->get('logged_user');
        $id = $userData['id'];
        if ($this->request->getMethod() == 'post') {

            $tab = $this->request->getVar('tab');
            $url = $this->request->getVar('url');

            if ($this->toolsModel->redotable($id, $tab)) {
                $this->session->setTempData('success', 'REDO SUBMITTED SUCCESSFULL', 3);
                return redirect()->to($url);
            } else {
                $this->session->setTempData('error', 'This activity was not attended,we do not take redo', 3);
                return redirect()->to($url);
            }
        }
    }
}