<?php
function display_error($validation, $field){
    if(isset($validation)){
        if($validation->hasError($field)){
            return $validation->getError($field);
        }
        else{
            return false;
        }
    }
}

?>
    
<!-- Contact section start -->
<div class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 align-self-center pad-0">
                <div class="form-section clearfix ">
                   
                    
                    <div class="clearfix"></div>
                    

                   
            <?php if(session()->getTempData('success')):?>
              <div class="alert alert-success"><?= session()->getTempData('success')?></div>
            <?php endif;?>
            <?php if(session()->getTempData('error')):?>
              <div class="alert alert-danger"><?= session()->getTempData('error')?></div>
            <?php endif;?>
            <?=form_open();?>
            <h3>Sign In</h3>
            <!-- <a href="#" class="btn-md btn-theme btn btn-lg btn-block " type="button"><i class="fa fa-google mr-2"></i> Sign in with Google</a>
            <a href="#" class="btn-md btn-theme btn btn-lg btn-block my-2" type="button"><i class="fa fa-facebook mr-2"></i> Sign in with Facebook</a> -->

                       <hr>
                        <div class="form-group form-box">
                            <input type="email" name="Email" value="<?= set_value('Email');?>" class="input-text" placeholder="Email Address">
                            <small class="text-danger"><?= display_error($validation, 'Email')?></small>
                        </div>
                        
                        
                        <div class="form-group form-box clearfix">
                            <input type="password" name="Password" value="<?= set_value('Password');?>" class="input-text" placeholder="Password">
                            <small class="text-danger"><?= display_error($validation, 'Password')?></small>
                        </div>
                        
                       
                        <div class="form-group clearfix mb-0">
                            <button type="submit" class="btn-md btn-theme float-left">Login</button>
                            <a href="forgot-password.html" class="forgot-password">Forgot Password</a>
                        </div>

                       

                        
                    </form>
                </div>
            </div>
           
        </div>
    </div>
</div>
<!-- Contact section end -->

