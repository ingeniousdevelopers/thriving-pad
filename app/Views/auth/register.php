<?php
function display_error($validation, $field){
    if(isset($validation)){
        if($validation->hasError($field)){
            return $validation->getError($field);
        }
        else{
            return false;
        }
    }
}

?>
    
<!-- Contact section start -->
<div class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 align-self-center pad-0">
                <div class="form-section clearfix py-0">
                    <h2>Sign Up</h2>
                    <h6> <b>Create your free account!</b></h6>
                    
                    <div class="clearfix"></div>
                    

                    <?php $page_session = \Config\Services::session();?>
            <?php if($page_session->getTempData('success')):?>
              <div class="alert alert-success"><?= $page_session->getTempData('success')?></div>
            <?php endif;?>
            <?php $page_session = \Config\Services::session();?>
            <?php if($page_session->getTempData('error')):?>
              <div class="alert alert-danger"><?= $page_session->getTempData('error')?></div>
            <?php endif;?>
            <?=form_open();?>
          
                                    
                        <div class="form-group form-box">
                            <input type="text" name="Fullname" value="<?= set_value('Fullname');?>" class="input-text" placeholder="Full Name">
                            <small class="text-danger"><?php display_error($validation,'Fullname') ?></small>
                        </div>
                        <div class="form-group form-box">
                            <input type="email" name="Email" value="<?= set_value('Email');?>" class="input-text" placeholder="Email Address">
                            <small class="text-danger"><?php display_error($validation, 'Email')?></small>
                        </div>
                        
                        
                        <div class="form-group form-box clearfix">
                            <input type="password" name="Password" value="<?= set_value('Password');?>" class="input-text" placeholder="Password">
                            <small class="text-danger"><?php display_error($validation, 'Password')?></small>
                        </div>
                        <div class="form-group form-box clearfix">
                            <input type="password" name="ConfirmPassword" value="<?= set_value('ConfirmPassword');?>" class="input-text" placeholder="Confirm password">
                            <small class="text-danger"><?php display_error($validation, 'ConfirmPassword')?></small>
                        </div>
                        <div class="form-group">
                        <small >By signing up, you indicate that you agree to
the Pronto Terms & Conditions.</small>
                        </div>
                        <div class="form-group clearfix mb-0">
                            <button type="submit" class="btn-md btn-theme float-left">Register</button>
                        </div>
                    </form>
                </div>
            </div>
           
        </div>
    </div>
</div>
<!-- Contact section end -->


<script src="<?= base_url() ?>/public/assets/js/jquery-2.2.0.min.js"></script>

<script  src="<?= base_url() ?>/public/assets/js/app.js"></script>
