<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
}

?>

<!-- Contact section start -->
<div class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 align-self-center pad-0">
                <div class="form-section clearfix py-0">
                <?php $page_session = \Config\Services::session(); ?>
                    <?php if ($page_session->getTempData('success')) : ?>
                        <div class="alert alert-success"><?= $page_session->getTempData('success') ?></div>
                    <?php endif; ?>
                    <?php $page_session = \Config\Services::session(); ?>
                    <?php if ($page_session->getTempData('error')) : ?>
                        <div class="alert alert-danger"><?= $page_session->getTempData('error') ?></div>
                    <?php endif; ?>

                    <h2>Payment for Course : <?php echo $price .' INR' ?></h2>
                    <h6 class="mb-5"> <b>User's Name : <?php echo $username ?> <br>
                    Course Type : <?php echo $batch_type==1?'private':'group'?></b>
                    </h6>

                    <div class="clearfix"></div>
                    <button id="rzp-button1">Pay with Razorpay</button>
                 
                  
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Contact section end -->


<?php if($is_pay){

?>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<form name='razorpayform' action="<?= base_url() ?>/selectslot/verifypay" method="POST">
    <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
    <input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
    <input type="hidden" name="slot_id"  id="slot_id" value=<?php echo $time ?> >
</form>
<script>
// Checkout details as a json
<?php $json_data = json_encode($json); ?>

var options = <?php echo $json_data?>;

/**
 * The entire list of Checkout fields is available at
 * https://docs.razorpay.com/docs/checkout-form#checkout-fields
 */
options.handler = function (response){
    document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
    document.getElementById('razorpay_signature').value = response.razorpay_signature;
    document.razorpayform.submit();
};

// Boolean whether to show image inside a white frame. (default: true)
options.theme.image_padding = false;

options.modal = {
    ondismiss: function() {
        console.log("This code runs when the popup is closed");
    },
    // Boolean indicating whether pressing escape key
    // should close the checkout form. (default: true)
    escape: true,
    // Boolean indicating whether clicking translucent blank
    // space outside checkout form should close the form. (default: false)
    backdropclose: false
};

var rzp = new Razorpay(options);
if(<?php echo $is_pay ?>){
(function(e){
    rzp.open();
    e.preventDefault();
})();

document.getElementById('rzp-button1').onclick = function(e){
    rzp.open();
    e.preventDefault();
}

}
</script>
<?php } ?>