<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
}

?>

<!-- Contact section start -->
<div class="contact-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6 align-self-center pad-0">
                <div class="form-section clearfix py-0">
                    <h2>Select Slot & Batch Type</h2>
                    <h6 class="alert alert-danger"> <b>Make payment after selecting the batch type and
                            slot </b></h6>

                    <div class="clearfix"></div>


                    <?php $page_session = \Config\Services::session(); ?>
                    <?php if ($page_session->getTempData('success')) : ?>
                    <div class="alert alert-success"><?= $page_session->getTempData('success') ?></div>
                    <?php endif; ?>
                    <?php $page_session = \Config\Services::session(); ?>
                    <?php if ($page_session->getTempData('error')) : ?>
                    <div class="alert alert-success"><?= $page_session->getTempData('error') ?></div>
                    <?php endif; ?>
                    <form action="<?= base_url() ?>/selectslot/pay" method="get">
                        <div class="form-group">
                            <select class=" select" name="batchtype" id="batchtype" onchange="choice1(this)">
                                <option value="" disabled selected>Select Batch Type</option>
                                <option value=1>Private</option>
                                <option value=2>Group</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <select class=" select" name="day" id="day" onchange="choice1(this)">
                                <option value="" disabled selected>Select Day</option>
                                <?php foreach ($slots_list as $slot) { ?>
                                <option value=<?php echo $slot['day']; ?>><?php echo $slot['day']; ?></option>
                                <?php
                                } ?>
                            </select>
                            <h5 id="error" style="color:red;">
                            </h5>
                        </div>
                        <div class="form-group">
                            <select class=" select" name="time" id="filter_time">
                                <option value="0" disabled selected>Select Time</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <small>By signing up, you indicate that you agree to
                                the Pronto Terms & Conditions.</small>
                        </div>
                        <div class="form-group clearfix mb-0">
                            <input type="submit" class="btn-md btn-theme float-left" value="Proceed to Pay"
                                id="btnPay" />
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Contact section end -->

<script>
$(document).ready(function() {

    $('#day').change(function() {
        filter_time();
    });
    $('#batchtype').change(function() {
        filter_time();
    });


    function filter_time() {
        var day = $('#day').val();
        var batch_type = $('#batchtype').val();
        var time = $('#filter_time').val();
        $.ajax({
            url: " <?php echo base_url() ?>/selectslot/time",
            method: "GET",
            dataType: "JSON",
            data: {
                day: day,
                batch_type: batch_type
            },
            success: function(data) {
                console.log(data);
                var len = data.length;
                $("#filter_time").empty();
                for (var i = 0; i < len; i++) {
                    var value = data[i]['value'];
                    var name = data[i]['name'];
                    console.log(name);
                    $("#filter_time").append("<option value = '" + value + "' > " + name +
                        "</option>");
                }
                $("#error").hide();
                $(':input[type="submit"]').prop('disabled', false);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                if (day || time == "") {
                    $('#error').text("No slots available,Please select another day");
                    $(':input[type="submit"]').prop('disabled', true);

                }
            }
        })

    }
});

function choice1(select) {
    document.getElementById("btnPay").disabled = true;
}
</script>