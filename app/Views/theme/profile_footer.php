<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="index.html#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<script src="<?= base_url() ?>/public/assets/js/jquery-2.2.0.min.js"></script>
<script src="<?= base_url() ?>/public/assets/js/popper.min.js"></script>
<script src="<?= base_url() ?>/public/assets/js/bootstrap.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/bootstrap-submenu.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/rangeslider.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.mb.YTPlayer.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/bootstrap-select.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.easing.1.3.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.scrollUp.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/leaflet.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/leaflet-providers.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/leaflet.markercluster.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/dropzone.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/slick.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.filterizr.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.magnific-popup.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/maps.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.countdown.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/app.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="<?= base_url() ?>/public/assets/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script  src="<?= base_url() ?>/public/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>