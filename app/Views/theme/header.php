<html lang="zxx">

<head>
    <title><?php echo $page_title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap-submenu.css">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="<?= base_url() ?>/public/assets/css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>/public/assets/css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/dropzone.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/additional.css">


    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="<?= base_url() ?>/public/assets/css/skins/default.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?= base_url() ?>/public/assets/img/favicon.ico" type="image/x-icon">

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/ie10-viewport-bug-workaround.css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?= base_url() ?>/public/assets/js/ie-emulation-modes-warning.js"></script>
    <script src="<?= base_url() ?>/public/assets/js/jquery-2.2.0.min.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxy/1.6.1/scripts/jquery.ajaxy.min.js" integrity="sha512-bztGAvCE/3+a1Oh0gUro7BHukf6v7zpzrAb3ReWAVrt+bVNNphcl2tDTKCBr5zk7iEDmQ2Bv401fX3jeVXGIcA==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ajaxy/1.6.1/scripts/jquery.ajaxy.js" integrity="sha512-4WpSQe8XU6Djt8IPJMGD9Xx9KuYsVCEeitZfMhPi8xdYlVA5hzRitm0Nt1g2AZFS136s29Nq4E4NVvouVAVrBw==" crossorigin="anonymous"></script>
<style>
.navbar-nav .nav-link {
    color: rgba(0, 0, 0, .5);
    font-size: 15px;
    font-weight: bold;
}

.select {
  width: 100%;
  min-width: 15ch;
  max-width: 30ch;
  border: 1px solid var(--select-border);
  border-radius: 0.25em;
  padding: 0.25em 0.5em;
  font-size: 1.25rem;
  cursor: pointer;
  line-height: 1.1;
  background-color: #fff;
  background-image: linear-gradient(to top, #f9f9f9, #fff 33%);
}
</style>
</head>

<body>

    <!-- <div class="page_loader"></div> -->

    <!-- Main header start -->
    <header class="main-header fixed-header2">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand logo-3" href="<?= base_url() ?>">
                    <img src="<?= base_url() ?>/public/assets/img/logos/logo.png" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="expand" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?= base_url() ?>">
                                HOME
                            </a>

                        </li>
                        <li class="nav-item dropdown active">
                            <a class="nav-link" href="<?= base_url() ?>/live_sessions">
                                Live Sessions
                            </a>
                        </li>

                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                My Account
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <?php
                                $this->session = session();
                                $userData = $this->session->get('logged_user');
                                if (empty($userData['id']) && !isset($userData['id'])) { ?>
                                    <li><a class="dropdown-item" href="<?= base_url() ?>/login">Login</a></li>
                                    <li><a class="dropdown-item" href="<?= base_url() ?>/register">Register</a></li>
                                    <?php } else {
                                    if ($userData['role'] == 1) { ?>
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/dashboard">Dashboard</a></li>
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/myprofile">Profile</a></li>
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/myproperties">My Properties</a></li>
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/mybids">My Bids</a></li>
                                        
                                    <?php } elseif ($userData['role'] == 2) { ?>
                                        <li><a class="dropdown-item" href="<?= base_url() ?>/admin/dashboard">Dashboard</a></li>
                                <?php }
                                }  ?>

                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->
  

