<!DOCTYPE html>
<html lang="zxx">

<head>
    <title><?php echo $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap-submenu.css">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css"
        href="<?= base_url() ?>/public/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/dropzone.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/additional.css">


    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet"
        href="<?= base_url() ?>/public/assets/css/skins/default.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?= base_url() ?>/public/assets/img/favicon.ico" type="image/x-icon">

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/ie10-viewport-bug-workaround.css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="public/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <!-- <div class="page_loader"></div> -->
    <?php $baseurl = "localhost:8080/thrivepad";
    ?>
    <!-- Main header start -->
    <header class="main-header header-2 fixed-header">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand logo-3" href="">
                    <img src="<?= base_url() ?>/public/assets/img/logos/logo.png" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="fa fa-bars"></span>

                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto d-lg-none d-xl-none">
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/dashboard" class="nav-link">Dashboard</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/live-sessions/week1" class="nav-link">Live
                                Sessions
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/wheel-of-life" class="nav-link">Wheel Of Life</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/ncodes" class="nav-link">E Codes & N Codes</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/outcome-reinforcement" class="nav-link">Outcome Reinforcement</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/decision-wheel" class="nav-link">Decision Wheel</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/baseline-values" class="nav-link">Baseline Values</a>
                        </li>
                        <li class="nav-item dropdown active">
                            <a href="<?= base_url() ?>/myprofile" class="nav-link">My Profile</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="<?= base_url() ?>/logout" class="nav-link">Logout</a>
                        </li>
                    </ul>
                    <div class="navbar-buttons ml-auto d-none d-xl-block d-lg-block">
                        <ul>
                            <li>
                                <div class="dropdown btns">
                                    <a class="dropdown-toggle" data-toggle="dropdown">
                                       
                                        My Account
                                    </a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?= base_url() ?>/dashboard">Dashboard</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/messages" target="_blank">Live
                                            Sessions
                                        </a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/base-values"
                                            target="_blank">Base Values</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/wheel-of-life"
                                            target="_blank">Wheel Of Life</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/ncodes" target="_blank">E Codes
                                            & N Codes</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/outcome-reinforcement"
                                            target="_blank">Outcome Reinforcement</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/decision-wheel"
                                            target="_blank">Decision Wheel</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/myprofile">My profile</a>
                                        <a class="dropdown-item" href="<?= base_url() ?>/logout">Logout</a>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->



    <!-- Dashbord start -->
    <div class="dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="dashboard-nav" style="overflow:auto;">
                    <div class="dashboard-inner" style="overflow:auto;">
                        <h4>Main</h4>
                        <ul>
                            <li class="<?php if ($page_no == 1) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/dashboard"> Dashboard</a></li>
                            <li class="<?php if ($page_no == 2) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/instructions"> Instructions </a></li>
                            <li class="<?php if ($page_no == 3) {
                                            echo 'active';
                                        } ?>"><a href="" target="_blank"> Personality Test</a></li>
                        </ul>
                        <h4>Sessions</h4>
                        <ul>

                            <?php if ($count >= 1) { ?>
                            <li class="<?php if ($page_no == 4 && $count == 1) {
                                                echo 'active';
                                            } ?>"><a
                                    href="<?= base_url() ?>/live-sessions/week<?php echo $count ?>">Live
                                    Sessions</a>
                            </li>
                            <?php } else { ?>
                            <li class="<?php if ($page_no == 4 && $count == 0) {
                                                echo 'active';
                                            } ?>"><a href="<?= base_url() ?>/livesession">Live
                                    Sessions</a>
                            </li>
                            <?php } ?>
                            <li class="<?php if ($page_no == 5) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/base-values" target="_blank">Base
                                    Values</a>
                            </li>
                            <li class="<?php if ($page_no == 6) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/wheel-of-life" target="_blank">Wheel Of
                                    Life</a></li>
                            <li class="<?php if ($page_no == 7) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/ncodes" target="_blank">E Codes & N
                                    Codes</a>
                            </li>
                            <li class="<?php if ($page_no == 8) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/outcome-reinforcement" target="_blank">Outcome
                                    Reinforcement</a></li>
                            <li class="<?php if ($page_no == 9) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/decision-wheel" target="_blank">Decision
                                    Wheel</a></li>
                            <li class="<?php if ($page_no == 11) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/livesessions/myhabit">My Habit
                                </a></li>
                            <li class="<?php if ($page_no == 12) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/livesessions/accomplishment">Accomplishment
                                </a></li>
                            <li class=" <?php if ($page_no == 13) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/livesessions/breakthrough">Breakthrough
                                </a></li>
                        </ul>
                        <h4>Account</h4>
                        <ul>
                            <li class="<?php if ($page_no == 10) {
                                            echo 'active';
                                        } ?>"><a href="<?= base_url() ?>/myprofile"><i class="flaticon-male"></i>My
                                    Profile</a></li>
                            <li><a href="<?= base_url() ?>/logout"><i class="flaticon-logout"></i>Logout</a></li>
                        </ul>
                    </div>
                </div>
