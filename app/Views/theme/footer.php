<!-- Footer start -->
<footer class="footer" style="width:100%">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                <img src="<?= base_url() ?>/public/assets/img/logos/logo.png" alt="logo" width="100" height="100">
                    <div class="text">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae posuere sapien vitae posuere.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>Contact Us</h4>
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-pin"></i>20/F Greems Road, Royapettah, Chennai
                        </li>
                        <li>
                            <i class="flaticon-mail"></i><a href="mailto:sales@hotelempire.com">info@themevessel.com</a>
                        </li>
                        <li>
                            <i class="flaticon-phone"></i><a href="tel:+55-417-634-7071">+0477 85X6 552</a>
                        </li>
                        <li>
                            <i class="flaticon-fax"></i>+0477 85X6 552
                        </li>
                        <li>
                            <i class="flaticon-internet"></i><a href="mailto:info@green.com">info@green.com</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Useful Links
                    </h4>
                    <ul class="links">
                        <li>
                            <a href="<?= base_url() ?>"> 
                            Home
                            </a>      
                        </li>
                        <li>
                            <a  href="<?= base_url() ?>/browse_properties">
                                Browse Properties
                            </a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>/list_property">
                        List Property
                        </a>
                        </li>
                     
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>Subscribe</h4>
                    <div class="Subscribe-box">
                        <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>
                        <form class="form-inline" action="#" method="GET">
                            <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3" placeholder="Email Address">
                            <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <p class="copy">© 2020 <a href="#">Pronto</a></p>
                </div>
                <div class="col-lg-4 col-md-12">
                    <ul class="social-list clearfix">
                        <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer end -->

<script src="<?= base_url() ?>/public/assets/js/popper.min.js"></script>
<script src="<?= base_url() ?>/public/assets/js/bootstrap.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/bootstrap-submenu.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/rangeslider.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.mb.YTPlayer.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/bootstrap-select.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.easing.1.3.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.scrollUp.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/leaflet.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/leaflet-providers.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/leaflet.markercluster.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/dropzone.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/slick.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.filterizr.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.magnific-popup.min.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/jquery.countdown.js"></script>
<script  src="<?= base_url() ?>/public/assets/js/maps.js"></script>

<script src="<?= base_url() ?>/public/assets/js/app.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="<?= base_url() ?>/public/assets/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script  src="<?= base_url() ?>/public/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>