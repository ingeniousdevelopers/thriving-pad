<div class="dashboard-content">
    <div class="dashboard-header clearfix">
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <h4>Welcome To Thrivepad Dashboard</h4>
            </div>
            <div class="col-sm-12 col-md-7">
                <div class="breadcrumb-nav">
                    <ul>
                        <li>
                            <a href="<?= base_url() ?>">Home</a>
                        </li>
                        <li class="active">Dashboard
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-success">
                <div class="left">
                    <h4><?php echo $count; ?>/12</h4>


                    <p>Live Session Completed</p>
                </div>
                <div class="right">
                    <i class="fa fa-home"></i>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <p class="sub-banner-2 text-center">© 2020 Thrivepad</p>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- Dashbord end -->