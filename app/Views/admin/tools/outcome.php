<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">

            <div id="stick-here"></div>
            <div id="stickThis">
                <h1>Expected Value Calculator</h1>
            </div>

        </div>
    </div>
</header>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<?php
$id = $out[0]['user_id'];
if ($id) {
?>

<div class="container mb-40">
    <div class="row result-single-section result-section">
        <div class="x_title">
            <h2>Expected Value Calculator</h2>
            <div class="clearfix"></div>
        </div>
        <div>
            <ul class="pl-5" style="list-style-type: square;">
                <h5>List all the desires to be fulfilled from the 1st year</h5>
                <?php
                    $id = $out[0]['user_id'];
                    if ($id) {
                        foreach ($dq as $year) {
                            $year = $dq[0]['year'];
                            $year = explode('~', $year);
                            $question = $dq[0]['question'];
                            $question = explode('~', $question);
                            foreach ($year as $key => $y) {
                                if ($y == 1) {
                    ?><li>
                    <?php echo $question[$key] . '<br>'; ?>
                </li>
                <?php
                                }
                            } ?>

                <?php }
                    } ?>
            </ul>
        </div>
        <script>
        $(function() {

            $('#expected_cal_form').on('submit', function(e) {
                e.preventDefault();
                var isValid = true;
                $("#exp_error").empty();
                $('input[type="text"]').each(function() {
                    if ($.trim($(this).val()) == '') {
                        isValid = false;
                        $(this).css({
                            "border": "1px solid red",
                            "background": "#FFCECE"
                        });
                    } else {
                        $(this).css({
                            "border": "",
                            "background": ""
                        });
                    }
                });
                if (isValid == false) {
                    $("#exp_error").html(
                        "<div class='alert alert-danger'> Please fill all the fields.</div>");
                    e.preventDefault();
                }
                if (isValid == true) {
                    $.ajax({
                        type: 'post',
                        url: '<?= base_url(); ?>/outcome-reinforcement/create',
                        data: $('#expected_cal_form').serialize(),
                        success: function(data) {
                            //                                alert(data);
                            console.log(data);
                            var res = JSON.parse(data);
                            if (res['status'] == 3) {
                                $("#exp_error").html(
                                    "<br/><div class='alert alert-danger'> Please answer all your questions.</div>"
                                );
                            } else if (res['status'] == 1) {
                                $("#high_value").html(res['high_value']);
                                $('#show_result').trigger('click');
                            }
                        }
                    });
                }

            });

        });
        </script>
        <form action="<?= base_url() ?>/tools/outcomereinfrocement_create" method="POST" id="expected_cal_form">
            <div class="table-responsive">
                <table class="table table-hover value_calculator_tbl">
                    <thead>
                        <tr>
                            <th width="300px"></th>
                            <th>
                                <p class="top_span">(DO THIS FIRST)</p>
                            </th>
                            <th>
                                <p class="top_span">(1 most, 5 least)</p>
                            </th>
                            <th>
                                <p class="top_span">(1 most, 5 least)</p>
                            </th>
                            <th>
                                <p class="top_span">(1 most, 5 least)</p>
                            </th>
                            <th>
                                <p class="top_span">(1 least, 5 most)</p>
                            </th>
                            <th>
                                <p class="top_span">(1 least, 5 most)</p>
                            </th>
                            <th>
                                <p class="top_span">(1 least, 5 most)</p>
                            </th>
                            <th></th>
                        </tr>
                        <tr>
                            <th width="300px">Potential Priorities</th>
                            <th>Predicted Ranking</th>
                            <th>Time (Upfront)</th>
                            <th>Time (Ongoing)</th>
                            <th>Difficulty</th>
                            <th>Enjoyment</th>
                            <th>Impact (90 day)</th>
                            <th>Impact (25 yr)</th>
                            <th>Total Score*</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="highlight">
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('potential_priority1')) {
                                        $val = set_value('qpotential_priority19');
                                    } elseif (($out[0]['potential_priority1'])) {
                                        $potential_priority1 = $out[0]['potential_priority1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control" name="potential_priority1"
                                    <?php echo $attr; ?> value="<?php echo $potential_priority1 ?>" />
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('predict_ranking1')) {
                                        $val = set_value('predict_ranking1');
                                    } elseif (($out[0]['predict_ranking1'])) {
                                        $predict_ranking1 = $out[0]['predict_ranking1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control" name="predict_ranking1"
                                    onkeypress="return isNumberKey(event)" maxlength="1" <?php echo $attr; ?>
                                    value=" <?php echo $predict_ranking1 ?>" />
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('time_upfront1')) {
                                        $val = set_value('time_upfront1');
                                    } elseif (($out[0]['time_upfront1'])) {
                                        $time_upfront1 = $out[0]['time_upfront1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="time_upfront1"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $time_upfront1 ?>" />
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('time_ongoing1')) {
                                        $val = set_value('time_ongoing1');
                                    } elseif (($out[0]['time_ongoing1'])) {
                                        $time_ongoing1 = $out[0]['time_ongoing1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="time_ongoing1"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $time_ongoing1 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('difficulty1')) {
                                        $val = set_value('difficulty1');
                                    } elseif (($out[0]['difficulty1'])) {
                                        $difficulty1 = $out[0]['difficulty1'];

                                        $attr = 'disabled';
                                    }
                                    ?>

                                <input disabled type="text" class="form-control calc" name="difficulty1"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $difficulty1 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('enjoyment1')) {
                                        $val = set_value('enjoyment1');
                                    } elseif (($out[0]['enjoyment1'])) {
                                        $enjoyment1 = $out[0]['enjoyment1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="enjoyment1"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $enjoyment1 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('impact_90_day1')) {
                                        $val = set_value('impact_90_day1');
                                    } elseif (($out[0]['impact_90_day1'])) {
                                        $impact_90_day1 = $out[0]['impact_90_day1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="impact_90_day1"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $impact_90_day1 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('impact_25_yr1')) {
                                        $val = set_value('impact_25_yr1');
                                    } elseif (($out[0]['impact_25_yr1'])) {
                                        $impact_25_yr1 = $out[0]['impact_25_yr1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="impact_25_yr1"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $impact_25_yr1 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('total_score1')) {
                                        $val = set_value('total_score1');
                                    } elseif (($out[0]['total_score1'])) {
                                        $total_score1 = $out[0]['total_score1'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" readonly="" class="form-control total_score"
                                    name="total_score1" <?php echo $attr; ?> value=" <?php echo $total_score1 ?>">
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('potential_priority2')) {
                                        $val = set_value('potential_priority2');
                                    } elseif (($out[0]['potential_priority2'])) {
                                        $potential_priority2 = $out[0]['potential_priority2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control" name="potential_priority2"
                                    <?php echo $attr; ?> value=" <?php echo $potential_priority2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('predict_ranking2')) {
                                        $val = set_value('predict_ranking2');
                                    } elseif (($out[0]['predict_ranking2'])) {
                                        $predict_ranking2 = $out[0]['predict_ranking2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control" name="predict_ranking2"
                                    onkeypress="return isNumberKey(event)" maxlength="1" <?php echo $attr; ?>
                                    value=" <?php echo $predict_ranking2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('time_upfront2')) {
                                        $val = set_value('time_upfront2');
                                    } elseif (($out[0]['time_upfront2'])) {
                                        $time_upfront2 = $out[0]['time_upfront2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="time_upfront2"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $time_upfront2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('time_ongoing2')) {
                                        $val = set_value('time_ongoing2');
                                    } elseif (($out[0]['time_ongoing2'])) {
                                        $time_ongoing2 = $out[0]['time_ongoing2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="time_ongoing2"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $time_ongoing2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('difficulty2')) {
                                        $val = set_value('difficulty2');
                                    } elseif (($out[0]['difficulty2'])) {
                                        $difficulty2 = $out[0]['difficulty2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="difficulty2"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $difficulty2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('enjoyment2')) {
                                        $val = set_value('enjoyment2');
                                    } elseif (($out[0]['enjoyment2'])) {
                                        $enjoyment2 = $out[0]['enjoyment2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="enjoyment2"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $enjoyment2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('impact_90_day2')) {
                                        $val = set_value('impact_90_day2');
                                    } elseif (($out[0]['impact_90_day2'])) {
                                        $impact_90_day2 = $out[0]['impact_90_day2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="impact_90_day2"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $impact_90_day2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('impact_25_yr2')) {
                                        $val = set_value('impact_25_yr2');
                                    } elseif (($out[0]['impact_25_yr2'])) {
                                        $impact_25_yr2 = $out[0]['impact_25_yr2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="impact_25_yr2"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $impact_25_yr2 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('total_score2')) {
                                        $val = set_value('total_score2');
                                    } elseif (($out[0]['total_score2'])) {
                                        $total_score2 = $out[0]['total_score2'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" readonly="" class="form-control total_score"
                                    name="total_score2" <?php echo $attr; ?> value=" <?php echo $total_score2 ?>">
                            </td>
                        </tr>
                        <tr class="">
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('potential_priority3')) {
                                        $val = set_value('potential_priority3');
                                    } elseif (($out[0]['potential_priority3'])) {
                                        $potential_priority3 = $out[0]['potential_priority3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control" name="potential_priority3"
                                    <?php echo $attr; ?> value=" <?php echo $potential_priority3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('predict_ranking3')) {
                                        $val = set_value('predict_ranking3');
                                    } elseif (($out[0]['predict_ranking3'])) {
                                        $predict_ranking3 = $out[0]['predict_ranking3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control" name="predict_ranking3"
                                    onkeypress="return isNumberKey(event)" maxlength="1" <?php echo $attr; ?>
                                    value=" <?php echo $predict_ranking3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('time_upfront3')) {
                                        $val = set_value('time_upfront3');
                                    } elseif (($out[0]['time_upfront3'])) {
                                        $time_upfront3 = $out[0]['time_upfront3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="time_upfront3"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $time_upfront3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('time_ongoing3')) {
                                        $val = set_value('time_ongoing3');
                                    } elseif (($out[0]['time_ongoing3'])) {
                                        $time_ongoing3 = $out[0]['time_ongoing3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="time_ongoing3"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $time_ongoing3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('difficulty3')) {
                                        $val = set_value('difficulty3');
                                    } elseif (($out[0]['difficulty3'])) {
                                        $difficulty3 = $out[0]['difficulty3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="difficulty3"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $difficulty3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('enjoyment3')) {
                                        $val = set_value('enjoyment3');
                                    } elseif (($out[0]['enjoyment3'])) {
                                        $enjoyment3 = $out[0]['enjoyment3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="enjoyment3"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $enjoyment3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('impact_90_day3')) {
                                        $val = set_value('impact_90_day3');
                                    } elseif (($out[0]['impact_90_day3'])) {
                                        $impact_90_day3 = $out[0]['impact_90_day3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="impact_90_day3"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $impact_90_day3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('impact_25_yr3')) {
                                        $val = set_value('impact_25_yr3');
                                    } elseif (($out[0]['impact_25_yr3'])) {
                                        $impact_25_yr3 = $out[0]['impact_25_yr3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" class="form-control calc" name="impact_25_yr3"
                                    onkeypress="return isNumberKey(event)" min="1" max="5" <?php echo $attr; ?>
                                    value=" <?php echo $impact_25_yr3 ?>">
                            </td>
                            <td>
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('total_score3')) {
                                        $val = set_value('total_score3');
                                    } elseif (($out[0]['total_score3'])) {
                                        $total_score3 = $out[0]['total_score3'];

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <input disabled type="text" readonly="" class="form-control total_score"
                                    name="total_score3" <?php echo $attr; ?> value=" <?php echo $total_score3 ?>">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <small id="exp_error" class="text-center"> </small>

            </div>
        </form>
        <button type="button" style="display:none;" id="show_result" data-toggle="modal" data-target="#stepsModal"
            data-backdrop="static" data-keyboard="false">Result</button>
    </div>
    <div class="row result-single-section result-section">
        <div class="x_title">
            <h2>Criteria Descriptions</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="steps-single">
                <h4 class="service-heading hd_desc">Kickstart :</h4>
                <p class="text-muted">How long will this take to get this up and running and start receiving benefits?
                    Remember speed of implementation.</p>
            </div>
            <div class="steps-single">
                <h4 class="service-heading hd_desc">Continuity :</h4>
                <p class="text-muted">How long will this take to grow and maintain? Can this be passively managed in the
                    future? Remember opportunity cost.</p>
            </div>
            <div class="steps-single">
                <h4 class="service-heading hd_desc">Excitement :</h4>
                <p class="text-muted">How much does the idea of this excite me? Will I enjoy the process or have to use
                    willpower to push through?</p>
            </div>
            <div class="steps-single">
                <h4 class="service-heading hd_desc">Difficulty :</h4>
                <p class="text-muted">Do I currently have (or have access to) the necessary skillset? How likely is
                    sucess? How much variance in potential outcomes?</p>
            </div>
            <div class="steps-single">
                <h4 class="service-heading hd_desc">Impact (3 months) :</h4>
                <p class="text-muted">How much immediate impact will this have? What short-term opportunities does it
                    create? Momentum builds on itself.</p>
            </div>
            <div class="steps-single">
                <h4 class="service-heading hd_desc">Impact (25 yr) :</h4>
                <p class="text-muted">How much impact could this have towards my life's mission? Will the skills and
                    experience gained be worthwhile even if I fail?</p>
            </div>
        </div>
    </div>
    <div class="row mr-t-3em">
        <div class="col-md-12  text-center">
            <div class="steps-single">

                <a class="btn btn-orange btn-orange Broadcast-btn" href="<?= base_url() ?>/live-sessions/week6">Let's
                    go
                    back</a>

            </div>
        </div>

    </div>



</div>

<?php
} else { ?>
<div style="margin:30px;" class="alert alert-danger">No Results Found</div> <?php } ?>

<script src="<?= base_url() ?>/public/assets/tools/js/jquery.min.js"></script>
<script src="<?= base_url() ?>/public/assets/tools/js/bootstrap.min.js"></script>

<script>
/*$('#submit_btn').click(function (e) {
                   var isValid = true;
                   $("#exp_error").empty();
                   
                   $('input[type="text"]').each(function () {
                   if ($.trim($(this).val()) == '') {
                   isValid = false;
                   $(this).css({
                   "border": "1px solid red",
                   "background": "#FFCECE"
                   });
                   } else {
                   $(this).css({
                   "border": "",
                   "background": ""
                   });
                   }
                   });
                   if (isValid == false) {
                   $("#exp_error").html("<div class='alert alert-danger'> Please fill all the fields.</div>");
                   e.preventDefault();
                   }
                   
                   });*/

function isNumberKey(evt) {

    var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$('.calc').on('input', function() {

    var value = $(this).val();
    if ((value !== '') && (value.indexOf('.') === -1)) {

        $(this).val(Math.max(Math.min(value, 5), 1));
    }
});
$(document).ready(function() {
    $('.calc').on("keyup", function(event) {
        $('tr').each(function() {
            var sum = 0
            $(this).find('.calc').each(function() {
                var combat = $(this).val();
                if (!isNaN(combat) && combat.length !== 0) {
                    sum += parseFloat(combat * combat);
                }
            });
            $('.total_score', this).val(sum);
        });
    });
});
</script>

<script>
function data_delete(type) {
    $.ajax({
        type: 'POST',
        url: 'https://coachtofortune.com/coachinghub/coach/practice/exp_delete',
        data: '',
        success: function(data) {
            if (data == 1 && type == 1) {
                window.location = 'https://coachtofortune.com/coachinghub/' + "coach/practice";
            } else if (data == 1 && type == 2) {
                window.location = 'https://coachtofortune.com/coachinghub/' +
                    "coach/practice/expected_cal";
            }
        }
    });
}
</script>


<div class="modal fade" id="stepsModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body modalbody-content">
                <p class="text-muted tag-content">
                    <b>Your Priority should be the following outcome: <span id="high_value"></span></b>
                </p>

                <div class="text-center mt-20">
                    <button class="btn btn-orange btn-orange Broadcast-btn" onclick="location.reload();">Ok</button>
                </div>
            </div>


        </div>
    </div>
</div>

<script>
// $(document).ready(function() {
//     function disableBack() { window.history.forward() }

//     window.onload = disableBack();
//     window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
// });
</script>