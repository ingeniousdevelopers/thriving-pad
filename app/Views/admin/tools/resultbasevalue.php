<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>BASEVALUES</h1>
            <div id="stick-here" style="height: 0px;"></div>

        </div>
    </div>
</header>
<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list check-sentence">
            <?php

            ?>

            <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                <thead>

                    <tr>

                        <th>S.No</th>
                        <th>Basevalues</th>
                        <th>Definition</th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    $val = $baselinevalue[0]['array_baselinevalues'];
                    $splittedstring = explode("~", $val);
                    foreach ($basevalues as $values) {
                        foreach ($splittedstring as $val) {
                            if ($values['id'] == $val) {
                                $j = $j + 1;

                    ?> <tr>
                        <?php
                                    ?>
                        <td><?php echo $j; ?></td>
                        <td><?php echo $values['basevalues']; ?> </td>
                        <td><?php echo $values['definition']; ?> </td>

                        <?php

                                    ?>
                    </tr>
                    <?php
                            }
                        }
                        ?>
                </tbody>

                <?php
                    }


            ?>
        </div>
    </div>
</div>


<script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
<script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>



<script>
function sticktothetop() {
    var window_top = $(window).scrollTop();
    var top = $('#stick-here').offset().top;
    if (window_top > top) {
        $('#stickThis').addClass('stick');
        $('#stick-here').height($('#stickThis').outerHeight());
    } else {
        $('#stickThis').removeClass('stick');
        $('#stick-here').height(0);
    }
}
$(function() {
    $(window).scroll(sticktothetop);
    sticktothetop();
});
</script>