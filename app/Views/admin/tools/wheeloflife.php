<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>Wheel of Life</h1>
            <div id="stick-here" style="height: 0px;"></div>
            <p>Range your score for followings parts of life</p>
        </div>
    </div>
</header>

<?php

$id = $wheellife[0]['user_id'];
if ($id) {
?>

<div class="container training-container mb-40 mte-4">
    <div class="row">
        <form action="<?= base_url(); ?>/wheel-of-life/create" method="POST">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Health</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">

                                        <input class="input-range" name="col1" id="health" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Friends &amp; Family</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col2" id="friends_family" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Fun Leisure &amp; Recreation</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col3" id="funleasure_recreation" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Wealth</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col4" id="wealth" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Relationship</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col5" id="relationship" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Learning &amp; Personal Growth</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col6" id="learning_growth" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Possessions</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col7" id="possessions" type="text"
                                            data-slider-tooltip="always" data-slider-min="1" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>



            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="em_service_content m-b-20">
                    <div class="em_single_service_text">
                        <div class="service_top_text">

                            <div class="em-service-title-1">
                                <h2>Career</h2>
                            </div>
                        </div>
                        <div class="em-service-inner">
                            <div class="em-service-desc">
                                <div class="col-md-12 left-align-col">
                                    <div class="well slider-wrapper slider-strips slider-ghost">
                                        <input class="input-range" name="col8" id="career" type="text"
                                            data-slider-min="1" data-slider-tooltip="always" data-slider-max="10"
                                            data-slider-step="1" data-slider-value="1" data-value="1" value="1"
                                            style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </form>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/bootstrap-slider.min.js"></script>
        <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('.input-range').each(function() {
                    var value = $(this).attr('data-slider-value');
                    var separator = value.indexOf(',');
                    if (separator !== -1) {
                        value = value.split(',');
                        value.forEach(function(item, i, arr) {
                            arr[i] = parseFloat(item);
                        });
                    } else {
                        value = parseFloat(value);
                    }
                    $(this).slider({
                        formatter: function(value) {
                            //                        console.log(value);
                            return '' + value;
                        },
                        min: parseFloat($(this).attr('data-slider-min')),
                        max: parseFloat($(this).attr('data-slider-max')),
                        range: $(this).attr('data-slider-range'),
                        value: value,
                        tooltip_split: $(this).attr('data-slider-tooltip_split'),
                        tooltip: $(this).attr('data-slider-tooltip')
                    });
                });

            });
        })(jQuery);
        </script>
        <script src="<?= base_url() ?>/public/assets/tools/js/jquery.ui.touch-punch.min.js"></script>



    </div>

    <div class="row mr-t-3em">
        <div class="col-md-12  text-center">
            <div class="steps-single">

                <button class="btn btn-orange btn-orange Broadcast-btn" onclick="data_delete(1)">Let's go back</button>

            </div>
        </div>

    </div>
</div>
<?php
} else { ?>
<div style="margin:30px;" class="alert alert-danger">No Results Found</div> <?php } ?>


<script src="<?= base_url() ?>/public/assets/tools/js/bootstrap.min.js"></script>
<script>
function data_delete(type) {
    if (type == 1) {
        window.location = 'https://coachtofortune.com/coachinghub/' + "coach/practice";
    } else {
        $.ajax({
            type: 'POST',
            url: 'https://coachtofortune.com/coachinghub/coach/practice/wheel_delete',
            data: '',
            success: function(data) {
                if (data == 1 && type == 1) {
                    window.location = 'https://coachtofortune.com/coachinghub/' + "coach/practice";
                } else if (data == 1 && type == 2) {
                    window.location = 'https://coachtofortune.com/coachinghub/' +
                        "coach/practice/wheeloflife";
                }
            }
        });
    }
}
</script>




<script>
// $(document).ready(function() {
//     function disableBack() { window.history.forward() }

//     window.onload = disableBack();
//     window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
// });
</script>