<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>Practice N-CODES</h1>
            <div id="stick-here" style="height: 0px;"></div>
            <p>Select as many Ncode- as you can that you feel are important to you.</p>
            <!--<p>Select  words that describe what matters most to you.</p>-->
            <div id="stickThis">
                <div id="stickThis" class="row text-center">
                    <div class="container">
                        <div class="col-md-offset-3 col-md-3">
                            <input id="myInput" type="text" placeholder="Search Words.." class="form-control">
                        </div>
                        <div class="col-md-4" style="padding-top: 8px;">
                            <span class="questions_remaining pr">Words Selected: <span id="words_selected">0</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>

<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list check-sentence">
            <div class="row">
                <?php
                if (!empty($ncode)) {

                    $j = 1;

                ?>
                <form action="<?= base_url() ?>/tools/updatencode" method="POST">
                    <?php

                        $val = '';
                        if (set_value('array_ncode')) {
                            $val = set_value('array_ncode');
                        } elseif (($ncode[0]['array_ncode'])) {
                            $val = $ncode[0]['array_ncode'];
                            $splittedstring = explode("~", $val);
                            foreach ($ncode as $fil) {
                                if ($fil['filter'] == 5) {
                                    array_pop($splittedstring);
                                } elseif ($fil['filter'] == 4) {
                                    array_splice($splittedstring, 4);
                                } elseif ($fil['filter'] == 3) {
                                    array_splice($splittedstring, 3);
                                } elseif ($fil['filter'] == 2) {
                                    array_splice($splittedstring, 2);
                                }

                                if (count($splittedstring) >= 24) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT N-CODES MUST BE 12</b></div>
                    <?php
                                }
                                if (count($splittedstring) == 12) {
                                ?> <div class="alert alert-danger"><b>ATLEAST YOU CAN
                            SELECT N-CODES
                            MUST BE 6</b></div>

                    <?php
                                }
                                if (count($splittedstring) == 6) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT N-CODES MUST BE 5</b></div>
                    <?php
                                }
                                if (count($splittedstring) == 5) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT N-CODES MUST BE 4</b></div>
                    <?php
                                }
                                if (count($splittedstring) == 4) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT N-CODES MUST BE 3</b></div>
                    <?php
                                }

                                if (count($splittedstring) == 3) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT N-CODES MUST BE 2</b></div>
                    <?php
                                }
                                if (count($splittedstring) == 2) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT N-CODES MUST BE 1</b></div>
                    <?php
                                }
                            }

                            foreach ($ncodevalue as $code) {
                                foreach ($ncode as $fil) {
                                    if ($fil['filter'] != 1) {
                                        foreach ($splittedstring as $val) {
                                            if ($code['id'] == $val) {

                                    ?>
                    <div class="form-checkbox">
                        <input type="checkbox" name="sentence[]" value="<?php echo $code['id'] ?>"
                            id="<?php echo $code['id'] ?>" />
                        <label for="<?php echo $code['id'] ?>" title="<?php $code['ncodes'] ?>"
                            style=" text-align:left">
                            <?php echo $j++ . '. ' . $code['ncodes']; ?> </label>
                    </div>
                    <br>
                    <?php



                                            }
                                        }
                                    }
                                }
                            }
                        }

                        ?>
                    <div class=" text-center">
                        <small id="sentence_error" class="text-center"> </small>
                    </div>

                </form>

                <?php
                } else {

                ?>
                <form action="<?= base_url() ?>/tools/insertncode" method="POST">

                    <?php $j = 0;
                        foreach ($ncodevalue as $nc) {
                            $j = $j + 1; ?>

                    <div class="form-checkbox">
                        <input type="checkbox" name="sentence[]" id="<?php echo $nc['id'] ?>"
                            value="<?php echo $nc['id'] ?>" />
                        <label for="<?php echo $nc['id'] ?>" title="<?php echo $nc['ncodes']; ?>"
                            style="text-align:left">
                            <?php
                                    echo $j . '. ' . $nc['ncodes'];
                                    ?>
                        </label>
                    </div>

                    <br>
                    <?php
                        }
                        ?>
            </div>
        </div>
        <input type="hidden" value="1" id="level" name="level" />
        <input type="hidden" value="1" id="status" name="status" />
        <input type="hidden" value="6" id="filter" name="filter" />
        <div class="text-center">
            <small id="sentence_error" class="text-center"> </small>
        </div>


        </form>
        <?php
                }
    ?>

    </div>

    <script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
    <script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>