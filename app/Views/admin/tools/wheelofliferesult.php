<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>Wheel of Life</h1>
            <div id="stick-here" style="height: 0px;"></div>
            <p>Result of Wheel of Life</p>

        </div>
    </div>
</header>


<div class="container training-container mb-40 mte-4">
    <div class="row">
        <div class="container chartsContainer">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-8">
                    <div class="chor" style="width:100%; display:inline-block;">
                        <div class="main-canvas">
                            <div style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"
                                class="chartjs-size-monitor">
                                <div class="chartjs-size-monitor-expand"
                                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div>
                                </div>
                                <div class="chartjs-size-monitor-shrink"
                                    style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;">
                                    <div style="position:absolute;width:200%;height:200%;left:0; top:0"></div>
                                </div>
                            </div>
                            <canvas id="polarChart" style="display: block; height: 355px; width: 710px;" width="887"
                                height="443" class="chartjs-render-monitor"></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4 col-xs-4">
                    <div class="bhavesh">
                        <div class="color-index">
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #e8ea60;"></li>
                                <p class="color-text">Health</p>
                                <p class="color-value"><?php echo $result['col1']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #ff6f18;"></li>
                                <p class="color-text">Friends &amp; Family</p>
                                <p class="color-value"><?php echo $result['col2']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #40e262;"></li>
                                <p class="color-text">Fun,Leisure &amp; Recreation</p>
                                <p class="color-value"><?php echo $result['col3']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #0744c1;"></li>
                                <p class="color-text">Wealth</p>
                                <p class="color-value"><?php echo $result['col4']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #b148d4;"></li>
                                <p class="color-text">Relationship</p>
                                <p class="color-value"><?php echo $result['col5']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #66d8d7;"></li>
                                <p class="color-text">Learning &amp; Personal Growth</p>
                                <p class="color-value"><?php echo $result['col6']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #5b9232;"></li>
                                <p class="color-text">Possession</p>
                                <p class="color-value"><?php echo $result['col7']; ?></p>
                            </ul><br>
                            <ul style="display:inline-block;">
                                <li class="color-box" style="background-color: #00c0ff;"></li>
                                <p class="color-text">Career</p>
                                <p class="color-value"><?php echo $result['col8']; ?></p>
                            </ul>




                        </div>
                    </div>
                </div>

            </div>
        </div>



        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/highcharts-more.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script src="https://code.highcharts.com/modules/export-data.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.min.js"></script>

        <script type="text/javascript">
        var ctx = document.getElementById("polarChart").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'polarArea',
            data: {
                labels: [
                    "Health",
                    "Friends & Family",
                    "Fun,Leisure & Recreation",
                    "Wealth",
                    "Relationship",
                    "Learning & Personal Growth",
                    "Possession",
                    "Career"
                ],
                datasets: [{
                    backgroundColor: [
                        "#e8ea60",
                        "#ff6f18",
                        "#40e262",
                        "#0744c1",
                        "#b148d4",
                        "#66d8d7",
                        "#5b9232",
                        "#00c0ff"
                    ],
                    data: [<?php echo $result['col1']; ?>,
                        <?php echo $result['col2']; ?>,
                        <?php echo $result['col3']; ?>,
                        <?php echo $result['col4']; ?>,
                        <?php echo $result['col5']; ?>,
                        <?php echo $result['col6']; ?>,
                        <?php echo $result['col7']; ?>,
                        <?php echo $result['col8']; ?>
                    ],
                }],
            },
            options: {

                elements: {
                    arc: {
                        borderColor: "rgba(255,255,255,1)",
                        borderWidth: 3
                    }
                },
                scale: {
                    ticks: {
                        beginAtZero: true,
                        max: 10,
                        min: 0,
                        stepSize: 1,
                        fontFamily: "'Lato', sans-serif",
                        fontSize: 18,
                        fontColor: "#000",
                        display: false
                    },
                    gridLines: {
                        //lineWidth:1,
                        color: "transparent",
                    },
                },
                layout: {
                    padding: {
                        left: 0,
                        right: 0,
                        top: 0,
                        bottom: 0
                    }

                },
                legend: {
                    display: false,
                    position: "right",
                    labels: {
                        fontFamily: "'Lato', sans-serif",
                        fontSize: 18,
                        fontColor: "#fff"
                        // padding-right:40px;
                    }

                }
            }
        });
        </script>


    </div>

    <div class="row mr-t-3em">
        <div class="col-md-12  text-center">
            <div class="steps-single">

                <button class="btn btn-orange btn-orange Broadcast-btn complete2" onclick="data_delete(1)">Redo</button>

            </div>
        </div>

    </div>

    <div class="row mr-t-3em">
        <div class="col-md-12  text-center">
            <div class="steps-single">

                <button class="btn btn-orange btn-orange Broadcast-btn complete2" onclick="data_delete(1)">Let's go
                    back</button>

            </div>
        </div>

    </div>
</div>

<script>
function data_delete(type) {
    if (type == 1) {
        window.location = <?= base_url(); ?> + "/redo";
    } else {
        $.ajax({
            type: 'POST',
            url: <?= base_url(); ?> + "/redo",
            data: '',
            success: function(data) {
                if (data == 1 && type == 1) {
                    window.location = <?= base_url(); ?> + "/redo";
                } else if (data == 1 && type == 2) {
                    window.location = <?= base_url(); ?> + "/redo";
                }
            }
        });
    }
}
</script>