<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>Practice Baselines</h1>
            <div id="stick-here" style="height: 0px;"></div>
            <p>Select as many baselines as you can that you feel are important to you.</p>
            <!--<p>Select  words that describe what matters most to you.</p>-->
            <div id="stickThis">
                <div id="stickThis" class="row text-center">
                    <div class="container">
                        <div class="col-md-offset-3 col-md-3">
                            <input id="myInput" type="text" placeholder="Search Words.." class="form-control">
                        </div>
                        <div class="col-md-4" style="padding-top: 8px;">
                            <span class="questions_remaining pr">Words Selected: <span id="words_selected">0</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>

<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list text-center">
            <div class="row">
                <?php
                if (!empty($baselinevalue)) {

                    $j = 1;

                ?>
                <form action="<?= base_url() ?>/tools/updbasvalue" method="POST">
                    <?php

                        $val = '';
                        if (set_value('array_baselinevalues')) {
                            $val = set_value('array_baselinevalues');
                        } elseif (($baselinevalue[0]['array_baselinevalues'])) {
                            $val = $baselinevalue[0]['array_baselinevalues'];
                            $splittedstring = explode("~", $val);
                            foreach ($baselinevalue as $baseval) {
                                if ($baseval['filter'] == 5) {
                                    array_pop($splittedstring);
                                } elseif ($baseval['filter'] == 4) {
                                    array_splice($splittedstring, 4);
                                } elseif ($baseval['filter'] == 3) {
                                    array_splice($splittedstring, 3);
                                } elseif ($baseval['filter'] == 2) {
                                    array_splice($splittedstring, 2);
                                }
                            }
                            if (count($splittedstring) >= 24) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUES MUST BE 12</b></div>
                    <?php
                            }
                            if (count($splittedstring) == 12) {
                            ?> <div class="alert alert-danger"><b>ATLEAST YOU CAN
                            SELECT BASEVALUES
                            MUST BE 6</b></div>

                    <?php
                            }
                            if (count($splittedstring) == 6) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUESMUST BE 5</b></div>
                    <?php
                            }
                            if (count($splittedstring) == 5) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUES MUST BE 4</b></div>
                    <?php
                            }
                            if (count($splittedstring) == 4) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUES MUST BE 3</b></div>
                    <?php
                            }

                            if (count($splittedstring) == 3) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUES MUST BE 2</b></div>
                    <?php
                            }
                            if (count($splittedstring) == 2) { ?>
                    <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUES MUST BE 1</b></div>
                    <?php
                            }

                            foreach ($basevalues as $baseval) {
                                foreach ($baselinevalue as $baseline) {
                                    if ($baseline['filter'] != 1) {
                                        foreach ($splittedstring as $val) {
                                            if ($baseval['id'] == $val) {
                                ?>
                    <div class="col-md-3">

                        <div class="form-checkbox" style="width:100%;">
                            <a style="color:black;text-decoration:none" href="#" data-toggle="tooltip"
                                data-placement="right" title=" <?php echo $baseval['definition'] ?>">


                                <input type="checkbox" name="va_words[]" value="<?php echo $baseval['id'] ?>"
                                    id="<?php echo $baseval['id'] ?>" />
                                <label for="<?php echo $baseval['id'] ?>" title="<?php $baseval['basevalues'] ?>"
                                    style=" text-align:left">
                                    <?php echo $j++ . '. ' . $baseval['basevalues']; ?> </label>
                            </a>

                        </div>
                    </div>
                    <?php



                                            }
                                        }
                                    }
                                }
                            }
                        }

                        ?>
            </div>
            <div class=" text-center">
                <small id="words_error" class="text-center"> </small>
            </div>


            </form>

            <?php
                } else {

        ?>
            <div class="alert alert-danger"><b>ATLEAST YOU CAN SELECT BASEVALUES MORE THAN 24</b></div>
            <form action="<?= base_url() ?>/tools/insbasevalue" method="POST">
                <?php $j = 0;
                    foreach ($basevalues as $baseval) {
                        $j = $j + 1; ?>
                <div class="row col-md-3">

                    <div class="form-checkbox" style="width:100%;">
                        <a style="color:black;text-decoration:none" href="#" data-toggle="tooltip"
                            data-placement="right" title=" <?php echo $baseval['definition'] ?>">
                            <input type="checkbox" name="va_words[]" id="<?php echo $baseval['id'] ?>"
                                value="<?php echo $baseval['id'] ?>" />
                            <label for="<?php echo $baseval['id'] ?>" title="<?php echo $baseval['basevalues']; ?>"
                                style="text-align:left"><?php echo $j . '. ' . $baseval['basevalues']; ?></label>
                        </a>
                    </div>
                </div>

                <?php
                    }
                ?>
        </div>
    </div>
    <input type="hidden" value="1" id="level" name="level" />
    <input type="hidden" value="1" id="status" name="status" />
    <input type="hidden" value="6" id="filter" name="filter" />
    <div class="text-center">
        <small id="words_error" class="text-center"> </small>
    </div>

    </form>
    <?php
                }
?>

</div>

<script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
<script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>