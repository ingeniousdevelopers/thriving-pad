
<div class="dashboard-content">
    <div class="dashboard-header clearfix">

    </div>
    <?php if(session()->getTempData('success')):?>
              <div class="alert alert-success"><?= session()->getTempData('success')?></div>
            <?php endif;?>
            <?php if(session()->getTempData('error')):?>
              <div class="alert alert-danger"><?= session()->getTempData('error')?></div>
            <?php endif;?>
        <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Slots</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body" style="font-size: 12.5px;">
            <div class="table-responsive">
                <table  width="100%" class="table table-bordered table-hover display nowrap margin-top-10" >
                    <thead>
                        <tr>
                        <th>Time</th>
                        <th>6.30 AM</th>
                                     <th>8.30 AM</th>
                                    <th>10.30 AM</th>
                                    <th>12.30 PM</th>
                                    <th>2.30 PM</th>
                                    <th>4.30 PM</th>
                                    <th>6.30 PM</th>
                                    <th>8.30 PM</th>
                                    <th>10.30 PM</th>
                                    </tr>
                                    </thead>
                  <tbody>
                            <tr> 
               
                            <td> Sunday </td>
                            <?php
                                foreach ($sun_list as $sunday) {
                        ?>
  <td> <?php if($sunday['batch_type']==1)
  { 
      echo 'Private';
      foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $sunday['count']==0){
         echo '<br/>Unassigned'.'('.$sunday['count'].')';
         break;
       }
       else
       {
          echo '<br/>Assigned'.'('.$sunday['count'].')';
          break;
       }     
    }
}
     elseif($sunday['batch_type']==2)
   {
    echo 'Group'; 
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $sunday['count']==0){
            echo '<br/>Unassigned'.'('.$sunday['count'].')';
         break;
       }
       else
       {
        echo '<br/>Assigned'.'('.$sunday['count'].')';
        break;
       }     
    }
   }
   else
   {
    echo 'NIL';
   }
   
   ?>


    </td>
    <?php
                                
                             
                                 }     ?>
                        
                            </tr>
                            <tr> 
                            <td> Monday </td>
                            <?php
                                foreach ($mon_list as $monday) {
                        ?>
  <td> <?php if($monday['batch_type']==1)
  { echo 'Private';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $monday['count']==0){
            echo '<br/>Unassigned'.'('.$monday['count'].')';
        break;
         }
         else
         {
             $id=$monday['slot_id'];
            echo '<br/><a href="#" onClick="view_link('.$id.'); return false;" class="btn btn-theme btn-xs float-left"><b>Thrive - '.$id.' ( '.$monday['count'].') </a>';
            break;
         }
        }
   }     
   elseif($monday['batch_type']==2)
   {
    echo 'Group';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $monday['count']==0){
            echo '<br/>Unassigned'.'('.$monday['count'].')';
             break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$monday['count'].')';
            break;
         }
   }
}
   else
   {
    echo 'NIL';
   }
   ?>
    </td>
                                  <?php 
                                }?>
                        
                            </tr>
                            <tr><td>Tuesday</td>
                            <?php
                                foreach ($tues_list as $tuesday) {
                        ?>
  <td> <?php if($tuesday['batch_type']==1)
  { echo 'Private';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $tuesday['count']==0){
            echo '<br/>Unassigned'.'('.$tuesday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$tuesday['count'].')';
            break;
         }
        }
   }     
   elseif($tuesday['batch_type']==2)
   {
    echo 'Group';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $tuesday['count']==0){
            echo '<br/>Unassigned'.'('.$tuesday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$tuesday['count'].')';
            break;
         }
        }
   }
   else
   {
    echo 'NIL';
   }
   ?>
    </td>
                                  <?php 
                                }?></tr>
                            <tr><td>Wednesday</td>
                            <?php
                                foreach ($wednes_list as $wednesday) {
                        ?>
  <td> <?php if($wednesday['batch_type']==1)
  { echo 'Private';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $wednesday['count']==0){
            echo '<br/>Unassigned'.'('.$wednesday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$wednesday['count'].')';
            break;
         }
        }
   }     
   elseif($wednesday['batch_type']==2)
   {
    echo 'Group';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $wednesday['count']==0){
            echo '<br/>Unassigned'.'('.$wednesday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$wednesday['count'].')';
            break;
         }
        }
   }
   else
   {
    echo 'NIL';
   }
   ?>
    </td>
                                  <?php 
                                }?>
                            </tr>
                            <tr><td>Thrusday</td>
                            <?php
                                foreach ($thurs_list as $thursday) {
                        ?>
  <td> <?php if($thursday['batch_type']==1)
  { echo 'Private';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!="" && $thursday['count']==0){
            echo '<br/>Unassigned'.'('.$thursday['count'].')';
        break;
         }
         else
         {
            $id=$thursday['slot_id'];
            echo '<br/><a href="#" onClick="view_link('.$id.'); return false;" class="btn btn-theme btn-xs float-left"><b>Thrive - '.$id.' ( '.$thursday['count'].')</a>';
                break;
         }
        }
   }     
   elseif($thursday['batch_type']==2)
   {
    echo 'Group';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $thursday['count']==0){
            echo '<br/>Unassigned'.'('.$thursday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$thursday['count'].')';
            break;
         }
        }
   }
   else
   {
    echo 'NIL';
   }
   ?>
    </td>
                                  <?php 
                                }?></tr>
                            <tr><td>Friday</td>
                            <?php
                                foreach ($fri_list as $friday) {
                        ?>
  <td> <?php if($friday['batch_type']==1)
  { echo 'Private';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $friday['count']==0){
            echo '<br/>Unassigned'.'('.$friday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$friday['count'].')';
            break;
         }
        }
   }     
   elseif($friday['batch_type']==2)
   {
    echo 'Group';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $friday['count']==0){
            echo '<br/>Unassigned'.'('.$friday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$friday['count'].')';
            break;
         }
        }
   }
   else
   {
    echo 'NIL';
   }
   ?>
    </td>
                                  <?php 
                                }?></tr>
                            <tr><td>Saturday</td>
                            <?php
                                foreach ($satur_list as $saturday) {
                        ?>
  <td> <?php if($saturday['batch_type']==1)
  {
       echo 'Private';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $saturday['count']==0){
            echo '<br/>Unassigned'.'('.$saturday['count'].')';
        break;
         }
         else
         {
            $id=$saturday['slot_id'];
            echo '<br/><a href="#" onClick="view_link('.$id.'); return false;" class="btn btn-theme btn-xs float-left"><b>Thrive - '.$id.' ( '.$saturday['count'].')  </a>';
            break;
        }
        }
   }     
   elseif($saturday['batch_type']==2)
   {
    echo 'Group';
    foreach ($batch_list as $batch) {
        if($batch['batch_id']!="" and $batch['slot_id']!=""  && $saturday['count']==0){
            echo '<br/>Unassigned'.'('.$saturday['count'].')';
        break;
         }
         else
         {
            echo '<br/>Assigned'.'('.$saturday['count'].')';
            break;
         }
        }
   }
   else
   {
    echo 'NIL';
   }
   ?>
    </td>
                                  <?php 
                                }?></tr>
                              
                    </tbody>  

</table>

    <p class="sub-banner-2 text-center">©️ 2020 Thrivepad</p>
</div>
</div>                          



<div class="modal" id="viewModal" data-backdrop="false">
                                <div class="modal-dialog" style="top:20% !important;">
                                    <div class="modal-content">

                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Users Assigned to Batch Thrive - 
                                            <h4 id="batchviid"></h4>
                                           
                             <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
                                                 </div>

                                        <!-- Modal body -->
                                        <div class="modal-body">
                                
                <div id="filterslot_data"></div>

                                        </div>

                                        <!-- Modal footer -->
                                        <div class="modal-footer">
                                       <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                         </div>

                                    </div>
                                </div>
                              </div>



    </div>
</div>


<script type="text/javascript">

 function view_link(slotviewid) {
    $('#viewModal').modal('show');
    filterslot_data(slotviewid);
 
  
};


function filterslot_data(slotviewid) {
            $('#filterslot_data').html("<div id='loading'>Loading...,</div>");
            var action = 'fetch_data';
            var slotfid = slotviewid;
           $.ajax({
                url: "<?php echo base_url() ?>/admin/fetchslot_data",
                method: "POST",
                dataType: "JSON",
                data: {
                    action: action,
                    slotid: slotfid,
                },
                success: function(data) {
                    $('#batchviid').text(slotfid);
              
              $('#filterslot_data').html(data.user_list);
              
                }

 },
 
 
 )
 };

 
</script>
