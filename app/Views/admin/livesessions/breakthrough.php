<div class="col-sm-12 col-md-12">
    <h4>Breakthrough</h4>
</div>
</div>
</div>

<style>
.property-box-2 {
    box-shadow: 0 0 10px 1px rgb(71 85 95 / 8%);
    -webkit-box-shadow: 0 0 10px 1px rgb(71 85 95 / 8%);
    -moz-box-shadow: 0 0 10px 1px rgba(71, 85, 95, .08);
    position: inherit;
    margin-left: 300px;
    margin-right: 20px;
}

.alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
    margin-left: 300px;
    margin-right: 20px;
}
</style>

<div class="property-box-2">
    <div class=" row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>BREAKTHROUGH</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <form action="<?= base_url() ?>/live-sessions/insertbreakthrough" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('breakthrough')) {
                                $val = set_value('breakthrough');
                            } elseif (($breakthrough[0]['breakthrough'])) {
                                $breakthrough = $breakthrough[0]['breakthrough'];
                            }
                            ?>
                            <textarea class="form-control" id="breakthrough" name="breakthrough"
                                rows="10"><?php echo $breakthrough; ?></textarea>
                        </div>
                </div>

                <div class="row  mt-2 ml-3">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                    BREAKTHROUGH</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>