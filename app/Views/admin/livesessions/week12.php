<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 12 </h4>
</div>
</div>
</div>
<div class="container mb-40 col-md-4">
    <div class="row text-center">
        <div class="col-sm">
            <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                Let's go Back</a>
        </div>
        <div class="col-sm">
            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                <input type="hidden" name="tab" value="" />

                <input type="hidden" name="url"
                    value="<?= base_url() ?>/admin/live-sessions/week12/<?php echo $profiledata['id'] ?>" />

                <br /> <input type="submit" class="btn-md btn-danger" value="Redo" />
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>

<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>