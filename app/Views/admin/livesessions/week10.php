<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 10 </h4>
</div>
</div>
</div>

<!--Live Session-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 10</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row  mt-2 ml-3">
                    <div class="col-md-4">
                        <div class="form-group clearfix">
                            <!-- <button onclick="" type="submit" class="btn-md btn-theme float-left p-3">OPEN THE
                                SESSION</button> -->
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>YOUR SUCCESS BLUE PRINT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <h6>Write down all your 6 Base-Values</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $val = set_value('q1');
                            } elseif (($blueprint[0]['q1'])) {
                                $blueprintq1 = $blueprint[0]['q1'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q1" name="q1"
                                rows="3"><?php echo $blueprintq1; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <input type="submit" class="btn-md btn-theme float-left p-3"
                                                    value="SUBMIT BASEVALUE"> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class=" form-group col-md-12">
                            <h6>List all your P-codes</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $val = set_value('q2');
                            } elseif (($blueprint[0]['q2'])) {
                                $blueprintq2 = $blueprint[0]['q2'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q2" name="q2"
                                rows="3"><?php echo $blueprintq2; ?></textarea>

                            <div class=" row mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    P-CODES</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>List your ultimate SMART GOAL
                            </h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $val = set_value('q3');
                            } elseif (($blueprint[0]['q3'])) {
                                $blueprintq3 = $blueprint[0]['q3'];

                                $attr = 'disabled';
                            }
                            ?>


                            <textarea disabled class="form-control" id="q3" name="q3"
                                rows="3"><?php echo $blueprintq3; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    SMART GOALS</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>List priority steps to reach the SMART GOAL</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $val = set_value('q4');
                            } elseif (($blueprint[0]['q4'])) {
                                $blueprintq4 = $blueprint[0]['q4'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q4" name="q4"
                                rows="3"><?php echo $blueprintq4; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    SMART GOALS</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group col-md-12">
                            <h6>Write down top 3 desires to be fulfilled in next 3 years</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q5')) {
                                $val = set_value('q5');
                            } elseif (($blueprint[0]['q5'])) {
                                $blueprintq5 = $blueprint[0]['q5'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q5" name="q5"
                                rows="3"><?php echo $blueprintq5; ?></textarea>


                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    DESIRES</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <h6>Write down top 3 desires to be fulfilled in next 5 years</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q6')) {
                                $val = set_value('q6');
                            } elseif (($blueprint[0]['q6'])) {
                                $blueprintq6 = $blueprint[0]['q6'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q6" name="q6"
                                rows="3"><?php echo $blueprintq6; ?></textarea>


                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    DESIRES</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group col-md-12">
                            <h6>List your mastermind circle and support team who empower you to take
                                action</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q7')) {
                                $val = set_value('q7');
                            } elseif (($blueprint[0]['q7'])) {
                                $blueprintq7 = $blueprint[0]['q7'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q7" name="q7"
                                rows="3"><?php echo $blueprintq7; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    MASTERMIND</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>Write your ultimate definition of success, what it means to you? and
                                what will be
                                different? How life will be different when you will be successful?

                            </h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q8')) {
                                $val = set_value('q8');
                            } elseif (($blueprint[0]['q8'])) {
                                $blueprintq8 = $blueprint[0]['q8'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q8" name="q8"
                                rows="3"><?php echo $blueprintq8; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    SUCCESS</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>Anything else you know that needs to change that’s left out?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q9')) {
                                $val = set_value('q9');
                            } elseif (($blueprint[0]['q9'])) {
                                $blueprintq9 = $blueprint[0]['q9'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q9" name="q9"
                                rows="3"><?php echo $blueprintq9; ?></textarea>


                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    NEEDS</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-group col-md-12">
                            <h6>What new skills do you need to learn?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q10')) {
                                $val = set_value('q10');
                            } elseif (($blueprint[0]['q10'])) {
                                $blueprintq10 = $blueprint[0]['q10'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q10" name="q10"
                                rows="3"><?php echo $blueprintq10; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    NEW SKILLS</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>Which part of your character needs to improve? </h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q11')) {
                                $val = set_value('q11');
                            } elseif (($blueprint[0]['q11'])) {
                                $blueprintq11 = $blueprint[0]['q11'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q11" name="q11"
                                rows="3"><?php echo $blueprintq11; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    CHARACTER</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>Write down your power statement?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q12')) {
                                $val = set_value('q12');
                            } elseif (($blueprint[0]['q12'])) {
                                $blueprintq12 = $blueprint[0]['q12'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q12" name="q12"
                                rows="3"><?php echo $blueprintq12; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    STATEMENT</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <h6>What additional P-codes do you need to embrace to achieve your
                                Desires? These
                                P-codes should be based on your vision board.</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q13')) {
                                $val = set_value('q13');
                            } elseif (($blueprint[0]['q13'])) {
                                $blueprintq13 = $blueprint[0]['q13'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q13" name="q13"
                                rows="3"><?php echo $blueprintq13; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    P-CODES</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="form-group col-md-12">
                            <h6>How will you continue to use the personal insights gained through
                                this program in
                                the future?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q14')) {
                                $val = set_value('q14');
                            } elseif (($blueprint[0]['q14'])) {
                                $blueprintq14 = $blueprint[0]['q14'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q14" name="q14"
                                rows="3"><?php echo $blueprintq14; ?></textarea>

                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                                    FUTURE</button> -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row  mt-2 ml-3">
                                <div class="clearfix px-3">
                                    <div class="pull-left">
                                        <div class="form-group clearfix">
                                            <?php if ($blueprint) { ?>
                                            <a href="<?= base_url() ?>/admin/report/<?= $profiledata['id']; ?>"
                                                target="_blank" class="btn-md btn-theme float-left p-3">GENERATE
                                                BLUEPRINT
                                                Moment</a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="container col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="blueprint" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week10/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>