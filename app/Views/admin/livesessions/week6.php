<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 6 </h4>
</div>
</div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>OUTCOME REINFORCEMENT TOOL</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <a href="<?= base_url() ?>/admin//outcome-reinforcement/<?php echo $profiledata['id'] ?>"
                                    target="_blank" class="btn-md btn-theme float-left p-3">OPEN OUTCOME
                                    REFINEMENT TOOL</a>

                            </div>
                        </div>
                    </div>
                </div>
                <br /><br />
                <div class="container mt-4 col-md-12">
                    <div class="row" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="outcome" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week6/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>OVERCOMING CHALLENGES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5>
                        Convert your top 3 refined desires into SMART goals
                    </h5>
                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('desire')) {
                            $val = set_value('desire');
                        } elseif (($challenge[0]['desire'])) {
                            $desire = $challenge[0]['desire'];

                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="desire" name="desire"
                            rows="3"><?php echo $desire; ?></textarea>

                    </div>
                    <div class="form-group">
                        <h5 class="mt-4">
                            Express and check if your Base-Values are satisfied in your SMART goals and what can you
                            do for your Base-Values to be satisfied?
                        </h5>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('baseval')) {
                            $val = set_value('baseval');
                        } elseif (($challenge[0]['baseval'])) {
                            $baseval = $challenge[0]['baseval'];

                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="baseval" name="baseval"
                            rows="3"><?php echo $baseval; ?></textarea>

                    </div>
                    <h5 class="mt-4">
                        List Challenges that might come in way while achieving your Top 3 SMART goals

                    </h5>
                    <div class="row">
                        <?php for ($i = 0; $i <= 2; $i++) { ?>
                        <div class="form-group col-md-12">

                            <?php
                                $val = '';
                                $attr = '';
                                if (set_value('achieve')) {
                                    $val = set_value('achieve');
                                } elseif (($challenge[0]['achieve'])) {
                                    $achieve = $challenge[0]['achieve'];
                                    $achieve = explode('~', $achieve);

                                    $attr = 'disabled';
                                }
                                ?>
                            <textarea disabled class="form-control" id="achieve[]" name="achieve[]"
                                rows="3"><?php echo $achieve[$i]; ?></textarea>

                        </div>
                        <?php } ?>
                    </div>
                    <h5 class="mt-4">Write what N-codes come to your mind when you think of these challenges
                        and
                        SMART goals</h5>

                    <div class="row">
                        <?php for ($i = 0; $i <= 5; $i++) { ?>
                        <div class="form-group col-md-6">

                            <?php
                                $val = '';
                                $attr = '';
                                if (set_value('challenges')) {
                                    $val = set_value('challenges');
                                } elseif (($challenge[0]['challenges'])) {
                                    $challenges = $challenge[0]['challenges'];
                                    $challenges = explode('~', $challenges);

                                    $attr = 'disabled';
                                }
                                ?>
                            <textarea disabled class="form-control" id="challenges[]" name="challenges[]"
                                rows="3"><?php echo $challenges[$i]; ?></textarea>
                        </div>
                        <?php } ?>
                    </div>
                    <h5 class="mt-4">Convert them into powerful P-codes</h5>
                    <div class="row">
                        <?php for ($i = 0; $i <= 7; $i++) { ?>
                        <div class="form-group col-md-6">
                            <?php
                                $val = '';
                                $attr = '';
                                if (set_value('pcodes')) {
                                    $val = set_value('pcodes');
                                } elseif (($challenge[0]['pcodes'])) {
                                    $pcodes = $challenge[0]['pcodes'];
                                    $pcodes = explode('~', $pcodes);

                                    $attr = 'disabled';
                                }
                                ?>
                            <textarea disabled class="form-control" id="pcodes[]" name="pcodes[]"
                                rows="3"><?php echo $pcodes[$i]; ?></textarea>
                        </div>
                        <?php } ?>
                    </div>

                    <h5 class="mt-4">Brainstorm and list down various ways to overcome challenges</h5>
                    <div class="row">
                        <?php for ($i = 0; $i <= 7; $i++) { ?>
                        <div class="form-group col-md-6">
                            <?php
                                $val = '';
                                $attr = '';
                                if (set_value('brainstorm')) {
                                    $val = set_value('brainstorm');
                                } elseif (($challenge[0]['brainstorm'])) {
                                    $brainstorm = $challenge[0]['brainstorm'];
                                    $brainstorm = explode('~', $brainstorm);

                                    $attr = 'disabled';
                                }
                                ?>
                            <textarea disabled class="form-control" id="brainstorm[]" name="brainstorm[]"
                                rows="3"><?php echo $brainstorm[$i]; ?></textarea>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-40 col-md-12">
        <div class="row text-center" style="float:right">
            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
            <div class="col-sm mb-3">
                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                    <input type="hidden" name="tab" value="challengeovercome" />
                    <input type="hidden" name="url"
                        value="<?= base_url() ?>/admin/live-sessions/week6/<?php echo $profiledata['id'] ?>" />
                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                </form>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>SUCCESS ALIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('achieved')) {
                            $val = set_value('achieved');
                        } elseif (($success[0]['achieved'])) {
                            $achieved = $success[0]['achieved'];

                            $attr = 'disabled';
                        }
                        ?>
                        <h5>
                            How will you feel when your goal is achieved? What will be different in your life?
                        </h5>
                        <textarea disabled class="form-control" id="achieved" name="achieved"
                            rows="3"><?php echo $achieved; ?></textarea>
                    </div>
                    <div class="form-group">

                        <h5>
                            Do you have the skills and the tools to accomplish your goals?
                        </h5>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('skills')) {
                            $val = set_value('skills');
                        } elseif (($success[0]['skills'])) {
                            $skills = $success[0]['skills'];

                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="skills" name="skills"
                            rows="3"><?php echo $skills; ?></textarea>

                    </div>
                    <div class="form-group">

                        <h5>
                            Is it realistic and achievable? Has someone done it before?
                        </h5>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('successachieve')) {
                            $val = set_value('successachieve');
                        } elseif (($success[0]['successachieve'])) {
                            $successachieve = $success[0]['successachieve'];

                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="successachieve" name="successachieve"
                            rows="3"><?php echo $successachieve; ?></textarea>


                    </div>
                    <div class="form-group">

                        <h5>
                            What date is it going to be done? </h5>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('successdate')) {
                            $val = set_value('successdate');
                        } elseif (($success[0]['successdate'])) {
                            $successdate = $success[0]['successdate'];

                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="successdate" name="successdate"
                            rows="3"><?php echo $successdate;  ?></textarea>

                    </div>
                    <div class="form-group">

                        <h5>
                            Vibration precedes manifestation, hence feel like your goal has already been
                            achieved.
                            This trains the subconscious mind to accept the outcome as real, which helps you to
                            move
                            more effectively towards it. </h5>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-40 col-md-12">
        <div class="row text-center" style="float:right">
            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
            <div class="col-sm mb-3">
                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                    <input type="hidden" name="tab" value="successalign" />
                    <input type="hidden" name="url"
                        value="<?= base_url() ?>/admin/live-sessions/week6/<?php echo $profiledata['id'] ?>" />
                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                </form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>