<style>
ul li a {
    margin: 0;
    padding: 0;
    list-style: none;
    color: #FFF;
}

ul li a:hover {
    color: #FFF;
}
</style>
<div class="dashboard-content">
    <div class="dashboard-header clearfix">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <nav class="navbar navbar-expand-sm mb-5" style="background-color:#545454">
                    <ul class="navbar-nav ml-auto mr-auto navbar-expand"
                        style="align-items: stretch; justify-content: space-between; width: 100%;">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?= base_url() ?>/admin/user_profile/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 1) {
                                                                                                                                    echo 'color:#FFFFFF;background-color:#f5832c';
                                                                                                                                } ?>">
                                WEEK 1
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week2/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 2) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 2
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week3/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 3) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 3
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week4/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 4) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 4
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week5/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 5) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 5
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week6/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 6) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 6
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week7/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 7) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 7
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week8/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 8) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 8
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week9/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 9) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 9
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week10/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 10) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 10
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week11/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 11) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 11
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link"
                                href="<?= base_url() ?>/admin/live-sessions/week12/<?= $profiledata['id']; ?>"
                                style="<?php if ($live_session == 12) {
                                                                                                                                            echo 'color:#FFFFFF;background-color:#f5832';
                                                                                                                                        } ?>">
                                WEEK 12
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>