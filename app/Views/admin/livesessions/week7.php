<style>
.alert {
    text-transform: uppercase;
    font-size: 12px;
    border-radius: 0;
    margin-bottom: 10px;
    padding: 18px 20px;
    width: 100%;
}
</style>

<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 7 </h4>
</div>
</div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>REALISTIC FUTURE </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5><b>ACTION PLAN - PRIORITY STEPS</b></h5>
                    <h6>Life is a succession of lessons which must be lived to be understood - Ralph Waldo Emerson</h6>


                    <br />

                    <?php
                    $val = '';
                    $attr = '';
                    if (set_value('potential_priority1')) {
                        $val = set_value('potential_priority1');
                    } elseif (($outcome[0]['potential_priority1'])) {
                        $potential_priority1 = $outcome[0]['potential_priority1'];
                        $potential_priority2 = $outcome[0]['potential_priority2'];
                        $potential_priority3 = $outcome[0]['potential_priority3'];
                    }
                    $q = 0;
                    $t = 0;
                    $u = 0;
                    for ($s = 0; $s <= 2; $s++) {
                        if ($s == 0) {
                    ?>
                    <h5>
                        <label for="potential_priority[]">
                            <input type="hidden" name="potential_priority[]" id="potential_priority[]"
                                value="<?php echo $potential_priority3 ?>" />
                            <h6 style="text-transform:uppercase"> DESIRES : <b><?php echo $potential_priority3 ?>
                                </b>
                            </h6>
                        </label>

                    </h5>
                    <?php if (isset($smart) && empty($smart)) {
                            ?>
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="10">
                                    <b class="top_span">Smart Goals :</b>
                                </th>

                            </tr>

                        </thead>
                        <tr>
                            <th>PS</th>
                            <th>Steps to be taken</th>
                            <th>Who is responsible</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Resources required</th>
                            <th>Challenges for this PS</th>
                            <th>Outcome of this PS</th>
                            <!-- <th>Actions</th> -->

                        </tr>

                        <tbody id="tbody">
                            <?php for ($a = 1; $a <= 2; $a++) { ?>
                            <tr class="highlight">
                                <td><?php echo $a; ?>
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="taken[]" id="taken">
                                </td>
                                <td>
                                    <input disabled disabled type="text" class="form-control" name="response[]"
                                        id="response">
                                </td>
                                <td>
                                    <input disabled type='text' class='form-control' name='start_date[]'
                                        id="start_date">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="end_date[]" id="end_date">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="resource[]" id="resource">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="challenges[]"
                                        id="challenges">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="outcome[]" id="outcome">
                                </td>
                                <!-- <td>
                                        <button type="button" disabled onclick="deleteRow(this);"
                                            class=" btn-md btn-theme" disabled>
                                            Delete</button>

                                    </td> -->
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php }
                            if (!empty($smart)) {
                                $val = '';
                                $attr = '';
                                if (set_value('taken')) {
                                    $val = set_value('taken');
                                } elseif (($smart[0]['taken'])) {
                                    $taken = $smart[0]['taken'];
                                    $response = $smart[0]['response'];
                                    $start_date = $smart[0]['start_date'];
                                    $end_date = $smart[0]['end_date'];
                                    $resource = $smart[0]['resource'];
                                    $challenges = $smart[0]['challenges'];
                                    $outcome = $smart[0]['outcome'];

                                    $taken = explode('~', $taken);
                                    $response = explode('~', $response);
                                    $start_date = explode('~', $start_date);
                                    $end_date = explode('~', $end_date);
                                    $resource = explode('~', $resource);
                                    $challenges = explode('~', $challenges);
                                    $outcome = explode('~', $outcome);

                                    $attr = 'disabled';
                                ?>
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="10">
                                    <b class="top_span">Smart Goals :</b>


                                </th>

                            </tr>

                        </thead>
                        <tr>
                            <th>PS</th>
                            <th>Steps to be taken</th>
                            <th>Who is responsible</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Resources required</th>
                            <th>Challenges for this PS</th>
                            <th>Outcome of this PS</th>


                        </tr>

                        <tbody id="tbody">
                            <?php

                                            for ($i = 0; $i < count($taken); $i++) {
                                                $q++;                       ?>
                            <tr class="highlight">

                                <td><?php echo $q; ?>
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" value="<?php echo $taken[$i]; ?>"
                                        name=" taken[]" id="taken[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="response[]"
                                        value="<?php echo $response[$i]; ?>" id="response[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="start_date[]"
                                        value="<?php echo $start_date[$i]; ?>" id="start_date[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="end_date[]"
                                        value="<?php echo $end_date[$i]; ?>" id="end_date[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="resource[]"
                                        value="<?php echo $resource[$i]; ?>" id="resource[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="challenges[]"
                                        value="<?php echo $challenges[$i]; ?>" id="challenges[]">
                                </td>
                                <td>
                                    <input type="text" disabled class="form-control calc" name="outcome[]"
                                        value="<?php echo $outcome[$i]; ?>" id="outcome[]">
                                </td>

                            </tr>
                            <?php }
                                            ?>
                        </tbody>
                    </table>
                    <?php
                                }
                            }
                        }
                        if ($s == 1) {
                            ?>
                    <h5>
                        <label for="potential_priority[]">
                            <input type="hidden" name="potential_priority[]" id="potential_priority[]"
                                value="<?php echo $potential_priority1 ?>" />
                            <h6 style="text-transform:uppercase"> DESIRES : <b><?php echo $potential_priority1 ?>
                                </b>
                            </h6>
                        </label>

                    </h5>
                    <?php if (isset($smart) && empty($smart)) {
                            ?>
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="10">
                                    <b class="top_span">Smart Goals :</b>

                                    <!-- <button disabled type="button" onClick="addItem1();"
                                            class="btn-md btn-theme float-right">Add Smart
                                            Goals</button> -->

                                </th>

                            </tr>

                        </thead>
                        <tr>
                            <th>PS</th>
                            <th>Steps to be taken</th>
                            <th>Who is responsible</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Resources required</th>
                            <th>Challenges for this PS</th>
                            <th>Outcome of this PS</th>
                            <!-- <th>Actions</th> -->

                        </tr>

                        <tbody id="tbody1">
                            <?php for ($a = 1; $a <= 2; $a++) { ?>
                            <tr class="highlight">
                                <td><?php echo $a; ?>
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="taken1[]" id="taken1">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="response1[]" id="response1">
                                </td>
                                <td>
                                    <input disabled type='text' class='form-control' name='start_date1[]'
                                        id="start_date1">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="end_date1[]" id="end_date1">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="resource1[]" id="resource1">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="challenges1[]"
                                        id="challenges1">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="outcome1[]" id="outcome1">
                                </td>
                                <!-- <td>
                                        <button disabled type="button" onclick="deleteRow(this);"
                                            class=" btn-md btn-theme" disabled>
                                            Delete</button>

                                    </td> -->
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php }
                            if (!empty($smart)) {
                                $val = '';
                                $attr = '';
                                if (set_value('taken1')) {
                                    $val = set_value('taken1');
                                } elseif (($smart[0]['taken1'])) {
                                    $taken1 = $smart[0]['taken1'];
                                    $response1 = $smart[0]['response1'];
                                    $start_date1 = $smart[0]['start_date1'];
                                    $end_date1 = $smart[0]['end_date1'];
                                    $resource1 = $smart[0]['resource1'];
                                    $challenges1 = $smart[0]['challenges1'];
                                    $outcome1 = $smart[0]['outcome1'];

                                    $taken1 = explode('~', $taken1);
                                    $response1 = explode('~', $response1);
                                    $start_date1 = explode('~', $start_date1);
                                    $end_date1 = explode('~', $end_date1);
                                    $resource1 = explode('~', $resource1);
                                    $challenges1 = explode('~', $challenges1);
                                    $outcome1 = explode('~', $outcome1);

                                    $attr = 'disabled';
                                ?>
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="10">
                                    <b class="top_span">Smart Goals :</b>


                                </th>

                            </tr>

                        </thead>
                        <tr>
                            <th>PS</th>
                            <th>Steps to be taken</th>
                            <th>Who is responsible</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Resources required</th>
                            <th>Challenges for this PS</th>
                            <th>Outcome of this PS</th>

                        </tr>

                        <tbody id="tbody1">
                            <?php

                                            for ($i = 0; $i < count($taken1); $i++) {
                                                $t++;
                                            ?>
                            <tr class="highlight">
                                <td><?php echo $t; ?>
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" value="<?php echo $taken1[$i]; ?>"
                                        name=" taken1[]" id="taken1[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="response1[]"
                                        value="<?php echo $response1[$i]; ?>" id="response1[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="start_date1[]"
                                        value="<?php echo $start_date1[$i]; ?>" id="start_date1[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="end_date1[]"
                                        value="<?php echo $end_date1[$i]; ?>" id="end_date1[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="resource1[]"
                                        value="<?php echo $resource1[$i]; ?>" id="resource1[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="challenges1[]"
                                        value="<?php echo $challenges1[$i]; ?>" id="challenges1[]">
                                </td>
                                <td>
                                    <input type="text" disabled class="form-control calc" name="outcome1[]"
                                        value="<?php echo $outcome1[$i]; ?>" id="outcome1[]">
                                </td>
                            </tr>
                            <?php }
                                            ?>
                        </tbody>
                    </table>
                    <?php
                                }
                            }
                        }
                        if ($s == 2) {
                            ?>
                    <h5>
                        <label for="potential_priority[]">
                            <input type="hidden" name="potential_priority[]" id="potential_priority[]"
                                value="<?php echo $potential_priority2 ?>" />
                            <h6 style="text-transform:uppercase"> DESIRES : <b><?php echo $potential_priority2 ?>
                                </b>
                            </h6>
                        </label>

                    </h5>
                    <?php if (isset($smart) && empty($smart)) {
                            ?>
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="10">
                                    <b class="top_span">Smart Goals :</b>

                                    <!-- <button disabled type="button" onClick="addItem2();"
                                            class="btn-md btn-theme float-right">Add Smart
                                            Goals</button> -->

                                </th>

                            </tr>

                        </thead>
                        <tr>
                            <th>PS</th>
                            <th>Steps to be taken</th>
                            <th>Who is responsible</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Resources required</th>
                            <th>Challenges for this PS</th>
                            <th>Outcome of this PS</th>
                            <!-- <th>Actions</th> -->

                        </tr>

                        <tbody id="tbody2">
                            <?php
                                        for ($a = 1; $a <= 2; $a++) {
                                        ?>
                            <tr class="highlight">
                                <td><?php echo $a; ?>
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="taken2[]" id="taken2">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="response2[]" id="response2">
                                </td>
                                <td>
                                    <input disabled type='text' class='form-control' name='start_date2[]'
                                        id="start_date2">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="end_date2[]" id="end_date2">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="resource2[]" id="resource2">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="challenges2[]"
                                        id="challenges2">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="outcome2[]" id="outcome2">
                                </td>
                                <!-- <td>
                                        <button type="button" disabled onclick="deleteRow(this);"
                                            class=" btn-md btn-theme" disabled>
                                            Delete</button>

                                    </td> -->
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <?php }
                            if (!empty($smart)) {
                                $val = '';
                                $attr = '';
                                if (set_value('taken2')) {
                                    $val = set_value('taken2');
                                } elseif (($smart[0]['taken2'])) {
                                    $taken2 = $smart[0]['taken2'];
                                    $response2 = $smart[0]['response2'];
                                    $start_date2 = $smart[0]['start_date2'];
                                    $end_date2 = $smart[0]['end_date2'];
                                    $resource2 = $smart[0]['resource2'];
                                    $challenges2 = $smart[0]['challenges2'];
                                    $outcome2 = $smart[0]['outcome2'];

                                    $taken2 = explode('~', $taken2);
                                    $response2 = explode('~', $response2);
                                    $start_date2 = explode('~', $start_date2);
                                    $end_date2 = explode('~', $end_date2);
                                    $resource2 = explode('~', $resource2);
                                    $challenges2 = explode('~', $challenges2);
                                    $outcome2 = explode('~', $outcome2);

                                    $attr = 'disabled';

                                ?>
                    <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                        <thead>
                            <tr>
                                <th></th>
                                <th colspan="10">
                                    <b class="top_span">Smart Goals :</b>


                                </th>

                            </tr>

                        </thead>
                        <tr>
                            <th>PS</th>
                            <th>Steps to be taken</th>
                            <th>Who is responsible</th>
                            <th>Start date</th>
                            <th>Due date</th>
                            <th>Resources required</th>
                            <th>Challenges for this PS</th>
                            <th>Outcome of this PS</th>

                        </tr>

                        <tbody id="tbody2">
                            <?php

                                            for ($i = 0; $i < count($taken2); $i++) {
                                                $u++;
                                            ?>
                            <tr class="highlight">
                                <td><?php echo $u; ?>
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" value="<?php echo $taken2[$i]; ?>"
                                        name=" taken2[]" id="taken2[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="response2[]"
                                        value="<?php echo $response2[$i]; ?>" id="response2[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control" name="start_date2[]"
                                        value="<?php echo $start_date2[$i]; ?>" id="start_date2[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="end_date2[]"
                                        value="<?php echo $end_date2[$i]; ?>" id="end_date2[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="resource2[]"
                                        value="<?php echo $resource2[$i]; ?>" id="resource2[]">
                                </td>
                                <td>
                                    <input disabled type="text" class="form-control calc" name="challenges2[]"
                                        value="<?php echo $challenges2[$i]; ?>" id="challenges2[]">
                                </td>
                                <td>
                                    <input type="text" disabled class="form-control calc" name="outcome2[]"
                                        value="<?php echo $outcome2[$i]; ?>" id="outcome2[]">
                                </td>

                            </tr>
                            <?php }
                                            ?>
                        </tbody>
                    </table>
                    <?php
                                }
                            }
                        }
                        ?>


                    <?php     }
                    ?>


                    <small id="exp_error" class="text-center"> </small>


                    <button disabled type="button" style="display:none;" id="show_result" data-toggle="modal"
                        data-target="#stepsModal" data-backdrop="static" data-keyboard="false">Result</button>
                    <div class="container mb-40 col-md-12">
                        <div class="row text-center" style="float:right">
                            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                            <div class="col-sm mb-3">
                                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>"
                                    method="POST">
                                    <input type="hidden" name="tab" value="smartgoals" />
                                    <input type="hidden" name="url"
                                        value="<?= base_url() ?>/admin/live-sessions/week7/<?php echo $profiledata['id'] ?>" />
                                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <a href="#" tabindex="0">


                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HOME ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <h6>
                        <b> Past Decisions</b> – Think of some decisions in past which were not correct and it
                        adversely
                        affected you.
                    </h6>
                    <?php
                    $val = '';
                    $attr = '';
                    if (set_value('decision')) {
                        $val = set_value('decision');
                    } elseif (($decision[0]['decision'])) {
                        $decision = $decision[0]['decision'];

                        $attr = 'disabled';
                    }
                    ?>
                    <textarea disabled class="form-control" id="decision" name="decision"
                        rows="3"><?php echo $decision; ?></textarea>

                    <div class="footer">
                        <a href="#" tabindex="0">


                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-40 col-md-12">
        <div class="row text-center" style="float:right">
            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
            <div class="col-sm mb-3">
                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                    <input type="hidden" name="tab" value="decision" />
                    <input type="hidden" name="url"
                        value="<?= base_url() ?>/admin/live-sessions/week7/<?php echo $profiledata['id'] ?>" />
                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
var items = 2;
var items1 = 2;
var items2 = 2;


function addItem() {

    items++;
    var html = "<tr>";
    html += "<td>" + items + "</td>";
    html += "<td><input disabled type ='text' class='form-control' name='taken[]'></td> ";
    html += "<td><input disabled type ='text' class='form-control' name='response[]'></td> ";
    html += "<td><input disabled type='text' class='form-control' name='start_date[]'></td>";
    html += "<td><input disabled type ='text' class='form-control' name='end_date[]'></td> ";
    html += "<td><input disabled type ='text' class='form-control' name='resource[]'></td> ";
    html += "<td><input disabled type='text' class='form-control' name='challenges[]'></td>";
    html += "<td><input disabled type='text' class='form-control' name='outcome[]'></td>";
    html +=
        // "<td> <button disabled type='button' onclick='deleteRow(this);' class = 'btn-md btn-theme float-right'>Delete</button></td > ";
        html += "</tr>";


    var row = document.getElementById("tbody").insertRow();
    row.innerHTML = html;

}

function addItem1() {

    items1++;
    var html = "<tr>";
    html += "<td>" + items1 + "</td>";
    html += "<td><input disabled type ='text' class='form-control' name='taken1[]'></td> ";
    html += "<td><input disabled type ='text' class='form-control' name='response1[]'></td> ";
    html += "<td><input disabled type='text' class='form-control' name='start_date1[]'></td>";
    html += "<td><input disabled type ='text' class='form-control' name='end_date1[]'></td> ";
    html += "<td><input disabled type ='text' class='form-control' name='resource1[]'></td> ";
    html += "<td><input disabled type='text' class='form-control' name='challenges1[]'></td>";
    html += "<td><input disabled type='text' class='form-control' name='outcome1[]'></td>";
    html +=
        // "<td> <button disabled type='button' onclick='deleteRow(this);' class = 'btn-md btn-theme float-right'>Delete</button></td > ";
        html += "</tr>";


    var row = document.getElementById("tbody1").insertRow();
    row.innerHTML = html;

}

function addItem2() {

    items2++;
    var html = "<tr>";
    html += "<td>" + items2 + "</td>";
    html += "<td><input disabled type ='text' class='form-control' name='taken2[]'></td> ";
    html += "<td><input disabled type ='text' class='form-control' name='response2[]'></td> ";
    html += "<td><input disabled type='text' class='form-control' name='start_date2[]'></td>";
    html += "<td><input disabled type ='text' class='form-control' name='end_date2[]'></td> ";
    html += "<td><input disabled type ='text' class='form-control' name='resource2[]'></td> ";
    html += "<td><input disabled type='text' class='form-control' name='challenges2[]'></td>";
    html += "<td><input disabled type='text' class='form-control' name='outcome2[]'></td>";
    html +=
        // "<td> <button disabled type='button' onclick='deleteRow(this);' class = 'btn-md btn-theme float-right'>Delete</button></td > ";
        html += "</tr>";


    var row = document.getElementById("tbody2").insertRow();
    row.innerHTML = html;

}


function deleteRow(button) {
    button.parentElement.parentElement.remove();
}
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>