<div class="col-sm-12 col-md-12">
    <h4>Conclave</h4>
</div>
</div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>PERSONALITY TEST RESULT (QUESTIONNAIRE) </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="q1">
                                <h6>What did you discover about yourself through the personality test?</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $q1 = set_value('q1');
                            } elseif (($pq[0]['q1'])) {
                                $q1 = $pq[0]['q1'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled required <?php echo $attr;  ?> class="form-control" id="q1" name="q1"
                                rows="2"><?php echo $q1; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds1">
                                <h6>How different was it from your illusion of your oneself?</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $q2 = set_value('q2');
                            } elseif (($pq[0]['q2'])) {
                                $q2 = $pq[0]['q2'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled required <?php echo $attr;  ?> class="form-control" id="q2" name="q2"
                                rows="2"><?php echo $q2; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds3">
                                <h6>What changes do you wanna adapt to after going through the perusality test?</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $q3 = set_value('q3');
                            } elseif (($pq[0]['q3'])) {
                                $q3 = $pq[0]['q3'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled required <?php echo $attr;  ?> class="form-control" id="q3" name="q3"
                                rows="2"><?php echo $q3; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds4">
                                <h6>What traits do you feel connects you the most to your true self?</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $q4 = set_value('q4');
                            } elseif (($pq[0]['q4'])) {
                                $q4 = $pq[0]['q4'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled required <?php echo $attr;  ?> class="form-control" id="q4" name="q4"
                                rows="2"><?php echo $q4; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="q5">
                                <h6>Define the Aware you ?</h6><br />
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q5')) {
                                $q5 = set_value('q5');
                            } elseif (($pq[0]['q5'])) {
                                $q5 = $pq[0]['q5'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled required <?php echo $attr;  ?> class="form-control" id="q5" name="q5"
                                rows="2"><?php echo $q5; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="q6">
                                <h6>List three incidents from your life which you would react differently after
                                    being aware about your oneself!</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q6')) {
                                $q6 = set_value('q6');
                            } elseif (($pq[0]['q6'])) {
                                $q6 = $pq[0]['q6'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled required <?php echo $attr;  ?> class="form-control" id="q6" name="q6"
                                rows="2"><?php echo $q6; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="questionnaire" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/user_profile/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Wheel Of Life-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>Wheel Of Life</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Wheel of life represents your current state of life. You’ll have to follow sequence of steps
                        which will form Your unique wheel of life.
                    </h6>
                    <h5> Outcome :</h5>
                    <h6 style="text-align: justify;" class="pr-6">
                        Outcome of this activity is to find which areas of your life are strong at present and which
                        ones need serious attention. The activity will highlight your current status which will play a
                        vital role in coaching! It’s quintessential to be aware of our current status to experience and
                        acknowledge magnificent transformation in the process of Coaching.
                    </h6>
                </div>
                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <a class="btn btn-theme btn-md" target="_blank"
                                    href="<?= base_url() ?>/admin/wheel-of-life/<?php echo $profiledata['id'] ?>">
                                    WHEEL OF LIFE TOOL</a>
                            </div>
                        </div>
                    </div>
                </div>
                <br /><br />
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="wheeloflife" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/user_profile/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Wheel Of Life questionnaire-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>WOL QUESTIONNAIRE </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Answer the following question with reference to the wheel of life tool Results
                    </h6>
                    <div class="form-group">
                        <label for="questionnaire1">
                            <h6>1) Which areas of your life are the strongest?</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('questionnaire1')) {
                            $val = set_value('questionnaire1');
                        } elseif (($q[0]['q1'])) {
                            $val = $q[0]['q1'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="questionnaire1" name="questionnaire1"
                            rows="2"><?php echo $val; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="questionnaire2">
                            <h6>2) Which areas of put life are the weakest?</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('questionnaire2')) {
                            $val = set_value('questionnaire2');
                        } elseif (($q[0]['q2'])) {
                            $val = $q[0]['q2'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="questionnaire2" name="questionnaire2"
                            rows="2"><?php echo $val; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="questionnaire3">
                            <h6>3) Which area do you feel you critically need to work on?</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('questionnaire3')) {
                            $val = set_value('questionnaire3');
                        } elseif (($q[0]['q3'])) {
                            $val = $q[0]['q3'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="questionnaire3" name="questionnaire3"
                            rows="2"><?php echo $val; ?></textarea>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="wolquestionnaire" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/user_profile/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Celebrity speech-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>CELEBRATING YOU - TAKE AWAY ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('answer')) {
                            $val = set_value('answer');
                        } elseif (($c[0]['answer'])) {
                            $val = $c[0]['answer'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled <?php echo $attr;  ?> class="form-control" id="answer" name="answer"
                            rows="4"><?php echo $val; ?></textarea>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="celebratingyou" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/user_profile/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Discuss Key-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DISCUSS KEY -TAKEAWAYS FROM THE SESSION </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('discusskey')) {
                            $val = set_value('discusskey');
                        } elseif (($d[0]['discusskey'])) {
                            $val = $d[0]['discusskey'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="discusskey" name="discusskey"
                            rows="4"><?php echo $val; ?></textarea>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="week1_discusskey" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/user_profile/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
</div>

<!-- Modal Document -->
<div class="modal" id="document_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="document_title"></h4>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="document"></iframe>
                    </div>


                </div>
            </div>



        </div>
    </div>
</div>

<script type="text/javascript">
function opendocument(document, title) {
    $('#document').attr('src', document)
    $('#document_title').text(title);
    $('#document_modal').modal('show');
};
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>