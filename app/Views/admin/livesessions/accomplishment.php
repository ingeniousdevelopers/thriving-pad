<div class="col-sm-12 col-md-12">
    <h4>Accomplishment </h4>
</div>
</div>
</div>
<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
} ?>

<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>

<style>
.property-box-2 {
    box-shadow: 0 0 10px 1px rgb(71 85 95 / 8%);
    -webkit-box-shadow: 0 0 10px 1px rgb(71 85 95 / 8%);
    -moz-box-shadow: 0 0 10px 1px rgba(71, 85, 95, .08);
    position: inherit;
    margin-left: 300px;
    margin-right: 20px;
}

.alert-success {
    color: #155724;
    background-color: #d4edda;
    border-color: #c3e6cb;
    margin-left: 300px;
    margin-right: 20px;
}
</style>

<div class="property-box-2">
    <div class=" row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ACCOMPLISHMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <form action="<?= base_url() ?>/live-sessions/insertaccomplishment" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('accomplishment')) {
                                $val = set_value('accomplishment');
                            } elseif (($accomplishment[0]['accomplishment'])) {
                                $accomplishment = $accomplishment[0]['accomplishment'];
                            }
                            ?>
                            <textarea class="form-control" id="accomplishment" name="accomplishment"
                                rows="10"><?php echo $accomplishment; ?></textarea>
                        </div>
                </div>

                <div class="row  mt-2 ml-3">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                    ACCOMPLISHMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>