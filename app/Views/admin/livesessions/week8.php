<style>
.alert {
    text-transform: uppercase;
    font-size: 12px;
    border-radius: 0;
    margin-bottom: 10px;
    padding: 18px 20px;
    width: 100%;
}
</style>
<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 8 </h4>
</div>
</div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HYPOTHETICAL SCENARIOS</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5> <b>Scenario 1 :</b></h5>
                    <h6>
                        You wish to start a new business as you don’t like your well paid 9 to 5 job at an MNC because
                        your boss selectively tortures you with never ending work.
                    </h6>
                    <?php
                    $val = '';
                    $attr = '';
                    if (set_value('scenario')) {
                        $val = set_value('scenarios');
                    } elseif (($scenario1[0]['scenarios'])) {
                        $scenarios1 = $scenario1[0]['scenarios'];

                        $attr = 'disabled';
                    }
                    ?>
                    <textarea disabled class="form-control" id="scenarios1" name="scenarios1"
                        rows="3"><?php echo $scenarios1; ?></textarea>

                    <div class="footer">
                        <a href="#" tabindex="0">
                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-40 col-md-12">
        <div class="row text-center" style="float:right">
            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
            <div class="col-sm mb-3">
                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                    <input type="hidden" name="tab" value="scenarioone" />
                    <input type="hidden" name="url"
                        value="<?= base_url() ?>/admin/live-sessions/week8/<?php echo $profiledata['id'] ?>" />
                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                </form>
            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">

                    <h5> <b>Scenario 2 :</b></h5>

                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>What should Diya do?</h6>
                        </li>
                        <li>
                            <h6>Work through the problem by putting the decision making process into action.</h6>
                        </li>
                    </ul>
                    <?php
                    $val = '';
                    $attr = '';
                    if (set_value('scenarios')) {
                        $val = set_value('scenarios');
                    } elseif (($scenario2[0]['scenarios'])) {
                        $scenarios2 = $scenario2[0]['scenarios'];

                        $attr = 'disabled';
                    }
                    ?>
                    <textarea disabled class="form-control" id="scenarios2" name="scenarios2"
                        rows="3"><?php echo $scenarios2; ?></textarea>

                    <div class="footer">
                        <a href="#" tabindex="0">


                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-40 col-md-12">
        <div class="row text-center" style="float:right">
            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
            <div class="col-sm mb-3">
                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                    <input type="hidden" name="tab" value="scenariotwo" />
                    <input type="hidden" name="url"
                        value="<?= base_url() ?>/admin/live-sessions/week8/<?php echo $profiledata['id'] ?>" />
                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                </form>
            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">

                    <h5> <b>Scenario 3 :</b></h5>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <h6>What is the problem?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $val = set_value('q1');
                            } elseif (($scenario3[0]['q1'])) {
                                $q1 = $scenario3[0]['q1'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q1" name="q1"
                                rows="3"><?php echo $q1; ?></textarea>

                        </div>

                        <div class="form-group col-md-6">
                            <h6>Alternate choices? </h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $val = set_value('q2');
                            } elseif (($scenario3[0]['q2'])) {
                                $q2 = $scenario3[0]['q2'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q2" name="q2" rows="3">
                                    <?php echo $q2; ?></textarea>

                        </div>
                        <div class="form-group col-md-6">
                            <h6>Consequences of these Choices?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $val = set_value('q3');
                            } elseif (($scenario3[0]['q3'])) {
                                $q3 = $scenario3[0]['q3'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q3" name="q3"
                                rows="3"><?php echo $q3; ?></textarea>


                        </div>

                        <div class="form-group col-md-6">
                            <h6>Which Base Values come to your mind? </h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $val = set_value('q4');
                            } elseif (($scenario3[0]['q4'])) {
                                $q4 = $scenario3[0]['q4'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q4" name="q4"
                                rows="3"><?php echo $q4; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>Emotion and Feelings that it triggers in you?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q5')) {
                                $val = set_value('q5');
                            } elseif (($scenario3[0]['q5'])) {
                                $q5 = $scenario3[0]['q5'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q5" name="q5"
                                rows="3"><?php echo $q5; ?></textarea>



                        </div>

                        <div class="form-group col-md-6">
                            <h6>Any more information you need to have for clarity?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q6')) {
                                $val = set_value('q6');
                            } elseif (($scenario3[0]['q6'])) {
                                $q6 = $scenario3[0]['q6'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q6" name="q6" rows="3">
                                    <?php echo $q6; ?></textarea>



                        </div>
                        <div class="form-group col-md-6">
                            <h6>Who else can help you?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q7')) {
                                $val = set_value('q7');
                            } elseif (($scenario3[0]['q7'])) {
                                $q7 = $scenario3[0]['q7'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q7" name="q7"
                                rows="3"><?php echo $q7; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>What is your decision?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q8')) {
                                $val = set_value('q8');
                            } elseif (($scenario3[0]['q8'])) {
                                $q8 = $scenario3[0]['q8'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q8" name="q8"
                                rows="3"><?php echo $q8; ?></textarea>


                        </div>

                        <div class="form-group col-md-12">
                            <h6>Have you taken the right decision? Justify your decision as right</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q9')) {
                                $val = set_value('q9');
                            } elseif (($scenario3[0]['q9'])) {
                                $q9 = $scenario3[0]['q9'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q9" name="q9"
                                rows="3"><?php echo $q9; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="container mb-40 col-md-12">
        <div class="row text-center" style="float:right">
            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
            <div class="col-sm mb-3">
                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                    <input type="hidden" name="tab" value="scenariothree" />
                    <input type="hidden" name="url"
                        value="<?= base_url() ?>/admin/live-sessions/week8/<?php echo $profiledata['id'] ?>" />
                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                </form>
            </div>
        </div>
    </div>
</div>



<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DECISION WHEEL TOOL</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <a href="<?= base_url() ?>/admin/decision-wheel/<?= $profiledata['id']; ?>"
                                    class="btn btn-theme btn-md" target="_blank">
                                    DECISION WHEEL TOOL</a>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>MASTERMIND</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6 class="text-justify">
                        A Mastermind Group can significantly help you in shaping your life, personality, and business.
                    </h6>
                    <br />
                    <h5>Whom to Hang Out With !!!</h5><br />
                    <div class="row">
                        <div class="form-group col-md-6">
                            <h6>Who makes you laugh?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $val = set_value('q1');
                            } elseif (($master[0]['q1'])) {
                                $masterq1 = $master[0]['q1'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q1" name="q1"
                                rows="3"><?php echo $masterq1; ?></textarea>

                        </div>

                        <div class="form-group col-md-6">
                            <h6>Who makes you think differently?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $val = set_value('q2');
                            } elseif (($master[0]['q2'])) {
                                $masterq2 = $master[0]['q2'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q2" name="q2"
                                rows="3"><?php echo $masterq2; ?></textarea>

                        </div>
                        <div class="form-group col-md-6">
                            <h6>Who challenges you?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $val = set_value('q3');
                            } elseif (($master[0]['q3'])) {
                                $masterq3 = $master[0]['q3'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q3" name="q3"
                                rows="3"><?php echo $masterq3; ?></textarea>


                        </div>

                        <div class="form-group col-md-6">
                            <h6>Who inspires you?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $val = set_value('q4');
                            } elseif (($master[0]['q4'])) {
                                $masterq4 = $master[0]['q4'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q4" name="q4"
                                rows="3"><?php echo $masterq4; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>Who is your support team which helps you learn & grow?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q5')) {
                                $val = set_value('q5');
                            } elseif (($master[0]['q5'])) {
                                $masterq5 = $master[0]['q5'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q5" name="q5"
                                rows="3"><?php echo $masterq5; ?></textarea>



                        </div>

                        <div class="form-group col-md-6">
                            <h6>Who would you ideally like to add in your Mastermind group?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q6')) {
                                $val = set_value('q6');
                            } elseif (($master[0]['q6'])) {
                                $masterq6 = $master[0]['q6'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q6" name="q6"
                                rows="3"><?php echo $masterq6; ?></textarea>



                        </div>
                        <div class="form-group col-md-6">
                            <h6>How much time do you currently spend in your Mastermind group?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q7')) {
                                $val = set_value('q7');
                            } elseif (($master[0]['q7'])) {
                                $masterq7 = $master[0]['q7'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q7" name="q7"
                                rows="3"><?php echo $masterq7; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>Who spends most of the conversation being negative & complaining?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q8')) {
                                $val = set_value('q8');
                            } elseif (($master[0]['q8'])) {
                                $masterq8 = $master[0]['q8'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q8" name="q8"
                                rows="3"><?php echo $masterq8; ?></textarea>


                        </div>

                        <div class="form-group col-md-6">
                            <h6>Who else are you tolerating who do not add value to your life?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q9')) {
                                $val = set_value('q9');
                            } elseif (($master[0]['q9'])) {
                                $masterq9 = $master[0]['q9'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q9" name="q9"
                                rows="3"><?php echo $masterq9; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>How much time do you currently spend with people whom you tolerate?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q10')) {
                                $val = set_value('q10');
                            } elseif (($master[0]['q10'])) {
                                $masterq10 = $master[0]['q10'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q10" name="q10"
                                rows="3"><?php echo $masterq10; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>What can you do to spend more time with people who support & add value to your
                                life? </h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q11')) {
                                $val = set_value('q11');
                            } elseif (($master[0]['q11'])) {
                                $masterq11 = $master[0]['q11'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q11" name="q11"
                                rows="3"><?php echo $masterq11; ?></textarea>


                        </div>


                    </div>

                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="mastermind" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week8/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>TOXIC PEOPLE</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6 class="text-justify">
                        In order to detoxify our lives, we first need to understand and spot toxic people around us.
                    </h6>
                    <br />
                    <h5>Identify TOXIC people</h5><br />
                    <div class="row">
                        <div class="form-group col-md-6">
                            <h6>What am I gaining by spending my limited precious time with this person?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $val = set_value('q1');
                            } elseif (($toxic[0]['q1'])) {
                                $toxicq1 = $toxic[0]['q1'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q1" name="q1"
                                rows="3"><?php echo $toxicq1; ?></textarea>

                        </div>

                        <div class="form-group col-md-6">
                            <h6>What am I losing by spending my limited precious time with this person?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $val = set_value('q2');
                            } elseif (($toxic[0]['q2'])) {
                                $toxicq2 = $toxic[0]['q2'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q2" name="q2"
                                rows="3"><?php echo $toxicq2; ?></textarea>

                        </div>
                        <div class="form-group col-md-6">
                            <h6>What would happen if I didn’t spend my time with this person?<br /><br />
                            </h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $val = set_value('q3');
                            } elseif (($toxic[0]['q3'])) {
                                $toxicq3 = $toxic[0]['q3'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q3" name="q3"
                                rows="3"><?php echo $toxicq3; ?></textarea>


                        </div>

                        <div class="form-group col-md-6">
                            <h6>What are you going to do differently about how you spend your time with this
                                person?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $val = set_value('q4');
                            } elseif (($toxic[0]['q4'])) {
                                $toxicq4 = $toxic[0]['q4'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q4" name="q4"
                                rows="3"><?php echo $toxicq4; ?></textarea>


                        </div>


                    </div>
                    <div class="container mb-40 col-md-12">
                        <div class="row text-center" style="float:right">
                            <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                            <div class="col-sm mb-3">
                                <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>"
                                    method="POST">
                                    <input type="hidden" name="tab" value="toxic" />
                                    <input type="hidden" name="url"
                                        value="<?= base_url() ?>/admin/live-sessions/week8/<?php echo $profiledata['id'] ?>" />
                                    <input type="submit" class="btn-md btn-danger" value="Redo" />
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="footer">
                        <a href="#" tabindex="0">


                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
        </div>
    </div>
    <style>
    .row {
        display: -webkit-box;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-left: 5px;
        margin-right: 5px;
    }
    </style>