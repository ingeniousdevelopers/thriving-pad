<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 2 </h4>
</div>
</div>
</div>

<!--What is Acceptance-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>WHAT IS ACCEPTANCE? </h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <h5>
                        Express what are still stuck on!
                    </h5>
                    <br />
                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('acceptance')) {
                            $val = set_value('acceptance');
                        } elseif (($a[0]['acceptance'])) {
                            $val = $a[0]['acceptance'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="acceptance" name="acceptance"
                            rows="3"><?php echo $val; ?></textarea>
                    </div>

                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="acceptance" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week2/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Base- Values Satisfaction & Dis-satisfaction-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>BASE- VALUES SATISFACTION - TAKE AWAY ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Describe how your five base values are being expressed in each of the key areas in your life.
                    </h5>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="s1">
                                <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s1')) {
                                $val = set_value('s1');
                            } elseif (($bs[0]['s1'])) {
                                $val = $bs[0]['s1'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s1" name="s1"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s1">
                                <h6>BUSINESS , MONEY, POSSESSION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s2')) {
                                $val = set_value('s2');
                            } elseif (($bs[0]['s2'])) {
                                $val = $bs[0]['s2'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s2" name="s2"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s3">
                                <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s3')) {
                                $val = set_value('s3');
                            } elseif (($bs[0]['s3'])) {
                                $val = $bs[0]['s3'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s3" name="s3"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s4">
                                <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s4')) {
                                $val = set_value('s4');
                            } elseif (($bs[0]['s4'])) {
                                $val = $bs[0]['s4'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s4" name="s4"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s5">
                                <h6>ENJOYMENT, FUN & RECREATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s5')) {
                                $val = set_value('s5');
                            } elseif (($bs[0]['s5'])) {
                                $val = $bs[0]['s5'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s5" name="s5"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s6">
                                <h6>FAMILY </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s6')) {
                                $val = set_value('s6');
                            } elseif (($bs[0]['s6'])) {
                                $val = $bs[0]['s6'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s6" name="s6"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s7">
                                <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s7')) {
                                $val = set_value('s7');
                            } elseif (($bs[0]['s7'])) {
                                $val = $bs[0]['s7'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s7" name="s7"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s8">
                                <h6>HEALTH, BEAUTY, WELLNESS </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s8')) {
                                $val = set_value('s8');
                            } elseif (($bs[0]['s8'])) {
                                $val = $bs[0]['s8'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s8" name="s8"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="basevaluessatis" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week2/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>BASE- VALUES DIS-SATISFACTION - TAKE AWAY ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Describe how your five base values are NOT being expressed in each of the key areas in your
                        life?
                    </h5>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="ds1">
                                <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds1')) {
                                $val = set_value('ds1');
                            } elseif (($bds[0]['ds1'])) {
                                $val = $bds[0]['ds1'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds1" name="ds1"
                                rows="d2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds1">
                                <h6>BUSINESS , MONEY, POSSESSION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds2')) {
                                $val = set_value('ds2');
                            } elseif (($bds[0]['ds2'])) {
                                $val = $bds[0]['ds2'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds2" name="ds2"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds3">
                                <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds3')) {
                                $val = set_value('ds3');
                            } elseif (($bds[0]['ds3'])) {
                                $val = $bds[0]['ds3'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds3" name="ds3"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds4">
                                <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds4')) {
                                $val = set_value('ds4');
                            } elseif (($bds[0]['ds4'])) {
                                $val = $bds[0]['ds4'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds4" name="ds4"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds5">
                                <h6>ENJOYMENT, FUN & RECREATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds5')) {
                                $val = set_value('ds5');
                            } elseif (($bds[0]['ds5'])) {
                                $val = $bds[0]['ds5'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds5" name="ds5"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds6">
                                <h6>FAMILY </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds6')) {
                                $val = set_value('ds6');
                            } elseif (($bds[0]['ds6'])) {
                                $val = $bds[0]['ds6'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds6" name="ds6"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds7">
                                <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds7')) {
                                $val = set_value('ds7');
                            } elseif (($bds[0]['ds7'])) {
                                $val = $bds[0]['ds7'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds7" name="ds7"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="ds8">
                                <h6>HEALTH, BEAUTY, WELLNESS </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('ds8')) {
                                $val = set_value('ds8');
                            } elseif (($bds[0]['ds8'])) {
                                $val = $bds[0]['ds8'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="ds8" name="ds8"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="basevaluesdissatis" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week2/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
</div>

<!-- Modal Document -->
<div class="modal" id="document_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="document_title"></h4>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="document"></iframe>
                    </div>


                </div>
            </div>



        </div>
    </div>
</div>

<script type="text/javascript">
function opendocument(document, title) {
    $('#document').attr('src', document)
    $('#document_title').text(title);
    $('#document_modal').modal('show');
};
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>