<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 3 </h4>
</div>
</div>
</div>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<!--Live Session-->

<!--Lets Align-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>LET'S ALIGN</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Based on previous activity, where you wrote down how your base-values are not being met, what do
                        you need to change in each area of your life to have your base-values satisfied?
                    </h5>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="s1">
                                <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s1')) {
                                $val = set_value('s1');
                            } elseif (($bs[0]['s1'])) {
                                $val = $bs[0]['s1'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s1" name="s1"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s1">
                                <h6>BUSINESS , MONEY, POSSESSION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s2')) {
                                $val = set_value('s2');
                            } elseif (($bs[0]['s2'])) {
                                $val = $bs[0]['s2'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s2" name="s2"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s3">
                                <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s3')) {
                                $val = set_value('s3');
                            } elseif (($bs[0]['s3'])) {
                                $val = $bs[0]['s3'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s3" name="s3"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s4">
                                <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s4')) {
                                $val = set_value('s4');
                            } elseif (($bs[0]['s4'])) {
                                $val = $bs[0]['s4'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s4" name="s4"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s5">
                                <h6>ENJOYMENT, FUN & RECREATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s5')) {
                                $val = set_value('s5');
                            } elseif (($bs[0]['s5'])) {
                                $val = $bs[0]['s5'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s5" name="s5"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s6">
                                <h6>FAMILY </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s6')) {
                                $val = set_value('s6');
                            } elseif (($bs[0]['s6'])) {
                                $val = $bs[0]['s6'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s6" name="s6"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s7">
                                <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s7')) {
                                $val = set_value('s7');
                            } elseif (($bs[0]['s7'])) {
                                $val = $bs[0]['s7'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s7" name="s7"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="s8">
                                <h6>HEALTH, BEAUTY, WELLNESS </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('s8')) {
                                $val = set_value('s8');
                            } elseif (($bs[0]['s8'])) {
                                $val = $bs[0]['s8'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="s8" name="s8"
                                rows="2"><?php echo $val; ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="letsalign" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week3/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--New Habit-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DISCOVERIES ABOUT YOURSELF </h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <h5>
                        Mention 3 things you have discovered about yourself
                    </h5>
                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('habits')) {
                            $val = set_value('habits');
                        } elseif (($h[0]['habits'])) {
                            $val = $h[0]['habits'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="habits" name="habits"
                            rows="3"><?php echo $val; ?></textarea>
                    </div>

                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="habits" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week3/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--De-Labelling-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DE-LABELLING</h1>
                                </div>
                            </div>
                        </div>
                    </h5>


                    <h5>“Labels are for jars and not people.”</h5>
                    <br />
                    <label for="lables">
                        <h6>List as many labels you have got so far in life</h6>
                    </label>
                    <div class="form-group">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('lables')) {
                            $val = set_value('lables');
                        } elseif (($dl[0]['lables'])) {
                            $val = $dl[0]['lables'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="lables" name="lables"
                            rows="3"><?php echo $val; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="onelable">
                            <h6>Select a major one</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('onelable')) {
                            $val = set_value('onelable');
                        } elseif (($dl[0]['onelable'])) {
                            $val = $dl[0]['onelable'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="onelable" name="onelable"
                            rows="1"><?php echo $val; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="q1">
                            <h6>1. How did it serve me positively?</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('q1')) {
                            $val = set_value('q1');
                        } elseif (($dl[0]['q1'])) {
                            $val = $dl[0]['q1'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="q1" name="q1"
                            rows="2"><?php echo $val; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="q2">
                            <h6>2. How did it negatively affect me?</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('q2')) {
                            $val = set_value('q2');
                        } elseif (($dl[0]['q2'])) {
                            $val = $dl[0]['q2'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="q2" name="q2"
                            rows="2"><?php echo $val; ?></textarea>
                    </div>

                    <div class="form-group">
                        <label for="q3">
                            <h6>3. Do I need to drop it?</h6>
                        </label>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('q3')) {
                            $val = set_value('q3');
                        } elseif (($dl[0]['q3'])) {
                            $val = $dl[0]['q3'];
                            $attr = 'disabled';
                        }
                        ?>
                        <textarea disabled class="form-control" id="q3" name="q3"
                            rows="2"><?php echo $val; ?></textarea>
                    </div>

                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="delabelling" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week3/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
</div>

<!-- Modal Document -->
<div class="modal" id="document_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="document_title"></h4>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="document"></iframe>
                    </div>


                </div>
            </div>



        </div>
    </div>
</div>

<script type="text/javascript">
function opendocument(document, title) {
    $('#document').attr('src', document)
    $('#document_title').text(title);
    $('#document_modal').modal('show');
};
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>