<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 9 </h4>
</div>
</div>
</div>

<style>

</style>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HAPPINESS BOOSTERS</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6 class="text-justify">
                        <b> Activities which you love doing which makes you forget the track of time.</b>
                    </h6>
                    <br />
                    <div class="row">
                        <div class="form-group col-md-6">
                            <h6>What do you love doing?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $val = set_value('q1');
                            } elseif (($happy[0]['q1'])) {
                                $happyq1 = $happy[0]['q1'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q1" name="q1"
                                rows="3"><?php echo $happyq1; ?></textarea>

                        </div>

                        <div class="form-group col-md-6">
                            <h6>What activities do you find easy and are really good at?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $val = set_value('q2');
                            } elseif (($happy[0]['q2'])) {
                                $happyq2 = $happy[0]['q2'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q2" name="q2"
                                rows="3"><?php echo $happyq2; ?></textarea>

                        </div>
                        <div class="form-group col-md-6">
                            <h6>What activities do you do where time just disappears?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $val = set_value('q3');
                            } elseif (($happy[0]['q3'])) {
                                $happyq3 = $happy[0]['q3'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q3" name="q3"
                                rows="3"><?php echo $happyq3; ?></textarea>


                        </div>

                        <div class="form-group col-md-6">
                            <h6>If money/time/family commitments were no obstacle, what would you do?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $val = set_value('q4');
                            } elseif (($happy[0]['q4'])) {
                                $happyq4 = $happy[0]['q4'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q4" name="q4"
                                rows="3"><?php echo $happyq4; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>What did you enjoy doing as a kid (age 4-18)?</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q5')) {
                                $val = set_value('q5');
                            } elseif (($happy[0]['q5'])) {
                                $happyq5 = $happy[0]['q5'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q5" name="q5"
                                rows="3"><?php echo $happyq5; ?></textarea>



                        </div>

                        <div class="form-group col-md-6">
                            <h6>List everything you said you would be when you grew up.</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q6')) {
                                $val = set_value('q6');
                            } elseif (($happy[0]['q6'])) {
                                $happyq6 = $happy[0]['q6'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q6" name="q6"
                                rows="3"><?php echo $happyq6; ?></textarea>



                        </div>
                        <div class="form-group col-md-6">
                            <h6>What are you really good at? List everything you can think of no matter how
                                irrelevant it may seem when you first think about it.</h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q7')) {
                                $val = set_value('q7');
                            } elseif (($happy[0]['q7'])) {
                                $happyq7 = $happy[0]['q7'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q7" name="q7"
                                rows="3"><?php echo $happyq7; ?></textarea>


                        </div>
                        <div class="form-group col-md-6">
                            <h6>What impact will having more HB in your life have on you and those around you?
                            </h6>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q8')) {
                                $val = set_value('q8');
                            } elseif (($happy[0]['q8'])) {
                                $happyq8 = $happy[0]['q8'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q8" name="q8"
                                rows="3"><?php echo $happyq8; ?></textarea>


                        </div>

                        <div class="form-group col-md-12">
                            <h6>Who else are you tolerating who do not add value to your life?</h6>

                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q9')) {
                                $val = set_value('q9');
                            } elseif (($happy[0]['q9'])) {
                                $happyq9 = $happy[0]['q9'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea disabled class="form-control" id="q9" name="q9"
                                rows="3"><?php echo $happyq9; ?></textarea>


                        </div>
                    </div>

                </div>
                <div class="row  mt-2 ml-3">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT HAPPINESS BOOSTERS
                                    Moment</button> -->

                            </div>
                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="myhappy" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week9/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">

                    <h6>
                        You may add new habits in your habits sheet if you wish!


                    </h6>
                </div>

                <div class="row  mt-2 ml-3">
                    <div class="col-md-4">
                        <div class="form-group clearfix">
                            <a href="<?= base_url() ?>/admin/myhabit/<?= $profiledata['id']; ?>"
                                class="btn btn-theme btn-md" target="_blank">
                                MY HABIT</a>
                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="myhabit" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week9/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HOW TO MAKE A VISION BOARD</h1>
                                </div>
                            </div>
                        </div>
                    </h5>


                    <div class="clearfix px-3 ml-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <!-- <a href="https://www.canva.com/en_in/" class="btn btn-theme btn-md" target="_blank">
                                    CLICK TO MAKE VISION BOARD</a>-->
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3">
                        <!-- Edit profile photo -->
                        <?php if ($imag) {
                        ?>
                        <img width="400%" height="10%"
                            src="http://localhost:8080/thrivepad/public/uploads/week9/<?php echo $imag ?>" />
                        <?php
                        } else {
                        ?>
                        <div style="width:1000px" class="alert alert-danger">No Images Found
                        </div>

                        <?php
                        }
                        ?>
                        <br />
                        <div class=" col-md-4">
                            <div class="row  mt-2 ml-3">
                                <div class="form-group clearfix">
                                    <!-- <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                        VISION BOARD</button> -->
                                </div>
                            </div>
                        </div>
                        <div class="container mb-40 col-md-12">
                            <div class="row text-center" style="float:right">
                                <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                        Let's go Back</a>
                    </div> -->
                                <div class="col-sm mb-3">
                                    <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>"
                                        method="POST">
                                        <input type="hidden" name="tab" value="visionboard" />
                                        <input type="hidden" name="url"
                                            value="<?= base_url() ?>/admin/live-sessions/week9/<?php echo $profiledata['id'] ?>" />
                                        <input type="submit" class="btn-md btn-danger" value="Redo" />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <a href="#" tabindex="0">


                            </a>
                            <span>

                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">

                    <h6>
                        Celebrate Accomplishment + Breakthrough (journal sheet)


                    </h6>
                </div>

                <div class="clearfix px-3 ml-3">
                    <div class="pull-left">
                        <div class="form-group clearfix">
                            <a href="<?= base_url() ?>/admin/accomplishment/<?= $profiledata['id']; ?>"
                                class="btn btn-theme btn-md" target="_blank">
                                ACCOMPLISHMENT</a>
                            <a href="<?= base_url() ?>/admin/breakthrough/<?= $profiledata['id']; ?>"
                                class="btn btn-theme btn-md" target="_blank">
                                BREAKTHROUGH</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix px-3">
                    <div class="pull-left">
                        <div class="form-group clearfix">

                        </div>
                    </div>
                </div>
                <div class="container mb-40 col-md-12">
                    <div class="row text-center" style="float:right">
                        <!-- <div class="col-sm">
                             <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
                                Let's go Back</a>
                    </div> -->
                        <div class="col-sm mb-3">
                            <form action="<?= base_url() ?>/admin/redo/<?php echo $profiledata['id'] ?>" method="POST">
                                <input type="hidden" name="tab" value="breakthrough" />
                                <input type="hidden" name="url"
                                    value="<?= base_url() ?>/admin/live-sessions/week9/<?php echo $profiledata['id'] ?>" />
                                <input type="submit" class="btn-md btn-danger" value="Redo" />
                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>

<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-left: 5px;
    margin-right: 5px;
}
</style>