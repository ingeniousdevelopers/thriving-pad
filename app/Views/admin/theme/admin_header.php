<!DOCTYPE html>
<html lang="zxx">
<head>
    <title><?php echo $page_title; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap-submenu.css">

    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/magnific-popup.css">
    <link rel="stylesheet" href="css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="<?= base_url() ?>/public/assets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="<?= base_url() ?>/public/assets/css/dropzone.css">
    <link rel="stylesheet" type="text/css"  href="<?= base_url() ?>/public/assets/css/slick.css">
    <link rel="stylesheet" type="text/css"  href="<?= base_url() ?>/public/assets/css/additional.css">
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/vendors_css.css">
    <link rel="stylesheet" type="text/css"  href="<?= base_url() ?>/public/assets/css/tablestyle.css">




    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="<?= base_url() ?>/public/assets/css/skins/default.css">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="<?= base_url() ?>/public/assets/img/favicon.ico" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>/public/assets/css/ie10-viewport-bug-workaround.css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script  src="<?= base_url() ?>/public/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="js/html5shiv.min.js"></script>
    <script  src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- <div class="page_loader"></div> -->

<!-- Main header start -->
<header class="main-header header-2 fixed-header">
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand logo-3" href="<?= base_url() ?>">
                <img src="<?= base_url() ?>/public/assets/img/logos/logo.png" alt="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto d-lg-none d-xl-none">
                    <li class="nav-item dropdown">
                        <a href="<?= base_url() ?>/dashboard" class="nav-link">Dashboard</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?= base_url() ?>/admin/users" class="nav-link">Users</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?= base_url() ?>/admin/batches" class="nav-link">Bathces</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?= base_url() ?>/admin/slots" class="nav-link">Slots</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="<?= base_url() ?>/logout" class="nav-link">Logout</a>
                    </li>
                </ul>
                <div class="navbar-buttons ml-auto d-none d-xl-block d-lg-block">
                    <ul>
                        <li>
                            <div class="dropdown btns">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="http://placehold.it/45x45" alt="avatar">
                                    My Account
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="<?= base_url() ?>/dashboard">Dashboard</a>
                                    <a class="dropdown-item" href="<?= base_url() ?>/admin/users">Users</a>
                                    <a class="dropdown-item" href="<?= base_url() ?>/admin/batches">Batches</a>
                                    <a class="dropdown-item" href="<?= base_url() ?>/admin/slots">Slots</a>
                                    <a class="dropdown-item" href="<?= base_url() ?>/logout">Logout</a>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a class="btn btn-theme btn-md" href="<?= base_url() ?>/logout">Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>
<!-- Main header end -->



<!-- Dashbord start -->
<div class="dashboard">
    <div class="container-fluid">
        <div class="row">
            <div class="dashboard-nav">
                <div class="dashboard-inner">
                    <h4>Main</h4>
                    <ul>
                        <li class="<?php if($page_no==1){echo 'active';} ?>"><a href="<?= base_url() ?>/dashboard"></i> Dashboard</a></li>
                    </ul>
                    <h4>Sessions</h4>
                    <ul>
                        <li class="<?php if($page_no==2){echo 'active';} ?>"><a href="<?= base_url() ?>/admin/users">Users</a></li>
                        <li class="<?php if($page_no==3){echo 'active';} ?>"><a href="<?= base_url() ?>/admin/batches">Batches</a></li>
                        <li class="<?php if($page_no==4){echo 'active';} ?>"><a href="<?= base_url() ?>/admin/slots">Slots</a></li>
                    </ul>
                    <h4>Account</h4>
                    <ul>
                        <li><a href="<?= base_url() ?>/logout"><i class="flaticon-logout"></i>Logout</a></li>
                    </ul>
                </div>
            </div>