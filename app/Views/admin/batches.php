<div class="dashboard-content">
    <div class="dashboard-header clearfix">

    </div>
    <?php if (session()->getTempData('success')) : ?>
    <div class="alert alert-success"><?= session()->getTempData('success') ?></div>
    <?php endif; ?>
    <?php if (session()->getTempData('error')) : ?>
    <div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
    <?php endif; ?>
    <div class="box">
        <div class="box-header with-border">
            <span class="input-group-btn">
                <h3 class="box-title">Batches List</h3>
                <a onClick="create_batch(); return false;" class="btn btn-theme btn-md float-right" href="#">Create
                    Batch</a>
            </span>
            <h6 class="box-subtitle">Create and Manage Batches</h6>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">

                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                    <thead>
                        <tr>
                            <th style="width:11%">Batch ID</th>
                            <th>Type</th>
                            <th style="width:15%">Slot</th>
                            <th>Students</th>
                            <th>N.Session</th>
                            <th>Week</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($batch_list) && !empty($batch_list)) {
                            foreach ($batch_list as $batch) {
                        ?>
                        <tr>
                            <td>
                                <a style="text-decoration: underline;color:#44a5b0"
                                    onClick="view_link(<?php echo $batch['batch_id']; ?>); return false;"
                                    href=""><?php echo 'thrive-' . $batch['batch_id']; ?>
                                </a>
                            </td>
                            <td><?php echo $batch['batch_type'] == 1 ? 'Private' : 'Group'; ?></td>
                            <td><?php echo $batch['slot_name']; ?></td>
                            <td><?php echo $batch['count'] ?></td>
                            <td><?php echo $batch['next_session'] ?></td>
                            <td><?php echo $batch['session_count']; ?></td>
                            <td>
                                <?php
                                        if ($batch['batch_status'] == 0 && $batch['session_count'] != 12) { ?>
                                <a style="color:#fff;"
                                    onClick="session_link(<?php echo $batch['slot_id']; ?>,<?php echo $batch['batch_id']; ?>,<?php echo $batch['session_count']; ?>); return false;"
                                    class="btn btn-theme btn-md mr-1">Start Batch</a>
                                <?php }
                                        if ($batch['batch_status'] == 1 && $batch['session_count'] != 12) {
                                            $nextsessionlink = $batch['next_session_link'];
                                            $id = $batch['batch_id'];

                                            echo $nextsessionlink != "" ? '<a href="#" onClick="change_link(' . $id . '); return false;" class="btn btn-theme btn-md float-left">ChangeLink</a>' : '<a href="#" onClick="insert_link(' . $id . '); return false;" class="btn btn-theme btn-md float-left">InsertLINK</a>';    ?>
                                <?php
                                        }

                                        if ($batch['session_count'] >= 1 && $batch['session_count'] != 12) { ?>
                                <a onClick="session_link(<?php echo $batch['slot_id']; ?>,<?php echo $batch['batch_id']; ?>,<?php echo $batch['session_count']; ?>);
                                return false;" class="btn btn-theme btn-md ml-1" href="">Next Session</a>
                                <?php
                                        }
                                        if ($batch['session_count'] == 12) { ?>

                                <a onclick="return confirm('Are you sure want to batch end ?')"
                                    href="<?= base_url() ?>/admin/endbatch/<?php echo $batch['batch_id']; ?>"
                                    class="btn btn-theme btn-md ml-1 mt-1">End Batch</a>
                                <?php }
                                        if ($batch['session_count'] != 12) { ?>

                                <a onClick="edit_link(<?php echo $batch['slot_id']; ?>,<?php echo $batch['batch_id']; ?>);
                                return false;" class="btn btn-theme btn-md ml-1" href="">Edit</a>

                                <a onclick="return confirm('Are you sure want to delete ?')"
                                    href="<?= base_url() ?>/admin/deleteslot/<?php echo $batch['slot_id']; ?>"
                                    class="btn btn-warning btn-md ml-1 mt-1">Delete</a>
                                <!-- <a onClick="view_link(<?php echo $batch['batch_id']; ?>); return false;"
                                class="btn btn-theme btn-md ml-1" href="">View</a>-->
                                <?php } ?>
                            </td>

                        </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>

                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>


    <p class="sub-banner-2 text-center">©️ 2020 Thrivepad</p>
</div>
</div>

<div class="modal" id="createModal" data-backdrop="false">
    <div class="modal-dialog" style="top:20% !important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Create Batch</h4>
                <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <form action="<?= base_url(); ?>/admin/batches" method="post">

                    <div class="row justify-content-md-center pt-3">

                        <div class="col-md-6">
                            <div class="form-group">
                                <select class="form-control" name="slotid" id="slotid">
                                    <option value="BatchID" disabled selected="true">Slot ID</option>
                                    <?php if (isset($slot_list) && !empty($slot_list)) {
                                        foreach ($slot_list as $slot) {
                                            if (($slot['batch_type'] == 1 && $slot['count'] == 0) || ($slot['batch_type'] == 2 && $slot['count'] >= 0)) {                                    ?>
                                    <option value="<?php echo $slot['slot_id'] ?>"> <?php echo $slot['slot_name'] ?>
                                    </option>
                                    <?php
                                            }
                                        }
                                    } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <input type="submit" class="btn btn-md button-theme" value="Create Batch" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close" />
            </div>

        </div>
    </div>
</div>


<div class="modal" id="sessionModal" data-backdrop="false">
    <div class="modal-dialog" style="top:20% !important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"> Next Session Date of Week -
                    <h4 id="ses_co" class="ml-1"> </h4>
                    <input type="hidden" name="batchsesid" id="batchsesid" value="<?php echo $batch['batch_id'] ?>" />
                    <input type="hidden" name="slotsesid" id="slotsesid" value="<?php echo $batch['slot_id'] ?>" />
                    <input type="hidden" name="batchsesco" id="batchsesco"
                        value="<?php echo $batch['session_count'] ?>" />
                    <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            </div>

            <!-- Modal body -->
            <div class="modal-body">


                <div id="filtersession_data">
                    <?php echo $data1['count1']; ?>

                </div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close" />
            </div>
        </div>
    </div>
</div>


<div class="modal" id="viewModal" data-backdrop="false">
    <div class="modal-dialog" style="top:20% !important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Users Assigned to Batch Thrive -
                    <h4 id="batchviid"></h4>

                    <input type="hidden" name="batchviewid" id="batchviewid" value="<?php echo $batch['batch_id'] ?>" />
                    <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            </div>

            <!-- Modal body -->
            <div class="modal-body">

                <div id="filter_data"></div>

            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close" />
            </div>

        </div>
    </div>
</div>

<div class="modal" id="editModal" data-backdrop="false">
    <div class="modal-dialog" style="top:15% !important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Edit Batch Thrive -
                    <h4 id="batcheid"></h4>
                    <input type="hidden" name="sloteditid" id="sloteditid" value="<?php echo $batch['slot_id'] ?>" />
                    <input type="hidden" name="batcheditid" id="batcheditid" value="<?php echo $batch['batch_id'] ?>" />

                    <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div id="filteredit_data">
                </div>
                <div id="filteruser_data">
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close" />
            </div>

        </div>
    </div>
</div>


<div class="modal" id="insertModal" data-backdrop="false">
    <div class="modal-dialog" style="top:20% !important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Insert Session Link to Batch Thrive -
                    <h4 id="batch_id"></h4>
                    <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url(); ?>/admin/insertlink" method="POST">

                    <div class="form-group form-box">
                        <input type="text" name="next_session_link" placeholder="Meeting Link"
                            class="input-text form-control" placeholder="Meeting Link" value="-">
                    </div>
                    <div class="container">
                        <div class="row justify-content-md-center mb-3">
                            <div class="col-md-4">
                                <label for="next-session" class="mt-3">Next Session</label>
                            </div>
                            <div class="col-md-8">
                                <input type="datatime-local" name="next_session" id="next_session"
                                    class="input-text form-control" disabled
                                    value="<?php echo $batch['next_session'] ?>" />
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="batchid" id="batchid" value="<?php echo $batch['batch_id'] ?>" />
                    <input type="submit" class="btn-md btn-theme float-right" />
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close" />
            </div>

        </div>
    </div>
</div>


<div class="modal" id="changeModal" data-backdrop="false">
    <div class="modal-dialog" style="top:20% !important;">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Change Session Link to Batch Thrive- <h4 id="batchcid"></h4>
                    <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url(); ?>/admin/changelink" method="POST">
                    <input type="hidden" name="batchchangeid" id="batchchangeid"
                        value="<?php echo $batch['batch_id'] ?>" />

                    <div class="form-group form-box">
                        <input type="text" name="next_session_link" id="nextsessionlink" placeholder="Meeting Link"
                            class="input-text form-control" placeholder="Meeting Link" value="-">
                    </div>
                    <div class="container">
                        <div class="row justify-content-md-center mb-3">
                            <div class="col-md-4">
                                <label for="next-session" class="mt-3">Next Session</label>
                            </div>
                            <div class="col-md-8">
                                <input type="datatime-local" name="next_session" id="nextsession"
                                    class="input-text form-control" disabled
                                    value="<?php echo $batch['next_session'] ?>" />
                            </div>
                        </div>
                    </div>
                    <input type="submit" class="btn-md btn-theme float-right" />
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" data-dismiss="modal" value="Close" />
            </div>

        </div>
    </div>
</div>
</div>
</div>


<script type="text/javascript">
function insert_link(batchid) {
    $('#insertModal').modal('show');
    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('admin/batchidget'); ?>',
        data: 'batch_id=' + batchid,
        success: function(data) {
            var res = $.parseJSON(data);
            console.log(res);
            $('#batch_id').text(res.batchdata['batch_id']);
            $('#batchid').val(res.batchdata['batch_id']);
            $('#next_session').val(res.batchdata['next_session']);
            $('#insertModal').modal('show');
        }
    })
};

function change_link(batchchangeid) {
    $('#changeModal').modal('show');
    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('admin/batchidget'); ?>',
        data: 'batch_id=' + batchchangeid,
        success: function(data) {
            var res = $.parseJSON(data);
            console.log(res);
            $('#batchcid').text(res.batchdata['batch_id']);
            $('#batchchangeid').val(res.batchdata['batch_id']);
            $('#nextsession').val(res.batchdata['next_session']);
            $('#netsessionlink').val(res.batchdata['next_session_link']);
            $('#changeModal').modal('show');
        }
    })
};

function view_link(batchviewid) {
    $('#viewModal').modal('show');
    filter_data(batchviewid);

};


function session_link(slotsesid, batchsesid, batchsesco) {
    $('#sessionModal').modal('show');
    filtersession_data(slotsesid, batchsesid, batchsesco);
};

function edit_link(sloteditid, batcheditid) {
    $('#editModal').modal('show');
    filteredit_data(sloteditid, batcheditid);

};

function filter_data(batchviewid) {
    $('#filter_data').html("<div id='loading'>Loading...,</div>");
    var action = 'fetch_data';
    var batchfid = batchviewid;
    $.ajax({
            url: "<?php echo base_url() ?>/admin/fetch_data",
            method: "POST",
            dataType: "JSON",
            data: {
                action: action,
                batchid: batchfid,
            },
            success: function(data) {
                $('#batchviid').text(batchfid);

                $('#filter_data').html(data.user_list);

            }

        },


    )
};

function filteredit_data(sloteditid, batcheditid) {
    var action = 'filteredit_data';
    var slotfid = sloteditid;
    var batchfid = batcheditid;

    $.ajax({
        url: "<?php echo base_url() ?>/admin/fetchedit_data",
        method: "POST",
        dataType: "JSON",
        data: {
            action: action,
            sloteditid: slotfid,
            batcheditid: batchfid,
        },
        success: function(data) {
            $('#batcheid').text(batchfid);
            $('#sloteditid').val(slotfid);
            $('#batcheditid').val(batchfid);
            $('#filteredit_data').html(data.user_list);
            $('#filteruser_data').html(data.users_list);
        }

    })
};

function filtersession_data(slotsesid, batchsesid, batchsesco) {
    var action = 'filtersession_data';
    var slotsid = slotsesid;
    var batchsid = batchsesid;
    var sesco = batchsesco;
    if (sesco == 0) {
        sesco = 1;
    }

    $.ajax({
        url: "<?php echo base_url() ?>/admin/fetchsession_data",
        method: "POST",
        dataType: "JSON",
        data: {
            action: action,
            slotsesid: slotsid,
            batchsesid: batchsid,
            batchsesco: sesco,

        },
        success: function(data) {
            $('#slotsid').text(batchsid);
            $('#ses_co').text(sesco);
            $('#slotsesid').val(slotsid);
            $('#batchsesid').val(batchsid);
            $('#batchsesco').val(sesco);

            $('#filtersession_data').html(data.user_list);
        }

    })
};

function create_batch(batch_id) {
    $('#createModal').modal('show');
}
</script>