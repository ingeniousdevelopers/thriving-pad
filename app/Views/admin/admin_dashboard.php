<div class="dashboard-content">
    <div class="dashboard-header clearfix">
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <h4>Welcome To Thrivepad</h4>
            </div>
            <div class="col-sm-12 col-md-7">
                <div class="breadcrumb-nav">
                    <ul>
                        <li>
                            <a href="<?= base_url() ?>">Home</a>
                        </li>
                        <li class="active">Dashboard
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-success">
                <div class="left">
                    <h4><?php echo $regcount; ?></h4>
                    <p>No.of Registered Users</p>
                </div>
                <div class="right">
                    <i class="fa fa-home"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-warning">
                <div class="left">
                    <h4><?php echo $paidcount . '/' . $regcount . ''; ?></h4>
                    <p>No.of Paid Users</p>
                </div>
                <div class="right">
                    <i class="fa fa-home"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-active">
                <div class="left">
                    <h4><?php echo $unpaidcount . '/' . $regcount . ''; ?></h4>
                    <p>No.of Unpaid Users</p>
                </div>
                <div class="right">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-dark">
                <div class="left">
                    <h4><?php echo $privatecount . '/' . $slotscount . ''; ?></h4>
                    <p>No.of Private Batches</p>
                </div>
                <div class="right">
                    <i class="fa fa-heart-o"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-success">
                <div class="left">
                    <h4><?php echo $groupcount . '/' . $slotscount . ''; ?></h4>
                    <p>No.of Group Batches</p>
                </div>
                <div class="right">
                    <i class="fa fa-home"></i>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-warning">
                <div class="left">
                    <h4><?php echo $slotsavailable . '/' . $slotscount . ''; ?></h4>
                    <p>No.of Slots available</p>
                </div>
                <div class="right">
                    <i class="fa fa-home"></i>
                </div>
            </div>

        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-active">
                <div class="left">
                    <h4><?php echo $batchcompletecount . '/' . $batchcount . ''; ?></h4>
                    <p>No.of Batches Complete</p>
                </div>
                <div class="right">
                    <i class="fa fa-comments-o"></i>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-dark">
                <div class="left">
                    <h4><?php echo $privateavailable . '/' . $privatecount . ''; ?></h4>
                    <p>No.of Private Batches Available</p>
                </div>
                <div class="right">
                    <i class="fa fa-heart-o"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
            <div class="ui-item bg-success">
                <div class="left">
                    <h4><?php echo $groupavailable . '/' . $groupcount . ''; ?></h4>
                    <p>No.of Group Batches Available</p>
                </div>
                <div class="right">
                    <i class="fa fa-home"></i>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2021 Thrivepad</p>
    </div>
</div>

</div>
</div><!-- Dashbord end -->