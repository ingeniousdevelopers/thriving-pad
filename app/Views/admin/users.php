<div class="dashboard-content">

    <div class="dashboard-header clearfix">

    </div>
    <?php if (session()->getTempData('success')) : ?>
    <div class="alert alert-success"><?= session()->getTempData('success') ?></div>
    <?php endif; ?>
    <?php if (session()->getTempData('error')) : ?>
    <div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
    <?php endif; ?>

    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Users List</h3>
            <h6 class="box-subtitle">Create, Block and Manage Users</h6>
            <hr> <span class="input-group-btn">
                <h3 class="box-title">Filter</h3>
                <form action="<?= base_url(); ?>/admin/users" method="post">

                    <div class="row float-right">

                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" name="searchbatchid" id="searchbatchid">
                                    <option value="BatchID" selected>BatchID</option>
                                    <?php if (isset($batch_list) && !empty($batch_list)) {
                                        foreach ($batch_list as $batch) {
                                    ?> <option <?php if ($batchlist == $batch['batch_id']) { ?>selected="true"
                                        <?php }; ?> value="<?php echo $batch['batch_id'] ?>">
                                        <?php echo $batch['batch_id'] ?>
                                    </option>
                                    <?php
                                        }
                                    } ?>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <select class="form-control" name="slotid">
                                    <option value="">SlotID</option>
                                    <?php if (isset($slot_list) && !empty($slot_list)) {
                                        foreach ($slot_list as $slot) {
                                            if ($slot['batch_type'] != 0) {


                                    ?> <option <?php if ($slotlist == $slot['slot_id']) { ?>selected="true" <?php }; ?>
                                        value="<?php echo $slot['slot_id'] ?>"><?php echo $slot['slot_name'] ?></option>
                                    <?php

                                            }
                                        }
                                    }

                                    ?>
                                </select>

                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <input type="submit" class="btn btn-md button-theme" value="Search" />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </span>

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">

                <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                    <thead>

                        <tr>
                            <th>User ID</th>
                            <th>Name</th>
                            <th>Batch ID</th>
                            <th>Reg.Slot</th>
                            <th>N.Session</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($user_list) && !empty($user_list)) {
                            foreach ($user_list as $users) {
                                if ($users['status'] != 'inactive') {                                            ?>
                        <tr>
                            <td><?php echo $users['id']; ?></td>
                            <td><?php echo $users['username']; ?></td>
                            <td><?php echo 'thrive-' . $users['batch_id']; ?></td>
                            <td><?php echo $users['slot_name'] ?></td>
                            <td><?php echo $users['next_session'] ?></td>
                            <td>

                                <a class="btn btn-theme btn-md" target="_blank"
                                    href="<?= base_url() ?>/admin/user_profile/<?php echo $users['id']; ?>"> View</a>

                                <a onClick="meeting(<?php echo $users['id']; ?>); return false;"
                                    href="<?= base_url() ?>/admin/meet/<?php echo $users['id']; ?>"
                                    class="btn btn-theme btn-md">Meet</a>
                                <a class="btn btn-theme btn-md" target="_blank"
                                    href="<?= base_url() ?>/admin/user_edit/<?php echo $users['id']; ?>"> Edit</a>
                                <a class="btn btn-warning btn-md"
                                    href="<?= base_url() ?>/admin/blockstatus/<?php echo $users['id']; ?>">Block</a>
                            </td>
                        </tr>
                        <?php
                                }
                            }
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>User ID</th>
                            <th>Name</th>
                            <th>Batch ID</th>
                            <th>Reg.Slot</th>
                            <th>N.Session</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

        </div>
        <!-- /.box-body -->
    </div>
    <p class="sub-banner-2 text-center">©️ 2020 Thrivepad</p>
</div>
</div>
<div class="modal" id="meetModal" data-backdrop="false">
    <div class="modal-dialog" style="top:10% !important;" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header" style="text-align:center">

                <h4 class="modal-title">Schedule Meeting With </h4>
                <h4 class="modal-title" style="margin-left:2%" id="name"> </h4>
                <a href="#" class="close" data-dismiss="modal" aria-label="close">&times;</a>

            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <form action="<?= base_url(); ?>/admin/meet" method="post">

                    <input type="hidden" id="userid" name="userid" value="" />

                    <div class="form-group form-box">
                        <input type="text" name="meetlink" class="input-text" placeholder="Meeting Link" />
                    </div>
                    <div class="form-group form-box clearfix">
                        <input type="date" name="meetdate" class="input-text" placeholder="" />
                    </div>
                    <div class="form-group form-box clearfix">
                        <input type="time" name="meettime" class="input-text" placeholder="" />
                    </div>
                    <input type="submit" class="btn-md btn-theme float-right" />
                </form>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <input type="submit" class="btn btn-danger" value="close" data-dismiss="modal" />
            </div>

        </div>
    </div>

</div>
</div>
</div>




<script type="text/javascript">
function meeting(userid) {
    $('#meetModal').modal('show');
    $.ajax({
        type: 'GET',
        url: '<?php echo base_url('profile/userdata'); ?>',
        data: 'user_id=' + userid,
        success: function(data) {
            var res = $.parseJSON(data);
            console.log(res);
            $('#userid').val(res.profiledata['id']);
            $('#name').text(res.profiledata['username']);

            $('#meetModal').modal('show');
        }
    })
};
</script>