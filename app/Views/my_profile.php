<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
} ?>

<div class="dashboard-content">
    <div class="dashboard-header clearfix">
        <div class="row">
            <div class="col-sm-12 col-md-5">
                <h4>My Profile</h4>
            </div>
            <div class="col-sm-12 col-md-7">
                <div class="breadcrumb-nav">
                    <ul>
                        <li>
                            <a href="<?= base_url() ?>">Home</a>
                        </li>
                        <li>
                            <a href="<?= base_url() ?>/dashboard">Dashboard</a>
                        </li>
                        <li class="active">My Profile</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="dashboard-list">
        <h3 class="heading">Profile Details</h3>
        <div class="dashboard-message contact-2 bdr clearfix">
            <form action="<?= base_url(); ?>/myprofile" method="POST" enctype='multipart/form-data'>
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <!-- Edit profile photo -->
                        <div class="edit-profile-photo">
                            <img src="<?php if ($profiledata['profile_pic']) {
                                            echo base_url() . '/public/uploads/profilepic/' . $profiledata['profile_pic'];
                                        } else {
                                            echo 'http://placehold.it/223x223';
                                        } ?>" alt="profile-photo" class="img-fluid">
                            <div class="change-photo-btn">
                                <div class="photoUpload clip-home">
                                    <span><i class="fa fa-upload"></i></span>
                                    <input type="file" class="upload" name="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9">
                        <?php if (session()->getTempData('success')) : ?>
                        <div class="alert alert-success"><?= session()->getTempData('success') ?></div>
                        <?php endif; ?>
                        <?php if (session()->getTempData('error')) : ?>
                        <div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
                        <?php endif; ?>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group name">
                                    <label>First Name</label>
                                    <?php
                                    $val = '';
                                    if (set_value('username')) {
                                        $val = set_value('username');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['username'];
                                    }
                                    ?>
                                    <input type="text" name="username" value="<?php echo $val;  ?>" class="form-control"
                                        placeholder=" ">
                                    <small class="text-danger"><?= display_error($validation, 'username') ?></small>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group email">
                                    <label>Age</label>
                                    <?php
                                    $val = '';
                                    if (set_value('age')) {
                                        $val = set_value('age');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['age'];
                                    }
                                    ?>
                                    <input type="text" name="age" value="<?php echo $val; ?>" class="form-control"
                                        placeholder=" ">
                                    <small class="text-danger"><?= display_error($validation, 'age') ?></small>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group subject">
                                    <label>Last Name</label>
                                    <?php
                                    $val = '';
                                    if (set_value('lastname')) {
                                        $val = set_value('lastname');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['lastname'];
                                    }
                                    ?>
                                    <input type="text" name="lastname" value="<?php echo $val; ?>" class="form-control"
                                        placeholder="">
                                    <small class="text-danger"><?= display_error($validation, 'lastname') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group number">
                                    <label>City</label>
                                    <?php
                                    $val = '';
                                    if (set_value('city')) {
                                        $val = set_value('city');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['city'];
                                    }
                                    ?>
                                    <input type="text" name="city" value="<?php echo $val;  ?>" class="form-control"
                                        placeholder="">
                                    <small class="text-danger"><?= display_error($validation, 'city') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group subject">
                                    <label>Phone Number</label>
                                    <?php
                                    $val = '';
                                    if (set_value('mobile')) {
                                        $val = set_value('mobile');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['mobile'];
                                    }
                                    ?>
                                    <input type="text" name="mobile" value="<?php echo $val;  ?>" class="form-control"
                                        placeholder="">
                                    <small class="text-danger"><?= display_error($validation, 'mobile') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group number">
                                    <label>State</label>
                                    <?php
                                    $val = '';
                                    if (set_value('state')) {
                                        $val = set_value('state');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['state'];
                                    }
                                    ?>
                                    <input type="text" name="state" value="<?php echo $val;  ?>" class="form-control"
                                        placeholder="">
                                    <small class="text-danger"><?= display_error($validation, 'state') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group number">
                                    <label>Email Address</label>
                                    <?php
                                    $val = '';
                                    if (set_value('email')) {
                                        $val = set_value('email');
                                    } elseif (($profiledata)) {
                                        $val = $profiledata['email'];
                                    }
                                    ?>
                                    <input type="text" name="email" value="<?php echo $val;  ?>" class="form-control"
                                        placeholder="" disabled>
                                    <small class="text-danger"><?= display_error($validation, 'email') ?></small>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="send-btn">
                                <input type="submit" class="btn btn-md button-theme" value="Save Changes" />
                            </div>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class=" row">
        <div class="col-md-6">
            <div class="dashboard-list">
                <h3 class="heading">Change Password</h3>
                <div class="dashboard-message contact-2">
                    <?php if (session()->getTempData('password-success')) : ?>
                    <div class="alert alert-success">
                        <?= session()->getTempData('password-success') ?></div>
                    <?php endif; ?>

                    <?php if (session()->getTempData('password-error')) : ?>
                    <div class="alert alert-danger">
                        <?= session()->getTempData('password-error') ?></div>
                    <?php endif; ?>

                    <form action="<?= base_url(); ?>/myprofile/change-password" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group name">
                                    <label>Current Password</label>
                                    <input type="password" name="currentpassword" class="form-control" placeholder=" ">
                                    <small
                                        class="text-danger"><?= display_error($validation, 'currentpassword') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group email">
                                    <label>New Password</label>
                                    <input type="password" name="newpassword" class="form-control" placeholder=" ">
                                    <small class="text-danger"><?= display_error($validation, 'newpassword') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group subject">
                                    <label>Confirm New Password</label>
                                    <input type="password" name="confirmnewpassword" class="form-control"
                                        placeholder="  ">
                                    <small
                                        class="text-danger"><?= display_error($validation, 'confirmnewpassword') ?></small>

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="send-btn">
                                    <button type="submit" class="btn btn-md button-theme">Change
                                        Password</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-6">
                        <div class="dashboard-list">
                            <h3 class="heading">Trusted Connection</h3>
                            <div class="dashboard-message contact-2">
                                <div class="col-lg-12">
                                <div class="main-title">
                                    <p>Connect your Pronto account to these trusted identity providers and login with ease.</p>
                                </div>
                                <div class="col-lg-12"> 
                                <div class="send-btn">
                                    <button type="submit" class="btn btn-md button-google"> <i class="fa fa-google" aria-hidden="true"></i>
connect with Google</button>
                                </div>
                                </div>
                                <div class="col-lg-12 mt-2">
                                <div class="send-btn">
                                    <button type="submit" class="btn btn-md button-facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i> connect with Facebooh</button>                                
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>   -->
        <p class="sub-banner-2 text-center">© 2020 Pronto</p>
    </div>
</div>

<!-- Dashbord end -->