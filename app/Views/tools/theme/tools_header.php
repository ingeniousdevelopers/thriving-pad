<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page_title ?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url() ?>/public/assets/tools/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <!-- Custom Fonts -->
    <link href="<?= base_url() ?>/public/assets/tools/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Kaushan+Script" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic" rel="stylesheet"
        type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css">

    <!-- Theme CSS -->
    <link href="<?= base_url() ?>/public/assets/tools/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
    <style type="text/css">
    .img-class {
        width: 16%;
        vertical-align: top;
        padding-right: 15px;
    }
    </style>
</head>

<body id="page-top" class="index" data-new-gr-c-s-check-loaded="14.996.0" data-gr-ext-installed="">


    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top nav-menu-index">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="<?= base_url() ?>"><img
                        src="<?= base_url() ?>/public/assets/img/logos/logo.png" width=200 alt="" class=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo $userData['username'] ?>
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?= base_url() ?>/dashboard">Dashboard</a></li>
                            <li><a href="<?= base_url() ?>/messages" target="_blank">Live Sessions</a></li>
                            <li><a href="<?= base_url() ?>/base-values" target="_blank">Baseline Values</a></li>
                            <li><a href="<?= base_url() ?>/wheel-of-life" target="_blank">Wheel Of Life</a></li>
                            <li><a href="<?= base_url() ?>/ncodes" target="_blank">E Codes & N Codes</a></li>
                            <li><a href="<?= base_url() ?>/outcome-reinforcement" target="_blank">Outcome
                                    Reinforcement</a></li>
                            <li><a href="<?= base_url() ?>/decision-wheel" target="_blank">Decision Wheel</a></li>
                            <li><a href="<?= base_url() ?>/myprofile">My profile</a></li>
                            <li><a href="<?= base_url() ?>/logout">Logout</a></li>
                            <!--<li><a href="#">--</a></li>-->
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <script>
    $(document).ready(function() {
        $("#myInput").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $("#myDIV *").filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
            });
        });
    });
    </script>

    <style>
    small {
        color: red;
    }
    </style>
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.0/css/bootstrap-slider.min.css">