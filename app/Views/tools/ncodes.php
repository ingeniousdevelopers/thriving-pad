<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>Practice N-CODES</h1>
            <div id="stick-here" style="height: 0px;"></div>
            <p>Select as many Ncode- as you can that you feel are important to you.</p>
            <!--<p>Select  words that describe what matters most to you.</p>-->
            <div id="stickThis">
                <div id="stickThis" class="row text-center">
                    <div class="container">
                        <div class="col-md-offset-3 col-md-3">
                            <input id="myInput" type="text" placeholder="Search Words.." class="form-control">
                        </div>
                        <div class="col-md-4" style="padding-top: 8px;">
                            <span class="questions_remaining pr">Words Selected: <span id="words_selected">0</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>

<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list check-sentence">
            <div class="row">
                <?php
                if (!empty($ncode)) {

                    $j = 1;

                ?>
                <form action="<?= base_url() ?>/tools/updatencode" method="POST">
                    <?php

                        $val = '';
                        if (set_value('array_ncode')) {
                            $val = set_value('array_ncode');
                        } elseif (($ncode[0]['array_ncode'])) {
                            $val = $ncode[0]['array_ncode'];
                            $splittedstring = explode("~", $val);
                            foreach ($ncode as $fil) {
                                if ($fil['filter'] == 5) {
                                    array_pop($splittedstring);
                                } elseif ($fil['filter'] == 4) {
                                    array_splice($splittedstring, 4);
                                } elseif ($fil['filter'] == 3) {
                                    array_splice($splittedstring, 3);
                                } elseif ($fil['filter'] == 2) {
                                    array_splice($splittedstring, 2);
                                }

                                if (count($splittedstring) > 24) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 24</b></div>
                    <?php
                                } elseif (count($splittedstring) == 24) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 6</b></div>
                    <?php
                                } elseif (count($splittedstring) == 6) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 5</b></div>
                    <?php
                                } elseif (count($splittedstring) == 5) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 4</b></div>
                    <?php
                                } elseif (count($splittedstring) == 4) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 3</b></div>
                    <?php
                                } elseif (count($splittedstring) == 3) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 2</b></div>
                    <?php
                                } elseif (count($splittedstring) == 2) { ?>
                    <div class="alert alert-danger text-center"><b>CHOOSE ONLY 1</b></div>
                    <?php
                                }
                            }

                            foreach ($ncodevalue as $code) {
                                foreach ($ncode as $fil) {
                                    if ($fil['filter'] != 1) {
                                        foreach ($splittedstring as $val) {
                                            if ($code['id'] == $val) {

                                    ?>
                    <div class="form-checkbox">
                        <input type="checkbox" name="sentence[]" value="<?php echo $code['id'] ?>"
                            id="<?php echo $code['id'] ?>" />
                        <label for="<?php echo $code['id'] ?>" title="<?php $code['ncodes'] ?>"
                            style=" text-align:left">
                            <?php echo $j++ . '. ' . $code['ncodes']; ?> </label>
                    </div>
                    <br>
                    <?php



                                            }
                                        }
                                    }
                                }
                            }
                        }

                        ?>
                    <div class=" text-center">
                        <small id="sentence_error" class="text-center"> </small>
                    </div>
                    <div class="text-center">
                        <input type="submit" class="btn  btn-orange btn-width" id="submit_btn2"
                            onclick="return form_validation2()" value="Submit" />
                    </div>

                </form>

                <?php
                } else {

                ?>
                <div class="alert alert-danger text-center"><b>CHOOSE AS MANY VALUES YOU CAN RESONATE WITH</b></div>
                <form action="<?= base_url() ?>/tools/insertncode" method="POST">

                    <?php $j = 0;
                        foreach ($ncodevalue as $nc) {
                            $j = $j + 1; ?>

                    <div class="form-checkbox">
                        <input type="checkbox" name="sentence[]" id="<?php echo $nc['id'] ?>"
                            value="<?php echo $nc['id'] ?>" />
                        <label for="<?php echo $nc['id'] ?>" title="<?php echo $nc['ncodes']; ?>"
                            style="text-align:left">
                            <?php
                                    echo $j . '. ' . $nc['ncodes'];
                                    ?>
                        </label>
                    </div>

                    <br>
                    <?php
                        }
                        ?>
            </div>
        </div>
        <input type="hidden" value="1" id="level" name="level" />
        <input type="hidden" value="1" id="status" name="status" />
        <input type="hidden" value="6" id="filter" name="filter" />
        <div class="text-center">
            <small id="sentence_error" class="text-center"> </small>
        </div>
        <div class="text-center">
            <input type="submit" class="btn  btn-orange btn-width" id="submit_btn" onclick="return form_validation()"
                value="Submit" />
        </div>
        </form>
        <?php
                }
    ?>

    </div>

    <script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
    <script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>

    <script type="text/javascript">
    $('input[name="sentence[]"]').click(function() {
        var total_words = $('input[name="sentence[]"]:checked').length;
        $("#submit_btn").val("Submit & Next (" + total_words + ")");
        $("#submit_btn2").val("Submit & Next (" + total_words + ")");
        $("#words_selected").html(total_words);
    });

    function form_validation() {
        var total_words = $('input[name="sentence[]"]:checked').length;
        var sel_val = 24;
        if (sel_val != 24) {
            if (total_words < sel_val) {
                $("#sentence_error").html("Please Select " + sel_val + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words >= 24 && sel_val >= 24) {
                return true;
            } else if (total_words < sel_val || total_words > sel_val) {
                $("#sentence_error").html("Please Select " + sel_val + " words. Count=" + total_words);
                return false;
            }
        }
    }

    function form_validation2() {
        var total = $('input[name="sentence[]"]').length;
        var total_words = $('input[name="sentence[]"]:checked').length;
        var sel_val = 24;
        var sel_val2 = 6;
        var sel_val3 = 5
        var sel_val4 = 4;
        var sel_val5 = 3;
        var sel_val6 = 2;
        var sel_val7 = 1;

        if (total > 24) {
            if (sel_val != 24) {
                if (total_words < sel_val || total_words > sel_val) {
                    $("#sentence_error").html("Please Select More Than" + sel_val + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 24) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val || total_words > sel_val)) {
                    $("#sentence_error").html("Please Select " + sel_val + " words. Count=" + total_words);
                    return false;
                }
            }
        }
        if (total == 24) {
            if (sel_val2 != 6) {
                if (total_words < sel_val2 || total_words > sel_val2) {
                    $("#sentence_error").html("Please Select " + sel_val2 + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 6) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val2 + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val2 || total_words > sel_val2)) {
                    $("#sentence_error").html("Please Select " + sel_val2 + " words. Count=" + total_words);
                    return false;
                }
            }
        }

        if (total == 6) {
            if (sel_val3 != 5) {
                if (total_words < sel_val3 || total_words > sel_val3) {
                    $("#sentence_error").html("Please Select " + sel_val3 + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 5) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val3 + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val3 || total_words > sel_val3)) {
                    $("#sentence_error").html("Please Select " + sel_val3 + " words. Count=" + total_words);
                    return false;
                }

            }

        }
        if (total == 5) {
            if (sel_val4 != 4) {
                if (total_words < sel_val4 || total_words > sel_val4) {
                    $("#sentence_error").html("Please Select " + sel_val4 + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 4) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val4 + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val4 || total_words > sel_val4)) {
                    $("#sentence_error").html("Please Select " + sel_val4 + " words. Count=" + total_words);
                    return false;
                }
            }
        }

        if (total == 4) {
            if (sel_val5 != 4) {
                if (total_words < sel_val5 || total_words > sel_val5) {
                    $("#sentence_error").html("Please Select " + sel_val5 + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 3) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val5 + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val5 || total_words > sel_val5)) {
                    $("#sentence_error").html("Please Select " + sel_val5 + " words. Count=" + total_words);
                    return false;
                }
            }
        }

        if (total == 3) {
            if (sel_val6 != 3) {
                if (total_words < sel_val6 || total_words > sel_val6) {
                    $("#sentence_error").html("Please Select " + sel_val6 + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 2) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val6 + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val6 || total_words > sel_val6)) {
                    $("#sentence_error").html("Please Select " + sel_val6 + " words. Count=" + total_words);
                    return false;
                }
            }
        }

        if (total == 2) {
            if (sel_val7 != 2) {
                if (total_words < sel_val7 || total_words > sel_val7) {
                    $("#sentence_error").html("Please Select " + sel_val7 + " words. Count=" + total_words);
                    return false;
                }
            } else {
                if (total_words < 1) {
                    $("#sentence_error").html("Please Select Minimum " + sel_val7 + " words. Count=" +
                        total_words);
                    return false;
                } else if ((total_words < sel_val7 || total_words > sel_val7)) {
                    $("#sentence_error").html("Please Select " + sel_val7 + " words. Count=" + total_words);
                    return false;
                }
            }
        }
        return true;
    }
    </script>
    <script>
    function sticktothetop() {
        var window_top = $(window).scrollTop();
        var top = $('#stick-here').offset().top;
        if (window_top > top) {
            $('#stickThis').addClass('stick');
            $('#stick-here').height($('#stickThis').outerHeight());
        } else {
            $('#stickThis').removeClass('stick');
            $('#stick-here').height(0);
        }
    }
    $(function() {
        $(window).scroll(sticktothetop);
        sticktothetop();
    });
    </script>

    <script>
    function data_delete(type) {
        if (type == 1) {
            window.location = 'https://coachtofortune.com/coachinghub/' + "coach/practice";
        } else {
            $.ajax({
                type: 'POST',
                url: 'https://coachtofortune.com/coachinghub/coach/practice/beliefs_delete',
                data: '',
                success: function(data) {
                    if (data == 1 && type == 1) {} else if (data == 1 && type == 2) {
                        window.location = 'https://coachtofortune.com/coachinghub/' +
                            "coach/practice/beliefs";
                    }
                }
            });
        }
    }
    </script>

    <script>
    // $(document).ready(function() {
    //     function disableBack() { window.history.forward() }

    //     window.onload = disableBack();
    //     window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
    // });
    </script>