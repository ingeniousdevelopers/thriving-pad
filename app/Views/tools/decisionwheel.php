<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">

            <div id="stick-here"></div>
            <div id="stickThis">
                <h1>Expected Value Calculator</h1>
            </div>

        </div>
    </div>
</header>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<div class="container mb-40">
    <form action="#" method="POST" id="decision_form">
        <script>

        </script>
        <div class="row mr-t-3em">

            <div class="col-md-12">
                <div class="questions scenario-questions">
                    <h4 class="service-heading">The Activity -</h4>
                    <p>Step 1: click in the center and write down the problem.</p>
                    <p>Step 2: one by one, click on each highlighted area and answer the respective questions.</p>
                    <p>Let’s begin.</p>
                </div>
            </div>
            <div class="col-md-12">
                <div style="display: block;overflow: hidden;">

                    <nav class="menunav menuNavCircular text-center">
                        <ul class="circle nav-tabs">

                            <li class="unsel circle  list-1">
                                <label for="unsel" class="clicky menuname active" data-target="#questionModal1"
                                    id="questionLabel1" data-toggle="modal" data-backdrop="static"
                                    data-keyboard="false">1.
                                    Problem</label>
                            </li>


                            <li class="coconut light slice fade-color list-2">
                                <!-- Menu labels -->
                                <label for="ococonut" class="circle over next-step-color " id="questionLabel2"
                                    data-toggle="modal" data-target=" #questionModal2" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">2.
                                        Choices</span></label>
                            </li>
                            <li class="vanilla light slice fade-color list-3">
                                <label for="ovanilla" data-target="#questionModal3" class="circle over"
                                    data-toggle="modal" id="questionLabel3" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">3.
                                        Consquences</span></label>
                            </li>
                            <li class="orange light slice fade-color list-4">
                                <label for="oorange" data-target="#questionModal4" class="circle over"
                                    data-toggle="modal" id="questionLabel4" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">4.
                                        Values</span></label>
                            </li>
                            <li class="almond light slice fade-color list-5">
                                <label for="oorange" data-target="#questionModal5" class="circle over"
                                    data-toggle="modal" id="questionLabel5" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">5.
                                        Feelings</span></label>
                            </li>
                            <li class="grape light slice fade-color list-6">
                                <label for="oorange" data-target="#questionModal6" class="circle over"
                                    data-toggle="modal" id="questionLabel6" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">6. More
                                        Info</span></label>
                            </li>
                            <li class="blackberry dark slice fade-color list-7">
                                <label for="oorange" data-target="#questionModal7" class="circle over"
                                    data-toggle="modal" id="questionLabel7" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">7. Who Can
                                        Help</span></label>
                            </li>
                            <li class="cherry dark slice fade-color list-8">
                                <label for="oorange" data-target="#questionModal8" class="circle over"
                                    data-toggle="modal" id="questionLabel8" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">8.
                                        Decision</span></label>
                            </li>

                            <li class="apple dark slice fade-color list-9">
                                <label for="oorange" data-target="#questionModal9" class="circle over"
                                    data-toggle="modal" id="questionLabel9" data-backdrop="static"
                                    data-keyboard="false"><span class="rotate-text">9. Assess
                                        Decision</span></label>
                            </li>
                            <!--  <li class='middle circle'></li> -->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </form>
</div>

<?php

if ($wheel[0]['q1'] && $wheel[0]['q2'] && $wheel[0]['q3'] && $wheel[0]['q4'] && $wheel[0]['q5'] && $wheel[0]['q6'] && $wheel[0]['q7'] && $wheel[0]['q8'] && $wheel[0]['q9']) { ?>
<div class="container mb-40" id="dec_result">

    <div class="row result-single-section result-section">
        <div class="x_title">
            <h2>Decision Making Result</h2>
            <div class="clearfix"></div>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="50%">Question</th>
                        <th>Answer</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach ($wheel as $dw) {
                        ?> <tr>
                        <td>1. What is the problem?</td>
                        <td><?php echo $dw['q1'] ?></td>
                    </tr>
                    <tr>
                        <td>2. What are the choices you have?</td>
                        <td><?php echo $dw['q2'] ?></td>
                    </tr>
                    <tr>
                        <td>3. What do you think the consequences of these choices will be for yourself and others
                            who are involved?</td>
                        <td><?php echo $dw['q3'] ?></td>
                    </tr>
                    <tr>
                        <td>4. What values do you need to consider?</td>
                        <td><?php echo $dw['q4'] ?></td>
                    </tr>
                    <tr>
                        <td>5. How do you feel about the situation?</td>
                        <td><?php echo $dw['q5'] ?></td>
                    </tr>
                    <tr>
                        <td>6. Is there anything else you need to learn about it?</td>
                        <td><?php echo $dw['q6'] ?></td>
                    </tr>
                    <tr>
                        <td>7. Do you need to ask for help? Who will you ask?</td>
                        <td><?php echo $dw['q7'] ?></td>
                    </tr>
                    <tr>
                        <td>8. What is your decision?</td>
                        <td><?php echo $dw['q8'] ?></td>
                    </tr>
                    <tr>
                        <td>9. Do you think you made the right decision? Why?</td>
                        <td><?php echo $dw['q9'] ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
<?php } ?>


<!-- accessDecision  Modal -->
<div class="modal fade" id="questionModal9" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q9')) {
                    $val = set_value('q9');
                } elseif (($wheel[0]['q9'])) {
                    $q9 = $wheel[0]['q9'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">9. Do you think you made the right decision? Why?</h4>
                    <input class="form-control" id="answerContent9" placeholder="Answer.." <?php echo $attr; ?>
                        value="<?php echo $q9 ?>">
                </div>
                <small id="answerContent_error9"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('9')">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- decision Modal -->
<div class="modal fade" id="questionModal8" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q8')) {
                    $val = set_value('q8');
                } elseif (($wheel[0]['q8'])) {
                    $q8 = $wheel[0]['q8'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">8. What is your decision?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q8 ?>" class="form-control" id="answerContent8"
                        placeholder="Answer..">
                </div>
                <small id="answerContent_error8"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('8')">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- whoCanHelp Modal -->
<div class="modal fade" id="questionModal7" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q7')) {
                    $val = set_value('q7');
                } elseif (($wheel[0]['q7'])) {
                    $q7 = $wheel[0]['q7'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">7. Do you need to ask for help? Who will you ask?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q7 ?>" class="form-control" id="answerContent7"
                        placeholder="Answer..">
                </div>
                <small id="answerContent_error7"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('7')">Submit</button>
            </div>
        </div>

    </div>
</div>


<!-- moreInfo  Modal -->
<div class="modal fade" id="questionModal6" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q6')) {
                    $val = set_value('q6');
                } elseif (($wheel[0]['q6'])) {
                    $q6 = $wheel[0]['q6'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">6. Is there anything else you need to learn about it?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q6 ?>" class="form-control" id="answerContent6"
                        placeholder="Answer..">
                </div>
                <small id="answerContent_error6"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('6')">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- feelings Modal -->
<div class="modal fade" id="questionModal5" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q5')) {
                    $val = set_value('q5');
                } elseif (($wheel[0]['q5'])) {
                    $q5 = $wheel[0]['q5'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">5. How do you feel about the situation?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q5 ?>" class="form-control" id="answerContent5"
                        placeholder="Answer..">
                </div>
                <small id="answerContent_error5"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('5')">Submit</button>
            </div>
        </div>

    </div>
</div>


<!-- values Modal -->
<div class="modal fade" id="questionModal4" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q4')) {
                    $val = set_value('q4');
                } elseif (($wheel[0]['q4'])) {
                    $q4 = $wheel[0]['q4'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">4. What values do you need to consider?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q4 ?>" class="form-control" id="answerContent4"
                        placeholder="Answer..">
                </div>
                <small id="answerContent_error4"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('4')">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- Consequences  Modal -->
<div class="modal fade" id="questionModal3" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q3')) {
                    $val = set_value('q3');
                } elseif (($wheel[0]['q3'])) {
                    $q3 = $wheel[0]['q3'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">3. What do you think the consequences of these choices will be for
                        yourself and others who are involved?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q3 ?>" type="text" id="answerContent3"
                        class="form-control" placeholder="Answer..">
                </div>
                <small id="answerContent_error3"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <button type="button" class="btn  btn-next-orange next-step" onclick="submitAnswer('3')">Submit</button>
            </div>
        </div>

    </div>
</div>

<!-- Choices  Modal -->
<div class="modal fade" id="questionModal2" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q2')) {
                    $val = set_value('q2');
                } elseif (($wheel[0]['q2'])) {
                    $q2 = $wheel[0]['q2'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">2. What are the choices you have?</h4>
                    <input <?php echo $attr; ?> value="<?php echo $q2 ?>" type="text" name="choices" id="answerContent2"
                        class="form-control" placeholder="Answer..">
                </div>
                <small id="answerContent_error2"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <input type="submit" class="btn  btn-next-orange next-step" onclick="submitAnswer('2');"
                    value="Submit" />
            </div>
        </div>

    </div>
</div>
<!-- Problem  Modal -->
<div class=" modal fade" id="questionModal1" role="dialog">
    <div class="modal-dialog modal-md">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>
            <div class="modal-body">
                <?php
                $val = '';
                $attr = '';
                if (set_value('q1')) {
                    $val = set_value('q1');
                } elseif (($wheel[0]['q1'])) {
                    $q1 = $wheel[0]['q1'];

                    $attr = 'disabled';
                }
                ?>
                <div class="questions">
                    <h4 class="service-heading">1. What is the problem?</h4>
                    <input type="hidden" name="qid" value="1" />
                    <input <?php echo $attr; ?> value="<?php echo $q1 ?>" type="text" id="answerContent1"
                        class="form-control" placeholder="Answer..">
                </div>
                <small id="answerContent_error1"></small>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <input type="submit" class="btn  btn-next-orange next-step" onclick="submitAnswer('1')"
                    value="Submit" />
            </div>
        </div>

    </div>
</div>

<button id=" success_btn" data-toggle="modal" data-target="#stepsModal" data-backdrop="static" data-keyboard="false"
    style="display: none;">Success</button>
<div class="modal fade" id="stepsModal" role="dialog">
    <div class="modal-dialog modal-md dec_modal_dialog">
        <!-- Modal content-->
        <div class="modal-content dec_modal_content">
            <!-- <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div> -->
            <div class="modal-body">
                <div class="questions">
                    <h4 class="service-heading color-orange">Awesome!</h4>
                    <p class="text-muted tag-content text-center">You have successfully completed your
                        assignment
                    </p>
                </div>
            </div>
            <div class="modal-footer modal-footer-center text-center">
                <a class="btn btn-orange btn-orange Broadcast-btn"
                    href="https://coachtofortune.com/coachinghub/coach/practice/decision">Ok</a>
            </div>
        </div>

    </div>
</div>




<script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    /*$(".next-step").click(function () {
     $(".active").addClass("complete");
     $(".active  label").addClass("disabled");
     });*/


});
</script>


</script>


<script type="text/javascript">
function submitAnswer(question_id) {
    $('#answerContent_error' + question_id).empty();
    var ans = $('#answerContent' + question_id).val();
    if (ans === '') {
        $('#answerContent' + question_id).focus();
        $('#answerContent_error' + question_id).html("Please Answer The Question!");
    } else {
        $.ajax({
            type: 'POST',
            url: '<?= base_url(); ?>/decision-wheel/create',
            data: {
                qid: question_id,
                qans: ans,
            },
            success: function(data) {
                $(" #questionModal" + question_id).modal('hide');
                if (question_id > 1) {
                    $("#questionLabel" + question_id).parent().children('label').removeClass(
                        'active');
                    $("#questionLabel" + question_id).parent().next('li').children('label')
                        .addClass('active');

                } else {
                    $("#questionLabel" + question_id).parent().children('label').removeClass(
                        'active');
                    $("#questionLabel" + question_id).parent().next('li').children('label')
                        .addClass(
                            'active');
                }

                $("#questionLabel" + question_id).click(function(event) {
                    event.stopPropagation();
                });

                if (question_id == 1) {
                    $("#questionLabel" + question_id).parent().addClass('complete');
                } else {
                    $("#questionLabel" + question_id).addClass('complete');
                }
                if (question_id == 9) {
                    // $('body').css('overflow-y', 'hidden');
                    $("#success_btn").trigger("click");
                    location.reload();
                }

            }
        });
    }
}


function delete_data() {
    $.ajax({
        type: 'POST',
        url: 'https://coachtofortune.com/coachinghub/coach/practice/dec_delete',
        data: '',
        success: function(data) {
            if (data == 1) {
                window.location = 'https://coachtofortune.com/coachinghub/' +
                    "coach/practice/decision";
            }
        }
    });
}

/*$('#stepsModal').modal().on('shown', function () {
$('body').css('overflow', 'hidden');
}).on('hidden', function () {
$('body').css('overflow', 'auto');
})*/
</script>


<script>
// $(document).ready(function() {
// function disableBack() { window.history.forward() }

// window.onload = disableBack();
// window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
// });
</script>

</body>
<div class="container mb-40">
    <div class="text-center">
        <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
            <button class="btn btn-orange btn-orange1 mr-t-2em">
                Let's
                go Back</button>
            <br />
            <?php ?>
            <form action="<?= base_url() ?>/tools/redo" method="POST">
                <input type="hidden" name="tab" value="decisionwheel" />
                <input type="hidden" name="url" value="<?= base_url() ?>/decision-wheel" />
                <br />
                <button type="submit" class="btn btn-orange btn-orange Broadcast-btn">Redo</button>
            </form>
    </div>
</div>

</html>