<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>N-CODES</h1>
            <div id="stick-here" style="height: 0px;"></div>

        </div>
    </div>
</header>
<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list check-sentence">
            <?php

            ?>

            <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                <thead>

                    <tr>

                        <th>S.No</th>
                        <th>N - Code</th>
                        <th>P - Code</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $val = $ncode[0]['array_ncode'];
                    $splittedstring = explode("~", $val);
                    $s1 = $splittedstring[0];
                    $s2 = $splittedstring[1];
                    $s3 = $splittedstring[2];
                    $s4 = $splittedstring[3];
                    $s5 = $splittedstring[4];
                    $s6 = $splittedstring[5];
                    ?>
                    <tr>
                        <td>1</td>
                        <td><?php echo $ncodevalue[$s1 - 1]['ncodes']; ?></td>
                        <td><?php echo $ncodevalue[$s1 - 1]['pcodes']; ?></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><?php echo $ncodevalue[$s2 - 1]['ncodes']; ?></td>
                        <td><?php echo $ncodevalue[$s2 - 1]['pcodes']; ?></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><?php echo $ncodevalue[$s3 - 1]['ncodes']; ?></td>
                        <td><?php echo $ncodevalue[$s3 - 1]['pcodes']; ?></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><?php echo $ncodevalue[$s4 - 1]['ncodes']; ?></td>
                        <td><?php echo $ncodevalue[$s4 - 1]['pcodes']; ?></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><?php echo $ncodevalue[$s5 - 1]['ncodes']; ?></td>
                        <td><?php echo $ncodevalue[$s5 - 1]['pcodes']; ?></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td><?php echo $ncodevalue[$s6 - 1]['ncodes']; ?></td>
                        <td><?php echo $ncodevalue[$s6 - 1]['pcodes']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</div>
<div class="container mb-40">
    <div class="text-center">
        <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
            <button class="btn btn-orange btn-orange1 mr-t-2em">
                Let's
                go Back</button>
            <br />
            <br />
            <form action="<?= base_url() ?>/tools/redo" method="POST">
                <input type="hidden" name="url" value="<?= base_url() ?>/ncodes" />

                <input type="hidden" name="tab" value="ncode" />
                <br /> <button type="submit" class="btn btn-orange btn-orange Broadcast-btn">Redo</button>
            </form>
    </div>
</div>


<script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
<script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>



<script>
function sticktothetop() {
    var window_top = $(window).scrollTop();
    var top = $('#stick-here').offset().top;
    if (window_top > top) {
        $('#stickThis').addClass('stick');
        $('#stick-here').height($('#stickThis').outerHeight());
    } else {
        $('#stickThis').removeClass('stick');
        $('#stick-here').height(0);
    }
}
$(function() {
    $(window).scroll(sticktothetop);
    sticktothetop();
});
</script>