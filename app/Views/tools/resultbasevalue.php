<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>BASEVALUES</h1>
            <div id="stick-here" style="height: 0px;"></div>

        </div>
    </div>
</header>
<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list check-sentence">
            <?php

            ?>

            <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">

                <thead>

                    <tr>

                        <th>S.No</th>
                        <th>Basevalues</th>
                        <th>Definition</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $val = $baselinevalue[0]['array_baselinevalues'];
                    $splittedstring = explode("~", $val);
                    $s1 = $splittedstring[0];
                    $s2 = $splittedstring[1];
                    $s3 = $splittedstring[2];
                    $s4 = $splittedstring[3];
                    $s5 = $splittedstring[4];
                    $s6 = $splittedstring[5];
                    ?>
                    <tr>
                        <td>1</td>
                        <td><?php echo $basevalues[$s1 - 1]['basevalues']; ?></td>
                        <td><?php echo $basevalues[$s1 - 1]['definition']; ?></td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td><?php echo $basevalues[$s2 - 1]['basevalues']; ?></td>
                        <td><?php echo $basevalues[$s2 - 1]['definition']; ?></td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td><?php echo $basevalues[$s3 - 1]['basevalues']; ?></td>
                        <td><?php echo $basevalues[$s3 - 1]['definition']; ?></td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td><?php echo $basevalues[$s4 - 1]['basevalues']; ?></td>
                        <td><?php echo $basevalues[$s4 - 1]['definition']; ?></td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td><?php echo $basevalues[$s5 - 1]['basevalues']; ?></td>
                        <td><?php echo $basevalues[$s5 - 1]['definition']; ?></td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td><?php echo $basevalues[$s6 - 1]['basevalues']; ?></td>
                        <td><?php echo $basevalues[$s6 - 1]['definition']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="container mb-40">
    <div class="text-center">
        <a class="btn btn-theme btn-md" href="<?= base_url() ?>/dashboard">
            <button class="btn btn-orange btn-orange1 mr-t-2em">
                Let's
                go Back</button>
            <br />
            <br />
            <form method="POST" action="<?= base_url() ?>/tools/redo">
                <input type="hidden" name="tab" value="baselinevalues" />
                <input type="hidden" name="url" value="<?= base_url() ?>/base-values" />
                <br />
                <button type="submit" class="btn btn-orange btn-orange Broadcast-btn">Redo

            </form>
    </div>
</div>

<script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
<script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>



<script>
function sticktothetop() {
    var window_top = $(window).scrollTop();
    var top = $('#stick-here').offset().top;
    if (window_top > top) {
        $('#stickThis').addClass('stick');
        $('#stick-here').height($('#stickThis').outerHeight());
    } else {
        $('#stickThis').removeClass('stick');
        $('#stick-here').height(0);
    }
}
$(function() {
    $(window).scroll(sticktothetop);
    sticktothetop();
});
</script>