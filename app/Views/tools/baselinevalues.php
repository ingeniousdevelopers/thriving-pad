<header class="header-company-to-caoch">
    <div class="container-fluid">
        <div class="hder-text">
            <h1>Practice Baselines</h1>
            <div id="stick-here" style="height: 0px;"></div>
            <p>Select as many baselines as you can that you feel are important to you.</p>
            <!--<p>Select  words that describe what matters most to you.</p>-->
            <div id="stickThis">
                <div id="stickThis" class="row text-center">
                    <div class="container">
                        <div class="col-md-offset-3 col-md-3">
                            <input id="myInput" type="text" placeholder="Search Words.." class="form-control">
                        </div>
                        <div class="col-md-4" style="padding-top: 8px;">
                            <span class="questions_remaining pr">Words Selected: <span id="words_selected">0</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>

<div class="container mb-40">
    <div class="col-md-12" id="myDIV">
        <div class="check-content-list text-center">
            <div class="row">
                <?php
                if (!empty($baselinevalue)) {

                    $j = 1;

                ?>
                <form action="<?= base_url() ?>/tools/updbasvalue" method="POST">
                    <?php

                        $val = '';
                        if (set_value('array_baselinevalues')) {
                            $val = set_value('array_baselinevalues');
                        } elseif (($baselinevalue[0]['array_baselinevalues'])) {
                            $val = $baselinevalue[0]['array_baselinevalues'];
                            $splittedstring = explode("~", $val);
                            foreach ($baselinevalue as $baseval) {
                                if ($baseval['filter'] == 5) {
                                    array_pop($splittedstring);
                                } elseif ($baseval['filter'] == 4) {
                                    array_splice($splittedstring, 4);
                                } elseif ($baseval['filter'] == 3) {
                                    array_splice($splittedstring, 3);
                                } elseif ($baseval['filter'] == 2) {
                                    array_splice($splittedstring, 2);
                                }
                            }
                            if (count($splittedstring) > 24) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 24</b></div>
                    <?php
                            } elseif (count($splittedstring) == 24) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 6</b></div>
                    <?php
                            } elseif (count($splittedstring) == 6) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 5</b></div>
                    <?php
                            } elseif (count($splittedstring) == 5) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 4</b></div>
                    <?php
                            } elseif (count($splittedstring) == 4) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 3</b></div>
                    <?php
                            } elseif (count($splittedstring) == 3) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 2</b></div>
                    <?php
                            } elseif (count($splittedstring) == 2) { ?>
                    <div class="alert alert-danger"><b>CHOOSE ONLY 1</b></div>
                    <?php
                            }

                            foreach ($basevalues as $baseval) {
                                foreach ($baselinevalue as $baseline) {
                                    if ($baseline['filter'] != 1) {
                                        foreach ($splittedstring as $val) {
                                            if ($baseval['id'] == $val) {
                                ?>
                    <div class="col-md-3">

                        <div class="form-checkbox" style="width:100%;">
                            <a style="color:black;text-decoration:none" href="#" data-toggle="tooltip"
                                data-placement="right" title=" <?php echo $baseval['definition'] ?>">


                                <input type="checkbox" name="va_words[]" value="<?php echo $baseval['id'] ?>"
                                    id="<?php echo $baseval['id'] ?>" />
                                <label for="<?php echo $baseval['id'] ?>" title="<?php $baseval['basevalues'] ?>"
                                    style=" text-align:left">
                                    <?php echo $j++ . '. ' . $baseval['basevalues']; ?> </label>
                            </a>

                        </div>
                    </div>
                    <?php



                                            }
                                        }
                                    }
                                }
                            }
                        }

                        ?>
            </div>
            <div class=" text-center">
                <small id="words_error" class="text-center"> </small>
            </div>

            <div class="text-center">
                <input type="submit" class="btn  btn-orange btn-width" id="submit_btn2"
                    onclick="return form_validation2()" value="Submit" />
            </div>

            </form>

            <?php
                } else {

        ?>
            <div class="alert alert-danger"><b>CHOOSE AS MANY VALUES YOU CAN RESONATE WITH</b></div>
            <form action="<?= base_url() ?>/tools/insbasevalue" method="POST">
                <?php $j = 0;
                    foreach ($basevalues as $baseval) {
                        $j = $j + 1; ?>
                <div class="row col-md-3">

                    <div class="form-checkbox" style="width:100%;">
                        <a style="color:black;text-decoration:none" href="#" data-toggle="tooltip"
                            data-placement="right" title=" <?php echo $baseval['definition'] ?>">
                            <input type="checkbox" name="va_words[]" id="<?php echo $baseval['id'] ?>"
                                value="<?php echo $baseval['id'] ?>" />
                            <label for="<?php echo $baseval['id'] ?>" title="<?php echo $baseval['basevalues']; ?>"
                                style="text-align:left"><?php echo $j . '. ' . $baseval['basevalues']; ?></label>
                        </a>
                    </div>
                </div>

                <?php
                    }
                ?>
        </div>
    </div>
    <input type="hidden" value="1" id="level" name="level" />
    <input type="hidden" value="1" id="status" name="status" />
    <input type="hidden" value="6" id="filter" name="filter" />
    <div class="text-center">
        <small id="words_error" class="text-center"> </small>
    </div>
    <div class="text-center">
        <input type="submit" class="btn  btn-orange btn-width" id="submit_btn" onclick="return form_validation()"
            value="Submit" />
    </div>
    </form>
    <?php
                }
?>

</div>

<script src=" https://coachtofortune.com/coachinghub/assets/js/jquery.min.js"></script>
<script src="https://coachtofortune.com/coachinghub/assets/js/bootstrap.min.js"></script>

<script type="text/javascript">
$('input[name="va_words[]"]').click(function() {
    var total_words = $('input[name="va_words[]"]:checked').length;
    $("#submit_btn").val("Submit & Next (" + total_words + ")");
    $("#submit_btn2").val("Submit & Next (" + total_words + ")");
    $("#words_selected").html(total_words);
});

function form_validation() {
    var total_words = $('input[name="va_words[]"]:checked').length;
    var sel_val = 24;
    if (sel_val != 24) {
        if (total_words < sel_val) {
            $("#words_error").html("Please Select " + sel_val + " words. Count=" + total_words);
            return false;
        }
    } else {
        if (total_words >= 24 && sel_val >= 24) {
            return true;
        } else if (total_words < sel_val || total_words > sel_val) {
            $("#words_error").html("Please Select " + sel_val + " words. Count=" + total_words);
            return false;
        }
    }
}

function form_validation2() {
    var total = $('input[name="va_words[]"]').length;
    var total_words = $('input[name="va_words[]"]:checked').length;
    var sel_val = 24;
    var sel_val2 = 6;
    var sel_val3 = 5
    var sel_val4 = 4;
    var sel_val5 = 3;
    var sel_val6 = 2;
    var sel_val7 = 1;

    if (total > 24) {
        if (sel_val != 24) {
            if (total_words < sel_val || total_words > sel_val) {
                $("#words_error").html("Please Select More Than" + sel_val + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 24) {
                $("#words_error").html("Please Select Minimum " + sel_val + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val || total_words > sel_val)) {
                $("#words_error").html("Please Select " + sel_val + " words. Count=" + total_words);
                return false;
            }
        }
    }
    if (total == 24) {
        if (sel_val2 != 6) {
            if (total_words < sel_val2 || total_words > sel_val2) {
                $("words_error").html("Please Select " + sel_val2 + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 6) {
                $("#words_error").html("Please Select Minimum " + sel_val2 + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val2 || total_words > sel_val2)) {
                $("#words_error").html("Please Select " + sel_val2 + " words. Count=" + total_words);
                return false;
            }
        }
    }

    if (total == 6) {
        if (sel_val3 != 5) {
            if (total_words < sel_val3 || total_words > sel_val3) {
                $("#words_error").html("Please Select " + sel_val3 + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 5) {
                $("#words_error").html("Please Select Minimum " + sel_val3 + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val3 || total_words > sel_val3)) {
                $("#words_error").html("Please Select " + sel_val3 + " words. Count=" + total_words);
                return false;
            }

        }

    }
    if (total == 5) {
        if (sel_val4 != 4) {
            if (total_words < sel_val4 || total_words > sel_val4) {
                $("#words_error").html("Please Select " + sel_val4 + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 4) {
                $("#words_error").html("Please Select Minimum " + sel_val4 + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val4 || total_words > sel_val4)) {
                $("#words_error").html("Please Select " + sel_val4 + " words. Count=" + total_words);
                return false;
            }
        }
    }

    if (total == 4) {
        if (sel_val5 != 4) {
            if (total_words < sel_val5 || total_words > sel_val5) {
                $("#words_error").html("Please Select " + sel_val5 + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 3) {
                $("#words_error").html("Please Select Minimum " + sel_val5 + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val5 || total_words > sel_val5)) {
                $("#words_error").html("Please Select " + sel_val5 + " words. Count=" + total_words);
                return false;
            }
        }
    }

    if (total == 3) {
        if (sel_val6 != 3) {
            if (total_words < sel_val6 || total_words > sel_val6) {
                $("#words_error").html("Please Select " + sel_val6 + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 2) {
                $("#words_error").html("Please Select Minimum " + sel_val6 + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val6 || total_words > sel_val6)) {
                $("#words_error").html("Please Select " + sel_val6 + " words. Count=" + total_words);
                return false;
            }
        }
    }

    if (total == 2) {
        if (sel_val7 != 2) {
            if (total_words < sel_val7 || total_words > sel_val7) {
                $("#words_error").html("Please Select " + sel_val7 + " words. Count=" + total_words);
                return false;
            }
        } else {
            if (total_words < 1) {
                $("#words_error").html("Please Select Minimum " + sel_val7 + " words. Count=" +
                    total_words);
                return false;
            } else if ((total_words < sel_val7 || total_words > sel_val7)) {
                $("#words_error").html("Please Select " + sel_val7 + " words. Count=" + total_words);
                return false;
            }
        }
    }
    return true;
}
</script>
<script>
function sticktothetop() {
    var window_top = $(window).scrollTop();
    var top = $('#stick-here').offset().top;
    if (window_top > top) {
        $('#stickThis').addClass('stick');
        $('#stick-here').height($('#stickThis').outerHeight());
    } else {
        $('#stickThis').removeClass('stick');
        $('#stick-here').height(0);
    }
}
$(function() {
    $(window).scroll(sticktothetop);
    sticktothetop();
});
</script>

<script>
function data_delete(type) {
    if (type == 1) {
        window.location = 'https://coachtofortune.com/coachinghub/' + "coach/practice";
    } else {
        $.ajax({
            type: 'POST',
            url: 'https://coachtofortune.com/coachinghub/coach/practice/beliefs_delete',
            data: '',
            success: function(data) {
                if (data == 1 && type == 1) {} else if (data == 1 && type == 2) {
                    window.location = 'https://coachtofortune.com/coachinghub/' +
                        "coach/practice/beliefs";
                }
            }
        });
    }
}
</script>

<script>
// $(document).ready(function() {
//     function disableBack() { window.history.forward() }

//     window.onload = disableBack();
//     window.onpageshow = function(evt) { if (evt.persisted) disableBack() }
// });
</script>

<script>
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});
</script>