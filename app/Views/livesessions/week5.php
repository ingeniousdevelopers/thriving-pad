<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 5 </h4>
</div>
</div>
</div>
<style>
.alert {
    text-transform: uppercase;
    font-size: 12px;
    width: 100%;
    margin-bottom: 10px;
    padding: 18px 20px;
}
</style>

<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 5</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row mt-2 mr-3" style="float:right">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <a class="btn-md btn-theme float-right p-3" target="_blank" href="<?php echo $link; ?>">
                                OPEN THE SESSION</a>

                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1> DISCUSS THE P-CODES AND CREATE PERSONALIZED P-CODES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <?php
                    $val = $ncode[0]['array_ncode'];
                    $splittedstring = explode("~", $val);
                    $i = 0;
                    if (isset($discusspcode) && empty($discusspcode)) {
                        foreach ($ncodevalue as $codes) {
                            foreach ($splittedstring as $val) {

                                if ($codes['id'] == $val) {


                    ?>
                    <h5>
                        <label for="pcode_id[]">
                            <input type="hidden" name="pcode_id[]" id="pcode_id[]" value="<?php echo $codes['id'] ?>" />
                            <h6 style="text-transform:uppercase"> P-CODE : <b><?php echo $codes['pcodes'] ?>
                                </b>
                            </h6>
                        </label>

                    </h5>


                    <form action="<?= base_url() ?>/live-sessions/discusspcode" method="POST">
                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="s1">
                                    <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                                </label>

                                <textarea required class="form-control" id="s1[]" name="s1[]" rows="2"></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="s2">
                                    <h6>BUSINESS , MONEY, POSSESSION </h6>
                                </label>

                                <textarea required class="form-control" id="s2[]" name="s2[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s3">
                                    <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                                </label>
                                <textarea required class="form-control" id="s3[]" name="s3[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s4">
                                    <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                                </label>
                                <textarea required class="form-control" id="s4[]" name="s4[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s5">
                                    <h6>ENJOYMENT, FUN & RECREATION </h6>
                                </label>
                                <textarea required class="form-control" id="s5[]" name="s5[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s6">
                                    <h6>FAMILY </h6>
                                </label>
                                <textarea required class="form-control" id="s6[]" name="s6[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s7">
                                    <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                                </label>
                                <textarea required class="form-control" id="s7[]" name="s7[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s8">
                                    <h6>HEALTH, BEAUTY, WELLNESS </h6>
                                </label>

                                <textarea required class="form-control" id="s8[]" name="s8[]" rows="2"></textarea>
                            </div>

                        </div>

                        <?php

                                }
                            }
                        }
                    } else {
                        foreach ($ncodevalue as $codes) {
                            foreach ($splittedstring as $val) {

                                if ($codes['id'] == $val) {

                                    ?>
                        <h5>
                            <label for="pcode_id[]">
                                <input type="hidden" name="pcode_id[]" id="pcode_id[]"
                                    value="<?php echo $codes['id'] ?>" />
                                <h6 style="text-transform:uppercase"> P-CODE : <b><?php echo $codes['pcodes'] ?>
                                    </b>
                                </h6>
                            </label>

                        </h5>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="s1">
                                    <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                                </label>
                                <?php
                                                $val = '';
                                                $attr = '';
                                                if (set_value('s1')) {
                                                    $val = set_value('s1');
                                                } elseif (($discusspcode[0]['s1'])) {
                                                    $s1 = $discusspcode[0]['s1'];
                                                    $s1 = explode("~", $s1);

                                                    $attr = 'disabled';

                                                ?>
                                <textarea required <?php echo $attr; ?> class="form-control" id="s1[]" name="s1[]"
                                    rows="2"><?php echo $s1[$i] ?></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="s2">
                                    <h6>BUSINESS , MONEY, POSSESSION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s2')) {
                                                        $val = set_value('s2');
                                                    } elseif (($discusspcode[0]['s2'])) {
                                                        $s2 = $discusspcode[0]['s2'];
                                                        $s2 = explode("~", $s2);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr; ?> class="form-control" id="s2[]" name="s2[]"
                                    rows="2"><?php echo $s2[$i] ?></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="s3">
                                    <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s3')) {
                                                        $val = set_value('s3');
                                                    } elseif (($discusspcode[0]['s3'])) {

                                                        $s3 = $discusspcode[0]['s3'];
                                                        $s3 = explode("~", $s3);
                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s3[]" name="s3[]"
                                    rows="2"><?php echo $s3[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s4">
                                    <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s4')) {
                                                        $val = set_value('s4');
                                                    } elseif (($discusspcode[0]['s4'])) {
                                                        $s4 = $discusspcode[0]['s4'];
                                                        $s4 = explode("~", $s4);
                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s4[]" name="s4[]"
                                    rows="2"><?php echo $s4[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s5">
                                    <h6>ENJOYMENT, FUN & RECREATION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s5')) {
                                                        $val = set_value('s5');
                                                    } elseif (($discusspcode[0]['s5'])) {
                                                        $s5 = $discusspcode[0]['s5'];
                                                        $s5 = explode("~", $s5);
                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s5[]" name="s5[]"
                                    rows="2"><?php echo $s5[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s6">
                                    <h6>FAMILY </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s6')) {
                                                        $val = set_value('s6');
                                                    } elseif (($discusspcode[0]['s6'])) {
                                                        $s6 = $discusspcode[0]['s6'];
                                                        $s6 = explode("~", $s6);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s6[]" name="s6[]"
                                    rows="2"><?php echo $s6[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s7">
                                    <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s7')) {
                                                        $val = set_value('s7');
                                                    } elseif (($discusspcode[0]['s7'])) {
                                                        $s7 = $discusspcode[0]['s7'];
                                                        $s7 = explode("~", $s7);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s7[]" name="s7[]"
                                    rows="2"><?php echo $s7[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s8">
                                    <h6>HEALTH, BEAUTY, WELLNESS </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s8')) {
                                                        $val = set_value('s8');
                                                    } elseif (($discusspcode[0]['s8'])) {
                                                        $s8 = $discusspcode[0]['s8'];
                                                        $s8 = explode("~", $s8);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s8" name="s8"
                                    rows="2"><?php echo $s8[$i]; ?></textarea>
                            </div>

                        </div>
                        <?php
                                                    $i++;
                                                }
                                            }
                                        }
                                    }
                                }   ?>
                        <div class="row  mt-2" style="float:right">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE P-CODE
                                        </button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <a href="#" tabindex="0">
        </a>
        <span>

        </span>
    </div>
</div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DISCOVERED AND REALISED</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5>
                        Write down a full page describing the damage that the N-codes will cause as the result of them
                        holding on for the next 15 years.- Activity
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/nandpactivity" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('npactivity')) {
                                $val = set_value('npactivity');
                            } elseif (($npactivity[0]['nactivity'])) {
                                $nactivity = $npactivity[0]['nactivity'];
                                $pactivity = $npactivity[0]['pactivity'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="nactivity"
                                name="nactivity" rows="3"><?php echo $nactivity; ?></textarea>

                        </div>

                        <h5>
                            Now imagine, all N-codes have been converted into P-codes and write the new scenario
                            of
                            present life as well as 15 years down the line.- Activity
                        </h5>
                        <textarea required <?php echo $attr;  ?> class="form-control" id="pactivity" name="pactivity"
                            rows="3"><?php echo $pactivity; ?></textarea>

                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE P AND N ACTIVTY
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DISCOVERED AND REALISED</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <form action="<?= base_url() ?>/live-sessions/discover" method="POST">
                        <div class="form-group">
                            <h5>
                                Ask them what they discovered and realized?
                            </h5>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('life')) {
                                $val = set_value('life');
                            } elseif (($discover[0]['life'])) {
                                $life = $discover[0]['life'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="life" name="life"
                                rows="3"><?php echo $life; ?></textarea>

                        </div>
                        <div class="form-group">
                            <h5>
                                Impact of P-codes on your individual Wheel of life
                            </h5>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('discovered')) {
                                $val = set_value('discovered');
                            } elseif (($discover[0]['discovered'])) {
                                $val = $discover[0]['discovered'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="discovered"
                                name="discovered" rows="3"><?php echo $val; ?></textarea>

                        </div>


                </div>
                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT DISCOVER
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>PRACTICAL IMPLEMENTATION OF P-CODES IN DAILY LIFE</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>Create a brief and specific Poem, Audio, Wallpaper, Slide-shows,
                                painting etc. which
                                includes one dynamic emotion or a feeling word </h6>
                        </li>
                        <li>
                            <h6>Practice them Thrice a day. </h6>
                        </li>
                        <li>
                            <h6>Look into the mirror and speak out loud like you own it.</h6>
                        </li>
                        <li>
                            <h6>Recite them before going to bed and after you wake up.</h6>
                        </li>
                        <li>
                            <h6>Read aloud your P-codes in the session.</h6>
                        </li>
                        <li>
                            <h6>You may list certain New Habits in your Sheet.</h6>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
</div>
<!--Live Session-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DESIRE QUESTIONS</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <form id="desireForm" action="<?= base_url() ?>/live-sessions/desirequestion" method="POST">
                        <div class="row">


                            <?php
                            if (isset($dq) && !empty($dq)) {
                                for ($i = 1; $i <= 50; $i++) {
                                    $i = 0;
                                    do {
                            ?>
                            <div class="form-group col-md-6">
                                <label for="year" <h5 class="location">
                                    <div class="heading-properties-3">
                                        <div class="clearfix">
                                            <div class="pull-left">
                                                <h1>PRACTICAL IMPLEMENTATION OF P-CODES IN DAILY LIFE</h1>
                                            </div>
                                        </div>
                                    </div>
                                    </h5>
                                    >

                                </label>
                                <?php
                                            $val = '';
                                            $attr = '';
                                            if (set_value('year')) {
                                                $val = set_value('year');
                                            } elseif (($dq[0]['year'])) {
                                                $val = $dq[0]['year'];
                                                $attr = 'disabled';
                                                $splittedstring = explode("~", $val);
                                            }
                                            $count = count($splittedstring);
                                            ?>
                                <select style="float:right; margin-bottom:2%" <?php echo $attr;  ?> name="y[]"
                                    id="<?php echo "y$i" ?>">
                                    <option value="Year" <?php echo $attr;  ?> selected disabled>Year </option>
                                    <option value="1" <?php echo $attr;  ?> <?php if ($splittedstring[$i] === "1") { ?>
                                        selected="true" <?php }  ?>>1 </option>
                                    <option value="3" <?php echo $attr;  ?> <?php if ($splittedstring[$i] === "3") { ?>
                                        selected="true" <?php }; ?>>3 </option>
                                    <option value="5" <?php echo $attr;  ?> <?php if ($splittedstring[$i] === "5") { ?>
                                        selected="true" <?php }; ?>>5 </option>
                                </select>
                                <label for="question">

                                </label>
                                <?php
                                            $val = '';
                                            $attr = '';
                                            if (set_value('question')) {
                                                $val = set_value('question');
                                            } elseif (($dq[0]['question'])) {
                                                $val = $dq[0]['question'];
                                                $attr = 'disabled';
                                                $splittedstring = explode("~", $val);

                                                $count = count($splittedstring);
                                            ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="question"
                                    name="dq[]"><?php echo $splittedstring[$i]; ?></textarea>
                            </div>
                            <?php $i++;
                                            }
                                        } while ($i < $count);
                                    }

                            ?>
                        </div>
                        <?php if (isset($dq) && empty($dq[0]['year'])) { ?>
                        <div class="row  mt-2 ml-3" style="float:right">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT YEAR
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } ?>
                    </form>

                    <?php

                            }
                            if (isset($dq) && empty($dq)) {
                                for ($i = 1; $i <= 50; $i++) {
                    ?>
                    <div class="form-group col-md-6">
                        <label for="question">

                        </label>
                        <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('question')) {
                                        $val = set_value('question');
                                    } elseif (($dq[0]['question'])) {
                                        $val = $dq[0]['question'];
                                        $attr = 'disabled';
                                    }
                            ?>
                        <textarea required <?php echo $attr;  ?> class="form-control" row="2" id="question"
                            name="dq[]"><?php echo $val ?></textarea>

                    </div>

                    <?php
                                }      ?>


                </div>
            </div>
            <div class="row mt-2 ml-3 justify-content-right" style="float:right;margin-right:25px;">
                <div class="clearfix px-3">
                    <div class="pull-right">
                        <div class="form-group clearfix">
                            <button type="submit" class="btn-md btn-theme float-right p-3">SUBMIT DESIRE
                            </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>

            <ul class="pl-5 pt-5" style="list-style-type: square;">
                <h5>List all the desires to be fulfilled from the 1st year</h5>
                <?php
            if (!empty($dq)) {
                foreach ($dq as $year) {
                    $year = $dq[0]['year'];
                    $year = explode('~', $year);
                    $question = $dq[0]['question'];
                    $question = explode('~', $question);
                    foreach ($year as $key => $y) {
                        if ($y == 1) {
            ?><li>
                    <?php echo $question[$key] . '<br>'; ?>
                </li>
                <?php
                        }
                    }
                } ?>

                <?php } else { ?>
                <div class="alert alert-danger">No Data's in One Year
                </div>
                <?php
            } ?>
            </ul>
        </div>

    </div>

</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>

<script>
function submitForm(action) {
    document.getElementById('desireForm').action = action;
    document.getElementById('desireForm').submit();
}
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 2px;
}
</style>