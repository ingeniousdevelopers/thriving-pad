<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
} ?>


<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 2 </h4>
</div>
</div>
</div>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<!--Live Session-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">

                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 2</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row mt-2 mr-3">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <a class="btn-md btn-theme float-right p-3" target="_blank" href="<?php echo $link; ?>">
                                OPEN THE SESSION</a>

                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--What is Acceptance-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>WHAT IS ACCEPTANCE? </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>Acceptance is the willingness to ‘Let Go’ of your emotional opposition to the reality of
                                ‘what is’</h6>
                        </li>
                        <li>
                            <h6>Acceptance of a situation does not mean that the situation is morally acceptable, it
                                means ‘it is so right now’</h6>
                        </li>
                        <li>
                            <h6>Only when you accept what is, you can figure out with greater calmness and clarity, what
                                you can do or how you can better your circumstance. </h6>
                        </li>
                    </ul>
                    <h5>
                        Express what are still stuck on!
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/acceptance" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('acceptance')) {
                                $val = set_value('acceptance');
                            } elseif (($a[0]['acceptance'])) {
                                $val = $a[0]['acceptance'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="acceptance"
                                name="acceptance" rows="3"><?php echo $val; ?></textarea>
                        </div>

                </div>
                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT The
                                    Acceptance</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Affirmation-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>AFFIRMATION</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <b>READ OUT LOUD</b>
                    <h5>
                        “GRANT ME SERENITY, TO ACCEPT THE THINGS I CANNOT CHANGE THE COURAGE TO CHANGE THE THINGS I CAN
                        AND WISDOM TO KNOW THE DIFFERENCE.”
                    </h5>
                </div>

                <div class="row  mt-2 ml-3">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">

                            </div>
                        </div>
                    </div>


                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Base Values-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DISCOVERING BASE VALUES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5>What is Base Value?</h5>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>Every person from childhood is influenced by his/her parents, siblings, peer group and
                                surrounding, which forms his/her base values. </h6>
                        </li>
                        <li>
                            <h6>We all live our lives through our base values. </h6>
                        </li>
                        <li>
                            <h6>They define how we feel, think and act that in-turn define our lives.</h6>
                        </li>
                        <li>
                            <h6>They are abstract words which you cannot touch or hold, but play a vital role in the
                                functionality of our lives.</h6>
                        </li>
                        <li>
                            <h6>Base-Values are subconsciously picked. Each one of us may have 5-7 important
                                Base-Values.</h6>
                        </li>
                        <li>
                            <h6>By knowing and aligning these Base-Values in life we are capable of making way better
                                decisions with ease, which leads to happiness and contentment.</h6>
                        </li>
                    </ul>
                </div>

                <div class="row  mt-2" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/base-values">
                                    <button type="submit" class="btn-md btn-theme float-left p-3">DISCOVER THE TOP &
                                        BASE_VALUES</button>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Base- Values Satisfaction & Dis-satisfaction-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>BASE- VALUES SATISFACTION - TAKE AWAY ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Describe how your five base values are being expressed in each of the key areas in your life.
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/basevalues-satisfaction" method="POST">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="s1">
                                    <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s1')) {
                                    $val = set_value('s1');
                                } elseif (($bs[0]['s1'])) {
                                    $val = $bs[0]['s1'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s1" name="s1"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s1">
                                    <h6>BUSINESS , MONEY, POSSESSION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s2')) {
                                    $val = set_value('s2');
                                } elseif (($bs[0]['s2'])) {
                                    $val = $bs[0]['s2'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s2" name="s2"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s3">
                                    <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s3')) {
                                    $val = set_value('s3');
                                } elseif (($bs[0]['s3'])) {
                                    $val = $bs[0]['s3'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s3" name="s3"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s4">
                                    <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s4')) {
                                    $val = set_value('s4');
                                } elseif (($bs[0]['s4'])) {
                                    $val = $bs[0]['s4'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s4" name="s4"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s5">
                                    <h6>ENJOYMENT, FUN & RECREATION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s5')) {
                                    $val = set_value('s5');
                                } elseif (($bs[0]['s5'])) {
                                    $val = $bs[0]['s5'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s5" name="s5"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s6">
                                    <h6>FAMILY </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s6')) {
                                    $val = set_value('s6');
                                } elseif (($bs[0]['s6'])) {
                                    $val = $bs[0]['s6'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s6" name="s6"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s7">
                                    <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s7')) {
                                    $val = set_value('s7');
                                } elseif (($bs[0]['s7'])) {
                                    $val = $bs[0]['s7'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s7" name="s7"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s8">
                                    <h6>HEALTH, BEAUTY, WELLNESS </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('s8')) {
                                    $val = set_value('s8');
                                } elseif (($bs[0]['s8'])) {
                                    $val = $bs[0]['s8'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s8" name="s8"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                        </div>
                </div>

                <div class="row  mt-2 ml-3" style="float:right;margin-right:25px">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                    ASSIGNMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>BASE- VALUES DIS-SATISFACTION - TAKE AWAY ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Describe how your five base values are NOT being expressed in each of the key areas in your
                        life?
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/basevalues-dissatisfaction" method="POST">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="ds1">
                                    <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds1')) {
                                    $val = set_value('ds1');
                                } elseif (($bds[0]['ds1'])) {
                                    $val = $bds[0]['ds1'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds1" name="ds1"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds1">
                                    <h6>BUSINESS , MONEY, POSSESSION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds2')) {
                                    $val = set_value('ds2');
                                } elseif (($bds[0]['ds2'])) {
                                    $val = $bds[0]['ds2'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds2" name="ds2"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds3">
                                    <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds3')) {
                                    $val = set_value('ds3');
                                } elseif (($bds[0]['ds3'])) {
                                    $val = $bds[0]['ds3'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds3" name="ds3"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds4">
                                    <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds4')) {
                                    $val = set_value('ds4');
                                } elseif (($bds[0]['ds4'])) {
                                    $val = $bds[0]['ds4'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds4" name="ds4"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds5">
                                    <h6>ENJOYMENT, FUN & RECREATION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds5')) {
                                    $val = set_value('ds5');
                                } elseif (($bds[0]['ds5'])) {
                                    $val = $bds[0]['ds5'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds5" name="ds5"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds6">
                                    <h6>FAMILY </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds6')) {
                                    $val = set_value('ds6');
                                } elseif (($bds[0]['ds6'])) {
                                    $val = $bds[0]['ds6'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds6" name="ds6"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds7">
                                    <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds7')) {
                                    $val = set_value('ds7');
                                } elseif (($bds[0]['ds7'])) {
                                    $val = $bds[0]['ds7'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds7" name="ds7"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="ds8">
                                    <h6>HEALTH, BEAUTY, WELLNESS </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('ds8')) {
                                    $val = set_value('ds8');
                                } elseif (($bds[0]['ds8'])) {
                                    $val = $bds[0]['ds8'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="ds8" name="ds8"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>
                        </div>
                </div>

                <div class="row  mt-2 ml-3" style="float:right;margin-right:25px">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                    ASSIGNMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
</div>

<!-- Modal Document -->
<div class="modal" id="document_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="document_title"></h4>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="document"></iframe>
                    </div>


                </div>
            </div>



        </div>
    </div>
</div>



<script type="text/javascript">
function opendocument(document, title) {
    $('#document').attr('src', document)
    $('#document_title').text(title);
    $('#document_modal').modal('show');
};
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 2px;
}
</style>