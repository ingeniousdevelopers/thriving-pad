<style>
ul li a {
    margin: 0;
    padding: 0;
    list-style: none;
    color: #FFF;
}

ul li a:hover {
    color: #FFF;
}
</style>
<div class="dashboard-content">
    <div class="dashboard-header clearfix">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <nav class="navbar navbar-expand-sm mb-5" style="background-color:#545454">
                    <ul class="navbar-nav ml-auto mr-auto navbar-expand"
                        style="align-items: stretch; justify-content: space-between; width: 100%;">
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (1 <= $count) {
                                                            echo base_url() . '/live-sessions/week1';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 1) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 1
                            </a>

                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (2 <= $count) {
                                                            echo base_url() . '/live-sessions/week2';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 2) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 2
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (3 <= $count) {
                                                            echo base_url() . '/live-sessions/week3';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 3) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 3
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (4 <= $count) {
                                                            echo base_url() . '/live-sessions/week4';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 4) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 4
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (5 <= $count) {
                                                            echo base_url() . '/live-sessions/week5';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 5) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 5
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (6 <= $count) {
                                                            echo base_url() . '/live-sessions/week6';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 6) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 6
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (7 <= $count) {
                                                            echo base_url() . '/live-sessions/week7';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 7) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 7
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (8 <= $count) {
                                                            echo base_url() . '/live-sessions/week8';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 8) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 8
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (9 <= $count) {
                                                            echo base_url() . '/live-sessions/week9';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 9) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 9
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (10 <= $count) {
                                                            echo base_url() . '/live-sessions/week10';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 10) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 10
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (11 <= $count) {
                                                            echo base_url() . '/live-sessions/week11';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 11) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 11
                            </a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="<?php if (12 <= $count) {
                                                            echo base_url() . '/live-sessions/week12';
                                                        } else {
                                                            echo '';
                                                        } ?>" style="<?php if ($live_session == 12) {
                                                                            echo 'color:#FFFFFF;background-color:#f5832c';
                                                                        } ?>">
                                WEEK 12
                            </a>
                        </li>
                    </ul>
                </nav>

            </div>