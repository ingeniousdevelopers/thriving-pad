<style>
.check-content-list.check-sentence .form-checkbox input[type=checkbox]:checked+label {
    color: #fff;
}

input[type=checkbox] {
    box-sizing: border-box;
    display: none;
    padding: 0;
}

.form-checkbox input[type="checkbox"]:checked+label {
    background-color: #f5832c;
    border-bottom: 2px solid #7b3602;
}

input[type=checkbox]:checked+label:before {
    /* color: #44a5b0; */

    display: none;
}

.check-sentence .form-checkbox input[type=checkbox]+label {
    color: #000;
    background-color: #fff;
    border-bottom: 2px solid #fff;
}

.check-content-list.check-sentence .form-checkbox input[type=checkbox]:checked+label {
    color: #fff;
}

.form-checkbox input[type="checkbox"]:checked+label {
    background-color: #f5832c;
    border-bottom: 2px solid #7b3602;
    cursor: pointer;
}

.check-sentence .form-checkbox input[type=checkbox]+label {
    color: #000;
    background-color: #fff;
    border-bottom: 2px solid #fff;
}

.check-content-list.check-sentence .form-checkbox label {
    width: auto;
    max-width: 100%;
}

.check-content-list label {
    position: relative;
    cursor: pointer;
    color: #666;
    font-size: 30px;
}

.form-checkbox input[type=checkbox]+label {
    font-weight: 500;
    font-size: 14px;
    letter-spacing: 1px;
    font-family: 'Montserrat', sans-serif;
    color: #fff;
    background-color: #44a5b0;
    border-bottom: 2px solid #7abfc7;
    padding: 5px 10px;
    border-radius: 5px;
}
</style>
<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
} ?>


<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 4 </h4>
</div>
</div>
</div>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<!--Live Session-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 4</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row mt-2 mr-3">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <a class="btn-md btn-theme float-right p-3" target="_blank" href="<?php echo $link; ?>">
                                OPEN THE SESSION</a>

                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Negative Codes-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>THE NEGATIVELY CODED ELEPHANT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Read the Instructions
                    </h6>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button
                                    onclick="opendocument('<?= base_url() ?>/public/assets/documents/week4/Impacts.pdf','Instructions For Vission + Mission')"
                                    type="submit" class="btn-md btn-theme float-left p-3">OPEN THE INSTRUCTIONS</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- MOMENTS THAT CHANGED EVERYTHING -->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>POSITIVE MOMENTS THAT CHANGED EVERYTHING </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Share one Positive incident from your childhood or life which powerfully impacted you, answer
                        these series of questions.
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/positivemoments" method="POST"
                        enctype='multipart/form-data'>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <h5><b>Postive Moment - 1</b></h5>
                                <?php
                                if (!isset($p[0]['images']) || empty($p[0]['images'])) {

                                ?> <h5>Write down the Positive Moments for atleast two pages and scan as images and
                                    upload here </h5>
                                <input type="file" class="upload" name="images[]" multiple>

                                <?php
                                } else {
                                    $imgarr = [];
                                    $imgarr = explode(',', $p[0]['images']);
                                    foreach ($imgarr as $img) { ?>


                                <img src="<?= base_url() . '/public/uploads/' . $img;   ?>"
                                    alt="Upload Your Photos of Writing" width="1000 " height="500">

                                <?php
                                    }
                                }
                                ?>

                            </div>


                            <div class="form-group col-md-6">
                                <label for="q1">
                                    <h6>1. Describe the Moment?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q1')) {
                                    $val = set_value('q1');
                                } elseif (($p[0]['q1'])) {
                                    $val = $p[0]['q1'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q1" name="q1"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q2">
                                    <h6>2. Your age at that time?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q2')) {
                                    $val = set_value('q2');
                                } elseif (($p[0]['q2'])) {
                                    $val = $p[0]['q2'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q2" name="q2"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q3">
                                    <h6>3. People who were there, and what was said??</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q3')) {
                                    $val = set_value('q3');
                                } elseif (($p[0]['q3'])) {
                                    $val = $p[0]['q3'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q3" name="q3"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q4">
                                    <h6>4. What did the little voice inside you tell you?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q4')) {
                                    $val = set_value('q4');
                                } elseif (($p[0]['q4'])) {
                                    $val = $p[0]['q4'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q4" name="q4"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q5">
                                    <h6>5. What would you have wanted?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q5')) {
                                    $val = set_value('q5');
                                } elseif (($p[0]['q5'])) {
                                    $val = $p[0]['q5'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q5" name="q5"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q6">
                                    <h6>6. What were your expectations? </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q6')) {
                                    $val = set_value('q6');
                                } elseif (($p[0]['q6'])) {
                                    $val = $p[0]['q6'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q6" name="q6"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q7">
                                    <h6>7. Which great thing did you learn from this event?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q7')) {
                                    $val = set_value('q7');
                                } elseif (($p[0]['q7'])) {
                                    $val = $p[0]['q7'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q7" name="q7"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q8">
                                    <h6>8. Which P-codes/N-codes have you formed since then? </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q8')) {
                                    $val = set_value('q8');
                                } elseif (($p[0]['q8'])) {
                                    $val = $p[0]['q8'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q8" name="q8"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>


                        </div>
                </div>

                <div class="row  mt-2" style="float:right;margin-right:25px">
                    <div class=" clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE POSITIVE
                                    MOMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/public/assets/documents/week4/PositiveMoments.docx">
                                    <button type="submit" class="btn-md btn-theme float-left p-3">POSITIVE MOMENT-2
                                        ASSIGNMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/public/assets/documents/week4/PositiveMoments.docx">
                                    <button type="submit" class="btn-md btn-theme float-left p-3">POSITIVE MOMENT-3
                                        ASSIGNMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>NEGATIVE MOMENTS THAT CHANGED EVERYTHING </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5 class="mb-4">
                        Share one Negative incident from your childhood or life which powerfully impacted you, answer
                        these series of questions.
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/negativemoments" method="POST"
                        enctype='multipart/form-data'>
                        <div class="row">
                            <div class="form-group col-md-12">
                                <h5><b>Negative Moment - 1</b></h5>

                                <?php
                                if (!isset($n[0]['images']) || empty($n[0]['images'])) {

                                ?> <h5>Write down the Negative Moments for atleast two pages and scan as images and
                                    upload here </h5>
                                <input type="file" class="upload" name="images[]" multiple>

                                <?php
                                } else {
                                    $imgarr = [];
                                    $imgarr = explode(',', $n[0]['images']);
                                    foreach ($imgarr as $img) { ?>


                                <img src="<?= base_url() . '/public/uploads/' . $img;   ?>"
                                    alt="Upload Your Photos of Writing" width="1000 " height="500">

                                <?php
                                    }
                                }
                                ?>
                            </div>


                            <div class="form-group col-md-6">
                                <label for="q1">
                                    <h6>1. Describe the Moment?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q1')) {
                                    $val = set_value('q1');
                                } elseif (($n[0]['q1'])) {
                                    $val = $n[0]['q1'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q1" name="q1"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q2">
                                    <h6>2. Your age at that time?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q2')) {
                                    $val = set_value('q2');
                                } elseif (($n[0]['q2'])) {
                                    $val = $n[0]['q2'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q2" name="q2"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q3">
                                    <h6>3. People who were there, and what was said??</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q3')) {
                                    $val = set_value('q3');
                                } elseif (($n[0]['q3'])) {
                                    $val = $n[0]['q3'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q3" name="q3"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q4">
                                    <h6>4. What did the little voice inside you tell you?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q4')) {
                                    $val = set_value('q4');
                                } elseif (($n[0]['q4'])) {
                                    $val = $n[0]['q4'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q4" name="q4"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q5">
                                    <h6>5. What would you have wanted?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q5')) {
                                    $val = set_value('q5');
                                } elseif (($n[0]['q5'])) {
                                    $val = $n[0]['q5'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q5" name="q5"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q6">
                                    <h6>6. What were your expectations? </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q6')) {
                                    $val = set_value('q6');
                                } elseif (($n[0]['q6'])) {
                                    $val = $n[0]['q6'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q6" name="q6"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q7">
                                    <h6>7. Which great thing did you learn from this event?</h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q7')) {
                                    $val = set_value('q7');
                                } elseif (($n[0]['q7'])) {
                                    $val = $n[0]['q7'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q7" name="q7"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="q8">
                                    <h6>8. Which P-codes/N-codes have you formed since then? </h6>
                                </label>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q8')) {
                                    $val = set_value('q8');
                                } elseif (($n[0]['q8'])) {
                                    $val = $n[0]['q8'];
                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea <?php echo $attr;  ?> class="form-control" id="q8" name="q8"
                                    rows="2"><?php echo $val; ?></textarea>
                            </div>


                        </div>
                </div>

                <div class="row  mt-2 ml-3" style="float:right;margin-right:25px">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix ">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                    NEGATIVE MOMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix px-3">
                        <div class="pull-center">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/public/assets/documents/week4/NegativeMoments.docx">
                                    <button type="submit" class="btn-md btn-theme p-3">NEGATIVE MOMENT-2
                                        ASSIGNMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/public/assets/documents/week4/NegativeMoments.docx">
                                    <button type="submit" class="btn-md btn-theme float-left p-3">NEGATIVE MOMENT-3
                                        ASSIGNMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!--STORY SHARING ONE POSITIVE AND ONE NEGATIVE-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>STORY SHARING ONE POSITIVE AND ONE NEGATIVE</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <h5>
                        ONE POSITIVE:
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/storysharing" method="POST">
                        <div class="form-group">
                            <label for="q1">
                                <h6>Which positive beliefs did you adopt and how is it helping you to excel in life?
                                </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q1')) {
                                $val = set_value('q1');
                            } elseif (($ss[0]['q1'])) {
                                $val = $ss[0]['q1'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea <?php echo $attr;  ?> class="form-control" id="q1" name="q1"
                                rows="3"><?php echo $val; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="q2">
                                <h6>Why is it important for you to hold on to the beliefs and how will it make your life
                                    better?</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q2')) {
                                $val = set_value('q2');
                            } elseif (($ss[0]['q2'])) {
                                $val = $ss[0]['q2'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea <?php echo $attr;  ?> class="form-control" id="q2" name="q2"
                                rows="3"><?php echo $val; ?></textarea>
                        </div>

                        <h5>
                            ONE NEGATIVE:
                        </h5>

                        <div class="form-group">
                            <label for="q3">
                                <h6>Which negative belief did you adopt and how are they stopping you from excelling?
                                </h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q3')) {
                                $val = set_value('q3');
                            } elseif (($ss[0]['q3'])) {
                                $val = $ss[0]['q3'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea <?php echo $attr;  ?> class="form-control" id="q3" name="q3"
                                rows="3"><?php echo $val; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="q4">
                                <h6>Why is it important to get rid of the negative belief and how will make your life
                                    better?</h6>
                            </label>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('q4')) {
                                $val = set_value('q4');
                            } elseif (($ss[0]['q4'])) {
                                $val = $ss[0]['q4'];
                                $attr = 'disabled';
                            }
                            ?>
                            <textarea <?php echo $attr;  ?> class="form-control" id="q4" name="q4"
                                rows="3"><?php echo $val; ?></textarea>
                        </div>



                </div>
                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                    ACTIVITY</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- N - P tool -->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>N-P TOOL</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5>Brain collects proof to satisfy codes.</h5>
                    <p>
                        Once you have formed a code your brain will start justifying it by hunting for evidence to prove
                        it right so that you can hold on to it.
                    </p>
                    <p>
                        You will see what your code wants you to see because your brain will make world appear in that
                        particular way. This is where codes get embedded deep in your personality.
                    </p>
                    <p style="color: red;">
                        Like a magnet you attract situations that prove your code.
                    </p>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <a class="btn-md btn-theme float-left p-3" target="_blank"
                                    href="<?= base_url() ?>/tools/ncodes">
                                    OPEN THE N-P Tool</a>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>GENERATE P-CODES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        How have they adversely affected you so far, especially in your selected current OPT, Optimal
                        Transformation area
                    </h6>
                    <form action="<?= base_url() ?>/live-sessions/generatepcode" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('gp')) {
                                $val = set_value('gp');
                            } elseif (($gp[0]['currentopt'])) {
                                $currentopt = $gp[0]['currentopt'];
                                $statement = $gp[0]['statement'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea <?php echo $attr;  ?> class="form-control" id="currentopt" name="currentopt"
                                rows="3"><?php echo $currentopt; ?></textarea>

                        </div>
                        <h6>
                            List your P-codes generated by Tool. Convert this P-codes into powerful statements using
                            words that resonates with you. Make it positively exaggerated and give it an emotional
                            charge
                        </h6>
                        <textarea <?php echo $attr;  ?> class="form-control" id="statement" name="statement"
                            rows="3"><?php echo $statement; ?></textarea>

                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE P-CODE
                                </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1> DISCUSS THE P-CODES AND CREATE PERSONALIZED P-CODES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <?php
                    $val = $ncode[0]['array_ncode'];
                    $splittedstring = explode("~", $val);
                    $i = 0;
                    if (empty($discussncode)) {
                        foreach ($ncodevalue as $codes) {
                            foreach ($splittedstring as $val) {

                                if ($codes['id'] == $val) {


                    ?>

                    <label for="ncode_id[]">
                        <input type="hidden" name="ncode_id[]" id="ncode_id[]" value="<?php echo $codes['id'] ?>" />
                        <h6 style="text-transform:uppercase"> N-CODE : <b><?php echo $codes['ncodes'] ?>
                            </b>
                        </h6>
                    </label>




                    <form action="<?= base_url() ?>/live-sessions/discussncode" method="POST">
                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="s1">
                                    <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                                </label>

                                <textarea required class="form-control" id="s1[]" name="s1[]" rows="2"></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="s2">
                                    <h6>BUSINESS , MONEY, POSSESSION </h6>
                                </label>

                                <textarea required class="form-control" id="s2[]" name="s2[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s3">
                                    <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                                </label>
                                <textarea required class="form-control" id="s3[]" name="s3[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s4">
                                    <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                                </label>
                                <textarea required class="form-control" id="s4[]" name="s4[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s5">
                                    <h6>ENJOYMENT, FUN & RECREATION </h6>
                                </label>
                                <textarea required class="form-control" id="s5[]" name="s5[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s6">
                                    <h6>FAMILY </h6>
                                </label>
                                <textarea required class="form-control" id="s6[]" name="s6[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s7">
                                    <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                                </label>
                                <textarea required class="form-control" id="s7[]" name="s7[]" rows="2"></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s8">
                                    <h6>HEALTH, BEAUTY, WELLNESS </h6>
                                </label>

                                <textarea required class="form-control" id="s8[]" name="s8[]" rows="2"></textarea>
                            </div>

                        </div>

                        <?php

                                }
                            }
                        }
                    }
                    if (!empty($discussncode)) {
                        foreach ($ncodevalue as $codes) {
                            foreach ($splittedstring as $val) {

                                if ($codes['id'] == $val) {

                                    ?>
                        <h5>
                            <label for="ncode_id[]">
                                <input type="hidden" name="ncode_id[]" id="ncode_id[]"
                                    value="<?php echo $codes['id'] ?>" />
                                <h6 style="text-transform:uppercase"> N-CODE : <b><?php echo $codes['ncodes'] ?>
                                    </b>
                                </h6>
                            </label>

                        </h5>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="s1">
                                    <h6>AFFINITY, INTIMACY, LOVE & WARMTH </h6>
                                </label>
                                <?php
                                                $val = '';
                                                $attr = '';
                                                if (set_value('s1')) {
                                                    $val = set_value('s1');
                                                } elseif (($discussncode[0]['s1'])) {
                                                    $s1 = $discussncode[0]['s1'];
                                                    $s1 = explode("~", $s1);

                                                    $attr = 'disabled';

                                                ?>
                                <textarea required <?php echo $attr; ?> class="form-control" id="s1[]" name="s1[]"
                                    rows="2"><?php echo $s1[$i] ?></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="s2">
                                    <h6>BUSINESS , MONEY, POSSESSION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s2')) {
                                                        $val = set_value('s2');
                                                    } elseif (($discussncode[0]['s2'])) {
                                                        $s2 = $discussncode[0]['s2'];
                                                        $s2 = explode("~", $s2);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr; ?> class="form-control" id="s2[]" name="s2[]"
                                    rows="2"><?php echo $s2[$i] ?></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <label for="s3">
                                    <h6>CAREER, PASSION,WORK, OCCUPATION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s3')) {
                                                        $val = set_value('s3');
                                                    } elseif (($discussncode[0]['s3'])) {

                                                        $s3 = $discussncode[0]['s3'];
                                                        $s3 = explode("~", $s3);
                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s3[]" name="s3[]"
                                    rows="2"><?php echo $s3[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s4">
                                    <h6> SPIRITUALITY & PERSONAL DEVELOPMENT</h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s4')) {
                                                        $val = set_value('s4');
                                                    } elseif (($discussncode[0]['s4'])) {
                                                        $s4 = $discussncode[0]['s4'];
                                                        $s4 = explode("~", $s4);
                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s4[]" name="s4[]"
                                    rows="2"><?php echo $s4[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s5">
                                    <h6>ENJOYMENT, FUN & RECREATION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s5')) {
                                                        $val = set_value('s5');
                                                    } elseif (($discussncode[0]['s5'])) {
                                                        $s5 = $discussncode[0]['s5'];
                                                        $s5 = explode("~", $s5);
                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s5[]" name="s5[]"
                                    rows="2"><?php echo $s5[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s6">
                                    <h6>FAMILY </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s6')) {
                                                        $val = set_value('s6');
                                                    } elseif (($discussncode[0]['s6'])) {
                                                        $s6 = $discussncode[0]['s6'];
                                                        $s6 = explode("~", $s6);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s6[]" name="s6[]"
                                    rows="2"><?php echo $s6[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s7">
                                    <h6>GROUP & COMMUNITY PARTICIPATION </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s7')) {
                                                        $val = set_value('s7');
                                                    } elseif (($discussncode[0]['s7'])) {
                                                        $s7 = $discussncode[0]['s7'];
                                                        $s7 = explode("~", $s7);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s7[]" name="s7[]"
                                    rows="2"><?php echo $s7[$i]; ?></textarea>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="s8">
                                    <h6>HEALTH, BEAUTY, WELLNESS </h6>
                                </label>
                                <?php
                                                    $val = '';
                                                    $attr = '';
                                                    if (set_value('s8')) {
                                                        $val = set_value('s8');
                                                    } elseif (($discussncode[0]['s8'])) {
                                                        $s8 = $discussncode[0]['s8'];
                                                        $s8 = explode("~", $s8);

                                                        $attr = 'disabled';
                                                    }
                                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="s8" name="s8"
                                    rows="2"><?php echo $s8[$i]; ?></textarea>
                            </div>

                        </div>
                        <?php
                                                    $i++;
                                                }
                                            }
                                        }
                                    }
                                }   ?>
                        <div class="row  mt-2" style="float:right">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                            N-CODE</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <a href="#" tabindex="0">
        </a>
        <span>

        </span>
    </div>
</div>

</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>EMOTIONAL COLUMN CHART</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Also tick the negative emotions/feeling which are generated because of the
                        N-Codes in the
                        Emotional Column chart
                    </h6>
                    <br />

                    <form action="<?= base_url() ?>/live-sessions/insertncodechart" method="POST">
                        <div class="container">
                            <div class="row">
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('chartres')) {
                                    $val = set_value('chartres');
                                } elseif (($chartres[0]['emotion'])) {
                                    $emotion = $chartres[0]['emotion'];
                                    $emotion = explode("~", $emotion);

                                    $attr = 'disabled';
                                }
                                ?>
                                <?php
                                $id = $chartres[0]['user_id'];
                                if (!$id) {
                                    $j = 0;
                                    foreach ($ncodechart as $res) {
                                        $j++; ?>
                                <div class="form-checkbox col-md-3">
                                    <input type="checkbox" name="emotion[]" id="<?php echo $res['id'] ?>"
                                        value="<?php echo $res['id'] ?>" />
                                    <label for="<?php echo $res['id'] ?>" title="<?php echo $res['ncodes']; ?>"
                                        style="text-align:left">
                                        <?php
                                                echo $j . '. ' . $res['ncodes'];
                                                ?>
                                    </label>
                                </div>

                                <br>

                                <?php
                                    }
                                } else {
                                    $emotion = $chartres[0]['emotion'];
                                    $emotion = explode("~", $emotion);

                                    ?>
                                <table id="example" style="margin-left:5%;width:100px"
                                    class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                    <tbody>
                                        <tr>
                                            <?php
                                                $i = 0;
                                                $r = 1;
                                                foreach ($ncodechart as $res) {
                                                    if ($r == $i / 8) {
                                                        $r++;

                                                ?>
                                        <tr>
                                        </tr>
                                        <?php

                                                    }
                                                    foreach ($emotion as $emot) {
                                                        if ($res['id'] == $emot) {
                                                            $j++;
                                                            $i++;
                                            ?>
                                        <td style="background-color:#f5832c;color:#fff">
                                            <input style="background-color:#f5832c" type="checkbox" name="emotion[]"
                                                id="<?php echo $res['id'] ?>" value="<?php echo $res['id'] ?>" />
                                            <label for="<?php echo $res['id'] ?>" title="<?php echo $res['ncodes']; ?>"
                                                style="text-align:left">
                                                <?php
                                                            echo $res['ncodes'];
                                                        ?>
                                            </label>
                                        </td>

                                        <br>

                                        <?php

                                                        }
                                            ?>
                                        <?php

                                                        if ($r == $i / 8) {
                                                            $r++;

                                            ?>
                                        <tr>
                                        </tr>
                                        <?php

                                                        }
                                                    }
                                        ?>

                                        <td>
                                            <?php $i++; ?>
                                            <input type="checkbox" name="emotion[]" id="<?php echo $res['id'] ?>"
                                                value="<?php echo $res['id'] ?>" />
                                            <label for="<?php echo $res['id'] ?>" title="<?php echo $res['ncodes']; ?>"
                                                style="text-align:left">
                                                <?php echo $res['ncodes'];
                                                ?>
                                            </label>
                                        </td>

                                        <?php
                                                    if ($r == $i / 8) {
                                                        $r++;

                                        ?>
                                        <tr>
                                        </tr>
                                        <?php

                                                    }
                                                }
                                            }
                                ?>
                                        </tr>

                            </div>
                        </div>
                </div>

                </tbody>
                </table>
            </div>
            <div class="row  mt-2 ml-3" style="float:right">
                <div class="clearfix px-3">
                    <div class="pull-left">
                        <div class="form-group clearfix">
                            <button type="submit" class="btn-md btn-theme">SUBMIT THE EMOTIONAL CHART
                            </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
</div>

<!-- Modal Document -->
<div class="modal" id="document_modal">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="document_title"></h4>
                </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <iframe id="document"></iframe>
                    </div>


                </div>
            </div>



        </div>
    </div>
</div>

<script type="text/javascript">
function opendocument(document, title) {
    $('#document').attr('src', document)
    $('#document_title').text(title);
    $('#document_modal').modal('show');
};
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 2px;
}
</style>