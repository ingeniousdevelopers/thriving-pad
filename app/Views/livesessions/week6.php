<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 6 </h4>
</div>
</div>
</div>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 6</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row mt-2" style="float:right">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <a class="btn-md btn-theme float-right p-3" target="_blank" href="<?php echo $link; ?>">
                                OPEN THE SESSION</a>

                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>OUTCOME REINFORCEMENT TOOL</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <a href="<?= base_url() ?>/outcome-reinforcement" target="_blank"
                                    class="btn-md btn-theme float-left p-3">OPEN OUTCOME
                                    REFINEMENT TOOL</a>

                            </div>
                        </div>
                    </div>


                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h5>‘Goals excite, motivate and inspire us to take action and push our limits.
                                        Traditional method of goal setting doesn't work because they come from outside
                                        you, but goals aligned with your Base-Values and P-codes work like magic’
                                        especially when they are revamped as SMART goals
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </h5>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>SMARTENING UP A GOAL</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>When we have clarity of our Base-Values and Coding our desires become easy to achieve
                            </h6>
                        </li>
                        <li>
                            <h6>Have you ever met someone who is less skilled than you, yet seems to be more successful?
                                Do you know why? Their Base-Values and Coding are in alignment with their dreams and
                                actions.</h6>
                        </li>
                        <li>
                            <h6>Having focused goal gives your life a specified Direction</h6>
                        </li>
                        <li>
                            <h6>It is important to have measurable, real goals and not just randomly set vague goals.
                                A goal needs to be specific, measurable, attainable, relevant and timely</h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>EXAMPLES OF SMART GOALS</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <ul class="pl-5" style="list-style-type: square;">
                        <h6>Goal</h6>
                        <li class="ml-5">
                            <h6>I will reduce 12 kg weight.</h6>
                        </li>
                        <h6>Smart goal</h6>
                        <li class="ml-5">
                            <h6 class="text-justify">
                                For the next 3 months, I will weigh myself everyday, make a list of
                                food items and
                                reduce calorie intake to 1000 kcal /day , walk <br />12,000 steps /day,
                                I will discard all the
                                junk out of the kitchen and do weight training in the gym every
                                Tuesday and Thursday.
                            </h6>
                        </li>
                        <h6>Goal</h6>
                        <li class="ml-5">
                            <h6>I will teach in school.</h6>
                        </li>
                        <h6>Smart goal</h6>
                        <li class="ml-5">
                            <h6>I will obtain a job as a high school math teacher within three months after graduating
                                with my B. ED Degree.
                            </h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>OVERCOMING CHALLENGES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5>
                        Convert your top 3 refined desires into SMART goals
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/challengeovercome" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('desire')) {
                                $val = set_value('desire');
                            } elseif (($challenge[0]['desire'])) {
                                $desire = $challenge[0]['desire'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="desire" name="desire"
                                rows="3"><?php echo $desire; ?></textarea>

                        </div>
                        <div class="form-group">
                            <h5 class="mt-4">
                                Express and check if your Base-Values are satisfied in your SMART goals and what can you
                                do for your Base-Values to be satisfied?
                            </h5>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('baseval')) {
                                $val = set_value('baseval');
                            } elseif (($challenge[0]['baseval'])) {
                                $baseval = $challenge[0]['baseval'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="baseval" name="baseval"
                                rows="3"><?php echo $baseval; ?></textarea>

                        </div>
                        <h5 class="mt-4">
                            List Challenges that might come in way while achieving your Top 3 SMART goals

                        </h5>
                        <div class="row">
                            <?php for ($i = 0; $i <= 2; $i++) { ?>
                            <div class="form-group col-md-12">

                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('achieve')) {
                                        $val = set_value('achieve');
                                    } elseif (($challenge[0]['achieve'])) {
                                        $achieve = $challenge[0]['achieve'];
                                        $achieve = explode('~', $achieve);

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="achieve[]"
                                    name="achieve[]" rows="3"><?php echo $achieve[$i]; ?></textarea>

                            </div>
                            <?php } ?>
                        </div>
                        <h5 class="mt-4">Write what N-codes come to your mind when you think of these challenges
                            and
                            SMART goals</h5>

                        <div class="row">
                            <?php for ($i = 0; $i <= 5; $i++) { ?>
                            <div class="form-group col-md-6">

                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('challenges')) {
                                        $val = set_value('challenges');
                                    } elseif (($challenge[0]['challenges'])) {
                                        $challenges = $challenge[0]['challenges'];
                                        $challenges = explode('~', $challenges);

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="challenges[]"
                                    name="challenges[]" rows="3"><?php echo $challenges[$i]; ?></textarea>
                            </div>
                            <?php } ?>
                        </div>
                        <h5 class="mt-4">Convert them into powerful P-codes</h5>
                        <div class="row">
                            <?php for ($i = 0; $i <= 7; $i++) { ?>
                            <div class="form-group col-md-6">
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('pcodes')) {
                                        $val = set_value('pcodes');
                                    } elseif (($challenge[0]['pcodes'])) {
                                        $pcodes = $challenge[0]['pcodes'];
                                        $pcodes = explode('~', $pcodes);

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="pcodes[]"
                                    name="pcodes[]" rows="3"><?php echo $pcodes[$i]; ?></textarea>
                            </div>
                            <?php } ?>
                        </div>

                        <h5 class="mt-4">Brainstorm and list down various ways to overcome challenges</h5>
                        <div class="row">
                            <?php for ($i = 0; $i <= 7; $i++) { ?>
                            <div class="form-group col-md-6">
                                <?php
                                    $val = '';
                                    $attr = '';
                                    if (set_value('brainstorm')) {
                                        $val = set_value('brainstorm');
                                    } elseif (($challenge[0]['brainstorm'])) {
                                        $brainstorm = $challenge[0]['brainstorm'];
                                        $brainstorm = explode('~', $brainstorm);

                                        $attr = 'disabled';
                                    }
                                    ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="brainstorm[]"
                                    name="brainstorm[]" rows="3"><?php echo $brainstorm[$i]; ?></textarea>
                            </div>
                            <?php } ?>
                        </div>

                        <div class="row  mt-2" style="float:right">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT CHALLENGE
                                            OVERCOME
                                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>OVERCOING CHALLENGES</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5><b>Shake it off and take a step up</b></h5>
                    <h6>Life is a succession of lessons which must be lived to be understood - Ralph Waldo Emerson</h6>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>A farmer’s old donkey fell into an abandoned well, crying piteously
                                for hours. He and
                                his neighbors grabbed a shovel and began to shovel<br /> dirt into the well as
                                donkey
                                was old
                                and useless. Realizing what was happening, the donkey at first cried and wailed
                                horribly. Then, a few<br /> shovelfuls later, he quieted down completely.</h6>
                        </li>
                        <li>
                            <h6>With every shovelful of dirt that hit his back, the donkey would shake it off and
                                take
                                a step up on the new layer of dirt pretty soon, the donkey stepped up over the edge
                                of
                                the well and trotted off.</h6>
                        </li>
                        <li>
                            <h6>Life is going to shovel all kinds of dirt on you, the trick to getting out of the
                                well
                                is to not let it bury you, but to shake it off and take a step up. </h6>
                        </li>
                        <li>
                            <h6><b>
                                    <i>Each of our troubles is a stepping stone</i></b>
                            </h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>SUCCESS ALIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/successalign" method="POST">
                        <div class="form-group">
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('achieved')) {
                                $val = set_value('achieved');
                            } elseif (($success[0]['achieved'])) {
                                $achieved = $success[0]['achieved'];

                                $attr = 'disabled';
                            }
                            ?>
                            <h5>
                                How will you feel when your goal is achieved? What will be different in your life?
                            </h5>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="achieved" name="achieved"
                                rows="3"><?php echo $achieved; ?></textarea>
                        </div>
                        <div class="form-group">

                            <h5>
                                Do you have the skills and the tools to accomplish your goals?
                            </h5>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('skills')) {
                                $val = set_value('skills');
                            } elseif (($success[0]['skills'])) {
                                $skills = $success[0]['skills'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="skills" name="skills"
                                rows="3"><?php echo $skills; ?></textarea>

                        </div>
                        <div class="form-group">

                            <h5>
                                Is it realistic and achievable? Has someone done it before?
                            </h5>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('successachieve')) {
                                $val = set_value('successachieve');
                            } elseif (($success[0]['successachieve'])) {
                                $successachieve = $success[0]['successachieve'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="successachieve"
                                name="successachieve" rows="3"><?php echo $successachieve; ?></textarea>


                        </div>
                        <div class="form-group">

                            <h5>
                                What date is it going to be done? </h5>
                            <?php
                            $val = '';
                            $attr = '';
                            if (set_value('successdate')) {
                                $val = set_value('successdate');
                            } elseif (($success[0]['successdate'])) {
                                $successdate = $success[0]['successdate'];

                                $attr = 'disabled';
                            }
                            ?>
                            <textarea required <?php echo $attr;  ?> class="form-control" id="successdate"
                                name="successdate" rows="3"><?php echo $successdate;  ?></textarea>

                        </div>
                        <div class="form-group">

                            <h5>
                                Vibration precedes manifestation, hence feel like your goal has already been achieved.
                                This trains the subconscious mind to accept the outcome as real, which helps you to move
                                more effectively towards it. </h5>

                        </div>

                        <div class="row mt-2" style="float:right;margin-right:-12px;">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-right p-3">SUBMIT SUCCESS
                                            ALIGN
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>EMBRACING VULNERABILITY </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">

                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/public/assets/documents/week6/Vulnerability.pdf">
                                    <button type="submit" class="btn-md btn-theme float-left p-3">DOWNLOAD THE
                                        DOCUMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 2px;
}
</style>