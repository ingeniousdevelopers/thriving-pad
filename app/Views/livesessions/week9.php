<?php
function display_error($validation, $field)
{
    if (isset($validation)) {
        if ($validation->hasError($field)) {
            return $validation->getError($field);
        } else {
            return false;
        }
    }
} ?>


<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 9 </h4>
</div>
</div>
</div>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>
<!--Live Session-->
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 9</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row mt-2 mr-3" style="float:right">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <a class="btn-md btn-theme float-right p-3" target="_blank" href="<?php echo $link; ?>">
                                OPEN THE SESSION</a>

                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HAPPINESS BOOSTERS</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6 class="text-justify">
                        <b> Activities which you love doing which makes you forget the track of time.</b>
                    </h6>
                    <br />
                    <form action="<?= base_url() ?>/live-sessions/insertmyhappy" method="POST">

                        <div class="row">
                            <div class="form-group col-md-6">
                                <h6>What do you love doing?</h6>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q1')) {
                                    $val = set_value('q1');
                                } elseif (($happy[0]['q1'])) {
                                    $happyq1 = $happy[0]['q1'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q1" name="q1"
                                    rows="3"><?php echo $happyq1; ?></textarea>

                            </div>

                            <div class="form-group col-md-6">
                                <h6>What activities do you find easy and are really good at?</h6>

                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q2')) {
                                    $val = set_value('q2');
                                } elseif (($happy[0]['q2'])) {
                                    $happyq2 = $happy[0]['q2'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q2" name="q2"
                                    rows="3"><?php echo $happyq2; ?></textarea>

                            </div>
                            <div class="form-group col-md-6">
                                <h6>What activities do you do where time just disappears?</h6>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q3')) {
                                    $val = set_value('q3');
                                } elseif (($happy[0]['q3'])) {
                                    $happyq3 = $happy[0]['q3'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q3" name="q3"
                                    rows="3"><?php echo $happyq3; ?></textarea>


                            </div>

                            <div class="form-group col-md-6">
                                <h6>If money/time/family commitments were no obstacle, what would you do?</h6>

                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q4')) {
                                    $val = set_value('q4');
                                } elseif (($happy[0]['q4'])) {
                                    $happyq4 = $happy[0]['q4'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q4" name="q4"
                                    rows="3"><?php echo $happyq4; ?></textarea>


                            </div>
                            <div class="form-group col-md-6">
                                <h6>What did you enjoy doing as a kid (age 4-18)?</h6>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q5')) {
                                    $val = set_value('q5');
                                } elseif (($happy[0]['q5'])) {
                                    $happyq5 = $happy[0]['q5'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q5" name="q5"
                                    rows="3"><?php echo $happyq5; ?></textarea>



                            </div>

                            <div class="form-group col-md-6">
                                <h6>List everything you said you would be when you grew up.</h6>

                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q6')) {
                                    $val = set_value('q6');
                                } elseif (($happy[0]['q6'])) {
                                    $happyq6 = $happy[0]['q6'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q6" name="q6"
                                    rows="3"><?php echo $happyq6; ?></textarea>



                            </div>
                            <div class="form-group col-md-6">
                                <h6>What are you really good at? List everything you can think of no matter how
                                    irrelevant it may seem when you first think about it.</h6>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q7')) {
                                    $val = set_value('q7');
                                } elseif (($happy[0]['q7'])) {
                                    $happyq7 = $happy[0]['q7'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q7" name="q7"
                                    rows="3"><?php echo $happyq7; ?></textarea>


                            </div>
                            <div class="form-group col-md-6">
                                <h6>What impact will having more HB in your life have on you and those around you?
                                </h6>
                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q8')) {
                                    $val = set_value('q8');
                                } elseif (($happy[0]['q8'])) {
                                    $happyq8 = $happy[0]['q8'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q8" name="q8"
                                    rows="3"><?php echo $happyq8; ?></textarea>


                            </div>

                            <div class="form-group col-md-12">
                                <h6>Who else are you tolerating who do not add value to your life?</h6>

                                <?php
                                $val = '';
                                $attr = '';
                                if (set_value('q9')) {
                                    $val = set_value('q9');
                                } elseif (($happy[0]['q9'])) {
                                    $happyq9 = $happy[0]['q9'];

                                    $attr = 'disabled';
                                }
                                ?>
                                <textarea required <?php echo $attr;  ?> class="form-control" id="q9" name="q9"
                                    rows="3"><?php echo $happyq9; ?></textarea>


                            </div>
                        </div>

                </div>
                <div class="row  mt-2 mr-3" style="float:right">
                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT HAPPINESS BOOSTERS
                                    Moment</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                </form>

                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">

                    <h6>
                        You may add new habits in your habits sheet if you wish!


                    </h6>
                </div>

                <div class="row  mt-2 mr-3" style="float:right">
                    <div class="col-md-4">
                        <div class="form-group clearfix">
                            <a href="<?= base_url() ?>/livesessions/myhabit" class="btn btn-theme btn-md"
                                target="_blank">
                                MY HABIT</a>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>STORY OF SHARPENING AXE</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6><b>When was the last time YOU sharpened your AXE?</b></h6>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>Once upon a time, two woodcutters named Peter and John were in a wood
                                cutting
                                competition.
                            </h6>
                        </li>
                        <li>
                            <h6>After an hour Peter suddenly stopped chopping, seeing which John happily
                                started
                                cutting
                                at double the pace.</h6>
                        </li>
                        <li>
                            <h6>This went on the whole day. Every hour, Peter would stop chopping for
                                fifteen
                                minutes
                                while John kept going relentlessly. </h6>
                        </li>
                        <li>
                            <h6>So, when the competition ended, John was absolutely confident that he would
                                take the
                                triumph. But to John’s astonishment, Peter had actually cut down more wood.
                            </h6>
                        </li>
                        <li>
                            <h6>How did this even happen? Peter stopped for 15 mins every hour.</h6>
                        </li>
                        <li>
                            <h6>Well, every time Peter stopped work, while John was still chopping down
                                trees, he
                                was
                                actually sharpening his axe.</h6>
                        </li>
                        <li>
                            <h6>Take time to relax, think, meditate, learn & grow. Add more and more
                                happiness
                                boosters
                                to your life.</h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>IMPORTANCE OF VISUALIZATION</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6><b>Power of Visualization - Vision Board Activity </b></h6>
                    <br />
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>VISION BOARD</h1>
                                </div>
                            </div>
                        </div>
                    </h5>

                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>A dream board or vision board is a collage of images, pictures and
                                affirmations of
                                one's dreams and desires, designed to serve as a source of inspiration and
                                motivation,
                                and to use the law of attraction to attain goals.
                            </h6>
                        </li>
                        <li>
                            <h6>By creating a vision board and placing it in a spot you see every day, you
                                create
                                the
                                opportunity for consistent visualization to train your mind, body, and
                                spirit to
                                manifest your desires.</h6>
                        </li>
                        <li>
                            <h6>When you are visualizing, you are emitting a powerful frequency out into the
                                Universe.
                            </h6>
                        </li>
                        <li>
                            <h6>By representing your goals with pictures and images you will actually
                                strengthen and
                                stimulate your emotions because your mind responds strongly to visual
                                stimulation…
                                and
                                your emotions are the vibrational energy that activates the Law of
                                Attraction. </h6>
                        </li>
                        <li>
                            <h6>“A picture is worth a thousand words,”</h6>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HOW TO MAKE A VISION BOARD</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>Create a list of goals which you would like to achieve.
                            </h6>
                        </li>
                        <li>
                            <h6>Collect pictures that resembles and represents your goal.</h6>
                        </li>
                        <li>
                            <h6>Make a collage out of these inspiring pictures and stick them onto the board.
                            </h6>
                        </li>
                        <li>
                            <h6>Also add pictures of your Happiness Boosters to it. </h6>
                        </li>
                        <li>
                            <h6>You may add your P-codes, happy, joyful and emotional words to it.</h6>
                        </li>
                        <li>
                            <h6>Add date, year, time etc. to the picture.</h6>
                        </li>
                        <li>
                            <h6>Take a few moments to contemplate on your vision board everyday especially before
                                going to bed and first thing upon rising. Feel the inspiration it provides. Believe like
                                it’s already yours. </h6>
                        </li>
                    </ul>

                    <div class="clearfix px-3 ml-3" style="float:right">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <a href="https://www.canva.com/en_in/" class="btn btn-theme btn-md" target="_blank">
                                    CLICK TO MAKE VISION BOARD</a>
                            </div>
                        </div>
                    </div>
                    <br />
                    <form action="<?= base_url() ?>/live-sessions/visionboard" method="POST"
                        enctype='multipart/form-data'>
                        <div class="col-lg-12 col-md-12 mt-5">
                            <!-- Edit profile photo -->
                            <?php if ($vision[0]['vision_img']) { ?>

                            <img width="400%" height="10%" src="<?php echo base_url() . '/public/uploads/week9/' . $vision[0]['vision_img'];
                                                                    ?>" />
                            <?php
                            } else {
                                $val = '';
                                $attr = '';
                                if (set_value('vision_img')) {
                                    $val = set_value('vision_img');
                                } elseif (($vision[0]['vision_img'])) {
                                    $vision_img = $vision[0]['vision_img'];

                                    $attr = 'disabled';
                                }
                            ?>
                            <div class="row mb-3" style="float:right">
                                <input style="float:right" type="file" class="upload" name="vision_img"
                                    <?php echo $attr; ?> value="<?php echo $vision_img ?>">
                                <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT
                                    VISION BOARD
                                </button>
                            </div>
                        </div>
                </div>
            </div>
            <?php } ?>
        </div>
        <br />

    </div>
    </form>

    <div class="footer">
        <a href="#" tabindex="0">


        </a>
        <span>

        </span>
    </div>
</div>


<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">

                    <h6>
                        Celebrate Accomplishment + Breakthrough (journal sheet)


                    </h6>
                </div>

                <div class="clearfix px-3 ml-3" style="float:right">
                    <div class="pull-left">
                        <div class="form-group clearfix">
                            <a href="<?= base_url() ?>/livesessions/accomplishment" class="btn btn-theme btn-md"
                                target="_blank">
                                ACCOMPLISHMENT</a>
                            <a href="<?= base_url() ?>/livesessions/breakthrough" class="btn btn-theme btn-md"
                                target="_blank">
                                BREAKTHROUGH</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix px-3" style="float:right">
                    <div class="pull-left">
                        <div class="form-group clearfix justify-content-center">
                            <form action="<?= base_url() ?>/public/assets/documents/week4/PositiveMoments.docx">

                            </form>
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 2px;
}
</style>