<div class="col-sm-12 col-md-12">
    <h4>Live Session - Week 7 </h4>
</div>
</div>
</div>
<?php if (session()->getTempData('success')) : ?>
<div class="alert alert-success"><?= session()->getTempData('success') ?></div>
<?php endif; ?>
<?php if (session()->getTempData('error')) : ?>
<div class="alert alert-danger"><?= session()->getTempData('error') ?></div>
<?php endif; ?>


<style>
.alert {
    text-transform: uppercase;
    font-size: 12px;
    width: 100%;
    margin-bottom: 10px;
    padding: 18px 20px;
}
</style>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>ATTENT THE LIVE SESSION - WEEK 7</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself
                        so when you proceed ahead with this coaching program, you can make
                        the most out of it.
                    </h6>
                </div>

                <div class="row mt-2" style="float:right">
                    <div class="col-md-12">
                        <div class="form-group clearfix">
                            <a class="btn-md btn-theme float-right p-3" target="_blank" href="<?php echo $link; ?>">
                                OPEN THE SESSION</a>

                        </div>
                    </div>
                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>REALISTIC FUTURE </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h5><b>ACTION PLAN - PRIORITY STEPS</b></h5>
                    <h6>Life is a succession of lessons which must be lived to be understood - Ralph Waldo Emerson</h6>
                    <ul class="pl-5" style="list-style-type: square;">
                        <li>
                            <h6>An action plan offers a clear road-map. It supports efficiency by assigning a time
                                frame to individual step in the process</h6>
                        </li>
                        <li>
                            <h6>They also make it easy to track progress, keeping projects on schedule and on budget.
                                It provides accountability and a valuable reference.</h6>
                        </li>
                        <li>
                            <h6>Let’s write down at least 5 to 6 priority steps towards achieving your top most SMART
                                GOAL. </h6>
                        </li>
                        <li>
                            <h6>Speak out loud these Priority steps and create your Action Plan.
                            </h6>
                        </li>
                    </ul>

                    <br />

                    <form action="<?= base_url() ?>/live-sessions/smartgoals" method="POST">
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('potential_priority1')) {
                            $val = set_value('potential_priority1');
                        } elseif (($outcome[0]['potential_priority1'])) {
                            $potential_priority1 = $outcome[0]['potential_priority1'];
                            $potential_priority2 = $outcome[0]['potential_priority2'];
                            $potential_priority3 = $outcome[0]['potential_priority3'];
                        }
                        $q = 0;
                        $t = 0;
                        $u = 0;
                        for ($s = 0; $s <= 2; $s++) {
                            if ($s == 0) {
                        ?>
                        <h5>
                            <label for="potential_priority[]">
                                <input type="hidden" name="potential_priority[]" id="potential_priority[]"
                                    value="<?php echo $potential_priority3 ?>" />
                                <h6 style="text-transform:uppercase"> DESIRES : <b><?php echo $potential_priority3 ?>
                                    </b>
                                </h6>
                            </label>

                        </h5>
                        <?php if (isset($smart) && empty($smart)) {
                                ?>
                        <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="10">
                                        <b class="top_span">Smart Goals :</b>

                                        <button type="button" onClick="addItem();"
                                            class="btn-md btn-theme float-right">Add Smart
                                            Goals</button>

                                    </th>

                                </tr>

                            </thead>
                            <tr>
                                <th>PS</th>
                                <th>Steps to be taken</th>
                                <th>Who is responsible</th>
                                <th>Start date</th>
                                <th>Due date</th>
                                <th>Resources required</th>
                                <th>Challenges for this PS</th>
                                <th>Outcome of this PS</th>
                                <th>Actions</th>

                            </tr>

                            <tbody id="tbody">
                                <?php for ($a = 1; $a <= 2; $a++) { ?>
                                <tr class="highlight">
                                    <td><?php echo $a; ?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="taken[]" id="taken">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="response[]" id="response">
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='start_date[]' id="start_date">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="end_date[]" id="end_date">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="resource[]" id="resource">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="challenges[]" id="challenges">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="outcome[]" id="outcome">
                                    </td>
                                    <td>
                                        <button type="button" onclick="deleteRow(this);" class=" btn-md btn-theme"
                                            disabled>
                                            Delete</button>

                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php }
                                if (!empty($smart)) {
                                    $val = '';
                                    $attr = '';
                                    if (set_value('taken')) {
                                        $val = set_value('taken');
                                    } elseif (($smart[0]['taken'])) {
                                        $taken = $smart[0]['taken'];
                                        $response = $smart[0]['response'];
                                        $start_date = $smart[0]['start_date'];
                                        $end_date = $smart[0]['end_date'];
                                        $resource = $smart[0]['resource'];
                                        $challenges = $smart[0]['challenges'];
                                        $outcome = $smart[0]['outcome'];

                                        $taken = explode('~', $taken);
                                        $response = explode('~', $response);
                                        $start_date = explode('~', $start_date);
                                        $end_date = explode('~', $end_date);
                                        $resource = explode('~', $resource);
                                        $challenges = explode('~', $challenges);
                                        $outcome = explode('~', $outcome);

                                        $attr = 'disabled';
                                    ?>
                        <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="10">
                                        <b class="top_span">Smart Goals :</b>


                                    </th>

                                </tr>

                            </thead>
                            <tr>
                                <th>PS</th>
                                <th>Steps to be taken</th>
                                <th>Who is responsible</th>
                                <th>Start date</th>
                                <th>Due date</th>
                                <th>Resources required</th>
                                <th>Challenges for this PS</th>
                                <th>Outcome of this PS</th>


                            </tr>

                            <tbody id="tbody">
                                <?php

                                                for ($i = 0; $i < count($taken); $i++) {
                                                    $q++;                       ?>
                                <tr class="highlight">

                                    <td><?php echo $q; ?>
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control"
                                            value="<?php echo $taken[$i]; ?>" name=" taken[]" id="taken[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control" name="response[]"
                                            value="<?php echo $response[$i]; ?>" id="response[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control"
                                            name="start_date[]" value="<?php echo $start_date[$i]; ?>"
                                            id="start_date[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="end_date[]" value="<?php echo $end_date[$i]; ?>" id="end_date[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="resource[]" value="<?php echo $resource[$i]; ?>" id="resource[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="challenges[]" value="<?php echo $challenges[$i]; ?>"
                                            id="challenges[]">
                                    </td>
                                    <td>
                                        <input type="text" <?php echo $attr;  ?> class="form-control calc"
                                            name="outcome[]" value="<?php echo $outcome[$i]; ?>" id="outcome[]">
                                    </td>

                                </tr>
                                <?php }
                                                ?>
                            </tbody>
                        </table>
                        <?php
                                    }
                                }
                            }
                            if ($s == 1) {
                                ?>
                        <h5>
                            <label for="potential_priority[]">
                                <input type="hidden" name="potential_priority[]" id="potential_priority[]"
                                    value="<?php echo $potential_priority1 ?>" />
                                <h6 style="text-transform:uppercase"> DESIRES : <b><?php echo $potential_priority1 ?>
                                    </b>
                                </h6>
                            </label>

                        </h5>
                        <?php if (isset($smart) && empty($smart)) {
                                ?>
                        <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="10">
                                        <b class="top_span">Smart Goals :</b>

                                        <button type="button" onClick="addItem1();"
                                            class="btn-md btn-theme float-right">Add Smart
                                            Goals</button>

                                    </th>

                                </tr>

                            </thead>
                            <tr>
                                <th>PS</th>
                                <th>Steps to be taken</th>
                                <th>Who is responsible</th>
                                <th>Start date</th>
                                <th>Due date</th>
                                <th>Resources required</th>
                                <th>Challenges for this PS</th>
                                <th>Outcome of this PS</th>
                                <th>Actions</th>

                            </tr>

                            <tbody id="tbody1">
                                <?php for ($a = 1; $a <= 2; $a++) { ?>
                                <tr class="highlight">
                                    <td><?php echo $a; ?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="taken1[]" id="taken1">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="response1[]" id="response1">
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='start_date1[]' id="start_date1">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="end_date1[]" id="end_date1">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="resource1[]" id="resource1">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="challenges1[]" id="challenges1">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="outcome1[]" id="outcome1">
                                    </td>
                                    <td>
                                        <button type="button" onclick="deleteRow(this);" class=" btn-md btn-theme"
                                            disabled>
                                            Delete</button>

                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php }
                                if (!empty($smart)) {
                                    $val = '';
                                    $attr = '';
                                    if (set_value('taken1')) {
                                        $val = set_value('taken1');
                                    } elseif (($smart[0]['taken1'])) {
                                        $taken1 = $smart[0]['taken1'];
                                        $response1 = $smart[0]['response1'];
                                        $start_date1 = $smart[0]['start_date1'];
                                        $end_date1 = $smart[0]['end_date1'];
                                        $resource1 = $smart[0]['resource1'];
                                        $challenges1 = $smart[0]['challenges1'];
                                        $outcome1 = $smart[0]['outcome1'];

                                        $taken1 = explode('~', $taken1);
                                        $response1 = explode('~', $response1);
                                        $start_date1 = explode('~', $start_date1);
                                        $end_date1 = explode('~', $end_date1);
                                        $resource1 = explode('~', $resource1);
                                        $challenges1 = explode('~', $challenges1);
                                        $outcome1 = explode('~', $outcome1);

                                        $attr = 'disabled';
                                    ?>
                        <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="10">
                                        <b class="top_span">Smart Goals :</b>


                                    </th>

                                </tr>

                            </thead>
                            <tr>
                                <th>PS</th>
                                <th>Steps to be taken</th>
                                <th>Who is responsible</th>
                                <th>Start date</th>
                                <th>Due date</th>
                                <th>Resources required</th>
                                <th>Challenges for this PS</th>
                                <th>Outcome of this PS</th>

                            </tr>

                            <tbody id="tbody1">
                                <?php

                                                for ($i = 0; $i < count($taken1); $i++) {
                                                    $t++;
                                                ?>
                                <tr class="highlight">
                                    <td><?php echo $t; ?>
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control"
                                            value="<?php echo $taken1[$i]; ?>" name=" taken1[]" id="taken1[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control" name="response1[]"
                                            value="<?php echo $response1[$i]; ?>" id="response1[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control"
                                            name="start_date1[]" value="<?php echo $start_date1[$i]; ?>"
                                            id="start_date1[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="end_date1[]" value="<?php echo $end_date1[$i]; ?>" id="end_date1[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="resource1[]" value="<?php echo $resource1[$i]; ?>" id="resource1[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="challenges1[]" value="<?php echo $challenges1[$i]; ?>"
                                            id="challenges1[]">
                                    </td>
                                    <td>
                                        <input type="text" <?php echo $attr;  ?> class="form-control calc"
                                            name="outcome1[]" value="<?php echo $outcome1[$i]; ?>" id="outcome1[]">
                                    </td>
                                </tr>
                                <?php }
                                                ?>
                            </tbody>
                        </table>
                        <?php
                                    }
                                }
                            }
                            if ($s == 2) {
                                ?>
                        <h5>
                            <label for="potential_priority[]">
                                <input type="hidden" name="potential_priority[]" id="potential_priority[]"
                                    value="<?php echo $potential_priority2 ?>" />
                                <h6 style="text-transform:uppercase"> DESIRES : <b><?php echo $potential_priority2 ?>
                                    </b>
                                </h6>
                            </label>

                        </h5>
                        <?php if (isset($smart) && empty($smart)) {
                                ?>
                        <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="10">
                                        <b class="top_span">Smart Goals :</b>

                                        <button type="button" onClick="addItem2();"
                                            class="btn-md btn-theme float-right">Add Smart
                                            Goals</button>

                                    </th>

                                </tr>

                            </thead>
                            <tr>
                                <th>PS</th>
                                <th>Steps to be taken</th>
                                <th>Who is responsible</th>
                                <th>Start date</th>
                                <th>Due date</th>
                                <th>Resources required</th>
                                <th>Challenges for this PS</th>
                                <th>Outcome of this PS</th>
                                <th>Actions</th>

                            </tr>

                            <tbody id="tbody2">
                                <?php
                                            for ($a = 1; $a <= 2; $a++) {
                                            ?>
                                <tr class="highlight">
                                    <td><?php echo $a; ?>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="taken2[]" id="taken2">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="response2[]" id="response2">
                                    </td>
                                    <td>
                                        <input type='text' class='form-control' name='start_date2[]' id="start_date2">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="end_date2[]" id="end_date2">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="resource2[]" id="resource2">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="challenges2[]" id="challenges2">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="outcome2[]" id="outcome2">
                                    </td>
                                    <td>
                                        <button type="button" onclick="deleteRow(this);" class=" btn-md btn-theme"
                                            disabled>
                                            Delete</button>

                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php }
                                if (!empty($smart)) {
                                    $val = '';
                                    $attr = '';
                                    if (set_value('taken2')) {
                                        $val = set_value('taken2');
                                    } elseif (($smart[0]['taken2'])) {
                                        $taken2 = $smart[0]['taken2'];
                                        $response2 = $smart[0]['response2'];
                                        $start_date2 = $smart[0]['start_date2'];
                                        $end_date2 = $smart[0]['end_date2'];
                                        $resource2 = $smart[0]['resource2'];
                                        $challenges2 = $smart[0]['challenges2'];
                                        $outcome2 = $smart[0]['outcome2'];

                                        $taken2 = explode('~', $taken2);
                                        $response2 = explode('~', $response2);
                                        $start_date2 = explode('~', $start_date2);
                                        $end_date2 = explode('~', $end_date2);
                                        $resource2 = explode('~', $resource2);
                                        $challenges2 = explode('~', $challenges2);
                                        $outcome2 = explode('~', $outcome2);

                                        $attr = 'disabled';

                                    ?>
                        <table class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th colspan="10">
                                        <b class="top_span">Smart Goals :</b>


                                    </th>

                                </tr>

                            </thead>
                            <tr>
                                <th>PS</th>
                                <th>Steps to be taken</th>
                                <th>Who is responsible</th>
                                <th>Start date</th>
                                <th>Due date</th>
                                <th>Resources required</th>
                                <th>Challenges for this PS</th>
                                <th>Outcome of this PS</th>

                            </tr>

                            <tbody id="tbody2">
                                <?php

                                                for ($i = 0; $i < count($taken2); $i++) {
                                                    $u++;
                                                ?>
                                <tr class="highlight">
                                    <td><?php echo $u; ?>
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control"
                                            value="<?php echo $taken2[$i]; ?>" name=" taken2[]" id="taken2[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control" name="response2[]"
                                            value="<?php echo $response2[$i]; ?>" id="response2[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control"
                                            name="start_date2[]" value="<?php echo $start_date2[$i]; ?>"
                                            id="start_date2[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="end_date2[]" value="<?php echo $end_date2[$i]; ?>" id="end_date2[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="resource2[]" value="<?php echo $resource2[$i]; ?>" id="resource2[]">
                                    </td>
                                    <td>
                                        <input <?php echo $attr;  ?> type="text" class="form-control calc"
                                            name="challenges2[]" value="<?php echo $challenges2[$i]; ?>"
                                            id="challenges2[]">
                                    </td>
                                    <td>
                                        <input type="text" <?php echo $attr;  ?> class="form-control calc"
                                            name="outcome2[]" value="<?php echo $outcome2[$i]; ?>" id="outcome2[]">
                                    </td>

                                </tr>
                                <?php }
                                                ?>
                            </tbody>
                        </table>
                        <?php
                                    }
                                }
                            }
                            ?>


                        <?php     }
                        ?>


                        <small id="exp_error" class="text-center"> </small>
                        <div class="row mt-2" style="float:right;margin-right:-12px;">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-right p-3">SUBMIT THE
                                            DESIRES</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <button type="button" style="display:none;" id="show_result" data-toggle="modal"
                        data-target="#stepsModal" data-backdrop="static" data-keyboard="false">Result</button>

                    <div class="footer">
                        <a href="#" tabindex="0">


                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>DECISION MAKING </h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <h6>
                        Take this personality assessment quiz and discover who you really are,
                        what drives you and what you should know about yourself that you
                        probably didn't know. This will help you get a lot of clarity about yourself so when you
                        proceed ahead with this coaching program, you can make the most out of it.
                    </h6>
                </div>

                <div class="row  mt-2 ml-3" style="float:right">

                    <div class="clearfix px-3">
                        <div class="pull-left">
                            <div class="form-group clearfix">
                                <form action="<?= base_url() ?>/public/assets/documents/week6/decision making.docx">
                                    <button type="submit" class="btn-md btn-theme float-left p-3">DOWNLOAD THE
                                        DOCUMENT</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="footer">
                    <a href="#" tabindex="0">


                    </a>
                    <span>

                    </span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="property-box-2">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-pad">
            <div class="detail">
                <div class="hdg pl-5">
                    <h5 class="location">
                        <div class="heading-properties-3">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <h1>HOME ASSIGNMENT</h1>
                                </div>
                            </div>
                        </div>
                    </h5>
                    <form action="<?= base_url() ?>/live-sessions/decision" method="POST">

                        <h6>
                            <b> Past Decisions</b> – Think of some decisions in past which were not correct and it
                            adversely
                            affected you.
                        </h6>
                        <?php
                        $val = '';
                        $attr = '';
                        if (set_value('decision')) {
                            $val = set_value('decision');
                        } elseif (($decision[0]['decision'])) {
                            $decision = $decision[0]['decision'];

                            $attr = 'disabled';
                        }
                        ?>
                        <textarea required <?php echo $attr; ?> class="form-control" id="decision" name="decision"
                            rows="3"><?php echo $decision; ?></textarea>


                        <div class="row  mt-2" style="float:right;margin-right:-12px">
                            <div class="clearfix px-3">
                                <div class="pull-left">
                                    <div class="form-group clearfix">
                                        <button type="submit" class="btn-md btn-theme float-left p-3">SUBMIT THE
                                            ASSIGNMENT
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>

                    <div class="footer">
                        <a href="#" tabindex="0">


                        </a>
                        <span>

                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <p class="sub-banner-2 text-center">© 2020 ThrivePad</p>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>


<script type="text/javascript">
var items = 2;
var items1 = 2;
var items2 = 2;


function addItem() {

    items++;
    var html = "<tr>";
    html += "<td>" + items + "</td>";
    html += "<td><input type ='text' class='form-control' name='taken[]'></td> ";
    html += "<td><input type ='text' class='form-control' name='response[]'></td> ";
    html += "<td><input type='text' class='form-control' name='start_date[]'></td>";
    html += "<td><input type ='text' class='form-control' name='end_date[]'></td> ";
    html += "<td><input type ='text' class='form-control' name='resource[]'></td> ";
    html += "<td><input type='text' class='form-control' name='challenges[]'></td>";
    html += "<td><input type='text' class='form-control' name='outcome[]'></td>";
    html +=
        "<td> <button type='button' onclick='deleteRow(this);' class = 'btn-md btn-theme float-right'>Delete</button></td > ";
    html += "</tr>";


    var row = document.getElementById("tbody").insertRow();
    row.innerHTML = html;

}

function addItem1() {

    items1++;
    var html = "<tr>";
    html += "<td>" + items1 + "</td>";
    html += "<td><input type ='text' class='form-control' name='taken1[]'></td> ";
    html += "<td><input type ='text' class='form-control' name='response1[]'></td> ";
    html += "<td><input type='text' class='form-control' name='start_date1[]'></td>";
    html += "<td><input type ='text' class='form-control' name='end_date1[]'></td> ";
    html += "<td><input type ='text' class='form-control' name='resource1[]'></td> ";
    html += "<td><input type='text' class='form-control' name='challenges1[]'></td>";
    html += "<td><input type='text' class='form-control' name='outcome1[]'></td>";
    html +=
        "<td> <button type='button' onclick='deleteRow(this);' class = 'btn-md btn-theme float-right'>Delete</button></td > ";
    html += "</tr>";


    var row = document.getElementById("tbody1").insertRow();
    row.innerHTML = html;

}

function addItem2() {

    items2++;
    var html = "<tr>";
    html += "<td>" + items2 + "</td>";
    html += "<td><input type ='text' class='form-control' name='taken2[]'></td> ";
    html += "<td><input type ='text' class='form-control' name='response2[]'></td> ";
    html += "<td><input type='text' class='form-control' name='start_date2[]'></td>";
    html += "<td><input type ='text' class='form-control' name='end_date2[]'></td> ";
    html += "<td><input type ='text' class='form-control' name='resource2[]'></td> ";
    html += "<td><input type='text' class='form-control' name='challenges2[]'></td>";
    html += "<td><input type='text' class='form-control' name='outcome2[]'></td>";
    html +=
        "<td> <button type='button' onclick='deleteRow(this);' class = 'btn-md btn-theme float-right'>Delete</button></td > ";
    html += "</tr>";


    var row = document.getElementById("tbody2").insertRow();
    row.innerHTML = html;

}


function deleteRow(button) {
    button.parentElement.parentElement.remove();
}
</script>
<style>
.row {
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: 2px;
}
</style>